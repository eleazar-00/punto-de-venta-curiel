/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import modelo.*;
import vista.JFIAgente;

/**
 *
 * @author eleazar
 */
public class ControladorAgente implements ActionListener, KeyListener, MouseListener {

    JFIAgente interfaz = new JFIAgente();
    AgenteDAO instanceDAO = new AgenteDAO();
    Agente instance = new Agente();

    ResultSet rs;

    DefaultTableModel modeloTable = new DefaultTableModel();
    DefaultTableModel modeloTA = new DefaultTableModel();
    DefaultTableModel modeloTS = new DefaultTableModel();

    Almacen almacen = new Almacen();
    ControladorAlmacen controladorAlmacen = new ControladorAlmacen();

    SeriesyFolios serie = new SeriesyFolios();
    ControladorSeriesyFolios controladorserie = new ControladorSeriesyFolios();

    UsuarioDAO intanceUsuarioDAO = new UsuarioDAO();

    TipoAgenteDAO instanceTipoAgenteDAO = new TipoAgenteDAO();

    public ControladorAgente(JFIAgente interfaz, AgenteDAO instanceDAO) {
        this.interfaz = interfaz;
        this.instanceDAO = instanceDAO;
        this.interfaz.btnNuevo.addActionListener(this);
        this.interfaz.btnAceptar.addActionListener(this);
        this.interfaz.btnEditar.addActionListener(this);
        this.interfaz.btnActualizar.addActionListener(this);
        this.interfaz.btnEliminar.addActionListener(this);
        this.interfaz.btnCancelar.addActionListener(this);
        this.interfaz.btnSalir.addActionListener(this);

        this.interfaz.btnSeleccionarAlmacen.addActionListener(this);
        this.interfaz.btnSeleccionarSeries.addActionListener(this);

        this.interfaz.jTDatos.addMouseListener(this);
        this.interfaz.txtNombreCompleto.addKeyListener(this);
        this.interfaz.txtBuscar.addKeyListener(this);
        this.interfaz.jTableAlmacenes.addKeyListener(this);
        this.interfaz.jTableSeries.addKeyListener(this);
        mtdCabecera(this.interfaz.jTDatos);
        mtdIniciar();
        mtdRellenarTablaAlmacen();
        this.mtdRellenarTablaSeries();
    }

    private void mtdCabecera(JTable Jtable) {
        Jtable.setModel(modeloTable);
        modeloTable.addColumn("Clave ");
        modeloTable.addColumn("Nombre Completo");
        modeloTable.addColumn("Telefono");
    }

    public void mtdLimparTable(JTable tabla) {
        int numeroFila = tabla.getRowCount();
        modeloTable = (DefaultTableModel) tabla.getModel();
        for (int j = 0; j < numeroFila; j++) {
            modeloTable.removeRow(0);
        }
    }

    public void mtdRellenarTabla(JTable Jtable, Agente instance, String status, String filtro) {
        mtdLimparTable(Jtable);
        Object[] columna = new Object[3];
        int numerofila = this.instanceDAO.mtdRellenarAgente(instance, status, filtro).size();
        if (numerofila > 0) {
            for (int i = 0; i < numerofila; i++) {
                columna[0] = this.instanceDAO.mtdRellenarAgente(instance, status, filtro).get(i).getClave();
                columna[1] = this.instanceDAO.mtdRellenarAgente(instance, status, filtro).get(i).getNombre();
                columna[2] = this.instanceDAO.mtdRellenarAgente(instance, status, filtro).get(i).getTelefono();
                modeloTable.addRow(columna);
            }
        } else {
            JOptionPane.showMessageDialog(null, "no se encontro ningun registro");
        }
    }

    public void mtdCargarUsuario() {
        ArrayList<Usuario> usuarios = this.intanceUsuarioDAO.listaUsuario();
        for (Usuario usuario : usuarios) {
            this.interfaz.jComboUsuario.addItem(usuario);
        }
    }

    public void mtdCargarTipoAgente() {
        ArrayList<TipoAgente> tipoAgentes = this.instanceTipoAgenteDAO.listaTipoAgente("E");
        for (TipoAgente tipoAgente : tipoAgentes) {
            this.interfaz.jComboTipoAgente.addItem(tipoAgente);
        }
    }

    public void mtdRellenarTablaAlmacen() {
        this.interfaz.jTableAlmacenes.setModel(modeloTA);
        modeloTA.addColumn("CLAVE");
        modeloTA.addColumn("NOMBRE");
        controladorAlmacen.mtdLlenarTabla(this.interfaz.jTableAlmacenes, almacen, "agente");
    }

    public void mtdRellenarTablaSeries() {
        this.interfaz.jTableSeries.setModel(modeloTS);
        modeloTS.addColumn("CLAVE");
        modeloTS.addColumn("NOMBRE");
        serie.setClave("");
        this.controladorserie.mtdLlenarTable(this.interfaz.jTableSeries, serie, "todo");
    }

    private void mtdIniciar() {
        this.interfaz.jRadioButtonActivoBuscar.setSelected(true);
        this.interfaz.btnNuevo.setVisible(true);
        this.interfaz.btnAceptar.setVisible(false);
        this.interfaz.btnEditar.setVisible(true);
        this.interfaz.btnActualizar.setVisible(false);
        this.interfaz.btnCancelar.setVisible(false);
        this.interfaz.btnEliminar.setVisible(true);
        this.interfaz.btnSalir.setVisible(true);
        this.interfaz.jTabbedPaneAgente.setSelectedIndex(0);
        this.interfaz.jTabbedPaneAgente.setEnabledAt(0, true);
        this.interfaz.jTabbedPaneAgente.setEnabledAt(1, false);
        mtdLimpiarCampos();
        int itemCount = this.interfaz.jComboUsuario.getItemCount();
        for (int i = 0; i < itemCount; i++) {
            this.interfaz.jComboUsuario.removeItemAt(0);
        }
        int itemCountTa = this.interfaz.jComboTipoAgente.getItemCount();
        for (int i = 0; i < itemCountTa; i++) {
            this.interfaz.jComboTipoAgente.removeItemAt(0);
        }
    }

    private void mtdbtnNuevoEditar(String btn) {
        this.interfaz.btnNuevo.setVisible(false);
        this.interfaz.btnEditar.setVisible(false);
        this.interfaz.btnCancelar.setVisible(true);
        this.interfaz.btnSalir.setVisible(false);
        this.interfaz.btnEliminar.setVisible(false);
        this.interfaz.jTabbedPaneAgente.setSelectedIndex(1);
        this.interfaz.jTabbedPaneAgente.setEnabledAt(1, true);
        this.interfaz.jTabbedPaneAgente.setEnabledAt(0, false);
        this.interfaz.txtNombreCompleto.grabFocus();
        mtdCargarUsuario();
        mtdCargarTipoAgente();
        if (btn == "Nuevo") {
            this.interfaz.jRadioButtonActivo.setSelected(true);
            this.interfaz.jRadioButtonInactivo.setSelected(false);
            int ultimoM = this.instanceDAO.mtdUltimoIDdeMovimiento() + 1;
            this.interfaz.txtClaveAGente.setText(String.valueOf(ultimoM));
            this.interfaz.btnAceptar.setVisible(true);
            this.interfaz.jTabbedPaneAgente.setTitleAt(1, "Nuevo");
        }
        if (btn == "Editar") {
            this.interfaz.btnActualizar.setVisible(true);
            this.interfaz.jTabbedPaneAgente.setTitleAt(1, "Editar");
        }
        mtdLimpiarCampos();
    }

    private void mtdRellenarEditarYEliminar(String btn) {
        int numerofila = this.interfaz.jTDatos.getSelectedRow();
        if (numerofila < 0) {
            JOptionPane.showMessageDialog(null, "Seleccione una fila");
        } else {
            this.instance.setClave(Integer.parseInt(this.interfaz.jTDatos.getValueAt(numerofila, 0).toString()));
            if (btn == "Editar") {
                mtdbtnNuevoEditar("Editar");
                String status = "0,1";
                String filtro = "clave";
                this.interfaz.txtClaveAGente.setText(this.instanceDAO.mtdRellenarAgente(instance, status, filtro).get(0).getClave() + "");

                int statu = this.instanceDAO.mtdRellenarAgente(instance, status, filtro).get(0).getStatus();
                if (statu == 1) {
                    this.interfaz.jRadioButtonActivo.setSelected(true);
                }
                if (statu == 0) {
                    this.interfaz.jRadioButtonInactivo.setSelected(true);
                }
                this.interfaz.txtNombreCompleto.setText(this.instanceDAO.mtdRellenarAgente(instance, status, filtro).get(0).getNombre());
                this.interfaz.txtrfc.setText(this.instanceDAO.mtdRellenarAgente(instance, status, filtro).get(0).getRfc());
                this.interfaz.txtPais.setText(this.instanceDAO.mtdRellenarAgente(instance, status, filtro).get(0).getPais());
                this.interfaz.txtEstado.setText(this.instanceDAO.mtdRellenarAgente(instance, status, filtro).get(0).getEstado());
                this.interfaz.txtPoblacion.setText(this.instanceDAO.mtdRellenarAgente(instance, status, filtro).get(0).getPoblacion());
                this.interfaz.txtColonia.setText(this.instanceDAO.mtdRellenarAgente(instance, status, filtro).get(0).getColonia());
                this.interfaz.txtCodigoPostal.setText(this.instanceDAO.mtdRellenarAgente(instance, status, filtro).get(0).getCodigoPostal());
                this.interfaz.txtCalle.setText(this.instanceDAO.mtdRellenarAgente(instance, status, filtro).get(0).getCalle());
                this.interfaz.txtCelular.setText(this.instanceDAO.mtdRellenarAgente(instance, status, filtro).get(0).getCelular());
                this.interfaz.txtTelefono.setText(this.instanceDAO.mtdRellenarAgente(instance, status, filtro).get(0).getTelefono());
                this.interfaz.txtCorre.setText(this.instanceDAO.mtdRellenarAgente(instance, status, filtro).get(0).getCorreo());

                this.interfaz.txtAlmacenes.setText(this.instanceDAO.mtdRellenarAgente(instance, status, filtro).get(0).getAlmacenes());
                this.interfaz.txtSeries.setText(this.instanceDAO.mtdRellenarAgente(instance, status, filtro).get(0).getSeries());
                this.interfaz.txtPrecios.setText(this.instanceDAO.mtdRellenarAgente(instance, status, filtro).get(0).getPrecios());

                Usuario user = this.instanceDAO.mtdRellenarAgente(instance, status, filtro).get(0).getUsuario();
                this.interfaz.jComboUsuario.setSelectedIndex(0);
                this.interfaz.jComboUsuario.addItem(user);
                this.interfaz.jComboUsuario.setSelectedItem(user);

                TipoAgente ta = this.instanceDAO.mtdRellenarAgente(instance, status, filtro).get(0).getTipoAgente();
                this.interfaz.jComboTipoAgente.setSelectedIndex(0);
                this.interfaz.jComboTipoAgente.addItem(ta);
                this.interfaz.jComboTipoAgente.setSelectedItem(ta);

            } else if (btn == "Eliminar") {
                int rpt = JOptionPane.showConfirmDialog(null, "Esta seguro de eliminar el Empleado Seleccionado");
                if (rpt == 0) {
                    this.instanceDAO.mtdElimnar(instance);
                }
            }
        }
    }

    private void mtdActualizarAceptar(String btn) {
        if (this.interfaz.txtNombreCompleto.getText().length() < 4) {
            JOptionPane.showMessageDialog(null, "Campo Nombre vacio", "Error", JOptionPane.ERROR_MESSAGE);
            this.interfaz.txtNombreCompleto.setBackground(Color.red);
            this.interfaz.txtNombreCompleto.grabFocus();
        } else {
            mtdCacharValoresCampo();
            if (btn.equals("Aceptar")) {
                String respuesta = this.instanceDAO.mtdInsertar(instance);
                JOptionPane.showMessageDialog(null, respuesta);
                if (respuesta.equals("Creado")) {
                    mtdLimpiarCampos();
                    this.interfaz.txtNombreCompleto.grabFocus();
                }
            } else if (btn == "Actualizar") {
                instance.setClave(Integer.parseInt(this.interfaz.txtClaveAGente.getText()));
                int rpt = this.instanceDAO.mtdActualizar(instance);
                if (rpt == 1) {
                    JOptionPane.showMessageDialog(null, "Actualizado Correctamente");
                    this.mtdIniciar();
                } else {
                    JOptionPane.showMessageDialog(null, "Desconicido", "Erro", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }

    private void mtdLimpiarCampos() {
        this.interfaz.txtNombreCompleto.setText("");
        this.interfaz.txtrfc.setText("");
        this.interfaz.txtPais.setText("");
        this.interfaz.txtEstado.setText("");
        this.interfaz.txtPoblacion.setText("");
        this.interfaz.txtColonia.setText("");
        this.interfaz.txtCalle.setText("");
        this.interfaz.txtCodigoPostal.setText("");
        this.interfaz.txtCelular.setText("");
        this.interfaz.txtTelefono.setText("");
        this.interfaz.txtCorre.setText("");
        this.interfaz.txtSeries.setText("");
        this.interfaz.txtPrecios.setText("");
        this.interfaz.txtAlmacenes.setText("");
    }

    private void mtdCacharValoresCampo() {
        try {
            instance.setClave(Integer.parseInt(this.interfaz.txtClaveAGente.getText()));
            instance.setNombre(interfaz.txtNombreCompleto.getText());
            int status = 1;
            if (this.interfaz.jRadioButtonActivo.isSelected() == true) {
                status = 1;
            }
            if (this.interfaz.jRadioButtonInactivo.isSelected() == true) {
                status = 0;
            }
            instance.setStatus(status);
            instance.setRfc(interfaz.txtrfc.getText());
            instance.setPais(interfaz.txtPais.getText());
            instance.setEstado(interfaz.txtEstado.getText());
            instance.setPoblacion(this.interfaz.txtPoblacion.getText());
            instance.setColonia(this.interfaz.txtColonia.getText());
            instance.setCalle(interfaz.txtCalle.getText());
            instance.setCodigoPostal(interfaz.txtCodigoPostal.getText());
            instance.setCelular(interfaz.txtCelular.getText());
            instance.setTelefono(interfaz.txtTelefono.getText());
            instance.setCorreo(this.interfaz.txtCorre.getText());
            instance.setAlmacenes(this.interfaz.txtAlmacenes.getText());
            instance.setSeries(this.interfaz.txtSeries.getText());
            instance.setPrecios(this.interfaz.txtPrecios.getText());
            Usuario us = (Usuario) this.interfaz.jComboUsuario.getSelectedItem();
            instance.setUsuario(us);
            TipoAgente tA = (TipoAgente) interfaz.jComboTipoAgente.getSelectedItem();
            instance.setTipoAgente(tA);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == interfaz.btnNuevo) {
            mtdbtnNuevoEditar("Nuevo");
        }
        if (e.getSource() == interfaz.btnAceptar) {

            mtdActualizarAceptar("Aceptar");
        }
        if (e.getSource() == interfaz.btnEliminar) {
            this.mtdRellenarEditarYEliminar("Eliminar");
        }
        if (e.getSource() == interfaz.btnEditar) {
            this.mtdRellenarEditarYEliminar("Editar");
        }
        if (e.getSource() == interfaz.btnActualizar) {
            mtdActualizarAceptar("Actualizar");
        }
        if (e.getSource() == interfaz.btnCancelar) {
            int itemCount = this.interfaz.jComboTipoAgente.getItemCount();
            for (int i = 0; i < itemCount; i++) {
                this.interfaz.jComboTipoAgente.removeItemAt(0);
            }
            int itemCountU = this.interfaz.jComboUsuario.getItemCount();
            for (int i = 0; i < itemCountU; i++) {
                this.interfaz.jComboUsuario.removeItemAt(0);
            }
            mtdIniciar();
        }

        if (e.getSource() == interfaz.btnSalir) {
            this.interfaz.dispose();
        }
        if (e.getSource() == this.interfaz.btnSeleccionarAlmacen) {
            this.interfaz.jDialogAlmacen.setSize(400, 250);
            this.interfaz.jDialogAlmacen.setLocation(this.interfaz.btnSeleccionarAlmacen.getLocation().x, this.interfaz.btnSeleccionarAlmacen.getLocation().y);
            this.interfaz.jDialogAlmacen.setVisible(true);
        }
        if (e.getSource() == this.interfaz.btnSeleccionarSeries) {
            this.interfaz.jDialogSeries.setSize(400, 250);
            this.interfaz.jDialogSeries.setLocation(this.interfaz.btnSeleccionarSeries.getLocation().x, this.interfaz.btnSeleccionarSeries.getLocation().y);
            this.interfaz.jDialogSeries.setVisible(true);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            if (e.getSource() == this.interfaz.txtBuscar) {
                String filtro = "";
                String status = "";
                if (interfaz.jRadioButtonActivoBuscar.isSelected() == true) {
                    status = "1";
                }
                if (interfaz.jRadioButtonInactivoBuscar.isSelected() == true) {
                    status = "0";
                }
                if (interfaz.jRadioButtonTodosBuscar.isSelected() == true) {
                    status = "0,1";
                }
                if (this.interfaz.jComboBoxBuscar.getSelectedIndex() == 0) {
                    filtro = "todo";
                }
                if (this.interfaz.jComboBoxBuscar.getSelectedIndex() == 1) {
                    filtro = "clave";
                    if (this.interfaz.txtBuscar.getText().length() > 0) {
                        int clave = 0;
                        try {
                            clave = Integer.parseInt(this.interfaz.txtBuscar.getText());
                            if (clave > 0) {
                                this.instance.setClave(clave);
                            } else {
                                JOptionPane.showMessageDialog(null, "Ingrese una clave valida", "Advertencia", JOptionPane.WARNING_MESSAGE);
                            }
                        } catch (Exception ex) {
                            JOptionPane.showMessageDialog(null, "Ingrese una clave valida", "Advertencia", JOptionPane.WARNING_MESSAGE);
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Ingrese una clave ", "Advertencia", JOptionPane.WARNING_MESSAGE);
                    }
                }
                if (this.interfaz.jComboBoxBuscar.getSelectedIndex() == 2) {
                    filtro = "razon";
                    String razon = this.interfaz.txtBuscar.getText();
                    if (razon.length() > 0) {
                        this.instance.setNombre(razon);
                    } else {
                        JOptionPane.showMessageDialog(null, "Ingrese una Nombre valido", "Advertencia", JOptionPane.WARNING_MESSAGE);
                    }
                }
                mtdRellenarTabla(this.interfaz.jTDatos, instance, status, filtro);
            }
            if (e.getSource() == this.interfaz.jTableAlmacenes) {
                int[] numeroFila = interfaz.jTableAlmacenes.getSelectedRows();
                ArrayList listaAlmacen = new ArrayList();
                for (int i = 0; i < numeroFila.length; i++) {
                    listaAlmacen.add("'" + this.interfaz.jTableAlmacenes.getValueAt(numeroFila[i], 0) + "'");
                }
                System.out.println(listaAlmacen);
                this.interfaz.txtAlmacenes.setText(listaAlmacen.toString().replace("[", "").replace("]", ""));

                this.interfaz.jDialogAlmacen.dispose();
            }
            if (e.getSource() == this.interfaz.jTableSeries) {
                int[] numeroFila = interfaz.jTableSeries.getSelectedRows();
                ArrayList listaAlmacen = new ArrayList();
                for (int i = 0; i < numeroFila.length; i++) {
                    listaAlmacen.add("'" + this.interfaz.jTableSeries.getValueAt(numeroFila[i], 0) + "'");
                }
                this.interfaz.txtSeries.setText(listaAlmacen.toString().replace("[", "").replace("]", ""));
                this.interfaz.jDialogSeries.dispose();
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == interfaz.txtNombreCompleto) {
            this.interfaz.txtNombreCompleto.setBackground(Color.white);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}
