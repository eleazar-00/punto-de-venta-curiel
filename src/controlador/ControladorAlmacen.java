/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.ResultSet;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import modelo.Almacen;
import modelo.AlmacenDAO;
import modelo.Empresa;
import modelo.EmpresaDAO;

import vista.JFIAlmacen;

/**
 *
 * @author ELEAZAR
 */
public class ControladorAlmacen implements ActionListener, KeyListener {

    JFIAlmacen interfaz = new JFIAlmacen();
    AlmacenDAO instanceDAO = new AlmacenDAO();
    Almacen instance = new Almacen();

    DefaultTableModel modeloT = new DefaultTableModel();

    Empresa empresa = new Empresa();
    EmpresaDAO empresaDAO = new EmpresaDAO();
    ResultSet rs;
    ValidarCampos validar = new ValidarCampos();

    public ControladorAlmacen(JFIAlmacen interfaz, AlmacenDAO instanceDAO) {
        this.interfaz = interfaz;
        this.instanceDAO = instanceDAO;
        this.interfaz.btnNuevo.addActionListener(this);
        this.interfaz.btnEditar.addActionListener(this);
        this.interfaz.btnAceptar.addActionListener(this);
        this.interfaz.btnActualizar.addActionListener(this);
        this.interfaz.btnCancelar.addActionListener(this);
        this.interfaz.btnEliminar.addActionListener(this);
        this.interfaz.btnSalir.addActionListener(this);

        this.interfaz.btnNuevo.addKeyListener(this);
        this.interfaz.btnEditar.addKeyListener(this);
        this.interfaz.btnAceptar.addKeyListener(this);
        this.interfaz.btnActualizar.addKeyListener(this);
        this.interfaz.btnCancelar.addKeyListener(this);
        this.interfaz.btnEliminar.addKeyListener(this);
        this.interfaz.btnSalir.addKeyListener(this);
        this.interfaz.jtDatos.addKeyListener(this);
        this.interfaz.txtBuscar.addKeyListener(this);
        mtdCabeceraTabla(interfaz.jtDatos);
        mtdIniciar();
    }

    public ControladorAlmacen() {
    }

    public void mtdCabeceraTabla(JTable tableD) {
        tableD.setModel(modeloT);
        //nombre de la cabecera de la tabla
        modeloT.addColumn("CLAVE");
        modeloT.addColumn("NOMBRE");
        modeloT.addColumn("ESTADO");
        modeloT.addColumn("DIRECCIÓN");
        modeloT.addColumn("TELEFONO");
    }

    public void mtdLimparTable(JTable tableD) {
        int numeroFila = tableD.getRowCount();
        modeloT = (DefaultTableModel) tableD.getModel();
        for (int j = 0; j < numeroFila; j++) {
            modeloT.removeRow(0);
        }
    }

    public void mtdLlenarTabla(JTable tableD, Almacen instance, String controlador) {
        mtdLimparTable(tableD);
        Object[] columna = new Object[6];
        int numRegistro = instanceDAO.listaAlmacen(instance, "todo", "").size();
        ///metodo para rellenar la tabla con los datos/////
        for (int i = 0; i < numRegistro; i++) {
            columna[0] = instanceDAO.listaAlmacen(instance, "todo", "").get(i).getClave();
            columna[1] = instanceDAO.listaAlmacen(instance, "todo", "").get(i).getNombre();
            if (controlador.equals("articulo")) {
                columna[2] = 0;
                columna[3] = 0;
                columna[4] = 0;
                columna[5] = 0;
            } else {
                columna[2] = instanceDAO.listaAlmacen(instance, "todo", "").get(i).getEstado();
                columna[3] = instanceDAO.listaAlmacen(instance, "todo", "").get(i).getDireccion();
                columna[4] = instanceDAO.listaAlmacen(instance, "todo", "").get(i).getTelefono();
            }
            modeloT.addRow(columna);
        }
    }

    public void mtdIniciar() {
        mtdLimpiarTXT();
        this.interfaz.btnNuevo.setVisible(true);
        this.interfaz.btnEditar.setVisible(true);
        this.interfaz.btnAceptar.setVisible(false);
        this.interfaz.btnActualizar.setVisible(false);
        this.interfaz.btnCancelar.setVisible(false);
        this.interfaz.btnEliminar.setVisible(true);
        this.interfaz.btnSalir.setVisible(true);
        this.interfaz.jTabbedPaneAlmacen.setEnabledAt(0, true);
        this.interfaz.jTabbedPaneAlmacen.setEnabledAt(1, false);
        this.interfaz.jTabbedPaneAlmacen.setSelectedIndex(0);
    }

    public void mtdCacharTXT() {
        instance.setClave(interfaz.txtclave.getText());
        instance.setNombre(this.interfaz.txtnombre.getText());
        instance.setPais(this.interfaz.txtPais.getText());
        instance.setEstado(this.interfaz.txtestado.getText());
        instance.setPoblacion(this.interfaz.txtpoblacion.getText());
        instance.setDireccion(this.interfaz.txtdireccion.getText());
        instance.setTelefono(this.interfaz.txttelefono.getText());
        instance.setEmail(this.interfaz.txtcorreo.getText());
        String flgventa = "N", flgcompra = "N", flginterno = "N";
        if (this.interfaz.jcbCompra.isSelected() == true) {
            flgcompra = "S";
        }
        if (this.interfaz.jcbventa.isSelected() == true) {
            flgventa = "S";
        }
        if (this.interfaz.jcbmInterno.isSelected() == true) {
            flginterno = "S";
        }
        instance.setFlgalmacen(flgcompra + "" + flgventa + "" + flginterno);
    }

    public void mtdLimpiarTXT() {
        this.interfaz.txtclave.setText("");
        this.interfaz.txtnombre.setText("");
        this.interfaz.txtPais.setText("");
        this.interfaz.txtestado.setText("");
        this.interfaz.txtpoblacion.setText("");
        this.interfaz.txtdireccion.setText("");
        this.interfaz.txttelefono.setText("");
        this.interfaz.txtcorreo.setText("");
        this.interfaz.jcbCompra.setSelected(true);
        this.interfaz.jcbventa.setSelected(true);
        this.interfaz.jcbmInterno.setSelected(true);
    }

    public void mtdNuevoEditar() {
        this.interfaz.btnNuevo.setVisible(false);
        this.interfaz.btnEditar.setVisible(false);
        this.interfaz.btnCancelar.setVisible(true);
        this.interfaz.btnEliminar.setVisible(false);
        this.interfaz.btnSalir.setVisible(false);
        this.interfaz.jTabbedPaneAlmacen.setEnabledAt(1, true);
        this.interfaz.jTabbedPaneAlmacen.setEnabledAt(0, false);
        this.interfaz.jTabbedPaneAlmacen.setSelectedIndex(1);
    }

    public void btnNuevo() {
        this.interfaz.btnAceptar.setVisible(true);///btn nuevo
        this.interfaz.txtclave.setVisible(true);
        this.interfaz.jTabbedPaneAlmacen.setTitleAt(1, "Nuevo");///btn nuevo
        mtdNuevoEditar();
        this.interfaz.txtclave.setEditable(true);
        this.interfaz.txtclave.grabFocus();
    }

    public void btnEditar() {
        int numeroFila = this.interfaz.jtDatos.getSelectedRow();
        if (numeroFila >= 0) {
            this.instance.setClave(this.interfaz.jtDatos.getValueAt(numeroFila, 0).toString());
            this.interfaz.txtclave.setText(instanceDAO.listaAlmacen(instance, "clave", "").get(0).getClave());
            this.interfaz.txtnombre.setText(instanceDAO.listaAlmacen(instance, "clave", "").get(0).getNombre());
            this.interfaz.txtPais.setText(instanceDAO.listaAlmacen(instance, "clave", "").get(0).getPais());
            this.interfaz.txtestado.setText(instanceDAO.listaAlmacen(instance, "clave", "").get(0).getEstado());
            this.interfaz.txtpoblacion.setText(instanceDAO.listaAlmacen(instance, "clave", "").get(0).getPoblacion());
            this.interfaz.txtdireccion.setText(instanceDAO.listaAlmacen(instance, "clave", "").get(0).getDireccion());
            this.interfaz.txttelefono.setText(instanceDAO.listaAlmacen(instance, "clave", "").get(0).getTelefono());
            this.interfaz.txtcorreo.setText(instanceDAO.listaAlmacen(instance, "clave", "").get(0).getEmail());
            String flgalmacen = instanceDAO.listaAlmacen(instance, "clave", "").get(0).getFlgalmacen();
            if ((flgalmacen.charAt(0) + "").equals("N")) {
                this.interfaz.jcbCompra.setSelected(false);
            }
            if ((flgalmacen.charAt(1) + "").equals("N")) {
                this.interfaz.jcbventa.setSelected(false);
            }
            if ((flgalmacen.charAt(2) + "").equals("N")) {
                this.interfaz.jcbmInterno.setSelected(false);
            }
            this.interfaz.txtclave.setEditable(false);
            this.interfaz.btnActualizar.setVisible(true);
            this.interfaz.jTabbedPaneAlmacen.setTitleAt(1, "Editar");
            mtdNuevoEditar();
            this.interfaz.txtnombre.grabFocus();
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione una fila");
        }
    }

    public void btnAceptar() {
        if (this.interfaz.txtclave.getText().length() <= 2) {
            JOptionPane.showMessageDialog(null, "Campo Clave Obligatorio!!!", "Error", JOptionPane.ERROR_MESSAGE);
            this.interfaz.txtclave.grabFocus();
        } else if (this.interfaz.txtnombre.getText().length() <= 2) {
            JOptionPane.showMessageDialog(null, "Campo Nombre Obligatorio!!!", "Error", JOptionPane.ERROR_MESSAGE);
            this.interfaz.txtnombre.grabFocus();
        } else if (!validar.mtdEmail(this.interfaz.txtcorreo.getText()) && this.interfaz.txtcorreo.getText().length() > 0) {
            JOptionPane.showMessageDialog(null, "Correo Electronico no valido", "Error", JOptionPane.WARNING_MESSAGE);
            this.interfaz.txtcorreo.grabFocus();
        } else {
            mtdCacharTXT();
            String Respuesta = this.instanceDAO.mtdInserta(instance);
            JOptionPane.showMessageDialog(null, Respuesta);
            if (Respuesta.equals("Registrado")) {
                mtdLimpiarTXT();
            }
            this.interfaz.txtclave.grabFocus();
        }
    }

    public void btnActualizar() {
        if (this.interfaz.txtnombre.getText().length() <= 2) {
            JOptionPane.showMessageDialog(null, "Campo Nombre Obligatorio!!!", "Error", JOptionPane.ERROR_MESSAGE);
            this.interfaz.txtnombre.grabFocus();
        } else if (!validar.mtdEmail(this.interfaz.txtcorreo.getText()) && this.interfaz.txtcorreo.getText().length() > 0) {
            JOptionPane.showMessageDialog(null, "Correo Electronico no valido", "Error", JOptionPane.WARNING_MESSAGE);
            this.interfaz.txtcorreo.grabFocus();
        } else {
            mtdCacharTXT();
            int numero = this.instanceDAO.mtdActualizar(instance);
            if (numero == 1) {
                JOptionPane.showMessageDialog(null, "Actualizado Correctamente");
                mtdIniciar();
            } else {
                this.interfaz.txtnombre.grabFocus();
                JOptionPane.showMessageDialog(null, "Error desconocido al actualizar");
            }
        }
    }

    public void btnEliminar() {
        int numeroFila = this.interfaz.jtDatos.getSelectedRow();
        String claveDepartamento = "";
        if (numeroFila >= 0) {
            instance.setClave(String.valueOf(interfaz.jtDatos.getValueAt(numeroFila, 0)));
            int rptUsuario = JOptionPane.showConfirmDialog(null, "Desea eliminar el Almacen" + claveDepartamento);
            if (rptUsuario == 0) {
                this.instanceDAO.mtdEliminar(instance);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione un Almacen");

        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == interfaz.btnNuevo) {
            btnNuevo();
        }
        if (e.getSource() == interfaz.btnEditar) {
            btnEditar();
        }
        if (e.getSource() == interfaz.btnAceptar) {
            btnAceptar();
        }
        if (e.getSource() == interfaz.btnCancelar) {
            mtdIniciar();
        }
        if (e.getSource() == interfaz.btnActualizar) {
            btnActualizar();
        }
        if (e.getSource() == interfaz.btnEliminar) {
            btnEliminar();
        }
        if (e.getSource() == this.interfaz.btnSalir) {
            this.interfaz.dispose();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        if (e.getSource() == interfaz.txtnombre) {
            char c = e.getKeyChar();
            if (c < 'A' || c > 'Z' && c < 'a' || c > 'z') {
                e.consume();
            }
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            if (e.getSource() == this.interfaz.txtBuscar) {
                instance.setClave("");
                this.mtdLlenarTabla(this.interfaz.jtDatos, instance, "almacen");
            }
            if (e.getSource() == this.interfaz.jtDatos) {
                btnEditar();
            }
            if (e.getSource() == interfaz.btnNuevo) {
                btnNuevo();
            }
            if (e.getSource() == interfaz.btnEditar) {
                btnEditar();
            }
            if (e.getSource() == interfaz.btnAceptar) {
                btnAceptar();
            }
            if (e.getSource() == interfaz.btnCancelar) {
                mtdIniciar();
            }
            if (e.getSource() == interfaz.btnActualizar) {
                btnActualizar();
            }
            if (e.getSource() == interfaz.btnEliminar) {
                btnEliminar();
            }
        }
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            this.interfaz.dispose();
        }
        if (e.getKeyCode() == KeyEvent.VK_DELETE) {
            btnEliminar();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == this.interfaz.txtBuscar) {
            instance.setClave(this.interfaz.txtBuscar.getText());
            instance.setNombre(this.interfaz.txtBuscar.getText());
            this.mtdLlenarTabla(this.interfaz.jtDatos, instance, "almacen");
        }
    }
}
