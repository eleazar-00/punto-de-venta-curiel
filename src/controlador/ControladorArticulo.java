/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import modelo.*;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;
import vista.*;

/**
 *
 * @author ELEAZAR
 */
public class ControladorArticulo implements ActionListener, KeyListener {

    Articulo instance = new Articulo();
    JFIArticulo interfaz = new JFIArticulo();
    ArticuloDAO instanceDAO = new ArticuloDAO();

    Almacen instanceAlmancen = new Almacen();
    ControladorAlmacen instanceControladorAlmance = new ControladorAlmacen();

    ExistenciaAlmacen instanceExisAlmancen = new ExistenciaAlmacen();
    ControladorExistenciaAlmacen instanceExistenciaAlmacen = new ControladorExistenciaAlmacen();

    ArticuloProveedor instanceAP = new ArticuloProveedor();
    ControladorArticuloProveedor instanceControladorArticuloProveedor = new ControladorArticuloProveedor();
    ArticuloProveedorDAO instanceAPDAO = new ArticuloProveedorDAO();

    Proveedor intancePV = new Proveedor();

    Departamento instanceDep = new Departamento();
    ControladorDepartamento instanceDepControlador = new ControladorDepartamento();

    Marca instanceMar = new Marca();
    ControladorMarca instanceMarControlador = new ControladorMarca();

    Ubicacion instanceUbi = new Ubicacion();
    ControladorUbicacion instanceUbiControlador = new ControladorUbicacion();

    Unidad instanceUnidad = new Unidad();
    ControladorUnidad instanceUnidadControl = new ControladorUnidad();

    DefaultTableModel modeloT;
    ///relaciones
    DefaultTableModel modeloTU = new DefaultTableModel();
    DefaultTableModel modeloTM = new DefaultTableModel();
    DefaultTableModel modeloTD = new DefaultTableModel();
    DefaultTableModel modeloTUB = new DefaultTableModel();
    DefaultTableModel modeloPA = new DefaultTableModel();

    int i = 0;
 
    ValidarCampos validar = new ValidarCampos();
    Conexion conexion;

    public ControladorArticulo(JFIArticulo interfaz, ArticuloDAO instanceArticuloDAO) {
        this.instanceDAO = instanceArticuloDAO;
        this.interfaz = interfaz;
        //Jmenuitem
        this.interfaz.jMenuItemSalir.addActionListener(this);
        this.interfaz.jMenuItemNuevo.addActionListener(this);
        this.interfaz.jMenuItemCancelar.addActionListener(this);
        this.interfaz.jMenuItemGuardar.addActionListener(this);
        this.interfaz.jMenuItemBuscar.addActionListener(this);
        this.interfaz.jMenuItemActualizar.addActionListener(this);
        this.interfaz.jMenuItemEliminar.addActionListener(this);
        this.interfaz.jMenuItemEditar.addActionListener(this);

        ////btn Articulo////// 
        this.interfaz.btnNuevo.addActionListener(this);
        this.interfaz.btnEditar.addActionListener(this);
        this.interfaz.btnAceptar.addActionListener(this);
        this.interfaz.btnActualizar.addActionListener(this);
        this.interfaz.btnEliminar.addActionListener(this);
        this.interfaz.btnCancelar.addActionListener(this);
        this.interfaz.btnSalir.addActionListener(this);
        this.interfaz.btnreporte.addActionListener(this);
        this.interfaz.txtClaveproducto.addActionListener(this);
        this.interfaz.txtCodigosBarras.addActionListener(this);

        this.interfaz.txtBuscar.addKeyListener(this);
        this.interfaz.jComboBoxBuscar.addKeyListener(this);
        this.interfaz.jComboBoxBuscar.addActionListener(this);
        this.interfaz.jComboBoxDepartamento.addKeyListener(this);
        this.interfaz.jComboBoxDepartamento.addActionListener(this);
        this.interfaz.txtpreciodelista.addKeyListener(this);
        this.interfaz.txtprecio2.addKeyListener(this);
        this.interfaz.txtprecio3.addKeyListener(this);
        this.interfaz.txtporcientopreciodelista.addKeyListener(this);
        this.interfaz.txtporciento2.addKeyListener(this);
        this.interfaz.txtporciento3.addKeyListener(this);
//        mtdCabeceraTablaArticulo(this.interfaz.jtDatosArticulo);
        mtdIniciarArticulo();

        //Btn clave relacion
        this.interfaz.btnlinea.addActionListener(this);
        this.interfaz.btnmarca.addActionListener(this);
        this.interfaz.btnunidad.addActionListener(this);
         this.interfaz.btnubicacion.addActionListener(this);

        //Dar click en laas tablas
        this.interfaz.jtDatosArticulo.addKeyListener(this);
        this.interfaz.tablaUnidad.addKeyListener(this);
        this.interfaz.tablaMarca.addKeyListener(this);
        this.interfaz.tableLinea.addKeyListener(this);
        this.interfaz.tableUbicacion.addKeyListener(this);

        //Btn proveedor
        this.interfaz.jbtnnuevoproveedor.addActionListener(this);
        this.interfaz.jbtneliminarproveedor.addActionListener(this);
        this.interfaz.txtBuscarProveedor.addKeyListener(this);
        this.interfaz.btnAgregarProveedor.addActionListener(this);

        this.interfaz.txturlimagen.setVisible(false);
        ///btn cargar Imagen
        this.interfaz.btnimagen.addActionListener(this);
        
        //Validar campos
        this.interfaz.txtcostoultimo.addKeyListener(this);
        this.interfaz.txtCodigoSat.addKeyListener(this);
        this.interfaz.txtpreciodelista.addKeyListener(this);
        this.interfaz.txtprecio2.addKeyListener(this);
        this.interfaz.txtprecio3.addKeyListener(this);

        this.interfaz.txtcantidad2.addKeyListener(this);
        this.interfaz.txtcantidad3.addKeyListener(this);
         mtdCargarComboDepartamento();
    }

    public ControladorArticulo() {
    }

    private void mtdLimparTableArticulo(JTable tableD) {
        int numeroFila = tableD.getRowCount();
        modeloT = (DefaultTableModel) tableD.getModel();
        for (int j = 0; j < numeroFila; j++) {
            modeloT.removeRow(0);
        }
    }

    protected void mtdRellenarDatosTablaArticulo(JTable tableD, Articulo instance, String status, String filtro, String movimiento) {
        mtdLimparTableArticulo(tableD);
        Object[] columna = new Object[6];
        tableD.setDefaultRenderer(Object.class, new TablaImagen());
        if (movimiento.equals("ventaServicio")) {
            tableD.setRowHeight(150);
        } else {
            tableD.setRowHeight(30);
        }
        int numRegistro = instanceDAO.listaArticulo(instance, status, filtro).size();
        ///metodo para rellenar la tabla con los datos/////
        if (numRegistro > 0) {
            for (int i = 0; i < numRegistro; i++) {
                columna[0] = instanceDAO.listaArticulo(instance, status, filtro).get(i).getCodigo();
                columna[1] = instanceDAO.listaArticulo(instance, status, filtro).get(i).getNombre();
                if (movimiento.equals("compra")) {
                    columna[2] = instanceDAO.listaArticulo(instance, status, filtro).get(i).getCostoUltimo();
                }
                if (movimiento.equals("venta") || movimiento.equals("ventaServicio")) {
                    columna[2] = instanceDAO.listaArticulo(instance, status, filtro).get(i).getPrecio_lista();
                }
                if (movimiento.equals("ventaServicio")) {
                    String ruta = instanceDAO.listaArticulo(instance, status, filtro).get(i).getUrl_imagen();
                    ImageIcon img = new ImageIcon(ruta);
                    //Añadimos la imagen a la columna correspondiente
                    columna[3] = new JLabel(new ImageIcon(img.getImage().getScaledInstance(500, 200, Image.SCALE_AREA_AVERAGING)));
                } else {
                    columna[3] = 0;
                    columna[4] = instanceDAO.listaArticulo(instance, status, filtro).get(i).getUnidad().getClave();
                }
                modeloT.addRow(columna);
            }
        }
        if (numRegistro <= 0) {
            JOptionPane.showMessageDialog(null, "No se encontro ningun registro", "Error", JOptionPane.WARNING_MESSAGE);
        }
    }

    private void mtdlimpiarArticulo() {
        this.interfaz.txtClaveproducto.setText("");
        this.interfaz.txtCodigosBarras.setText("");
        this.interfaz.txtNombre.setText("");
        this.interfaz.txtDiasCaducidad.setText("0");
        this.interfaz.txtdescuentomaximo.setText("0");

        this.interfaz.txtlinea.setText("000");
        this.interfaz.txtmarca.setText("000");
        this.interfaz.txtubicacion.setText("000");
        this.interfaz.txtunidad.setText("PZ");

        this.interfaz.txtcostoultimo.setText("0.0");
        this.interfaz.txtcostoreposion.setText("0.0");
        this.interfaz.txtcostoanterior.setText("0.0");
        this.interfaz.txtcostopromedio.setText("0.0");

        this.interfaz.txtpreciodelista.setText("0");
        this.interfaz.txtporcientopreciodelista.setText("0");

        this.interfaz.txtprecio2.setText("0");
        this.interfaz.txtporciento2.setText("0");
        this.interfaz.txtcantidad2.setText("0");

        this.interfaz.txtprecio3.setText("0");
        this.interfaz.txtporciento3.setText("0");
        this.interfaz.txtcantidad3.setText("0");

        this.interfaz.txturlimagen.setText("");
        this.interfaz.txtimpuesto.setText("0");
        this.interfaz.txtCodigoSat.setText("");
    }

    private void mtdIniciarArticulo() {
        this.interfaz.jRadioButtonActivoBuscar.setSelected(true);
        this.interfaz.txtBuscar.setVisible(false);
        this.interfaz.jComboBoxDepartamento.setVisible(false);
        this.interfaz.jMenuItemNuevo.setVisible(true);
        this.interfaz.jMenuItemEditar.setEnabled(true);
        this.interfaz.jMenuItemGuardar.setEnabled(false);
        this.interfaz.jMenuItemActualizar.setEnabled(false);
        this.interfaz.jMenuItemEliminar.setEnabled(true);
        this.interfaz.jMenuItemCancelar.setEnabled(false);
        this.interfaz.jMenuItemSalir.setEnabled(true);
        this.interfaz.jMenuItemBuscar.setEnabled(true);

        this.interfaz.btnNuevo.setVisible(true);
        this.interfaz.btnEditar.setVisible(true);
        this.interfaz.btnAceptar.setVisible(false);
        this.interfaz.btnActualizar.setVisible(false);
        this.interfaz.btnEliminar.setVisible(true);
        this.interfaz.btnCancelar.setVisible(false);
        this.interfaz.btnSalir.setVisible(true);
        this.interfaz.btnreporte.setVisible(true);

        this.interfaz.jTabbedPaneArticulo.setEnabledAt(0, true);
        this.interfaz.jTabbedPaneArticulo.setEnabledAt(1, false);
        this.interfaz.jTabbedPaneArticulo.setEnabledAt(2, false);
        this.interfaz.jTabbedPaneArticulo.setEnabledAt(3, false);
        this.interfaz.jTabbedPaneArticulo.setEnabledAt(4, false);
        this.interfaz.jTabbedPaneArticulo.setSelectedIndex(0);
        this.interfaz.panelImpuesto.setVisible(false);
    }

    public void mtdCacharValoresTxtArticulo() {
        try {
            this.instance.setCodigo(this.interfaz.txtClaveproducto.getText().trim());
            this.instance.setCodigoBarras(this.interfaz.txtCodigosBarras.getText().trim());

            int status = 1;
            if (this.interfaz.jRadioButtonActivo.isSelected() == true) {
                status = 1;
            }
            if (this.interfaz.jRadioButtonInactivo.isSelected() == true) {
                status = 0;
            }
            if (this.interfaz.jRadioButtonActivoNoFacturable.isSelected() == true) {
                status = 2;
            }

            this.instance.setStatus(status);
            this.instance.setNombre(this.interfaz.txtNombre.getText());
            this.instance.setTipo(this.interfaz.jComboBoxTipo.getSelectedItem().toString());
            this.instance.setClave_sat(this.interfaz.txtCodigoSat.getText());
            this.instance.setDiasCaducidad(Integer.parseInt(this.interfaz.txtDiasCaducidad.getText()));

            if (this.interfaz.txturlimagen.getText().length() > 0) {
                this.instance.setUrl_imagen(this.interfaz.txturlimagen.getText());
                FileInputStream imagen = new FileInputStream(this.interfaz.txturlimagen.getText());
                this.instance.setImagen(imagen);
            }
            this.instance.setDescuentoMaximo(Float.parseFloat(interfaz.txtdescuentomaximo.getText()));

            String fechaMovimiento = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
            this.instance.setFechaActualizacionCosto(fechaMovimiento);

            this.instance.setCostoUltimo(Double.parseDouble(this.interfaz.txtcostoultimo.getText()));
            this.instance.setCostoReposicion(Double.parseDouble(this.interfaz.txtcostoreposion.getText()));
            this.instance.setCostoAnterior(Double.parseDouble(this.interfaz.txtcostoanterior.getText()));
            this.instance.setCostoPromedio(Double.parseDouble(this.interfaz.txtcostopromedio.getText()));

            this.instance.setPrecio_lista(Double.parseDouble(this.interfaz.txtpreciodelista.getText()));
            this.instance.setUtilidad_lista(Double.parseDouble(this.interfaz.txtporcientopreciodelista.getText()));

            this.instance.setPrecio_dos(Double.parseDouble(this.interfaz.txtprecio2.getText()));
            this.instance.setUtilidad_dos(Double.parseDouble(this.interfaz.txtporciento2.getText()));
            this.instance.setCantidad_dos(Double.parseDouble(this.interfaz.txtcantidad2.getText()));

            this.instance.setPrecio_tres(Double.parseDouble(this.interfaz.txtprecio3.getText()));
            this.instance.setUtilidad_tres(Double.parseDouble(this.interfaz.txtporciento3.getText()));
            this.instance.setCantidad_tres(Double.parseDouble(this.interfaz.txtcantidad3.getText()));

            this.instanceDep = new Departamento();
            this.instanceDep.setClave(this.interfaz.txtlinea.getText());
            this.instance.setDepartamento(instanceDep);

            this.instanceMar = new Marca();
            this.instanceMar.setClave(this.interfaz.txtmarca.getText());
            this.instance.setMarca(instanceMar);
            this.instanceUbi = new Ubicacion();
            this.instanceUbi.setClave(this.interfaz.txtubicacion.getText());
            this.instance.setUbicacion(instanceUbi);
            this.instanceUnidad = new Unidad();
            this.instanceUnidad.setClave(this.interfaz.txtunidad.getText());
            this.instance.setUnidad(instanceUnidad);
            String costoUtilizar = "";
            if (this.interfaz.jRadioBtnUltimo.isSelected() == true) {
                costoUtilizar = "U";
            }
            if (this.interfaz.jRadioBtbReposicion.isSelected() == true) {
                costoUtilizar = "R";
            }
            if (this.interfaz.jRadionBtnPromedio.isSelected() == true) {
                costoUtilizar = "P";
            }
            this.instance.setCostoUtilizar(costoUtilizar);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error cachar valores" + e);
        }
    }

    private void mtdBtnNuevoEditar() {
        this.interfaz.btnNuevo.setVisible(false);
        this.interfaz.btnEditar.setVisible(false);
        this.interfaz.btnEliminar.setVisible(false);
        this.interfaz.btnCancelar.setVisible(true);
        this.interfaz.btnSalir.setVisible(false);
        this.interfaz.btnreporte.setVisible(false);

        this.interfaz.jMenuItemNuevo.setVisible(false);
        this.interfaz.jMenuItemEditar.setEnabled(false);
        this.interfaz.jMenuItemCancelar.setEnabled(true);
        this.interfaz.jMenuItemBuscar.setEnabled(false);
        this.interfaz.jMenuItemSalir.setEnabled(false);
        this.interfaz.jMenuItemEliminar.setEnabled(false);

        this.interfaz.jTabbedPaneArticulo.setEnabledAt(0, false);
        this.interfaz.jTabbedPaneArticulo.setEnabledAt(1, true);
        this.interfaz.jTabbedPaneArticulo.setEnabledAt(1, true);
        this.interfaz.jTabbedPaneArticulo.setEnabledAt(2, true);
        this.interfaz.jTabbedPaneArticulo.setEnabledAt(3, true);
        this.interfaz.jTabbedPaneArticulo.setEnabledAt(4, true);
        this.interfaz.jTabbedPaneArticulo.setSelectedIndex(1);
    }

    private void mtdBtnNuevoEditar(String btn) {
        mtdBtnNuevoEditar();
        this.interfaz.jTabbedPaneArticulo.setTitleAt(1, btn);
        if (btn.equals("Nuevo")) {
            this.interfaz.btnAceptar.setVisible(true); //btn nuevo
            this.interfaz.txtClaveproducto.grabFocus();
            this.interfaz.txtClaveproducto.setEditable(true);
            this.interfaz.jRadioBtnUltimo.setSelected(true);
            this.interfaz.jRadioButtonActivo.setSelected(true);
            this.interfaz.jMenuItemGuardar.setEnabled(true);
            this.interfaz.jMenuItemActualizar.setEnabled(false);
            mtdCargarAlmacens();
        }
        if (btn.equals("Editar")) {
            this.interfaz.btnActualizar.setVisible(true);//btn editar
            this.interfaz.txtNombre.grabFocus();
            this.interfaz.txtClaveproducto.setEditable(false);
            this.interfaz.jMenuItemGuardar.setEnabled(false);
            this.interfaz.jMenuItemActualizar.setEnabled(true);
        }
    }

    private void mtdEditarEliminar(String btn) {
        int numerofila = this.interfaz.jtDatosArticulo.getSelectedRow();
        if (numerofila >= 0) {
            String codigoArticulo = this.interfaz.jtDatosArticulo.getValueAt(numerofila, 0).toString();
            this.instance.setCodigo(codigoArticulo);
            if (btn.equals("Editar")) {
                 String status = "2,1,0";
                mtdBtnNuevoEditar(btn);
                this.interfaz.txtClaveproducto.setText(instanceDAO.listaArticulosPorCodigo(instance, status).get(0).getCodigo());
                this.interfaz.txtCodigosBarras.setText(instanceDAO.listaArticulosPorCodigo(instance, status).get(0).getCodigoBarras());
                int statu = instanceDAO.listaArticulosPorCodigo(instance, status).get(0).getStatus();
                if (statu == 1) {
                    this.interfaz.jRadioButtonActivo.setSelected(true);
                }
                if (statu == 0) {
                    this.interfaz.jRadioButtonInactivo.setSelected(true);
                }
                if (statu == 2) {
                    this.interfaz.jRadioButtonActivoNoFacturable.setSelected(true);
                }
                this.interfaz.txtNombre.setText(instanceDAO.listaArticulosPorCodigo(instance, status).get(0).getNombre());
                this.interfaz.jComboBoxTipo.setSelectedIndex(0);

                this.interfaz.jComboBoxTipo.setSelectedItem(instanceDAO.listaArticulosPorCodigo(instance, status).get(0).getTipo());
                this.interfaz.txtCodigoSat.setText(instanceDAO.listaArticulosPorCodigo(instance, status).get(0).getClave_sat());
                this.interfaz.txtDiasCaducidad.setText(String.valueOf(instanceDAO.listaArticulosPorCodigo(instance, status).get(0).getDiasCaducidad()));
                this.interfaz.txtdescuentomaximo.setText(String.valueOf(instanceDAO.listaArticulosPorCodigo(instance, status).get(0).getDescuentoMaximo()));

                this.interfaz.txtlinea.setText(instanceDAO.listaArticulosPorCodigo(instance, status).get(0).getDepartamento().getClave());
                this.interfaz.txtmarca.setText(instanceDAO.listaArticulosPorCodigo(instance, status).get(0).getMarca().getClave());
                this.interfaz.txtubicacion.setText(instanceDAO.listaArticulosPorCodigo(instance, status).get(0).getUbicacion().getClave());
                this.interfaz.txtunidad.setText(instanceDAO.listaArticulosPorCodigo(instance, status).get(0).getUnidad().getClave());

                this.interfaz.txtcostoultimo.setText(String.valueOf(instanceDAO.listaArticulosPorCodigo(instance, status).get(0).getCostoUltimo()));
                this.interfaz.txtcostoreposion.setText(String.valueOf(instanceDAO.listaArticulosPorCodigo(instance, status).get(0).getCostoReposicion()));
                this.interfaz.txtcostoanterior.setText(String.valueOf(instanceDAO.listaArticulosPorCodigo(instance, status).get(0).getCostoAnterior()));
                this.interfaz.txtcostopromedio.setText(String.valueOf(instanceDAO.listaArticulosPorCodigo(instance, status).get(0).getCostoPromedio()));

                String costoUtilizar = String.valueOf(instanceDAO.listaArticulosPorCodigo(instance, status).get(0).getCostoUtilizar());
                if (costoUtilizar.equals("U")) {
                    this.interfaz.jRadioBtnUltimo.setSelected(true);
                }
                if (costoUtilizar.equals("P")) {
                    this.interfaz.jRadionBtnPromedio.setSelected(true);
                }
                if (costoUtilizar.equals("R")) {
                    this.interfaz.jRadioBtbReposicion.setSelected(true);
                }
                this.interfaz.txtpreciodelista.setText(instanceDAO.listaArticulosPorCodigo(instance, status).get(0).getPrecio_lista() + "");
                this.interfaz.txtporcientopreciodelista.setText(instanceDAO.listaArticulosPorCodigo(instance, status).get(0).getUtilidad_lista() + "");

                this.interfaz.txtprecio2.setText(instanceDAO.listaArticulosPorCodigo(instance, status).get(0).getPrecio_dos() + "");
                this.interfaz.txtporciento2.setText(instanceDAO.listaArticulosPorCodigo(instance, status).get(0).getUtilidad_dos() + "");
                this.interfaz.txtcantidad2.setText(instanceDAO.listaArticulosPorCodigo(instance, status).get(0).getCantidad_dos() + "");

                this.interfaz.txtprecio3.setText(instanceDAO.listaArticulosPorCodigo(instance, status).get(0).getPrecio_tres() + "");
                this.interfaz.txtporciento3.setText(instanceDAO.listaArticulosPorCodigo(instance, status).get(0).getUtilidad_tres() + "");
                this.interfaz.txtcantidad3.setText(instanceDAO.listaArticulosPorCodigo(instance, status).get(0).getCantidad_tres() + "");

                this.interfaz.txturlimagen.setText(instanceDAO.listaArticulosPorCodigo(instance, status).get(0).getUrl_imagen());
                ImageIcon img = new ImageIcon(instanceDAO.listaArticulosPorCodigo(instance, status).get(0).getUrl_imagen());
                interfaz.lblImagen.setIcon(new ImageIcon(img.getImage().getScaledInstance(interfaz.lblImagen.getWidth(), interfaz.lblImagen.getHeight(), Image.SCALE_DEFAULT)));
//                mtdEditarProveedorBD(instance);
                mtdLlenarExistecniasAlmacen(instance);
            }
            if (btn.equals("Eliminar")) {
                int rptu = JOptionPane.showConfirmDialog(null, "Esta seguro de eliminar el articulo seleccionado");
                if (rptu == 0) {
                    this.instanceDAO.mtdEliminar(instance);
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, "seleciones un Articulo");
        }
    }

    private void mtdAceptarActualizar(String btn) {
        try {
            if (this.interfaz.txtClaveproducto.getText().length() <= 0) {
                JOptionPane.showMessageDialog(null, "Campo codigo Articulo vacio", "Error", JOptionPane.ERROR_MESSAGE);
                this.interfaz.txtClaveproducto.grabFocus();
            } else if (this.interfaz.txtNombre.getText().length() <= 1) {
                JOptionPane.showMessageDialog(null, "Campo Nombre vacio", "Error", JOptionPane.ERROR_MESSAGE);
                this.interfaz.txtNombre.grabFocus();
            } else {
                mtdCacharValoresTxtArticulo();
                if (btn.equals("Aceptar")) {
                    String respuesta = this.instanceDAO.mtdInserta(instance);
                    if (respuesta.equals("Registro Exitoso")) {
                        mtdInsetAlmacen(instance.getCodigo());
                        JOptionPane.showMessageDialog(null, respuesta);
                        mtdlimpiarArticulo();
                        this.interfaz.jTabbedPaneArticulo.setSelectedIndex(1);
                        this.interfaz.txtClaveproducto.grabFocus();

                    }
                }
                if (btn.equals("Actualizar")) {
                    int rptu = this.instanceDAO.mtdActulizar(instance);
                    if (rptu == 1) {
                        instance.setCodigo(this.interfaz.txtClaveproducto.getText());
                        mtdEliminarExistenciasAlmacen(instance.getCodigo());
                        mtdInsetAlmacen(instance.getCodigo());
                        mtdIniciarArticulo();
                        JOptionPane.showMessageDialog(null, "Actualizado Correctamente" + rptu);
                    }
                }
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error Aceptar y Actualizar" + ex, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void mtdCargarComboDepartamento() {
        instanceDepControlador.mtdCargarComboDepartamento(this.interfaz.jComboBoxDepartamento);
    }

    /////Metodos Almacenes/////
    private void mtdCargarAlmacens() {
        instanceControladorAlmance.mtdLlenarTabla(this.interfaz.jtableExistenciasArticulo, instanceAlmancen, "articulo");
    }

    private void mtdInsetAlmacen(String claveArticulo) {
        this.instanceExistenciaAlmacen.mtdInsetAlmacen(this.interfaz.jtableExistenciasArticulo, claveArticulo);
    }

    private void mtdLlenarExistecniasAlmacen(Articulo instance) {
        instanceExisAlmancen.setArticulo(instance);
        this.instanceExistenciaAlmacen.mtdLlenarTabla(interfaz.jtableExistenciasArticulo, instanceExisAlmancen);
    }

    private void mtdEliminarExistenciasAlmacen(String claveArticulo) {
        this.instanceExistenciaAlmacen.mtdEliminarExistecniasDB(claveArticulo);
    }

    /////Termino metodo almacences///
    ////////// Articulo Proveedor ///////////////////////////////////////////////////////////////
    private void mtdNuevoProveedor() {
        this.interfaz.jDBuscarProveedor.setSize(870, 500);
        this.interfaz.jDBuscarProveedor.setLocationRelativeTo(null);
        this.interfaz.jDBuscarProveedor.setVisible(true);
        this.interfaz.txtBuscarProveedor.grabFocus();
    }

    private void mtdBtnAgregarPorvedor() {
        int numeroFila = this.interfaz.jTableProveedorBuscar.getSelectedRow();
        if (numeroFila >= 0) {
            int cantidadFilas = this.interfaz.jTablearticuloproveeedor.getRowCount();
            String respueta = "";
            for (i = 0; i < cantidadFilas; i++) {
                int clave = Integer.parseInt(interfaz.jTablearticuloproveeedor.getValueAt(i, 0).toString());
                int ClaveNuevo = Integer.parseInt(interfaz.jTableProveedorBuscar.getValueAt(numeroFila, 0).toString());
                if (clave == ClaveNuevo) {
                    respueta = "existe";
                    JOptionPane.showMessageDialog(null, "Proveedor ya registrado");
                }
            }
            if ("existe".equals(respueta)) {
            } else {
                String plazaentrega = JOptionPane.showInputDialog(null, "PLAZO DE ENTREGA");
                String prioridad = JOptionPane.showInputDialog(null, "PRIORIDAD");
                String COSTO = JOptionPane.showInputDialog(null, "ULTIMO COSTO DEL ARTICULO");
                modeloT = (DefaultTableModel) interfaz.jTablearticuloproveeedor.getModel();
                String elemento[] = {this.interfaz.jTableProveedorBuscar.getValueAt(numeroFila, 0).toString(), this.interfaz.jTableProveedorBuscar.getValueAt(numeroFila, 1).toString(), plazaentrega, prioridad, COSTO, "sadsdasd"};
                modeloT.addRow(elemento);
                this.interfaz.jDBuscarProveedor.dispose();
            }
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione una fila");
        }
    }

    private void mtdBtnEliminarProveedor() {
        this.instanceControladorArticuloProveedor.mtdBtnEliminarProveedor(this.interfaz.jTablearticuloproveeedor);
    }

    private void mtdInsertProveedorBD(Articulo instance) {
        this.instanceAP.setArticulo(instance);
        this.instanceControladorArticuloProveedor.mtdInsertProveedor(this.interfaz.jTablearticuloproveeedor, instanceAP);
    }

    private void mtdEditarProveedorBD(Articulo instance) {
        this.instanceAP.setArticulo(instance);
        this.instanceControladorArticuloProveedor.mtdListaProveedorBD(this.interfaz.jTablearticuloproveeedor, instanceAP);
    }

    private void mtdEliminarProveedorDB(Articulo instance) {
        this.instanceControladorArticuloProveedor.mtdEliminarProveedorDB(instance);
    }
    ///TERMINDAO METODO PROVEEDOR

    @Override
    public void actionPerformed(ActionEvent e) {
        //ACCION DE BTN DE ARTICULO
        if (e.getSource() == this.interfaz.btnNuevo || e.getSource() == this.interfaz.jMenuItemNuevo) {
            mtdlimpiarArticulo();
            mtdBtnNuevoEditar("Nuevo");
        }
        if (e.getSource() == this.interfaz.btnEditar || e.getSource() == this.interfaz.jMenuItemEditar) {
            mtdEditarEliminar("Editar");
        }
        if (e.getSource() == this.interfaz.btnAceptar || e.getSource() == this.interfaz.jMenuItemGuardar) {
            mtdAceptarActualizar("Aceptar");
        }
        if (e.getSource() == this.interfaz.btnActualizar || e.getSource() == this.interfaz.jMenuItemActualizar) {
            mtdAceptarActualizar("Actualizar");
        }
        if (e.getSource() == this.interfaz.btnEliminar || e.getSource() == this.interfaz.jMenuItemEliminar) {
            mtdEditarEliminar("Eliminar");
        }
        if (e.getSource() == this.interfaz.btnCancelar || e.getSource() == this.interfaz.jMenuItemCancelar) {
            this.mtdlimpiarArticulo();
            this.mtdIniciarArticulo();
        }
        if (e.getSource() == this.interfaz.jMenuItemBuscar) {
            this.interfaz.jComboBoxBuscar.grabFocus();
        }
        if (e.getSource() == this.interfaz.btnSalir || e.getSource() == this.interfaz.jMenuItemSalir) {
            this.interfaz.dispose();
        }
        //validar si al registrar un nuevo producto sy ya existe en la base
        if (e.getSource() == this.interfaz.txtClaveproducto) {
            this.instance.setCodigo(this.interfaz.txtClaveproducto.getText());
            int rptCantidad = this.instanceDAO.listaArticulo(instance, "1", "codigo").size();
            if (rptCantidad > 0) {
                JOptionPane.showMessageDialog(null, "Codigo del Articulo ya existe", "Error", JOptionPane.WARNING_MESSAGE);
                this.interfaz.txtClaveproducto.grabFocus();
            } else {
                this.interfaz.txtCodigosBarras.grabFocus();
            }
        }
        //eventos para clave relacionadas
        if (e.getSource() == this.interfaz.btnlinea) {
            mtdRellenarTablaDepartamento();
            this.interfaz.jDialogLinea.setSize(300, 250);
            this.interfaz.jDialogLinea.setLocation(this.interfaz.btnlinea.getLocation().x, this.interfaz.btnlinea.getLocation().y);
            this.interfaz.jDialogLinea.setVisible(true);
        }
        if (e.getSource() == this.interfaz.btnunidad) {
            this.mtdRellenarTablaUnidad();
            this.interfaz.jDialogUnidad.setSize(300, 250);
            this.interfaz.jDialogUnidad.setLocation(this.interfaz.btnunidad.getLocation().x, this.interfaz.btnunidad.getLocation().y);
            this.interfaz.jDialogUnidad.setVisible(true);
        }
        if (e.getSource() == this.interfaz.btnmarca) {
            this.mtdRellenarTablaMarca();
            this.interfaz.jDialogMarca.setSize(300, 250);
            this.interfaz.jDialogMarca.setLocation(this.interfaz.btnmarca.getLocation().x, this.interfaz.btnmarca.getLocation().y);
            this.interfaz.jDialogMarca.setVisible(true);
        }
        if (e.getSource() == this.interfaz.btnubicacion) {
            this.mtdRellenarTablaUbicacion();
            this.interfaz.jDialogUbicacion.setSize(300, 250);
            this.interfaz.jDialogUbicacion.setLocation(this.interfaz.btnubicacion.getLocation().x, this.interfaz.btnubicacion.getLocation().y);
            this.interfaz.jDialogUbicacion.setVisible(true);
        }
        ////btn agregar proveedores
        if (e.getSource() == this.interfaz.jbtnnuevoproveedor) {
            this.mtdNuevoProveedor();
        }
        if (e.getSource() == this.interfaz.btnAgregarProveedor) {
            mtdBtnAgregarPorvedor();
        }
        if (e.getSource() == this.interfaz.jbtneliminarproveedor) {
            mtdBtnEliminarProveedor();
        }
        ///btn proveedor terminiado
        if (e.getSource() == interfaz.btnimagen) {
            try {
                JFileChooser Chooses = new JFileChooser();
                Chooses.setDialogTitle("Seleccionar imagen");
                Chooses.setAcceptAllFileFilterUsed(false);
                FileFilter Filtro1 = new FileNameExtensionFilter("JPG", "jpg");
                FileFilter Filtro2 = new FileNameExtensionFilter("PNG", "png");
                Chooses.setFileFilter(Filtro1);
                Chooses.addChoosableFileFilter(Filtro2);
                Chooses.showOpenDialog(null);
                File archivo = null;
                try {
                    archivo = Chooses.getSelectedFile();

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex);
                }
                String filename = archivo.getAbsolutePath();
                interfaz.txturlimagen.setText(filename.replace("\\", "//"));
                String ruta;
                ruta = interfaz.txturlimagen.getText();
                ImageIcon img = new ImageIcon(ruta);
                interfaz.lblImagen.setIcon(new ImageIcon(img.getImage().getScaledInstance(interfaz.lblImagen.getWidth(), interfaz.lblImagen.getHeight(), Image.SCALE_DEFAULT)));
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Error al cachar la imagenn " + ex, "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
        if (e.getSource() == this.interfaz.btnreporte) {

            try {
                conexion = new Conexion();
                JasperPrint jasperPrint = null;
                String url = "C:\\pos\\reporte\\reporteArticulo.jasper";
                jasperPrint = JasperFillManager.fillReport(url, null, conexion.getConexion());
                JasperViewer ventanaVisor = new JasperViewer(jasperPrint, false);
                ventanaVisor.setTitle("Lista Articulos");
                ventanaVisor.setVisible(true);
            } catch (JRException ex) {
                System.out.println("errrr" + ex);
                Logger.getLogger(ControladorReporteVenta.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(null, ex);
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, ex);
                Logger.getLogger(ControladorReporteVenta.class.getName()).log(Level.SEVERE, null, ex);
            }
            conexion.cerrarConexio();
        }

    }

    @Override
    public void keyTyped(KeyEvent e) {
        // validar Campo de texto;
        if (e.getSource()== this.interfaz.txtcostoultimo) {
            validar.mtdvalidarNumerosPunto(e);       
        }
        //validar txt numero del Sat
        if(e.getSource()==this.interfaz.txtCodigoSat){
          validar.mtdvalidarSoloNumeros(e);
          validar.mtdvalidarCantidadCaracteres(e,this.interfaz.txtCodigoSat,8);
        } 
        if(e.getSource()==this.interfaz.txtpreciodelista ||e.getSource()==this.interfaz.txtprecio2  || e.getSource()==this.interfaz.txtprecio3){
          validar.mtdvalidarNumerosPunto(e);
         }
        //validar cantidades
         if(e.getSource()==this.interfaz.txtcantidad2 ||e.getSource()==this.interfaz.txtcantidad3){
          validar.mtdvalidarSoloNumeros(e);
         } 
     }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            if (e.getSource() == this.interfaz.txtBuscar) {
                 this.mtdBuscarArticulo(this.interfaz.jtDatosArticulo, this.interfaz.txtBuscar, this.interfaz.jRadioButtonActivoBuscar, this.interfaz.jRadioButtonInactivoBuscar, this.interfaz.jRadioButtonTodoBuscar, this.interfaz.jComboBoxBuscar, this.interfaz.jComboBoxDepartamento, "");
            }
            if (e.getSource() == this.interfaz.jtDatosArticulo) {
                mtdEditarEliminar("Editar");
            }
            //metodoas para dar enter a las tabla de relacion
            if (e.getSource() == this.interfaz.tablaUnidad) {
                int numeroFila = this.interfaz.tablaUnidad.getSelectedRow();
                if (numeroFila >= 0) {
                    this.interfaz.txtunidad.setText(this.interfaz.tablaUnidad.getValueAt(numeroFila, 0).toString());
                    this.interfaz.jDialogUnidad.dispose();
                } else {
                    JOptionPane.showMessageDialog(null, "Selecccione una unidad", "Unidad", JOptionPane.WARNING_MESSAGE);
                }
            }
            if (e.getSource() == this.interfaz.tablaMarca) {
                int numerofila = this.interfaz.tablaMarca.getSelectedRow();
                if (numerofila >= 0) {
                    this.interfaz.txtmarca.setText(this.interfaz.tablaMarca.getValueAt(numerofila, 0).toString());
                    this.interfaz.jDialogMarca.dispose();
                } else {
                    JOptionPane.showMessageDialog(null, "Seleccione una marca");
                }
            }
            if (e.getSource() == this.interfaz.tableLinea) {
                int numerofila = this.interfaz.tableLinea.getSelectedRow();
                if (numerofila >= 0) {
                    this.interfaz.txtlinea.setText(this.interfaz.tableLinea.getValueAt(numerofila, 0).toString());
                    this.interfaz.jDialogLinea.dispose();
                } else {
                    JOptionPane.showMessageDialog(null, "Seleccione una marca");
                }
            }
            if (e.getSource() == this.interfaz.tableUbicacion) {
                int numerofila = this.interfaz.tableUbicacion.getSelectedRow();
                if (numerofila >= 0) {
                    this.interfaz.txtubicacion.setText(this.interfaz.tableUbicacion.getValueAt(numerofila, 0).toString());
                    this.interfaz.jDialogUbicacion.dispose();
                } else {
                    JOptionPane.showMessageDialog(null, "Seleccione una marca");
                }
            }
            if (e.getSource() == this.interfaz.txtBuscarProveedor) {
                ControladorProveedor controllerProveedor = new ControladorProveedor();
                controllerProveedor.mtdBuscarProveedor(this.interfaz.jTableProveedorBuscar, this.interfaz.txtBuscarProveedor.getText(), this.interfaz.jRadioButtonActivoBuscar, this.interfaz.jRadioButtonInactivoBuscar, this.interfaz.jRadioButtonTodoBuscar, this.interfaz.jComboBoxBuscarproveedor);
            }
         //////////////Evento Busqueda articulo////////////////// 
        if (e.getSource() == this.interfaz.jComboBoxBuscar) {
            if (this.interfaz.jComboBoxBuscar.getSelectedIndex() ==0) {
                this.interfaz.jComboBoxDepartamento.setVisible(false);
                this.interfaz.txtBuscar.setVisible(true);
                this.interfaz.txtBuscar.setBounds(130, 10, 330, 27);
                this.interfaz.txtBuscar.grabFocus();
            }
            if (this.interfaz.jComboBoxBuscar.getSelectedIndex() == 1) {
                this.interfaz.jComboBoxDepartamento.setVisible(true);
                this.interfaz.txtBuscar.setVisible(false);
                this.interfaz.jComboBoxDepartamento.grabFocus();
            }
        }
        if (e.getSource() == this.interfaz.jComboBoxDepartamento) {
             this.mtdBuscarArticulo(this.interfaz.jtDatosArticulo, this.interfaz.txtBuscar, this.interfaz.jRadioButtonActivoBuscar, this.interfaz.jRadioButtonInactivoBuscar, this.interfaz.jRadioButtonTodoBuscar, this.interfaz.jComboBoxBuscar, this.interfaz.jComboBoxDepartamento, "venta");
        }
        //Termina evento Busqueda articulo
        }

    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == this.interfaz.txtpreciodelista) {
            float precio = Float.parseFloat(this.interfaz.txtpreciodelista.getText());
            mtdCalcularPrecio("utilidad", precio, 0, 1, this.interfaz.jRadioBtnUltimo, this.interfaz.jRadioBtbReposicion, this.interfaz.jRadionBtnPromedio);
        }
        if (e.getSource() == this.interfaz.txtprecio2) {
            float precio = Float.parseFloat(this.interfaz.txtprecio2.getText());
            mtdCalcularPrecio("utilidad", precio, 0, 2, this.interfaz.jRadioBtnUltimo, this.interfaz.jRadioBtbReposicion, this.interfaz.jRadionBtnPromedio);
        }
        if (e.getSource() == this.interfaz.txtprecio3) {
            float precio = Float.parseFloat(this.interfaz.txtprecio3.getText());
            mtdCalcularPrecio("utilidad", precio, 0, 3, this.interfaz.jRadioBtnUltimo, this.interfaz.jRadioBtbReposicion, this.interfaz.jRadionBtnPromedio);
        }
        if (e.getSource() == this.interfaz.txtporcientopreciodelista) {
            float porcentajeUtilidad = Float.parseFloat(this.interfaz.txtporcientopreciodelista.getText());
            mtdCalcularPrecio("precio", 0, porcentajeUtilidad, 1, this.interfaz.jRadioBtnUltimo, this.interfaz.jRadioBtbReposicion, this.interfaz.jRadionBtnPromedio);
        }
        if (e.getSource() == this.interfaz.txtporciento2) {
            float porcentajeUtilidad = Float.parseFloat(this.interfaz.txtporciento2.getText());
            mtdCalcularPrecio("precio", 0, porcentajeUtilidad, 2, this.interfaz.jRadioBtnUltimo, this.interfaz.jRadioBtbReposicion, this.interfaz.jRadionBtnPromedio);
        }
        if (e.getSource() == this.interfaz.txtporciento3) {
            float porcentajeUtilidad = Float.parseFloat(this.interfaz.txtporciento3.getText());
            mtdCalcularPrecio("precio", 0, porcentajeUtilidad, 3, this.interfaz.jRadioBtnUltimo, this.interfaz.jRadioBtbReposicion, this.interfaz.jRadionBtnPromedio);
        }
       
    }

    public void mtdRellenarTablaDepartamento() {
        instanceDepControlador.mtdRellenarTabla(this.interfaz.tableLinea, instanceDep);
    }

    public void mtdRellenarTablaMarca() {
        instanceMarControlador.mtdRellenarTabla(this.interfaz.tablaMarca, instanceMar);
    }

    public void mtdRellenarTablaUbicacion() {
        this.instanceUbiControlador.mtdRellenarTabla(this.interfaz.tableUbicacion, instanceUbi);
    }

    public void mtdRellenarTablaUnidad() {
        instanceUnidadControl.mtdLlenarTable(this.interfaz.tablaUnidad, instanceUnidad);
    }

    private void mtdCalcularPrecio(String tipo, float precio, float utilidad, int numeroUtilidad, JRadioButton ultimo, JRadioButton reposicion, JRadioButton promedio) {
        float costoUltimo = 0;
        if (ultimo.isSelected() == true) {
            costoUltimo = Float.parseFloat(this.interfaz.txtcostoultimo.getText());
        }
        if (reposicion.isSelected() == true) {
            costoUltimo = Float.parseFloat(this.interfaz.txtcostoreposion.getText());
        }
        if (promedio.isSelected() == true) {
            costoUltimo = Float.parseFloat(this.interfaz.txtcostopromedio.getText());
        }
        DecimalFormat df = new DecimalFormat("#.######");//
        if (costoUltimo > 0) {
            if (tipo.equals("precio")) {
                float formulaprecio = (costoUltimo) + (costoUltimo * (utilidad / 100));
                double precioRedondeado = roundToHalf(Double.parseDouble(df.format(formulaprecio)));
                if (numeroUtilidad == 1) {
                    this.interfaz.txtpreciodelista.setText(precioRedondeado + "");
                }
                if (numeroUtilidad == 2) {
                    this.interfaz.txtprecio2.setText(precioRedondeado + "");
                }
                if (numeroUtilidad == 3) {
                    this.interfaz.txtprecio3.setText(precioRedondeado + "");
                }
            }
            if (tipo.equals("utilidad")) {
                 float Formulautilidad = ((100 * precio) / costoUltimo) - 100;
                if (numeroUtilidad == 1) {
                    this.interfaz.txtporcientopreciodelista.setText(df.format(Formulautilidad)+"");
                }
                if (numeroUtilidad == 2) {
                    this.interfaz.txtporciento2.setText(df.format(Formulautilidad)+ "");
                }
                if (numeroUtilidad == 3) {
                    this.interfaz.txtporciento3.setText(df.format(Formulautilidad)+ "");
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, "El costo debe ser mayor a cero", "Erro al registrar precio de lista", JOptionPane.WARNING_MESSAGE);
        }
    }

    public static double roundToHalf(double d) {
        return Math.round(d * 6) / 2.0;
    }

    public void mtdBuscarArticulo(JTable table, JTextField txtBuscar, JRadioButton activoB, JRadioButton inactivoB, JRadioButton todoB, JComboBox comboBuscar, JComboBox comboDepartamento, String movimiento) {
        String filtro = "";
        String status = "";
         if(txtBuscar.getText().length()<=1 && comboBuscar.getSelectedIndex() == 0){
            JOptionPane.showMessageDialog(null,"Ingresar una descripcion de articulo a buscar");
            txtBuscar.grabFocus();
        }else{
        //INI JRadioButton 
        if (activoB.isSelected() == true) {
            status = "1,2";
        }
        if (inactivoB.isSelected() == true) {
            status = "0";
        }
        if (todoB.isSelected() == true) {
            status = "0,1,2";
        }
        if (comboBuscar.getSelectedIndex() == 0) {
            filtro = "descripcion";
            this.instance.setNombre(txtBuscar.getText());
        }
        if (comboBuscar.getSelectedIndex() == 1) {
            filtro = "departamento";
            Departamento dp = (Departamento) comboDepartamento.getSelectedItem();
            this.instance.setDepartamento(dp);
        }
        mtdRellenarDatosTablaArticulo(table, instance, status, filtro, movimiento);
        }
    }
    public void mtdBuscarServicios(JTable table, String movimiento) {
        String status = "1,2";
        String filtro = "ventaServicio";
        mtdRellenarDatosTablaArticulo(table, instance, status, filtro, movimiento);

    }
}
