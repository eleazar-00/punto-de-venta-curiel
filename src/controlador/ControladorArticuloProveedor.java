/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import modelo.Articulo;
import modelo.ArticuloProveedor;
import modelo.ArticuloProveedorDAO;
import modelo.Proveedor;

/**
 *
 * @author VISORUS SOFT
 */
public class ControladorArticuloProveedor {
    ArticuloProveedor instances = new ArticuloProveedor();
    ArticuloProveedorDAO instanceDAO = new ArticuloProveedorDAO();
    Proveedor instancePV = new Proveedor();
    DefaultTableModel modeloT = new DefaultTableModel();
    
    public ControladorArticuloProveedor() {
    }
    
    public void mtdCabeceraTabla(JTable tableD) {
        tableD.setModel(modeloT);
        //nombre de la cabecera de la tabla
        modeloT.addColumn("CLAVE");
        modeloT.addColumn("PROVEEDOR");
        modeloT.addColumn("PLAZO DE ENTREGA");
        modeloT.addColumn("PRIORIDAD");
        modeloT.addColumn("ULTIMO COSTO");
        modeloT.addColumn("ULTIMA COMPRA");
    }
    
    public void mtdListaProveedorBD(JTable tableD,ArticuloProveedor instance) {
        this.mtdLimpiarTablaProveedor(tableD);
        Object[] columns = new Object[6];
        int numeroFilas = this.instanceDAO.mtdLista(instance).size();
        for (int i = 0; i < numeroFilas; i++) {
            columns[0] = this.instanceDAO.mtdLista(instance).get(i).getProveedor().getClave();
            columns[1] = this.instanceDAO.mtdLista(instance).get(i).getNombreProveedor();
            columns[2] = this.instanceDAO.mtdLista(instance).get(i).getPlazoEntrega();
            columns[3] = this.instanceDAO.mtdLista(instance).get(i).getPrioridad();
            columns[4] = this.instanceDAO.mtdLista(instance).get(i).getCostoUltimoArticulo();
            columns[5] = this.instanceDAO.mtdLista(instance).get(i).getFechaUltimacompra();
            modeloT.addRow(columns);
        }
    }
    protected void mtdLimpiarTablaProveedor(JTable tableD) {
        int numeroFila = tableD.getRowCount();
        modeloT = (DefaultTableModel) tableD.getModel();
        for (int j = 0; j < numeroFila; j++) {
            modeloT.removeRow(0);
        }
    }
    protected void mtdBtnEliminarProveedor(JTable tableD) {
        int numeroFila = tableD.getSelectedRow();
        if (numeroFila >= 0) {
            int rptUsuario = JOptionPane.showConfirmDialog(null, "Eliminar el Proveedor seleccionado");
            if (rptUsuario == 0) {
                int fila = tableD.getSelectedRow();
                modeloT = (DefaultTableModel) tableD.getModel();
                modeloT.removeRow(fila);
            }
        } else {
            JOptionPane.showMessageDialog(null, "seleccione el Proveedor");
        }
    }
    protected void mtdInsertProveedor(JTable tableD, ArticuloProveedor instance) {
        int cantidadFilas = tableD.getRowCount();
        for (int i = 0; i < cantidadFilas; i++) {
            this.instancePV.setClave(Integer.parseInt(tableD.getValueAt(i, 0).toString()));
            instance.setProveedor(instancePV);
            instance.setNombreProveedor(tableD.getValueAt(i, 1).toString());
            instance.setPlazoEntrega(Integer.parseInt(tableD.getValueAt(i, 2).toString()));
            instance.setPrioridad(Integer.parseInt(tableD.getValueAt(i, 3).toString()));
            instance.setCostoUltimoArticulo(Float.parseFloat(tableD.getValueAt(i, 4).toString()));
            instance.setFechaUltimacompra("00/00/0000");
            this.instanceDAO.mtdRegistrar(instance);
        }
        this.mtdLimpiarTablaProveedor(tableD);
    }
    protected void mtdEliminarProveedorDB(Articulo instance) {
         this.instanceDAO.mtdEliminar(instance);
    } 

}
