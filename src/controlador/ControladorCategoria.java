/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.ResultSet;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import modelo.Categoria;
import modelo.CategoriaDAO;
import vista.JFICategoria;

/**
 *
 * @author ELEAZAR
 */
public class ControladorCategoria implements ActionListener, KeyListener {

    JFICategoria interfaz = new JFICategoria();
    CategoriaDAO instanceDAO = new CategoriaDAO();
    Categoria instance=new Categoria();
    public ControladorCategoria(JFICategoria interfaz, CategoriaDAO instanceDAO) {
        this.interfaz = interfaz;
        this.instanceDAO = instanceDAO;        
        this.interfaz.btnNuevo.addActionListener(this);
        this.interfaz.btnEditar.addActionListener(this);
        this.interfaz.btnAceptar.addActionListener(this);
        this.interfaz.btnActualizar.addActionListener(this);
        this.interfaz.btnCancelar.addActionListener(this);
        this.interfaz.btnEliminar.addActionListener(this);
        this.interfaz.btnSalir.addActionListener(this);
        
        
         mtdRellenarTable(interfaz.jtDatos);
        this.interfaz.jPanelRegistrar.setVisible(false);
    }

    public void mtdRellenarTable(JTable tableD) {
        DefaultTableModel model = new DefaultTableModel();
        tableD.setModel(model);
        model.addColumn("CLAVE CATEGORIA");
        model.addColumn("NOMBRE CATEGORIA");
        model.addColumn("DESCRIPCION");
        model.addColumn("DEPARTAMENTO");
        Object[] columna = new Object[4];
        int numRegistro = instanceDAO.listaCategoria(instance).size();
        for (int i = 0; i < numRegistro; i++) {
            columna[0] = instanceDAO.listaCategoria(instance).get(i).getClaveCategoria();
            columna[1] = instanceDAO.listaCategoria(instance).get(i).getNombreCategoria();
            columna[2] = instanceDAO.listaCategoria(instance).get(i).getDescripcionCategoria();
            model.addRow(columna);
        }
    }

    public void mtdActivarBtn() {
        this.interfaz.btnNuevo.setEnabled(true);
        this.interfaz.btnEditar.setEnabled(true);
        this.interfaz.btnEliminar.setEnabled(true);
    }

    public void mtdDesativarBtn() {
        this.interfaz.btnNuevo.setEnabled(false);
        this.interfaz.btnEditar.setEnabled(false);
        this.interfaz.btnEliminar.setEnabled(false);
    }

    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == interfaz.btnSalir) {
            interfaz.dispose();
        }
        if (e.getSource() == interfaz.btnNuevo) {
            mtdDesativarBtn();
            this.interfaz.jPanelRegistrar.setVisible(true);
            this.interfaz.btnActualizar.setEnabled(false);
            this.interfaz.btnAceptar.setEnabled(true);
            this.interfaz.txtClave.setEnabled(true);
            this.interfaz.txtClave.grabFocus();
        }
        if (e.getSource() == interfaz.btnCancelar) {
            this.interfaz.txtClave.setText("");
            this.interfaz.txtDescripcion.setText("");
            this.interfaz.txtNombre.setText("");
            this.interfaz.jPanelRegistrar.setVisible(false);
            mtdActivarBtn();
        }
        if (e.getSource() == interfaz.btnAceptar) {
            String clave = interfaz.txtClave.getText();
            String nombre = interfaz.txtNombre.getText();
            if (clave.length() <= 1) {
                JOptionPane.showMessageDialog(null, "Campo Clave vacio", "Error", JOptionPane.ERROR_MESSAGE);
                this.interfaz.txtClave.grabFocus();
            } else if (nombre.length() <= 3) {
                JOptionPane.showMessageDialog(null, "Campo nombre  vacio", "Error", JOptionPane.ERROR_MESSAGE);
                  this.interfaz.txtNombre.grabFocus();
            } else {
                String descripcion = interfaz.txtDescripcion.getText();
                String departamento = String.valueOf(interfaz.jcDepartamento.getSelectedItem());
                String respuesta = this.instanceDAO.mtdRegristar(instance);
                this.interfaz.txtClave.setText("");
                this.interfaz.txtDescripcion.setText("");
                this.interfaz.txtNombre.setText("");
                mtdRellenarTable(interfaz.jtDatos);
                JOptionPane.showMessageDialog(null, respuesta);
            }

        }
        if (e.getSource() == interfaz.btnEliminar) {
            int numeroFila = interfaz.jtDatos.getSelectedRow();
            String clave = "";
            if (numeroFila >= 0) {
                clave = String.valueOf(interfaz.jtDatos.getValueAt(numeroFila, 0));
                int rptUsuario = JOptionPane.showConfirmDialog(null, "Desea Eliminar la fila Seleccionada");
                if (rptUsuario == 0) {
                    this.instanceDAO.mtdELiminar(instance);
                    mtdRellenarTable(interfaz.jtDatos);

                }
            } else {
                JOptionPane.showMessageDialog(null, "Seleccione un fila");
            }
        }
        if (e.getSource() == interfaz.btnEditar) {
            int numerofila = interfaz.jtDatos.getSelectedRow();
            if (numerofila >= 0) {
                this.interfaz.jPanelRegistrar.setVisible(true);
                this.interfaz.txtNombre.grabFocus();
                this.interfaz.txtClave.setText(String.valueOf(this.interfaz.jtDatos.getValueAt(numerofila, 0)));
                this.interfaz.txtNombre.setText(String.valueOf(interfaz.jtDatos.getValueAt(numerofila, 1)));
                this.interfaz.txtDescripcion.setText(String.valueOf(this.interfaz.jtDatos.getValueAt(numerofila, 2)));
                this.interfaz.jcDepartamento.setSelectedIndex(0);
                this.interfaz.jcDepartamento.setSelectedItem(this.interfaz.jtDatos.getValueAt(numerofila, 3));
                this.interfaz.txtClave.setEnabled(false);
                this.interfaz.btnAceptar.setEnabled(false);
                mtdDesativarBtn();
                this.interfaz.btnActualizar.setEnabled(true);

            } else {
                JOptionPane.showMessageDialog(null, "Seleccione una fila");
            }

        }
        if (e.getSource() == interfaz.btnActualizar) {
            String clave = interfaz.txtClave.getText();
            String nombre = interfaz.txtNombre.getText();
            String descripcion = interfaz.txtDescripcion.getText();
            String departamento = String.valueOf(interfaz.jcDepartamento.getSelectedItem());
            int respuesta = this.instanceDAO.mtdActualizar(instance);
            if (respuesta == 1) {
                JOptionPane.showMessageDialog(null, "Actualizado Correctamente");
                this.interfaz.txtClave.setText("");
                this.interfaz.txtDescripcion.setText("");
                this.interfaz.txtNombre.setText("");
                mtdActivarBtn();
                mtdRellenarTable(interfaz.jtDatos);
                this.interfaz.jPanelRegistrar.setVisible(false);
            } else {
                JOptionPane.showMessageDialog(null, "Error desconocido" + respuesta, "Error", JOptionPane.ERROR_MESSAGE);

            }

        }

    }

    @Override
    public void keyTyped(KeyEvent e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void keyPressed(KeyEvent e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void keyReleased(KeyEvent e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
