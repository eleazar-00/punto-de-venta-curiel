/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import modelo.Cliente;
import modelo.ClienteDAO;
import modelo.TipoAgente;
import modelo.TipoAgenteDAO;
import vista.JFICliente;

/**
 *
 * @author eleazar
 */
public class ControladorCliente implements ActionListener, KeyListener {

    Cliente instance = new Cliente();
    JFICliente interfaz = new JFICliente();
    ClienteDAO instancesDAO = new ClienteDAO();
    DefaultTableModel modeloT = new DefaultTableModel();

    TipoAgenteDAO instanceTipoAgenteDAO = new TipoAgenteDAO();
    ControladorTipoAgente controllerTipoAgente = new ControladorTipoAgente();
    DefaultTableModel modeloTTA = new DefaultTableModel();

    public ControladorCliente(JFICliente interfaz, ClienteDAO instance) {
        this.interfaz = interfaz;
        this.instancesDAO = instance;
        this.interfaz.btnNuevo.addActionListener(this);
        this.interfaz.btnEditar.addActionListener(this);
        this.interfaz.btnAceptar.addActionListener(this);
        this.interfaz.btnActiualizar.addActionListener(this);
        this.interfaz.btncancelar.addActionListener(this);
        this.interfaz.btnEliminar.addActionListener(this);
        this.interfaz.btnSalir.addActionListener(this);
        this.interfaz.btnClaveTipo.addActionListener(this);

        this.interfaz.txtBuscar.addKeyListener(this);
        this.interfaz.btnNuevo.addKeyListener(this);
        this.interfaz.btnEditar.addKeyListener(this);
        this.interfaz.btnAceptar.addKeyListener(this);
        this.interfaz.btnActiualizar.addKeyListener(this);
        this.interfaz.btncancelar.addKeyListener(this);
        this.interfaz.btnEliminar.addKeyListener(this);
        this.interfaz.btnSalir.addKeyListener(this);
        this.interfaz.jTDatos.addKeyListener(this);
        this.interfaz.jTableTipoA.addKeyListener(this);

        mtdCabecera(this.interfaz.jTDatos);
        mtdIniciar();
        mtdRellenarCabeceraTipo();
    }

    public ControladorCliente() {
    }

    public void mtdCabecera(JTable tabla) {
        tabla.setModel(modeloT);
        modeloT.addColumn("Clave");
        modeloT.addColumn("Nombre");
        modeloT.addColumn("Tipo");
    }

    private void mtdRellenarCabeceraTipo() {
        this.interfaz.jTableTipoA.setModel(modeloTTA);
        //nombre de la cabecera de la tabla
        modeloTTA.addColumn("CLAVE TIPO");
        modeloTTA.addColumn("NOMBRE TIPO");
        controllerTipoAgente.mtdRellenarTabla(this.interfaz.jTableTipoA, "C");
    }

    public void mtdLimparTable(JTable tabla) {
        int numeroFila = tabla.getRowCount();
        modeloT = (DefaultTableModel) tabla.getModel();
        for (int j = 0; j < numeroFila; j++) {
            modeloT.removeRow(0);
        }
    }

    public void mtdRellenarTabla(JTable tabla, Cliente instance, String status, String filtro) {
        mtdLimparTable(tabla);
        Object[] columna = new Object[3];
        int numeroFila = instancesDAO.mtdList(instance, status, filtro).size();
        for (int i = 0; i < numeroFila; i++) {
            columna[0] = this.instancesDAO.mtdList(instance, status, filtro).get(i).getClave();
            columna[1] = this.instancesDAO.mtdList(instance, status, filtro).get(i).getNombre();
            columna[2] = this.instancesDAO.mtdList(instance, status, filtro).get(i).getTipoAgente().getNombre();
            modeloT.addRow(columna);
        }
    }

    public void mtdIniciar() {
        this.interfaz.jrbActivoB.setSelected(true);
        this.interfaz.btnNuevo.setVisible(true);
        this.interfaz.btnEditar.setVisible(true);
        this.interfaz.btnAceptar.setVisible(false);
        this.interfaz.btnActiualizar.setVisible(false);
        this.interfaz.btncancelar.setVisible(false);
        this.interfaz.btnEliminar.setVisible(true);
        this.interfaz.btnSalir.setVisible(true);
        this.interfaz.jTabbedPaneCliente.setEnabledAt(1, false);
        this.interfaz.jTabbedPaneCliente.setEnabledAt(0, true);
        this.interfaz.jTabbedPaneCliente.setSelectedIndex(0);
    }

    public void mtdCacharTXt() {
        try {
            instance.setClave(Integer.parseInt(this.interfaz.txtClave.getText()));
            if (interfaz.jrbActivo.isSelected() == true) {
                instance.setStatus(1);
            }
            if (interfaz.jrbInactivo.isSelected() == true) {
                instance.setStatus(0);
            }
            instance.setNombre(interfaz.txtNombre.getText());
            instance.setRfc(this.interfaz.txtrfc.getText());
            TipoAgente instanceTA=new TipoAgente();
            instanceTA.setClave(this.interfaz.txtClaveTipo.getText());
            instance.setTipoAgente(instanceTA);
            instance.setPais(this.interfaz.txtPais.getText());
            instance.setEstado(this.interfaz.txtEstado.getText());
            instance.setPoblacion(this.interfaz.txtPoblacion.getText());
            instance.setColonia(this.interfaz.txtColonia.getText());
            instance.setCalle(interfaz.txtCalle.getText());
            instance.setCodigoPostal(interfaz.txtCodigoPostal.getText());
            instance.setCelular(interfaz.txtCelular.getText());
            instance.setTelefono(interfaz.txtTelefono.getText());
            instance.setCorreo(this.interfaz.txtCorreo.getText());
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error"+e, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void mtdLimpiaraTxt() {
        this.interfaz.txtClave.setText("");
        this.interfaz.jrbActivo.setSelected(true);
        this.interfaz.jrbInactivo.setSelected(false);
        this.interfaz.txtNombre.setText("");
        this.interfaz.txtrfc.setText("");
        this.interfaz.txtClaveTipo.setText("0");
        this.interfaz.txtPais.setText("");
        this.interfaz.txtEstado.setText("");
        this.interfaz.txtPoblacion.setText("");
        this.interfaz.txtTelefono.setText("");
        this.interfaz.txtCelular.setText("");
        this.interfaz.txtCorreo.setText("");
        this.interfaz.txtColonia.setText("");
        this.interfaz.txtCodigoPostal.setText("");
        this.interfaz.txtCalle.setText("");
    }

    public void mtdNuevoEditar(String btn) {
        this.interfaz.btncancelar.setVisible(true);
        this.interfaz.btnNuevo.setVisible(false);
        this.interfaz.btnEditar.setVisible(false);
        this.interfaz.btnEliminar.setVisible(false);
        this.interfaz.btnSalir.setVisible(false);
        this.interfaz.jTabbedPaneCliente.setEnabledAt(1, true);
        this.interfaz.jTabbedPaneCliente.setEnabledAt(0, false);
        this.interfaz.jTabbedPaneCliente.setSelectedIndex(1);
        this.interfaz.txtNombre.grabFocus();
        if (btn == "Nuevo") {
            this.interfaz.btnAceptar.setVisible(true);//solo btn nuevo
            this.interfaz.jrbActivo.setSelected(true);
            this.interfaz.jrbInactivo.setSelected(false);
            this.interfaz.jTabbedPaneCliente.setTitleAt(1, "Nuevo");//solo btn nuevo
            int utm = instancesDAO.mtdUltimoIDdeMovimiento();
            this.interfaz.txtClave.setEditable(false);
            this.interfaz.txtClave.setText((utm + 1) + "");
        }
        if (btn == "Editar") {
            this.interfaz.btnActiualizar.setVisible(true);
            this.interfaz.txtClave.setEditable(false);
            this.interfaz.jTabbedPaneCliente.setTitleAt(1, "Editar");
        }
    }

    public void btnEditar() {
        String status = "0,1";
        String filtro = "clave";
        int numeroFila = this.interfaz.jTDatos.getSelectedRow();
        if (numeroFila >= 0) {
            this.instance.setClave(Integer.parseInt(this.interfaz.jTDatos.getValueAt(numeroFila, 0).toString()));
            this.interfaz.txtClave.setText(String.valueOf(this.instancesDAO.mtdList(instance, status, filtro).get(0).getClave()));
            int statu = Integer.parseInt(String.valueOf(this.instancesDAO.mtdList(instance, status, filtro).get(0).getStatus()));
            if (statu == 0) {
                this.interfaz.jrbActivo.setSelected(false);
                this.interfaz.jrbInactivo.setSelected(true);
            }
            if (statu == 1) {
                this.interfaz.jrbActivo.setSelected(true);
                this.interfaz.jrbInactivo.setSelected(false);
            }
            this.interfaz.txtNombre.setText(String.valueOf(this.instancesDAO.mtdList(instance, status, filtro).get(0).getNombre()));
            this.interfaz.txtrfc.setText(String.valueOf(this.instancesDAO.mtdList(instance, status, filtro).get(0).getRfc()));
            this.interfaz.txtClaveTipo.setText(this.instancesDAO.mtdList(instance, status, filtro).get(0).getTipoAgente().getClave());
            this.interfaz.txtPais.setText(String.valueOf(this.instancesDAO.mtdList(instance, status, filtro).get(0).getPais()));
            this.interfaz.txtEstado.setText(String.valueOf(this.instancesDAO.mtdList(instance, status, filtro).get(0).getEstado()));
            this.interfaz.txtPoblacion.setText(String.valueOf(this.instancesDAO.mtdList(instance, status, filtro).get(0).getPoblacion()));
            this.interfaz.txtColonia.setText(String.valueOf(this.instancesDAO.mtdList(instance, status, filtro).get(0).getColonia()));
            this.interfaz.txtCalle.setText(String.valueOf(this.instancesDAO.mtdList(instance, status, filtro).get(0).getCalle()));
            this.interfaz.txtCodigoPostal.setText(String.valueOf(this.instancesDAO.mtdList(instance, status, filtro).get(0).getCodigoPostal()));
            this.interfaz.txtCelular.setText(String.valueOf(this.instancesDAO.mtdList(instance, status, filtro).get(0).getCelular()));
            this.interfaz.txtTelefono.setText(String.valueOf(this.instancesDAO.mtdList(instance, status, filtro).get(0).getTelefono()));
            this.interfaz.txtCorreo.setText(String.valueOf(this.instancesDAO.mtdList(instance, status, filtro).get(0).getCorreo()));
            mtdNuevoEditar("Editar");
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione una fila");
        }
    }

    public void btnAceptar() {
        String nombre = this.interfaz.txtNombre.getText();
        if (!nombre.isEmpty()) {
            if(!this.interfaz.txtrfc.getText().isEmpty()){
             mtdCacharTXt();
            String respuesta = this.instancesDAO.mtdInsertar(instance);
            if (respuesta.equals("Registrado correctamente")) {
                mtdLimpiaraTxt();
                int utm = instancesDAO.mtdUltimoIDdeMovimiento();
                this.interfaz.txtClave.setText((utm + 1) + "");
            }
            JOptionPane.showMessageDialog(null, respuesta);
            }else{
                JOptionPane.showMessageDialog(null, "Campo RFC es Obligatorio!!!", "Error", JOptionPane.ERROR_MESSAGE);
                this.interfaz.txtrfc.grabFocus();
            }
        } else {
            JOptionPane.showMessageDialog(null, "Campo Nombre o Razón Social es Obligatorio!!!", "Error", JOptionPane.ERROR_MESSAGE);
            this.interfaz.txtNombre.grabFocus();
        }
    }

    public void btnActualizar() {
        String nombre = this.interfaz.txtNombre.getText();
        if (nombre.length() > 3) {
            this.instance.setClave(Integer.parseInt(this.interfaz.txtClave.getText()));
            mtdCacharTXt();
            int rpt = this.instancesDAO.mtdActualizar(instance);
            if (rpt == 1) {
                JOptionPane.showMessageDialog(null, "Actualizado Correctamente");
                mtdLimpiaraTxt();
                mtdIniciar();
                this.interfaz.txtBuscar.grabFocus();
            }
        } else {
            JOptionPane.showMessageDialog(null, "Campo Nombre Vasio", "Error", JOptionPane.ERROR_MESSAGE);
            this.interfaz.txtNombre.grabFocus();
        }
    }
    
    

    public void btnEliminar() {
        int numeroFila = this.interfaz.jTDatos.getSelectedRow();
        if (numeroFila >= 0) {
            this.instance.setClave(Integer.parseInt(this.interfaz.jTDatos.getValueAt(numeroFila, 0).toString()));
            int rptU = JOptionPane.showConfirmDialog(null, "Esta seguro de eliminar la fila seleccionada");
            if (rptU == 0) {
                this.instancesDAO.mtdEliminar(instance);
            }
            this.interfaz.txtBuscar.grabFocus();
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione una fila");
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.interfaz.btnNuevo) {
            mtdNuevoEditar("Nuevo");

        }
        if (e.getSource() == this.interfaz.btnEditar) {
            btnEditar();
        }

        if (e.getSource() == this.interfaz.btnAceptar) {
            btnAceptar();
        }
        if (e.getSource() == this.interfaz.btnActiualizar) {
            btnActualizar();
        }
        if (e.getSource() == this.interfaz.btncancelar) {
            mtdLimpiaraTxt();
            mtdIniciar();
            this.interfaz.txtBuscar.grabFocus();
        }
        if (e.getSource() == this.interfaz.btnEliminar) {
            btnEliminar();
        }
        if (e.getSource() == this.interfaz.btnSalir) {
            this.interfaz.dispose();
        }
        if (e.getSource() == this.interfaz.btnClaveTipo) {
            this.interfaz.jDialogTipo.setSize(300, 250);
            this.interfaz.jDialogTipo.setLocation(this.interfaz.btnClaveTipo.getLocation().x, this.interfaz.btnClaveTipo.getLocation().y);
            this.interfaz.jDialogTipo.setVisible(true);
        }

    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            if (e.getSource() == this.interfaz.txtBuscar) {
               this.mtdBuscarCliente(this.interfaz.jTDatos,this.interfaz.txtBuscar.getText(),this.interfaz.jrbActivoB,this.interfaz.jrbInactivoB,this.interfaz.jrbTodosB,this.interfaz.jComboBoxBuscar);
            }

            if (e.getSource() == this.interfaz.jTDatos) {
                btnEditar();

            }
            if (e.getSource() == this.interfaz.btnNuevo) {
                mtdNuevoEditar("Nuevo");
            }
            if (e.getSource() == this.interfaz.btnEditar) {
                btnEditar();
            }
            if (e.getSource() == this.interfaz.btnAceptar) {
                btnAceptar();
            }
            if (e.getSource() == this.interfaz.btnActiualizar) {
                btnActualizar();
            }
            if (e.getSource() == this.interfaz.btncancelar) {
                mtdLimpiaraTxt();
                mtdIniciar();
                this.interfaz.txtBuscar.grabFocus();
            }
            if (e.getSource() == this.interfaz.btnEliminar) {
                btnEliminar();
            }
            if (e.getSource() == this.interfaz.btnSalir) {
                this.interfaz.dispose();
            }
            if (e.getSource() == this.interfaz.jTableTipoA) {
                int numeroFila = this.interfaz.jTableTipoA.getSelectedRow();
                if (numeroFila >= 0) {
                    this.interfaz.txtClaveTipo.setText(this.interfaz.jTableTipoA.getValueAt(numeroFila, 0).toString());
                    this.interfaz.jDialogTipo.dispose();
                } else {
                    JOptionPane.showMessageDialog(null, "Selecccione una unidad", "Unidad", JOptionPane.WARNING_MESSAGE);
                }
            }
        }
        if (e.getKeyCode() == KeyEvent.VK_DELETE) {
            if (e.getSource() == this.interfaz.jTDatos) {
                btnEliminar();
            }
        }
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            this.interfaz.dispose();
        }
        if (e.getKeyCode() == KeyEvent.VK_CONTROL) {
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
    public void mtdBuscarCliente(JTable table,String buscar,JRadioButton activoB,JRadioButton inactivoB,JRadioButton todoB,JComboBox combo){
                 String filtro = "";
                String status = "";
                if (activoB.isSelected() == true) {
                    status = "1";
                }
                if (inactivoB.isSelected() == true) {
                    status = "0";
                }
                if (todoB.isSelected() == true) {
                    status = "0,1";
                }
                if (combo.getSelectedIndex() == 0) {
                    filtro = "todo";
                }
                if (combo.getSelectedIndex() == 1) {
                    filtro = "clave";
                    if (buscar.length() > 0) {
                        int clave = Integer.parseInt(buscar);
                        if (clave > 0) {
                            this.instance.setClave(clave);
                        } else {
                            JOptionPane.showMessageDialog(null, "Ingrese una clave valida", "Advertencia", JOptionPane.WARNING_MESSAGE);
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Ingrese una clave ", "Advertencia", JOptionPane.WARNING_MESSAGE);
                    }
                }
                if (combo.getSelectedIndex() == 2) {
                    filtro = "razon";
                     if (buscar.length() > 0) {
                        this.instance.setNombre(buscar);
                    } else {
                        JOptionPane.showMessageDialog(null, "Ingrese una Razón Social valida", "Advertencia", JOptionPane.WARNING_MESSAGE);
                    }
                }
                mtdRellenarTabla(table, instance, status, filtro);
    }
}
