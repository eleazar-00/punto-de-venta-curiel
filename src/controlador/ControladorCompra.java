/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import modelo.Almacen;
import modelo.AlmacenDAO;
import modelo.Articulo;
import modelo.ArticuloDAO;
import modelo.Departamento;
import modelo.DepartamentoDAO;
import modelo.DetalleMovimiento;
import modelo.DetalleMovimientoDAO;
import modelo.EntradaySalida;
import modelo.EntradaySalidaDAO;
import modelo.ExistenciaAlmacen;
import modelo.ExistenciaAlmacenDAO;
import modelo.Movimiento;
import modelo.MovimientoDAO;
import modelo.SeriesyFolios;
import modelo.SeriesyFoliosDAO;
import modelo.TipoPago;
import modelo.TipoPagoDAO;
import vista.JFICompraDirecta;

/**
 *
 * @author ELEAZAR
 */
public class ControladorCompra implements ActionListener, KeyListener {

    JFICompraDirecta interfaz = new JFICompraDirecta();

    Almacen instanceAlmacen = new Almacen();
    AlmacenDAO instanceAlmacenDAO = new AlmacenDAO();

    TipoPago instanceTipopago = new TipoPago();
    TipoPagoDAO instanceTIPODAO = new TipoPagoDAO();

    SeriesyFolios instanceSerie = new SeriesyFolios();
    SeriesyFoliosDAO instanceSFDAO = new SeriesyFoliosDAO();

    EntradaySalida instaceEys = new EntradaySalida();
    EntradaySalidaDAO instaceEysDAO = new EntradaySalidaDAO();
    
    Departamento instanceDepartamento = new Departamento();
    DepartamentoDAO instanceDepartamentoDAO = new DepartamentoDAO();
    
    Movimiento movimientoInstance = new Movimiento();
    ControladorMovimiento instanceControladorMov = new ControladorMovimiento();
    MovimientoDAO movimientoDAO = new MovimientoDAO();

    MetodoMovimiento mtdMovimiento = new MetodoMovimiento();

    DetalleMovimiento detmovimientoInstance = new DetalleMovimiento();
    DetalleMovimientoDAO DetalleDB = new DetalleMovimientoDAO();

    ExistenciaAlmacen instacenExistencia = new ExistenciaAlmacen();
    ExistenciaAlmacenDAO instanceExistenciaDAO = new ExistenciaAlmacenDAO();

    Articulo instanceArticulo = new Articulo();
    ArticuloDAO instanceArticuloDAO = new ArticuloDAO();
    ControladorArticulo instanceControladorArticulo = new ControladorArticulo();
    ControladorReporteCompra controladorReporteCompra = new ControladorReporteCompra();
    ValidarCampos validar=new ValidarCampos();

    DefaultTableModel modeloT = new DefaultTableModel();
    int claveEmpleado;
    String Almacenes;
    String Series;

    public ControladorCompra(JFICompraDirecta interfaz, int claveEmpleado, String Almacenes, String Series) {
        this.interfaz = interfaz;
        this.claveEmpleado = claveEmpleado;
        this.Almacenes = Almacenes;
        this.Series = Series;
        this.interfaz.txtBuscarCodigoBarrasNombre.addActionListener(this);
        this.interfaz.jTableProveedorBuscar.addKeyListener(this);
        this.interfaz.txtClaveProveedor.addKeyListener(this);
        this.interfaz.btnBuscarProveedor.addActionListener(this);
        this.interfaz.txtBuscarProveedor.addKeyListener(this);
        this.interfaz.btnAgregarProveedor.addActionListener(this);
        this.interfaz.btnDSalir.addActionListener(this);
        this.interfaz.jComboBoxBuscar.addKeyListener(this);

//      Menu Buscar articulo
        this.interfaz.jComboBoxBuscarArticulo.addKeyListener(this);
        this.interfaz.jComboBoxDepartamentos.addKeyListener(this);
        this.interfaz.txtBuscarArticulo.addKeyListener(this);
        this.interfaz.btnAgregarArticulo.addActionListener(this);
        this.interfaz.jtBuscarArticulo.addKeyListener(this);
        this.interfaz.btnCancelarBuscarArticulo.addActionListener(this);

        //btn menus
        this.interfaz.btnsiguiente.addActionListener(this);
        this.interfaz.btnatras.addActionListener(this);
        this.interfaz.btnSalir.addActionListener(this);
        this.interfaz.btnEliminarArticulo.addActionListener(this);
        this.interfaz.btnBuscarArticulo.addActionListener(this);
        this.interfaz.btnNuevaCompra.addActionListener(this);
        this.interfaz.btnGuardarVenta.addActionListener(this);
        this.interfaz.btnGrabarVenta.addActionListener(this);

        //btn cobro
        this.interfaz.txtRecibi.addKeyListener(this);
        this.interfaz.btnAplicar.addActionListener(this);
        this.interfaz.btnCancelarCobro.addActionListener(this);
        //Buscar Nota
        this.interfaz.btnImprimirNota.addActionListener(this);
        this.interfaz.btnNota.addActionListener(this);
        this.interfaz.btnBuscarMovimientos.addActionListener(this);
        this.interfaz.jTableNotaBuscar.addKeyListener(this);
        this.interfaz.btncancelarcompra.addActionListener(this);
        //JtemMenus 
        this.interfaz.jMenuItemSiguiente.addActionListener(this);
        this.interfaz.jMenuItemAtras.addActionListener(this);
        this.interfaz.jMenuItemNuevo.addActionListener(this);
        this.interfaz.jMenuItemEliminar.addActionListener(this);
        this.interfaz.jMenuItemBuscarArticulo.addActionListener(this);
        this.interfaz.jMenuItemAplicar.addActionListener(this);
        this.interfaz.jMenuItemGuardar.addActionListener(this);
        this.interfaz.jMenuItemAceptarCobro.addActionListener(this);
        this.interfaz.jMenuItemCancelarCobro.addActionListener(this);
        this.interfaz.jMenuItemSalir.addActionListener(this);

        mtdCargarComboAlmacen();
        mtdCargarComboSerie();
        mtdCargarComboTipoPago();
        mtdCargarComboDepartamento();
        mdtHora();
        mtdIniciar();
    }

    public ControladorCompra() {
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        // Evento proveedor//
        if (e.getSource() == this.interfaz.btnBuscarProveedor) {
            mtdBtnBuscarProveedor();
        }
        if (e.getSource() == this.interfaz.btnAgregarProveedor) {
            mtdAgregarProveedor();
        }
        if (e.getSource() == this.interfaz.btnDSalir) {
            this.interfaz.jDBuscarProveedor.dispose();
            this.interfaz.txtBuscarProveedor.setText("");
        }
        /////Evento artiuclo/////////////////////////////////////
        if (e.getSource() == this.interfaz.txtBuscarCodigoBarrasNombre) {
            this.mtdBuscarArticulo(this.interfaz.txtBuscarCodigoBarrasNombre.getText());
        }
        if (e.getSource() == this.interfaz.btnCancelarBuscarArticulo) {
            this.interfaz.jdBuscarArticulo.dispose();
            this.interfaz.txtBuscarCodigoBarrasNombre.setText("");
            this.interfaz.txtBuscarCodigoBarrasNombre.grabFocus();
        }
        if (e.getSource() == this.interfaz.btnAgregarArticulo) {
            mtdAgregarArticulo();
        }

        if (e.getSource() == this.interfaz.btnGrabarVenta || e.getSource() == this.interfaz.jMenuItemAceptarCobro) {
            mtdGrabar(1);
        }
        if (e.getSource() == this.interfaz.btnGuardarVenta || e.getSource() == this.interfaz.jMenuItemGuardar) {
            mtdGrabar(0);
        }
        if (e.getSource() == this.interfaz.btnSalir || e.getSource() == this.interfaz.jMenuItemSalir) {
            salir();
        }
        if (e.getSource() == this.interfaz.btnsiguiente || e.getSource() == this.interfaz.jMenuItemSiguiente) {
            if (this.interfaz.txtnombreProveedor.getText().toString().length() > 0) {
                if(this.interfaz.JComboClaveAlmacen.getSelectedItem()==null){
                    JOptionPane.showMessageDialog(null, "El usuario no tiene asignado ningun almacen de compra");
                }else{
                     if(this.interfaz.jComboxSerie.getSelectedItem()==null){
                        JOptionPane.showMessageDialog(null, "El usuario no tiene asignado ninguna Serie de compra");
                      }else{
                      mtdsiguienteatras("siguiente");
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "Seleccione un proveedor");
                this.interfaz.txtClaveProveedor.grabFocus();
            }
        }
        if (e.getSource() == this.interfaz.btnatras || e.getSource() == this.interfaz.jMenuItemAtras) {
            mtdsiguienteatras("atras");
        }

        if (e.getSource() == this.interfaz.btnNuevaCompra || e.getSource() == this.interfaz.jMenuItemNuevo) {
            this.mtdNuevaVenta();
        }

        if (e.getSource() == this.interfaz.btnEliminarArticulo || e.getSource() == this.interfaz.jMenuItemEliminar) {
            this.mtdEliminarArticulo();
        }
        //Cobro
        if (e.getSource() == this.interfaz.btnAplicar || e.getSource() == this.interfaz.jMenuItemAplicar) {
            mtdAplicar();

        }
        if (e.getSource() == this.interfaz.btnCancelarCobro || e.getSource() == this.interfaz.jMenuItemCancelarCobro) {
            this.mtdCancelarCobro();
        }
        if (e.getSource() == this.interfaz.btnBuscarArticulo || e.getSource() == this.interfaz.jMenuItemBuscarArticulo) {
            mtdTableArticulo();
        }
        
        //Eventos Nota
                if (e.getSource() == this.interfaz.btnNota) {
            this.mtdAbrirBuscarNota();
        }
        
        if (e.getSource() == this.interfaz.btnBuscarMovimientos) {
            String fechaInicial = new SimpleDateFormat("yyyy-MM-dd").format(interfaz.JDFechaInicialNota.getDate());
            String fechaFinal = new SimpleDateFormat("yyyy-MM-dd").format(interfaz.JDFechaFinalNota.getDate());
            String numerosMovimiento=this.interfaz.jComboBoxImprimirNota.getSelectedItem().toString();
            this.instanceSerie = (SeriesyFolios) this.interfaz.jComboxSerie.getSelectedItem();
            instanceControladorMov.mtdRellenarTabla(this.interfaz.jTableNotaBuscar,instanceSerie.getClave(), "1", numerosMovimiento,fechaInicial,fechaFinal);
        }
        if (e.getSource() == this.interfaz.btnImprimirNota) {
            int fila = this.interfaz.jTableNotaBuscar.getSelectedRow();
            if(fila>=0){
                 this.interfaz.jDNota.dispose();
                 int folio=Integer.parseInt(this.interfaz.jTableNotaBuscar.getValueAt(fila,1).toString());
                 String serie=this.interfaz.jTableNotaBuscar.getValueAt(fila,0).toString();
                 controladorReporteCompra.mtdImprimirNota(folio,serie);
            }
            else{
            JOptionPane.showMessageDialog(null,"Seleccciones una folio movimiento");
            }
        }
        if (e.getSource() == this.interfaz.btncancelarcompra) {
            int fila = this.interfaz.jTableNotaBuscar.getSelectedRow();
            if (fila >= 0) {
                int folio = Integer.parseInt(this.interfaz.jTableNotaBuscar.getValueAt(fila, 1).toString());
                String serie = this.interfaz.jTableNotaBuscar.getValueAt(fila, 0).toString();
                String  signo="-";
                this.instanceAlmacen = (Almacen) this.interfaz.JComboClaveAlmacen.getSelectedItem();
                instanceControladorMov.mtdCancelarMovimiento(serie,folio,signo,instanceAlmacen.getClave());
                 this.mtdAbrirBuscarNota();
            } else {
                JOptionPane.showMessageDialog(null, "Seleccciones una compra");
            }
        }
        //end evento nota
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            //Metodo Buscar proveedor
            if (e.getSource() == this.interfaz.txtClaveProveedor) {
                mtdBtnBuscarProveedor();
            }
            if (e.getSource() == this.interfaz.txtBuscarProveedor) {
                ControladorProveedor controllerProveedor = new ControladorProveedor();
                controllerProveedor.mtdBuscarProveedor(this.interfaz.jTableProveedorBuscar, this.interfaz.txtBuscarProveedor.getText(), this.interfaz.jRadioButtonActivoBuscar, this.interfaz.jRadioButtonInactivoBuscar, this.interfaz.jRadioButtonTodoBuscar, this.interfaz.jComboBoxBuscar);
            }
            if (e.getSource() == this.interfaz.jTableProveedorBuscar) {
                mtdAgregarProveedor();
            }
            if (e.getSource() == this.interfaz.jComboBoxBuscar) {
                this.interfaz.txtBuscarProveedor.grabFocus();
            }
            //////////////Evento articulo//////////////////
            if (e.getSource() == interfaz.txtBuscarArticulo) {
                instanceControladorArticulo.mtdBuscarArticulo(this.interfaz.jtBuscarArticulo, this.interfaz.txtBuscarArticulo, this.interfaz.jRadioButtonActivoBuscar, this.interfaz.jRadioButtonInactivoBuscar, this.interfaz.jRadioButtonTodoBuscar, this.interfaz.jComboBoxBuscarArticulo, this.interfaz.jComboBoxDepartamentos, "compra");
            }
            
            if (e.getSource() == this.interfaz.jtBuscarArticulo) {
                mtdAgregarArticulo();
            }
            if (e.getSource() == this.interfaz.jComboBoxBuscarArticulo) {
                  if (this.interfaz.jComboBoxBuscarArticulo.getSelectedIndex() == 0) {
                    this.interfaz.jComboBoxDepartamentos.setVisible(false);
                    this.interfaz.txtBuscarArticulo.setVisible(true);
                    this.interfaz.txtBuscarArticulo.setBounds(200, 15,500, 25);
//                                        this.interfaz.txtBuscarArticulo.setBounds(200, 18, 700, 30);
                    this.interfaz.txtBuscarArticulo.grabFocus();
                }
                if (this.interfaz.jComboBoxBuscarArticulo.getSelectedIndex() == 1) {
                    this.interfaz.jComboBoxDepartamentos.setVisible(true);
                    this.interfaz.txtBuscarArticulo.setVisible(false);
                    this.interfaz.jComboBoxDepartamentos.grabFocus();
                }
             }
            if (e.getSource() == this.interfaz.jComboBoxDepartamentos) {
                this.interfaz.txtBuscarArticulo.setText("");
                instanceControladorArticulo.mtdBuscarArticulo(this.interfaz.jtBuscarArticulo, this.interfaz.txtBuscarArticulo, this.interfaz.jRadioButtonActivoBuscar, this.interfaz.jRadioButtonInactivoBuscar, this.interfaz.jRadioButtonTodoBuscar, this.interfaz.jComboBoxBuscarArticulo, this.interfaz.jComboBoxDepartamentos, "compra");
            }

        }

    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == this.interfaz.txtRecibi) {
            this.mtdMovimiento.mtdCalcularPago(this.interfaz.txtTotal, this.interfaz.txtRecibi, this.interfaz.txtCambio);
        }
    }

    public void mdtHora() {
        Reloj reloj = new Reloj(this.interfaz.lbhora);
        reloj.start();
    }

    public void mtdIniciar() {
        this.interfaz.jTabbedPaneDocumento.setSelectedIndex(0);
        this.interfaz.jTabbedPaneDocumento.setEnabledAt(1, false);
        this.interfaz.jTabbedPaneDocumento.setEnabledAt(0, true);
        //desactivar JTemMenus
        this.interfaz.jMenuItemSiguiente.setEnabled(true);
        this.interfaz.jMenuItemAtras.setEnabled(false);
        this.interfaz.jMenuItemNuevo.setEnabled(false);
        this.interfaz.jMenuItemEliminar.setEnabled(false);
        this.interfaz.jMenuItemBuscarArticulo.setEnabled(false);
        this.interfaz.jMenuItemAplicar.setEnabled(false);
        this.interfaz.jMenuItemGuardar.setEnabled(false);
        this.interfaz.txtClaveProveedor.grabFocus();
    }

    public void mtdCargarComboAlmacen() {
        ArrayList<Almacen> almacenes = this.instanceAlmacenDAO.listaAlmacen(instanceAlmacen, "compra", this.Almacenes);
        for (Almacen almacen : almacenes) {
            this.interfaz.JComboClaveAlmacen.addItem(almacen);
        }
    }

    public void mtdCargarComboSerie() {
        ArrayList<SeriesyFolios> series = this.instanceSFDAO.lista(instanceSerie, "compra", this.Series);
        for (SeriesyFolios serie : series) {
            this.interfaz.jComboxSerie.addItem(serie);
        }
    }

    public void mtdCargarComboMovimiento(EntradaySalida instaceEys, String filtro) {
        ArrayList<EntradaySalida> entradasysalidas = this.instaceEysDAO.listaTipoEYS(instaceEys, filtro);
        for (EntradaySalida entradasysalida : entradasysalidas) {
            this.interfaz.jComboBoxMovimiento.addItem(entradasysalida);
        }
    }

    public void mtdCargarComboTipoPago() {
        ArrayList<TipoPago> tipopagos = this.instanceTIPODAO.mtdLista();
        for (TipoPago tipoPago : tipopagos) {
            this.interfaz.jComboTipoPago.addItem(tipoPago);
        }
    }
    
    public void mtdCargarComboDepartamento() {
        ArrayList<Departamento> departamentos = this.instanceDepartamentoDAO.mtdLista(instanceDepartamento);
        for (Departamento departamento : departamentos) {
            this.interfaz.jComboBoxDepartamentos.addItem(departamento);
        }
    }

    public void mtdBtnBuscarProveedor() {
        this.interfaz.jDBuscarProveedor.setSize(870, 500);
        this.interfaz.jDBuscarProveedor.setLocationRelativeTo(null);
        this.interfaz.jDBuscarProveedor.setVisible(true);
        this.interfaz.txtBuscarProveedor.grabFocus();
    }

    private void mtdAgregarProveedor() {
        int numerofila = this.interfaz.jTableProveedorBuscar.getSelectedRow();
        if (numerofila >= 0) {
            this.interfaz.txtClaveProveedor.setText(this.interfaz.jTableProveedorBuscar.getValueAt(numerofila, 0).toString());
            this.interfaz.txtnombreProveedor.setText(interfaz.jTableProveedorBuscar.getValueAt(numerofila, 1).toString());
            this.interfaz.txtplazo.setText(this.interfaz.jTableProveedorBuscar.getValueAt(numerofila, 4).toString());
            this.interfaz.jDBuscarProveedor.dispose();
            this.interfaz.txtBuscarProveedor.setText("");
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione  un Provedor");
        }
    }

    protected void mtdAgregarArticulo() {
        int fila = this.interfaz.jtBuscarArticulo.getSelectedRow();
        try {
            if (fila >= 0) {
                this.interfaz.jdBuscarArticulo.dispose();
                mtdBuscarArticulo(String.valueOf(this.interfaz.jtBuscarArticulo.getValueAt(fila, 0)));
            } else {
                JOptionPane.showMessageDialog(null, "Seleccione un Articulo");
            }
        } catch (Exception Oe) {
            JOptionPane.showMessageDialog(null, "Error " + Oe, "Error", JOptionPane.ERROR_MESSAGE);

        }
    }

    protected void mtdBuscarArticulo(String codigoBarras) {
        String status = "1,2";
          this.instanceArticulo.setCodigo(codigoBarras);
        int numeroFila = this.instanceArticuloDAO.listaArticulosPorCodigo(instanceArticulo, status).size();
        if (numeroFila == 1) {
            Double cantidad = validar.redondeoDecimalesCantidad(Double.parseDouble(this.interfaz.txtCantidadBuscarProductos.getText()));
            if (cantidad > 0) {
                String clave = this.instanceArticuloDAO.listaArticulosPorCodigo(instanceArticulo, status).get(0).getCodigo();
                String nombre = this.instanceArticuloDAO.listaArticulosPorCodigo(instanceArticulo, status).get(0).getNombre();
                String costoUtilizar = String.valueOf(instanceArticuloDAO.listaArticulosPorCodigo(instanceArticulo, status).get(0).getCostoUtilizar());
                Double costoUnitario = 0.0;
                if (costoUtilizar.equals("U")) {
                    costoUnitario = Double.parseDouble(String.valueOf(this.instanceArticuloDAO.listaArticulosPorCodigo(instanceArticulo, status).get(0).getCostoUltimo()));
                }
                if (costoUtilizar.equals("P")) {
                    costoUnitario = Double.parseDouble(String.valueOf(this.instanceArticuloDAO.listaArticulosPorCodigo(instanceArticulo, status).get(0).getCostoPromedio()));
                }
                if (costoUtilizar.equals("R")) {
                    costoUnitario = Double.parseDouble(String.valueOf(this.instanceArticuloDAO.listaArticulosPorCodigo(instanceArticulo, status).get(0).getCostoReposicion()));
                }
                if (costoUtilizar.equals("A")) {
                    costoUnitario = Double.parseDouble(String.valueOf(this.instanceArticuloDAO.listaArticulosPorCodigo(instanceArticulo, status).get(0).getCostoAnterior()));
                }
                String unidad = String.valueOf(this.instanceArticuloDAO.listaArticulosPorCodigo(instanceArticulo, status).get(0).getUnidad().getClave());
                Double total = validar.redondeoDecimalesPreciosCostos(cantidad * validar.redondeoDecimalesPreciosCostos(costoUnitario));
                
                modeloT = (DefaultTableModel) interfaz.jtDatosCompra.getModel();
                Object elemento[] = {cantidad, clave, nombre,validar.redondeoDecimalesPreciosCostos(costoUnitario), 0, total, unidad,cantidad,0};
                modeloT.addRow(elemento);
                this.interfaz.txtBuscarCodigoBarrasNombre.setText("");
                this.interfaz.txtCantidadBuscarProductos.setText("1");
                mtdMovimiento.buscarArticulo(modeloT, interfaz.jtDatosCompra, total, 0.0, total, this.interfaz.txtSubTotal, this.interfaz.txtDescuento, this.interfaz.txtTotal, cantidad, this.interfaz.lblCantidadArticulo);
                mtdUpdateTableCOmpra(interfaz.jtDatosCompra, this.interfaz.txtSubTotal, this.interfaz.txtTotal,this.interfaz.txtDescuento,this.interfaz.lblCantidadArticulo);
                this.interfaz.txtCantidadBuscarProductos.grabFocus();

            } else {
                JOptionPane.showMessageDialog(null, "Cantidad Invalida", "ERROR", JOptionPane.ERROR_MESSAGE);
                this.interfaz.txtCantidadBuscarProductos.grabFocus();
            }
        }
        if (numeroFila > 1) {
            mtdTableArticulo();
        }
        if (numeroFila == 0) {
            JOptionPane.showMessageDialog(null, "No se encontro ningun Articulo con ese codigo", "No hay resultados en la busquesa", JOptionPane.WARNING_MESSAGE);
            mtdTableArticulo();
        }
    }
    protected void mtdAbrirBuscarNota() {
        this.interfaz.jDNota.setSize(700, 500);
        this.interfaz.jDNota.setLocationRelativeTo(null);
        this.interfaz.jDNota.setVisible(true);
    }
    protected void mtdUpdateTableCOmpra(JTable table, JTextField txtsubTotal, JTextField txtTotal,JTextField txtDescuento,JLabel txtCantidad) {
        modeloT.addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent e) {
                if (e.getType() == TableModelEvent.UPDATE) {
                    int columna = e.getColumn();
                    int row = e.getFirstRow();
                    mtdMovimiento.actualizarTabla(columna, row, table, txtsubTotal, txtTotal,txtDescuento,txtCantidad);
                }
            }
        });
    }

    protected void mtdAplicar() {
        this.interfaz.JDCobrar.setSize(330, 450);
        this.interfaz.JDCobrar.setLocationRelativeTo(null);
        this.interfaz.JDCobrar.setVisible(true);
        this.interfaz.txtRecibi.grabFocus();
    }

    protected void mtdsiguienteatras(String tipo) {
        if (tipo.equals("siguiente")) {
            this.interfaz.jTabbedPaneDocumento.setSelectedIndex(1);
            this.interfaz.jTabbedPaneDocumento.setEnabledAt(1, true);
            this.interfaz.jTabbedPaneDocumento.setEnabledAt(0, false);
            this.interfaz.txtBuscarCodigoBarrasNombre.grabFocus();
            this.interfaz.jMenuItemSiguiente.setEnabled(false);
            this.interfaz.jMenuItemAtras.setEnabled(true);
            this.interfaz.jMenuItemNuevo.setEnabled(true);
            this.interfaz.jMenuItemEliminar.setEnabled(true);
            this.interfaz.jMenuItemBuscarArticulo.setEnabled(true);
            this.interfaz.jMenuItemAplicar.setEnabled(true);
            this.interfaz.jMenuItemGuardar.setEnabled(true);
        }
        if (tipo.equals("atras")) {
            mtdIniciar();
        }
        this.instanceSerie = (SeriesyFolios) this.interfaz.jComboxSerie.getSelectedItem();
        int itemCount = this.interfaz.jComboBoxMovimiento.getItemCount();
        for (int i = 0; i < itemCount; i++) {
            this.interfaz.jComboBoxMovimiento.removeItemAt(0);
        }
        instaceEys.setClave(instanceSerie.getEntradaysalida().getClave());
        this.mtdCargarComboMovimiento(instaceEys, "clave");
    }

    private void salir() {
        mtdMovimiento.mdtSalir(this.interfaz.jtDatosCompra, null, this.interfaz);
    }

    protected void mtdNuevaVenta() {
        mtdMovimiento.mtdNuevaVenta(interfaz.jtDatosCompra, modeloT, this.interfaz.txtSubTotal, this.interfaz.txtDescuento, this.interfaz.txtTotal, this.interfaz.lblCantidadArticulo, this.interfaz.txtBuscarCodigoBarrasNombre);
    }

    protected void mtdEliminarArticulo() {
        mtdMovimiento.mtdEliminarArticulo(interfaz.jtDatosCompra, modeloT, this.interfaz.txtSubTotal, this.interfaz.txtDescuento, this.interfaz.txtTotal, this.interfaz.lblCantidadArticulo, this.interfaz.txtBuscarCodigoBarrasNombre);
    }

    public void mtdGrabar(int status) {
        //    Grabar Movimiento
        movimientoInstance.setClave(0);
        movimientoInstance.setStatus(status);
        movimientoInstance.setNombre(this.interfaz.txtnombreProveedor.getText());
        movimientoInstance.setImporteTotal(Double.parseDouble(this.interfaz.txtTotal.getText()));
        movimientoInstance.setIvaTotal(0);
        movimientoInstance.setDescuentoTotal(Float.parseFloat(this.interfaz.txtDescuento.getText()));
        String fechaMovimiento = new SimpleDateFormat("yyyy-MM-dd").format(interfaz.JDFechaVentaMostrador.getDate());
        movimientoInstance.setFechaAplicado(fechaMovimiento);
        movimientoInstance.setHoraA(this.interfaz.lbhora.getText());
        movimientoInstance.setFechaDocumento("");
        movimientoInstance.setHoraD("");
        movimientoInstance.setClienteopreveedor(this.interfaz.txtClaveProveedor.getText());
        this.instanceAlmacen = (Almacen) this.interfaz.JComboClaveAlmacen.getSelectedItem();
        this.movimientoInstance.setAlmacen(instanceAlmacen);
        movimientoInstance.getAgente().setClave(this.claveEmpleado);
        instanceTipopago = (TipoPago) this.interfaz.jComboTipoPago.getSelectedItem();
        movimientoInstance.setTipopago(instanceTipopago);
        this.instaceEys = (EntradaySalida) this.interfaz.jComboBoxMovimiento.getSelectedItem();
        movimientoInstance.setEntradaysalida(instaceEys);
        this.instanceSerie = (SeriesyFolios) this.interfaz.jComboxSerie.getSelectedItem();
        movimientoInstance.setSerieyfolio(instanceSerie);
        movimientoInstance.setNumerofolio(instanceSFDAO.mtdUpdateNumeroFolio(instanceSerie));
        int respuestaMovimientoId = this.movimientoDAO.mtdInsertar(movimientoInstance);
        if (respuestaMovimientoId > 0) {
//                Detalle movimiento
            JOptionPane.showMessageDialog(null, "Cabecera registrado correctamente id=" + respuestaMovimientoId);
            try {
                int numeroFila = this.interfaz.jtDatosCompra.getRowCount();
                for (int i = 0; i < numeroFila; i++) {
                    detmovimientoInstance.setClave(0);
                    detmovimientoInstance.getMovimiento().setClave(respuestaMovimientoId);
                    detmovimientoInstance.getArticulo().setCodigo(this.interfaz.jtDatosCompra.getValueAt(i, 1).toString());
                    detmovimientoInstance.setCodigoBarras(this.interfaz.jtDatosCompra.getValueAt(i, 1).toString());
                    detmovimientoInstance.setCantidadArticulo(Float.parseFloat(this.interfaz.jtDatosCompra.getValueAt(i, 0).toString()));
                    detmovimientoInstance.setNombreArticulo(this.interfaz.jtDatosCompra.getValueAt(i, 2).toString());
                    detmovimientoInstance.setClaveUnidad(this.interfaz.jtDatosCompra.getValueAt(i, 6).toString());
                    detmovimientoInstance.setPrecioUnitarioArticulo(Float.parseFloat(this.interfaz.jtDatosCompra.getValueAt(i, 3).toString()));
                    detmovimientoInstance.setPrecioTotalArticulo(Double.parseDouble(this.interfaz.jtDatosCompra.getValueAt(i, 5).toString()));
                    detmovimientoInstance.setIvaUnitarioArticulo(0);
                    detmovimientoInstance.setIvaTotalArticulo(0);
                    detmovimientoInstance.setDescuentoUnitarioArticulo(0);
                    detmovimientoInstance.setDescuentoTotalArticulo(0);
                    
                    ///Actualizar Existencias
                    String signo = movimientoInstance.getEntradaysalida().getSigno();
                    Double existencias = Double.parseDouble(signo + this.interfaz.jtDatosCompra.getValueAt(i, 0).toString());
                    instanceArticulo.setCodigo(this.interfaz.jtDatosCompra.getValueAt(i, 1).toString());
                    instacenExistencia.setArticulo(instanceArticulo);
                    instacenExistencia.setAlmacen(instanceAlmacen);
                    instacenExistencia.setExistencias(existencias);
                    instanceExistenciaDAO.mtdActualizar(instacenExistencia);
                    ///Terminar existencias
                    
                    ///Actualizar Costo///
                    instanceArticulo.setCodigoBarras(this.interfaz.jtDatosCompra.getValueAt(i, 1).toString());
                    instanceArticulo.setCostoUltimo(Float.parseFloat(this.interfaz.jtDatosCompra.getValueAt(i, 3).toString()));
                    instanceArticulo.setFechaActualizacionCosto(fechaMovimiento);
                    instanceArticuloDAO.mtdActulizarCostos(instanceArticulo);
                    ///Termina Actualizar Costo//
                    String respuesta = DetalleDB.mtdInsertarDetalller(detmovimientoInstance);

                    if (i + 1 == numeroFila) {
                        mtdNuevaVenta();
                        this.interfaz.JDCobrar.dispose();
                        this.interfaz.txtRecibi.setText("");
                        this.interfaz.txtCambio.setText("");
                        JOptionPane.showMessageDialog(null, respuesta);
                    }
                }
            } catch (Exception ee) {
                JOptionPane.showMessageDialog(null, "erro" + ee);
            }
//           Fin detalle Movimiento
        } else {
            JOptionPane.showMessageDialog(null, "Error al registrar la cabecera del Movimiento");
        }
    }

    //metodos cobro
    private void mtdCancelarCobro() {
        this.interfaz.JDCobrar.dispose();
        this.interfaz.txtRecibi.setText("");
        this.interfaz.txtCambio.setText("");
    }

    private void mtdTableArticulo() {
        this.interfaz.txtBuscarArticulo.setVisible(false);
        this.interfaz.txtBuscarArticulo.setVisible(false);
        this.interfaz.jComboBoxDepartamentos.setVisible(false);
        this.interfaz.jdBuscarArticulo.setSize(1100, 700);
        this.interfaz.jdBuscarArticulo.setLocationRelativeTo(null);
        this.interfaz.jdBuscarArticulo.setVisible(true);
        this.interfaz.txtBuscarArticulo.grabFocus();
    }
}
