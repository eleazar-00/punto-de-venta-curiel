/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import java.util.ArrayList;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import modelo.Departamento;
import modelo.DepartamentoDAO;
import vista.JFIDepartamento;

/**
 *
 * @author ELEAZAR
 */
public class ControladorDepartamento implements ActionListener, KeyListener {

    JFIDepartamento interfaz = new JFIDepartamento();
    DepartamentoDAO DbDepartamentp = new DepartamentoDAO();
    Departamento instance = new Departamento();
    DefaultTableModel model = new DefaultTableModel();

    public ControladorDepartamento(JFIDepartamento interfaz, DepartamentoDAO DbDepartamentp) {
        this.interfaz = interfaz;
        this.DbDepartamentp = DbDepartamentp;
        this.interfaz.btnNuevo.addActionListener(this);
        this.interfaz.btnEditar.addActionListener(this);
        this.interfaz.btnAceptar.addActionListener(this);
        this.interfaz.btnActualizar.addActionListener(this);
        this.interfaz.btnEliminar.addActionListener(this);
        this.interfaz.btnCancelar.addActionListener(this);
        this.interfaz.btnSalir.addActionListener(this);
//        this.interfaz.btnVistaPrevia.addActionListener(this);

        this.interfaz.btnNuevo.addKeyListener(this);
        this.interfaz.btnEditar.addKeyListener(this);
        this.interfaz.btnAceptar.addKeyListener(this);
        this.interfaz.btnActualizar.addKeyListener(this);
        this.interfaz.btnCancelar.addKeyListener(this);
        this.interfaz.btnEliminar.addKeyListener(this);
        this.interfaz.btnSalir.addKeyListener(this);
//        this.interfaz.btnVistaPrevia.addKeyListener(this);
        this.interfaz.txtBuscar.addKeyListener(this);
        mtdIniciar();
        mtdRellenarCabecera(this.interfaz.jTableLinea);
    }

    public ControladorDepartamento() {
    }

    public void mtdCargarComboDepartamento(JComboBox jcombobox) {
        ArrayList<Departamento> departamentos = this.DbDepartamentp.mtdLista(instance);
        for (Departamento departamento : departamentos) {
            jcombobox.addItem(departamento);
        }
    }

    private void mtdRellenarCabecera(JTable tableD) {
        tableD.setModel(model);
        //nombre de la cabecera de la tabla
        model.addColumn("Clave");
        model.addColumn("Departamento");
    }

    public void mtdRellenarTabla(JTable tableD, Departamento instance) {
        mtdLimparTable(tableD);
        Object[] columna = new Object[2];
        int numRegistro = DbDepartamentp.mtdLista(instance).size();
        ///metodo para rellenar la tabla con los datos/////
        for (int i = 0; i < numRegistro; i++) {
            columna[0] = DbDepartamentp.mtdLista(instance).get(i).getClave();
            columna[1] = DbDepartamentp.mtdLista(instance).get(i).getNombre();
            model.addRow(columna);
        }
    }

    public void mtdLimparTable(JTable tableD) {
        int numeroFila = tableD.getRowCount();
        model = (DefaultTableModel) tableD.getModel();
        for (int j = 0; j < numeroFila; j++) {
            model.removeRow(0);
        }
    }

    private void mtdLimparTxt() {
        this.interfaz.txtClave.setText("");
        this.interfaz.txtNombre.setText("");
    }

    private void mtdIniciar() {
        mtdLimparTxt();
        this.interfaz.btnNuevo.setVisible(true);
        this.interfaz.btnEditar.setVisible(true);
        this.interfaz.btnCancelar.setVisible(false);
        this.interfaz.btnAceptar.setVisible(false);
        this.interfaz.btnActualizar.setVisible(false);
        this.interfaz.btnEliminar.setVisible(true);
        this.interfaz.btnSalir.setVisible(true);
//        this.interfaz.btnVistaPrevia.setVisible(true);
        this.interfaz.jTabbedPaneDepartamento.setEnabledAt(0, true);
        this.interfaz.jTabbedPaneDepartamento.setEnabledAt(1, false);
        this.interfaz.jTabbedPaneDepartamento.setSelectedIndex(0);
    }

    private void mtdBtnNuevoBtnEditar() {
        this.interfaz.btnNuevo.setVisible(false);
        this.interfaz.btnEditar.setVisible(false);
        this.interfaz.btnCancelar.setVisible(true);
        this.interfaz.btnEliminar.setVisible(false);
        this.interfaz.btnSalir.setVisible(false);
//        this.interfaz.btnVistaPrevia.setVisible(false);
        this.interfaz.jTabbedPaneDepartamento.setEnabledAt(1, true);
        this.interfaz.jTabbedPaneDepartamento.setEnabledAt(0, false);
        this.interfaz.jTabbedPaneDepartamento.setSelectedIndex(1);
    }

    private void mdtBtnNUevo() {
        mtdBtnNuevoBtnEditar();
        this.interfaz.jTabbedPaneDepartamento.setTitleAt(1, "Nuevo");
        this.interfaz.btnAceptar.setVisible(true);
        this.interfaz.txtClave.setEditable(true);
        this.interfaz.txtClave.grabFocus();
    }

    public void btnEditar() {
        int numeroFila = this.interfaz.jTableLinea.getSelectedRow();
        if (numeroFila >= 0) {
            this.interfaz.txtClave.setText(String.valueOf(this.interfaz.jTableLinea.getValueAt(numeroFila, 0)));
            this.interfaz.txtNombre.setText(String.valueOf(this.interfaz.jTableLinea.getValueAt(numeroFila, 1)));
            this.interfaz.jTabbedPaneDepartamento.setTitleAt(1, "Editar");
            this.interfaz.btnActualizar.setVisible(true);
            this.interfaz.txtClave.setEditable(false);
            mtdBtnNuevoBtnEditar();
            this.interfaz.txtNombre.grabFocus();
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione un fila");
            this.interfaz.txtBuscar.grabFocus();
        }

    }

    private void mtdBtnAceptar() {
        String clave = interfaz.txtClave.getText();
        String nombre = interfaz.txtNombre.getText();
        if (clave.length() <= 2) {
            JOptionPane.showMessageDialog(null, "Campo Clave vacio", "Error", JOptionPane.WARNING_MESSAGE);
            this.interfaz.txtClave.grabFocus();
        } else if (nombre.length() <= 2) {
            JOptionPane.showMessageDialog(null, "Campo Descripción vacio", "Error", JOptionPane.WARNING_MESSAGE);
            this.interfaz.txtNombre.grabFocus();
        } else {
            String respuesta = "";
            instance.setClave(clave);
            instance.setNombre(nombre);
            respuesta = this.DbDepartamentp.mtdRegistrar(instance);
            if (respuesta.equals("Registrado Correctamente")) {
                this.interfaz.txtClave.grabFocus();
                mtdLimparTxt();
            }
            JOptionPane.showMessageDialog(null, respuesta);
        }
    }

    private void mtdBtnActualizar() {
        String clave = interfaz.txtClave.getText();
        String nombre = interfaz.txtNombre.getText();
        if (clave.length() <= 1 && nombre.length() <= 1) {
            JOptionPane.showMessageDialog(null, "Hay campos vacios", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            instance.setClave(clave);
            instance.setNombre(nombre);
            int respuesta = this.DbDepartamentp.mtdActualizar(instance);
            if (respuesta == 1) {
                JOptionPane.showMessageDialog(null, "Actualizado Correctamente");
                this.mtdIniciar();
                this.mtdLimparTxt();
            }
        }
    }

    public void btnEliminar() {
        int numeroFila = this.interfaz.jTableLinea.getSelectedRow();
        if (numeroFila >= 0) {
            this.instance.setClave(String.valueOf(this.interfaz.jTableLinea.getValueAt(numeroFila, 0)));
            int rpt = JOptionPane.showConfirmDialog(null, "Esta seguro de eliminar el departamento seleccionado");
            if (rpt == 0) {
                this.DbDepartamentp.mtdELiminar(instance);
                mtdRellenarTabla(this.interfaz.jTableLinea, instance);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione un fila");
            this.interfaz.txtBuscar.grabFocus();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == interfaz.btnNuevo) {
            mdtBtnNUevo();
        }
        if (e.getSource() == interfaz.btnEditar) {
            btnEditar();
        }
        if (e.getSource() == interfaz.btnAceptar) {
            mtdBtnAceptar();
        }
        if (e.getSource() == interfaz.btnCancelar) {
            mtdIniciar();
        }
        if (e.getSource() == interfaz.btnSalir) {
            this.interfaz.dispose();
        }
        if (e.getSource() == interfaz.btnActualizar) {
            mtdBtnActualizar();
        }
        if (e.getSource() == interfaz.btnEliminar) {
            btnEliminar();
        }
        if (e.getSource() == this.interfaz.btnCancelar) {
            this.mtdIniciar();
        }
        if (e.getSource() == this.interfaz.btnSalir) {
            this.interfaz.dispose();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            if (e.getSource() == interfaz.btnNuevo) {
                mdtBtnNUevo();
            }
            if (e.getSource() == interfaz.btnEditar) {
                btnEditar();
            }
            if (e.getSource() == this.interfaz.txtBuscar) {
                instance.setClave("");
                this.mtdRellenarTabla(this.interfaz.jTableLinea, instance);
            }
            if (e.getSource() == interfaz.btnAceptar) {
                mtdBtnAceptar();
            }
            if (e.getSource() == interfaz.btnCancelar) {
                mtdIniciar();
            }
            if (e.getSource() == interfaz.btnSalir) {
                this.interfaz.dispose();
            }
            if (e.getSource() == interfaz.btnActualizar) {
                mtdBtnActualizar();
            }
            if (e.getSource() == interfaz.btnEliminar) {
                btnEliminar();
            }
            if (e.getSource() == this.interfaz.btnCancelar) {
                this.mtdIniciar();
            }
            if (e.getSource() == this.interfaz.btnSalir) {
                this.interfaz.dispose();
            }
        }
        if (e.getKeyCode() == KeyEvent.VK_DELETE) {
            btnEliminar();
        }
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            this.interfaz.dispose();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

}
