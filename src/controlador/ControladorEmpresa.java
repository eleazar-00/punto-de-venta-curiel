/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.Image;
import java.awt.event.*;
import modelo.*;
import vista.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author ELEAZAR
 */
public class ControladorEmpresa implements ActionListener, KeyListener, MouseListener {

    JFIEmpresa vistaempresa = new JFIEmpresa();
    EmpresaDAO modeloempresa = new EmpresaDAO();
    Empresa empresa=new Empresa();
    Principal principal = new Principal();

    public ControladorEmpresa(JFIEmpresa vistaempresa, EmpresaDAO modeloempresa) {
        this.modeloempresa = modeloempresa;
        this.vistaempresa = vistaempresa;
        this.vistaempresa.btnEditar.addActionListener(this);
        this.vistaempresa.btnGuardar.addActionListener(this);
        this.vistaempresa.btnLogo.addActionListener(this);
        this.vistaempresa.btnCancelar.addActionListener(this);
        this.vistaempresa.btnSalir.addActionListener(this);
        
        this.vistaempresa.btnCancelar.addKeyListener(this);
        this.vistaempresa.btnSalir.addKeyListener(this);
        this.vistaempresa.btnEditar.addKeyListener(this);
        this.vistaempresa.btnGuardar.addKeyListener(this);
        this.vistaempresa.btnLogo.addKeyListener(this);
        this.vistaempresa.jtDatos.addKeyListener(this);
        this.vistaempresa.jtDatos.addMouseListener(this);
        
        this.vistaempresa.txttelefono.addKeyListener(this);

        mtdLlenarTabla(vistaempresa.jtDatos);
        mtdIniciar();
    }

    public void mtdLlenarTabla(JTable tableD) {
        DefaultTableModel modeloT = new DefaultTableModel();
        tableD.setModel(modeloT);
        //nombre de la cabecera de la tabla
        modeloT.addColumn("NOMBRE DE LA EMPRESA");
        modeloT.addColumn("POBLACIÓN");
        modeloT.addColumn("CALLE");
        modeloT.addColumn("TELEFONO");
        modeloT.addColumn("EMAIL");
        Object[] columna = new Object[5];
        int numRegistro = modeloempresa.listEmpresa().size();
        ///metodo para rellenar la tabla con los datos/////
        for (int i = 0; i < numRegistro; i++) {
            columna[0] = modeloempresa.listEmpresa().get(i).getRazonSocial();
            columna[1] = modeloempresa.listEmpresa().get(i).getPoblacion();
            columna[2] = modeloempresa.listEmpresa().get(i).getCalle();
            columna[3] = modeloempresa.listEmpresa().get(i).getTelefono();
            columna[4] = modeloempresa.listEmpresa().get(i).getEmail();
            modeloT.addRow(columna);
        }
    }
    public void mtdIniciar(){
    this.vistaempresa.btnCancelar.setVisible(false);
    this.vistaempresa.btnGuardar.setVisible(false);
    this.vistaempresa.btnSalir.setVisible(true);
    this.vistaempresa.btnEditar.setVisible(true);
    this.vistaempresa.jTabbedPaneEmpresa.setEnabledAt(0, true);
    this.vistaempresa.jTabbedPaneEmpresa.setEnabledAt(1, false);
    this.vistaempresa.jTabbedPaneEmpresa.setEnabledAt(2, false);
    this.vistaempresa.jTabbedPaneEmpresa.setSelectedIndex(0);

    }
    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vistaempresa.btnEditar) {
            mtdEditar();
        }
        if (e.getSource() == vistaempresa.btnGuardar) {
            mdtActualizar();
        }
        if (e.getSource() == vistaempresa.btnLogo) {
            try{
                JFileChooser Chooses = new JFileChooser();
                Chooses.setDialogTitle("Seleccionar imagen");
                Chooses.setAcceptAllFileFilterUsed(false);
                FileFilter Filtro1 = new FileNameExtensionFilter("JPG", "jpg");
                FileFilter Filtro2 = new FileNameExtensionFilter("PNG", "png");
                Chooses.setFileFilter(Filtro1);
                Chooses.addChoosableFileFilter(Filtro2);
                Chooses.showOpenDialog(null);
                File archivo = null;
                try{
                      archivo  = Chooses.getSelectedFile();
 
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, ex);
                }
                String filename = archivo.getAbsolutePath();
                vistaempresa.txtRuta.setText(filename.replace("\\", "//"));
                String ruta;
                ruta = vistaempresa.txtRuta.getText();
                ImageIcon img = new ImageIcon(ruta);
                vistaempresa.lblImagen.setIcon(new ImageIcon(img.getImage().getScaledInstance(vistaempresa.lblImagen.getWidth(), vistaempresa.lblImagen.getHeight(), Image.SCALE_DEFAULT)));
            }
            catch(Exception ex){
                     JOptionPane.showMessageDialog(null,"Error al cachar la imagenn "+ ex,"Error" , JOptionPane.ERROR_MESSAGE);
            }
            }
        if (e.getSource() == vistaempresa.btnCancelar) {
            mtdIniciar();

         }
        if (e.getSource() == this.vistaempresa.btnSalir) {
            this.vistaempresa.dispose();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        if (e.getSource() == vistaempresa.txtclave || e.getSource() == vistaempresa.txtcalle || e.getSource() == vistaempresa.txtpoblacion || e.getSource() == vistaempresa.txtemail) {
          char c=e.getKeyChar();
          if(c<'A'||c >'Z' && c<'a'||c >'z'){
            e.consume();
          }
        }
        if (e.getSource() == vistaempresa.txttelefono) {
            char c = e.getKeyChar();
            if (c < '0' || c > '9') {
                e.consume();
            }

        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
//        Evento al dar enter en un boton
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            if (e.getSource() == this.vistaempresa.btnSalir) {
                this.vistaempresa.dispose();
            }
            if (e.getSource() == vistaempresa.btnCancelar) {
                mtdIniciar();
             }
            if (e.getSource() == this.vistaempresa.jtDatos) {
                mtdEditar();
            }
            if (e.getSource() == vistaempresa.btnEditar) {
                mtdEditar();
            }
            if (e.getSource() == vistaempresa.btnGuardar) {
                mdtActualizar();
            }
        }
           if (e.getKeyCode() == KeyEvent.VK_F1) {
               vistaempresa.dispose();
           }


    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == this.vistaempresa.jtDatos) {
            mtdEditar();
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    public void mtdEditar() {
        int filaEditar = vistaempresa.jtDatos.getSelectedRow();
        int numFS = vistaempresa.jtDatos.getSelectedRowCount();
        if (filaEditar >= 0 && numFS == 1) {
            //////////CAMBIAR PANEL
            this.vistaempresa.jTabbedPaneEmpresa.setEnabledAt(0, false);
            this.vistaempresa.jTabbedPaneEmpresa.setEnabledAt(1, true);
            this.vistaempresa.jTabbedPaneEmpresa.setEnabledAt(2, true);
            this.vistaempresa.jTabbedPaneEmpresa.setTitleAt(1, "Edtar datos generales");
            this.vistaempresa.jTabbedPaneEmpresa.setSelectedIndex(1);
            this.vistaempresa.btnCancelar.setVisible(true);
            this.vistaempresa.btnGuardar.setVisible(true);
            this.vistaempresa.btnEditar.setVisible(false);
            this.vistaempresa.btnSalir.setVisible(false);
            ///////////////////////
            vistaempresa.txtclave.setText(modeloempresa.listEmpresa().get(0).getClave());
            vistaempresa.txtnombre.setText(modeloempresa.listEmpresa().get(0).getRazonSocial());
            this.vistaempresa.txtrfc.setText(modeloempresa.listEmpresa().get(0).getRfc());
            vistaempresa.txtPais.setText(modeloempresa.listEmpresa().get(0).getPais());
            vistaempresa.txtEstado.setText(modeloempresa.listEmpresa().get(0).getEstado());
            vistaempresa.txtpoblacion.setText(modeloempresa.listEmpresa().get(0).getPoblacion());
            vistaempresa.txtcolonia.setText(modeloempresa.listEmpresa().get(0).getColonia());
            vistaempresa.txtcalle.setText(modeloempresa.listEmpresa().get(0).getCalle());
            vistaempresa.txtNumeroI.setText(String.valueOf(modeloempresa.listEmpresa().get(0).getNumeroInterior()));
            vistaempresa.txtNumeroE.setText(String.valueOf(modeloempresa.listEmpresa().get(0).getNumeroExterior()));
            vistaempresa.txtCodigoPostal.setText(modeloempresa.listEmpresa().get(0).getCodigoPostal());
            vistaempresa.txttelefono.setText(modeloempresa.listEmpresa().get(0).getTelefono());
            vistaempresa.txtemail.setText(modeloempresa.listEmpresa().get(0).getEmail());
            vistaempresa.txtRuta.setText(modeloempresa.listEmpresa().get(0).getRutaImagen());
            String ruta = vistaempresa.txtRuta.getText();
            ImageIcon img = new ImageIcon(ruta);
            vistaempresa.lblImagen.setIcon(new ImageIcon(img.getImage().getScaledInstance(vistaempresa.lblImagen.getWidth(),vistaempresa.lblImagen.getHeight(),Image.SCALE_DEFAULT)));
             vistaempresa.txtclave.setEnabled(false);
            this.vistaempresa.txtnombre.grabFocus();

        } else {
            JOptionPane.showMessageDialog(null, "Debe seleccinar la empresa");
        }
    }
   private void cacharDatos() throws FileNotFoundException{
       empresa.setClave(vistaempresa.txtclave.getText());
       empresa.setRazonSocial(vistaempresa.txtnombre.getText());
       empresa.setRfc(vistaempresa.txtrfc.getText());
       empresa.setPais( vistaempresa.txtPais.getText());
       empresa.setEstado(vistaempresa.txtEstado.getText());
       empresa.setPoblacion(vistaempresa.txtpoblacion.getText());
       empresa.setColonia(vistaempresa.txtcolonia.getText());
       empresa.setCalle(vistaempresa.txtcalle.getText());
       empresa.setNumeroInterior(vistaempresa.txtNumeroI.getText());
       empresa.setNumeroExterior(vistaempresa.txtNumeroE.getText());
       empresa.setCodigoPostal(vistaempresa.txtCodigoPostal.getText());
       empresa.setTelefono(vistaempresa.txttelefono.getText());
       empresa.setEmail(vistaempresa.txtemail.getText());
       empresa.setRutaImagen(vistaempresa.txtRuta.getText());
       FileInputStream imagen = new FileInputStream(vistaempresa.txtRuta.getText());
       empresa.setImagen(imagen);
   }
    public void mdtActualizar() {  
         try {
            cacharDatos();
            int rptEdit = modeloempresa.mtdEditar(empresa);
            if (rptEdit > 0) {
                mtdLlenarTabla(vistaempresa.jtDatos);
                vistaempresa.txtclave.setEnabled(true);
                mtdIniciar();
                JOptionPane.showMessageDialog(null, "Actualizado Correctamente, Reinicie el programa");
 
            } else {
                 JOptionPane.showMessageDialog(null, "No se puedo realizar la Actualización");
             }
         } catch (FileNotFoundException ex) {
            Logger.getLogger(ControladorEmpresa.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
