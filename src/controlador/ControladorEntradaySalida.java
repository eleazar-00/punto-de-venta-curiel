/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import modelo.EntradaySalida;
import modelo.EntradaySalidaDAO;

import vista.JFIEntradaySalida;

/**
 *
 * @author mary
 */
public class ControladorEntradaySalida implements ActionListener, KeyListener {

    JFIEntradaySalida vista = new JFIEntradaySalida();//instanciar vista
    EntradaySalidaDAO bdTipoMovimiento = new EntradaySalidaDAO();//instanciar el modelo
    EntradaySalida instance = new EntradaySalida();
    DefaultTableModel modeloT = new DefaultTableModel();

// constructor
    public ControladorEntradaySalida(JFIEntradaySalida vista, EntradaySalidaDAO bdTipoMovimiento) { //recibe los combbjetos con su respectivo tipo//
        this.vista = vista;
        this.bdTipoMovimiento = bdTipoMovimiento;

        this.vista.btnNuevo.addActionListener(this);
        this.vista.btnAceptar.addActionListener(this);
        this.vista.btnEditar.addActionListener(this);
        this.vista.btnActualizar.addActionListener(this);
        this.vista.btnCancelar.addActionListener(this);
        this.vista.btnEliminar.addActionListener(this);
        this.vista.btnSalir.addActionListener(this);

        this.vista.txtClaveBuscar.addKeyListener(this);
        
        mtdCabecera(this.vista.jTMovimientoEYS);
        mtdIniciar();
//        mtdLlenarTabla(vista.jTMovimientoEYS);
    }

    private void mtdCabecera(JTable tableD) {
        tableD.setModel(modeloT);
        //nombre de la cabecera de la tabla
        modeloT.addColumn("Movimiento");
        modeloT.addColumn("Descripción");
        modeloT.addColumn("Operació");

    }

    private void mtdLimpiarTabla(JTable tableD) {
        int numeroFila = tableD.getRowCount();
        modeloT = (DefaultTableModel) tableD.getModel();
        for (int j = 0; j < numeroFila; j++) {
            modeloT.removeRow(0);
        }
    }

    private void mtdLlenarTabla(JTable tableD,String filtro) {
        mtdLimpiarTabla(tableD);
        Object[] columna = new Object[3];
        int numRegistro = bdTipoMovimiento.listaTipoEYS(instance, filtro).size();
        ///metodo para rellenar la tabla con los datos/////
        for (int i = 0; i <= numRegistro; i++) {
            columna[0] = bdTipoMovimiento.listaTipoEYS(instance, filtro).get(i).getClave();
            columna[1] = bdTipoMovimiento.listaTipoEYS(instance, filtro).get(i).getNombre();
            columna[2] = bdTipoMovimiento.listaTipoEYS(instance, filtro).get(i).getSigno();
            modeloT.addRow(columna);
        }
    }

    public void mtdIniciar() {
        this.vista.btnNuevo.setVisible(true);
        this.vista.btnAceptar.setVisible(false);
        this.vista.btnEditar.setVisible(true);
        this.vista.btnActualizar.setVisible(false);
        this.vista.btnCancelar.setVisible(false);
        this.vista.btnEliminar.setVisible(true);
        this.vista.btnSalir.setVisible(true);
        this.vista.jTabbedPaneEyS.setEnabledAt(0, true);
        this.vista.jTabbedPaneEyS.setEnabledAt(1, false);
        this.vista.jTabbedPaneEyS.setSelectedIndex(0);
        mtdLimpiar();
    }

    public void mtdBotonNuevoEditar(String btn) {
        this.vista.btnNuevo.setVisible(false);
        this.vista.btnEditar.setVisible(false);
        this.vista.btnCancelar.setVisible(true);
        this.vista.btnEliminar.setVisible(false);
        this.vista.btnSalir.setVisible(false);
        this.vista.jTabbedPaneEyS.setEnabledAt(1, true);
        this.vista.jTabbedPaneEyS.setEnabledAt(0, false);
        this.vista.jTabbedPaneEyS.setSelectedIndex(1);
        if (btn == "Nuevo") {
            this.vista.btnAceptar.setVisible(true);
            this.vista.jTabbedPaneEyS.setTitleAt(1, "Nuevo");
            this.vista.txtClaveTipoMovimiento.setEditable(true);
            this.vista.txtClaveTipoMovimiento.grabFocus();
        } else if (btn == "Editar") {
            this.vista.btnActualizar.setVisible(true);
            this.vista.setTitle("Editar");
            this.vista.jTabbedPaneEyS.setTitleAt(1, "Editar");
            this.vista.txtClaveTipoMovimiento.setEditable(false);
            this.vista.txtNombreTipoMovimiento.grabFocus();
        }
    }

    public void mtdLimpiar() {
        vista.txtClaveTipoMovimiento.setText("");
        vista.txtClaveTipoMovimiento.setEditable(true);
        vista.txtNombreTipoMovimiento.setText("");
    }

    private void mtdCacharTxt() {
        this.instance.setClave(vista.txtClaveTipoMovimiento.getText());
        this.instance.setNombre(vista.txtNombreTipoMovimiento.getText());
        this.instance.setSigno(vista.jcSigno.getSelectedItem().toString());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.btnNuevo) {
            this.mtdBotonNuevoEditar("Nuevo");
        }
        if (e.getSource() == vista.btnAceptar) {
            try {
                if (vista.txtClaveTipoMovimiento.getText().length() <= 1) {
                    JOptionPane.showMessageDialog(null, "Campo Clave vacio", "Error", JOptionPane.ERROR_MESSAGE);
                    this.vista.txtClaveTipoMovimiento.grabFocus();
                } else if (vista.txtNombreTipoMovimiento.getText().length() <= 1) {
                    JOptionPane.showMessageDialog(null, "Campo NOmbre vacio", "Error", JOptionPane.ERROR_MESSAGE);
                    this.vista.txtNombreTipoMovimiento.grabFocus();

                } else {
                    mtdCacharTxt();
                    String respuesta = bdTipoMovimiento.mtdInsertarEYS(instance);
                    JOptionPane.showMessageDialog(null, respuesta);
                    this.mtdLimpiar();
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Error" + ex, "Error", JOptionPane.ERROR_MESSAGE);
            }

        }
        if (e.getSource() == vista.btnEditar) {
            int filaEditar = vista.jTMovimientoEYS.getSelectedRow();
            if (filaEditar >= 0) {
                this.mtdBotonNuevoEditar("Editar");
                vista.txtClaveTipoMovimiento.setText(String.valueOf(vista.jTMovimientoEYS.getValueAt(filaEditar, 0)));
                vista.txtNombreTipoMovimiento.setText(String.valueOf(vista.jTMovimientoEYS.getValueAt(filaEditar, 1)));
                vista.jcSigno.setSelectedIndex(0);
                vista.jcSigno.setSelectedItem(vista.jTMovimientoEYS.getValueAt(filaEditar, 2));

            } else {
                JOptionPane.showMessageDialog(null, "Debe seleccinar una fila");
            }
        }

        if (e.getSource() == vista.btnActualizar) {
            this.mtdCacharTxt();
            int respEdit = bdTipoMovimiento.mtdActualizar(instance);
            if (respEdit > 0) {
                vista.txtClaveTipoMovimiento.setEnabled(true);
                JOptionPane.showMessageDialog(null, "Actualizado Correctamente");
                mtdIniciar();

            } else {
                JOptionPane.showMessageDialog(null, respEdit);
            }
        }
        //  --------------    
        if (e.getSource() == vista.btnCancelar) {
            this.mtdIniciar();
        }
        if (e.getSource() == vista.btnEliminar) {
            int fila = vista.jTMovimientoEYS.getSelectedRow();
            if (fila >= 0) {
                int respUser = JOptionPane.showConfirmDialog(null, "Desea eliminar la fila seleccionada");
                if (respUser == 0) {
                    instance.setClave(String.valueOf(this.vista.jTMovimientoEYS.getValueAt(fila, 0)));
                    this.bdTipoMovimiento.mtdEliminar(instance);
                }

            } else {
                JOptionPane.showMessageDialog(null, "Selecciona una fila para eliminar");
            }
        }

        if (e.getSource() == vista.btnSalir) {
            vista.dispose();
        }

    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            if (e.getSource() == this.vista.txtClaveBuscar) {
                 this.mtdLlenarTabla(this.vista.jTMovimientoEYS,"todo");
            }
        }
    }
    @Override
    public void keyReleased(KeyEvent e) {
    }
}
