/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import modelo.Almacen;
import modelo.Articulo;
import modelo.ExistenciaAlmacen;
import modelo.ExistenciaAlmacenDAO;

/**
 *
 * @author VISORUS SOFT
 */
public class ControladorExistenciaAlmacen {
    ExistenciaAlmacen instance=new ExistenciaAlmacen();
    ExistenciaAlmacenDAO instanceDAO=new ExistenciaAlmacenDAO();
    Articulo instanceArticulo=new Articulo();
    Almacen instanceAlmacen=new Almacen();
    
    DefaultTableModel modeloT = new DefaultTableModel();
  
    public ControladorExistenciaAlmacen() {
        
    }
    public void mtdCabeceraTabla(JTable tableD) {
        tableD.setModel(modeloT);
        //nombre de la cabecera de la tabla
        modeloT.addColumn("CLAVE");
        modeloT.addColumn("ALMACEN");
        modeloT.addColumn("EXISTECNIAS");
        modeloT.addColumn("APARTADO");
        modeloT.addColumn("STOCK MINIMO");
        modeloT.addColumn("STOCK MAXIMO");
    }

    public void mtdLimparTable(JTable tableD) {
        int numeroFila = tableD.getRowCount();
        modeloT = (DefaultTableModel) tableD.getModel();
        for (int j = 0; j < numeroFila; j++) {
            modeloT.removeRow(0);
        }
    }
    
    public void mtdLlenarTabla(JTable tableD,ExistenciaAlmacen instance) {
        mtdLimparTable(tableD);
        Object[] columna = new Object[7];
        int numRegistro = instanceDAO.listaExistencias(instance).size();
         ///metodo para rellenar la tabla con los datos/////
         for (int i = 0; i < numRegistro; i++) {
            columna[0] = instanceDAO.listaExistencias(instance).get(i).getAlmacen().getClave();
            columna[1] = instanceDAO.listaExistencias(instance).get(i).getAlmacen().getNombre();
            columna[2] = instanceDAO.listaExistencias(instance).get(i).getExistencias();
            columna[3] = instanceDAO.listaExistencias(instance).get(i).getApartado();
            columna[4] = instanceDAO.listaExistencias(instance).get(i).getStockMinimo();
            columna[5] = instanceDAO.listaExistencias(instance).get(i).getStackMaximo();
            modeloT.addRow(columna);
        }
    }
   
   protected void mtdInsetAlmacen(JTable tableD,String calveArticulo) {
        int numeroFilas = tableD.getRowCount();
        for (int i = 0; i < numeroFilas; i++) {
            instanceAlmacen.setClave(tableD.getValueAt(i, 0).toString());
            instanceArticulo.setCodigo(calveArticulo);
            instance.setArticulo(instanceArticulo);
            instance.setAlmacen(instanceAlmacen);
            instance.setExistencias(Double.parseDouble(tableD.getValueAt(i, 2).toString()));
            instance.setApartado(0.0);
            Double stockminimo = Double.parseDouble(tableD.getValueAt(i, 4).toString());
            Double stockmaximo = Double.parseDouble(tableD.getValueAt(i, 5).toString());
            if (stockminimo < 0) {
                stockminimo = 0.0;
            }
            if (stockmaximo < 0) {
                stockmaximo = 0.0;
            }
            instance.setStockMinimo(stockminimo);
            instance.setStackMaximo(stockmaximo);
            this.instanceDAO.mtdInsertar(instance);
         }
    }
   protected void mtdEliminarExistecniasDB(String clavearticulo) {
         this.instanceDAO.mtdEliminar(clavearticulo);
    } 
}
