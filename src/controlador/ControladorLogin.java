/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JOptionPane;
import modelo.Agente;
import modelo.AgenteDAO;
import modelo.PefilDAO;
import modelo.Usuario;
import modelo.UsuarioDAO;
import vista.Login;
import vista.Principal;

/**
 *
 * @author ELEAZAR
 */
public class ControladorLogin implements ActionListener, KeyListener {

    Login Interfaz = new Login();
    Usuario instance = new Usuario();
    UsuarioDAO UsuarioBD = new UsuarioDAO();

    AgenteDAO AgenteDAO = new AgenteDAO();
    Agente agente = new Agente();
    PefilDAO perfilDao=new PefilDAO();

    public ControladorLogin(Login Intefaces, UsuarioDAO UsuarioBD) {
        this.Interfaz = Intefaces;
        this.UsuarioBD = UsuarioBD;
        this.Interfaz.txtPassword.addKeyListener(this);
        this.Interfaz.btnAceptar.addActionListener(this);
//        this.Interfaz.btnCancelar.addActionListener(this);
        this.Interfaz.btnAceptar.addKeyListener(this);
//        this.Interfaz.btnCancelar.addKeyListener(this);
    }

    private void limpiar() {
        this.Interfaz.txtPassword.setText("");
        this.Interfaz.txtUsuario.setText("");
    }

    private void validar() {
          /*getproperty*/
        instance.setUsuario(Interfaz.txtUsuario.getText());
        instance.setPassword(Interfaz.txtPassword.getText());
        if (!UsuarioBD.mtdValidarUsuario(instance).isEmpty()) {
             String menu=UsuarioBD.mtdValidarUsuario(instance);
            agente.setUsuario(instance);
            int cantidadAgente = AgenteDAO.mtdDatosUsuarioLogin(agente).size();
            if (cantidadAgente == 1) {
                if (AgenteDAO.mtdDatosUsuarioLogin(agente).get(0).getStatus() == 1) {
                    Principal interfaz = new Principal();
                    interfaz.UsuarioPricipal.setText(Interfaz.txtUsuario.getText());
                    interfaz.CempleadoP.setText(AgenteDAO.mtdDatosUsuarioLogin(agente).get(0).getClave() + "");
                    interfaz.NempleadoP.setText(AgenteDAO.mtdDatosUsuarioLogin(agente).get(0).getNombre());
                    interfaz.AlmacenesP.setText(AgenteDAO.mtdDatosUsuarioLogin(agente).get(0).getAlmacenes());
                    interfaz.SeriesP.setText(AgenteDAO.mtdDatosUsuarioLogin(agente).get(0).getSeries());
                    ControladorPrincipal controladoaar= new ControladorPrincipal(interfaz,menu);
                    interfaz.setVisible(true);
                    this.Interfaz.dispose();
                    limpiar();
                }
                if (AgenteDAO.mtdDatosUsuarioLogin(agente).get(0).getStatus() == 0) {
                    JOptionPane.showMessageDialog(null,"Empleado Inactivo");
                }
            }
            if (cantidadAgente == 0) {
                JOptionPane.showMessageDialog(null, "El usuario no esta ligado a ningun empleado");
            }
            if (cantidadAgente > 1) {
                JOptionPane.showMessageDialog(null, "Existen varios Empleados asignado al usuario Error");
            }

        } else {
            JOptionPane.showMessageDialog(null, "Usuario/Contraseña Invalidos", "Advertencia", JOptionPane.WARNING_MESSAGE);
            limpiar();
            this.Interfaz.txtUsuario.grabFocus();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == Interfaz.btnAceptar) {
            validar();
        }
//        if (e.getSource() == Interfaz.btnCancelar) {
//            this.Interfaz.dispose();
//
//        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            if (e.getSource() == this.Interfaz.btnAceptar) {
                this.validar();
            }
            if (e.getSource() == this.Interfaz.txtPassword) {
                this.validar();
            }
//            if (e.getSource() == this.Interfaz.btnCancelar) {
//                this.Interfaz.dispose();
//            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

}
