/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import modelo.Almacen;
import modelo.Articulo;
import modelo.DetalleMovimientoDAO;
import modelo.ExistenciaAlmacen;
import modelo.ExistenciaAlmacenDAO;
 import modelo.Movimiento;
import modelo.MovimientoDAO;

/**
 *
 * @author CURIEL
 */
public class ControladorMovimiento {
MovimientoDAO instanceDAO= new MovimientoDAO();
DetalleMovimientoDAO instanceDetalleDAO=new DetalleMovimientoDAO();
DefaultTableModel modeloT = new DefaultTableModel();
Movimiento instance=new Movimiento();

ExistenciaAlmacen instacenExistencia = new ExistenciaAlmacen();
ExistenciaAlmacenDAO instanceExistenciaDAO = new ExistenciaAlmacenDAO();

    public ControladorMovimiento(MovimientoDAO instanceDAO,Movimiento instance) {
        this.instanceDAO=instanceDAO;
        this.instance=instance;
    }

    ControladorMovimiento() {
     }
 
    public void mtdLimparTable(JTable tabla) {
        int numeroFila = tabla.getRowCount();
        modeloT = (DefaultTableModel) tabla.getModel();
        for (int j = 0; j < numeroFila; j++) {
            modeloT.removeRow(0);
        }
    }

    public void mtdRellenarTabla(JTable tabla,String serie, String status, String filtro,String fechaIni,String FechaFin) {
        mtdLimparTable(tabla);
        Object[] columna = new Object[5];
        int numeroFila = instanceDAO.listafiltros(instance,serie,filtro, status, fechaIni, FechaFin).size();
        for (int i = 0; i < numeroFila; i++) {      
            columna[0] = this.instanceDAO.listafiltros(instance,serie, filtro, status, fechaIni, FechaFin).get(i).getSerieyfolio().getClave();
            columna[1] = this.instanceDAO.listafiltros(instance,serie, filtro, status, fechaIni, FechaFin).get(i).getNumerofolio();
            columna[2] = this.instanceDAO.listafiltros(instance,serie, filtro, status, fechaIni, FechaFin).get(i).getNombre();
            columna[3] = this.instanceDAO.listafiltros(instance,serie, filtro, status, fechaIni, FechaFin).get(i).getImporteTotal();
            columna[4] = this.instanceDAO.listafiltros(instance,serie, filtro, status, fechaIni, FechaFin).get(i).getFechaAplicado()+" "+this.instanceDAO.listafiltros(instance,serie, filtro, status, fechaIni, FechaFin).get(i).getHoraA();
            modeloT.addRow(columna);
        }
    }
    
    public void mtdCancelarMovimiento(String serie,int folio,String signo,String claveAlmacen){
        int respuestaMovimiento=instanceDAO.mtdCancelarMmovimiento(serie, folio);
        if(respuestaMovimiento==1){
            int idMovimiento=instanceDAO.mtdIdMovimiento(serie, folio);
                if(idMovimiento>0){
                    Articulo articulo=new Articulo();
                    Almacen almacen=new Almacen();
                    int numeroFila = instanceDetalleDAO.listaDellate(idMovimiento).size();
                    for (int i = 0; i < numeroFila; i++) {
                       Double existencias = Double.parseDouble(signo+instanceDetalleDAO.listaDellate(idMovimiento).get(i).getCantidadArticulo());
                       articulo.setCodigo(instanceDetalleDAO.listaDellate(idMovimiento).get(i).getArticulo().getCodigo()+"");
                       almacen.setClave(claveAlmacen);
                       instacenExistencia.setArticulo(articulo);
                       instacenExistencia.setAlmacen(almacen);
                       instacenExistencia.setExistencias(existencias);
                       instanceExistenciaDAO.mtdActualizar(instacenExistencia); 
                       } 
                    JOptionPane.showMessageDialog(null,"El movimiento fue cancelado correctamente");
                }else{
                  JOptionPane.showMessageDialog(null,"Error a seleccionar el id del movimiento");
                }
        }else{
          JOptionPane.showMessageDialog(null,"Error al actualizar el movimiento");
         }
    }
}
