/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import modelo.PefilDAO;
import modelo.Perfil;
 import vista.JFIPerfil;

 /**
 *
 * @author ELEAZAR
 */
public class ControladorPerfil implements ActionListener, KeyListener {

    JFIPerfil jfPerfil = new JFIPerfil();
    PefilDAO perfilInstance = new PefilDAO();
    Perfil instance=new Perfil();

    public ControladorPerfil(JFIPerfil jfPerfil, PefilDAO perfilInstance) {
        this.jfPerfil = jfPerfil;
        this.perfilInstance = perfilInstance;
        this.jfPerfil.btnAceptar.addActionListener(this);
        this.jfPerfil.btncancelar.addActionListener(this);
        this.jfPerfil.btnnuevo.addActionListener(this);
        this.jfPerfil.btneliminar.addActionListener(this);
        this.jfPerfil.btnEditar.addActionListener(this);
        this.jfPerfil.btnOk.addActionListener(this);
        inicializar();
    }

    public void inicializar() {
        this.jfPerfil.btnAceptar.setEnabled(false);
        this.jfPerfil.btncancelar.setEnabled(false);
        this.jfPerfil.txtcableperfil.setEnabled(false);
        this.jfPerfil.txtnombreperfil.setEnabled(false);
        this.jfPerfil.btnOk.setEnabled(false);
        this.jfPerfil.jCheckBoxEmpresa.setSelected(true);
        this.jfPerfil.jCheckBoxEmpleados.setSelected(true);
        this.jfPerfil.jCheckBoxVenta.setSelected(true);
        this.jfPerfil.jCheckBoxCompra.setSelected(true);
        this.jfPerfil.jCheckBoxInventario.setSelected(true);
        this.jfPerfil.jCheckBoxReporte.setSelected(true);
        this.jfPerfil.jCheckBoxSistemas.setSelected(true);
        mtdRellenarTable(jfPerfil.jtDatos);
    }

    public void mtdActivarBotones() {
        this.jfPerfil.btnnuevo.setEnabled(true);
        this.jfPerfil.btnEditar.setEnabled(true);

        this.jfPerfil.btneliminar.setEnabled(true);
        this.jfPerfil.btnSalir.setEnabled(true);
    }

    public void mtdDesactivarBotones() {
        this.jfPerfil.btnnuevo.setEnabled(false);
        this.jfPerfil.btnEditar.setEnabled(false);
        this.jfPerfil.btnOk.setEnabled(false);
        this.jfPerfil.btneliminar.setEnabled(false);
        this.jfPerfil.btnSalir.setEnabled(false);
    }

    public void mtdRellenarTable(JTable tableD) {
        DefaultTableModel model = new DefaultTableModel();
        tableD.setModel(model);
        model.addColumn("Clave");
        model.addColumn("Pelfil");
        model.addColumn("Menu");
        Object[] columna = new Object[3];

        int numRegistro = perfilInstance.mtdLista().size();

        for (int i = 0; i < numRegistro; i++) {
            columna[0] = perfilInstance.mtdLista().get(i).getClavePerfil();
            columna[1] = perfilInstance.mtdLista().get(i).getNombrePerfil();
            columna[2] = perfilInstance.mtdLista().get(i).getMenu();
            model.addRow(columna);
        }
    }

    public void mtdLimpiar() {
        this.jfPerfil.txtcableperfil.setText("");
        this.jfPerfil.txtnombreperfil.setText("");
        this.jfPerfil.btnAceptar.setEnabled(true);
        this.jfPerfil.btncancelar.setEnabled(true);
        this.jfPerfil.txtcableperfil.setEnabled(true);
        this.jfPerfil.txtnombreperfil.setEnabled(true);
    }
    private void mtdCacharValaresChex(){
      instance.setClavePerfil(jfPerfil.txtcableperfil.getText());
      instance.setNombrePerfil(jfPerfil.txtnombreperfil.getText());
      String menu="";
     if(this.jfPerfil.jCheckBoxEmpresa.isSelected()==true){
         menu+="1";
     }else{
         menu+="0";
     }
     if(this.jfPerfil.jCheckBoxEmpleados.isSelected()==true){
         menu+="1";
     }else{
         menu+="0";
     }if(this.jfPerfil.jCheckBoxVenta.isSelected()==true){
         menu+="1";
     }else{
         menu+="0";
     }if(this.jfPerfil.jCheckBoxCompra.isSelected()==true){
         menu+="1";
     }else{
         menu+="0";
     }if(this.jfPerfil.jCheckBoxInventario.isSelected()==true){
         menu+="1";
     }else{
         menu+="0";
     }if(this.jfPerfil.jCheckBoxReporte.isSelected()==true){
         menu+="1";     
     }else{
         menu+="0";         
     }if(this.jfPerfil.jCheckBoxSistemas.isSelected()==true){
         menu+="1";     
     }else{
         menu+="0";
     }
      instance.setMenu(menu);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == jfPerfil.btnAceptar) {
             if (jfPerfil.txtcableperfil.getText().length() >= 1 && jfPerfil.txtnombreperfil.getText().length() >= 1) {
                mtdCacharValaresChex();
                String Respuesta = perfilInstance.mtdRegistrar(instance);
                JOptionPane.showMessageDialog(null, Respuesta);
                mtdLimpiar();
                inicializar();
                mtdActivarBotones();
                mtdRellenarTable(jfPerfil.jtDatos);
            }else{
                  JOptionPane.showMessageDialog(null,"Campos Obligatorios vacios","Error",JOptionPane.ERROR_MESSAGE);
             }
        }
        if (e.getSource() == jfPerfil.btncancelar) {
            mtdLimpiar();
            inicializar();
            this.jfPerfil.btnnuevo.setEnabled(true);
            mtdActivarBotones();

        }
        if (e.getSource() == jfPerfil.btnnuevo) {
            mtdLimpiar();
            mtdDesactivarBotones();
            this.jfPerfil.txtcableperfil.grabFocus();
        }
        if (e.getSource() == jfPerfil.btneliminar) {
            int numeroFila = jfPerfil.jtDatos.getSelectedRow();
             String clavePerfil = "";
            if (numeroFila >= 0) {
                 clavePerfil = String.valueOf(jfPerfil.jtDatos.getValueAt(numeroFila, 0));
                int confir = JOptionPane.showConfirmDialog(null, "Esta seguro que deseas eliminar el perfil?");
                if (confir == 0) {
                    perfilInstance.mtdEliminar(clavePerfil);
                    mtdRellenarTable(jfPerfil.jtDatos);

                }

            } else {
                JOptionPane.showMessageDialog(null, "Seleccione un Perfil");
            }
         }
        if (e.getSource() == jfPerfil.btnEditar) {
            int numroFila = jfPerfil.jtDatos.getSelectedRow();
            if (numroFila >= 0) {
                this.jfPerfil.txtcableperfil.setText(String.valueOf(jfPerfil.jtDatos.getValueAt(numroFila, 0)));
                this.jfPerfil.txtnombreperfil.setText(String.valueOf(jfPerfil.jtDatos.getValueAt(numroFila, 1)));
                String menu=String.valueOf(jfPerfil.jtDatos.getValueAt(numroFila, 2));
                if(menu.charAt(0)=='1'){
                    this.jfPerfil.jCheckBoxEmpresa.setSelected(true);
                }else{
                    this.jfPerfil.jCheckBoxEmpresa.setSelected(false);
                }
                if(menu.charAt(1)=='1'){
                    this.jfPerfil.jCheckBoxEmpleados.setSelected(true);
                }else{
                    this.jfPerfil.jCheckBoxEmpleados.setSelected(false);
                }if(menu.charAt(2)=='1'){
                    this.jfPerfil.jCheckBoxVenta.setSelected(true);
                }else{
                    this.jfPerfil.jCheckBoxVenta.setSelected(false);
                }if(menu.charAt(3)=='1'){
                    this.jfPerfil.jCheckBoxCompra.setSelected(true);
                }else{
                    this.jfPerfil.jCheckBoxCompra.setSelected(false);

                }if(menu.charAt(4)=='1'){
                    this.jfPerfil.jCheckBoxInventario.setSelected(true);
                }else{
                    this.jfPerfil.jCheckBoxInventario.setSelected(false);                    
                }if(menu.charAt(5)=='1'){
                    this.jfPerfil.jCheckBoxReporte.setSelected(true);                    
                }else{
                    this.jfPerfil.jCheckBoxReporte.setSelected(false);                    
                }
                if(menu.charAt(6)=='1'){
                    this.jfPerfil.jCheckBoxSistemas.setSelected(true);                    
                }else{
                    this.jfPerfil.jCheckBoxSistemas.setSelected(false);                    
                }
                this.jfPerfil.txtnombreperfil.setEnabled(true);
                mtdDesactivarBotones();
                this.jfPerfil.txtnombreperfil.grabFocus();
                this.jfPerfil.btncancelar.setEnabled(true);
                this.jfPerfil.btnOk.setEnabled(true);
            } else {
                JOptionPane.showMessageDialog(null,"Seleccione un Perfil");
            }
        }
        if (e.getSource() == jfPerfil.btnOk) {                
            mtdCacharValaresChex();
            int respuesta = perfilInstance.mtdActualizar(instance);
            if (respuesta == 1) {
                mtdLimpiar();
                mtdActivarBotones();
                inicializar();
                this.jfPerfil.btnOk.setEnabled(false);
                JOptionPane.showMessageDialog(null, "Actualizado Correctamente");
            } else {
                mtdLimpiar();
                mtdActivarBotones();
                JOptionPane.showMessageDialog(null, "No se pudo Actualizar");
            }
        }


        if (e.getSource() == jfPerfil.btnSalir) {
            this.jfPerfil.dispose();
        }

    }

    @Override
    public void keyTyped(KeyEvent e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void keyPressed(KeyEvent e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void keyReleased(KeyEvent e) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
