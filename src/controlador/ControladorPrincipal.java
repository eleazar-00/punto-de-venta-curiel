/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyVetoException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import modelo.*;
import vista.*;

/**
 *
 * @author ELEAZAR
 */
public class ControladorPrincipal implements ActionListener, KeyListener {

    Principal Interfaz = new Principal();
    EmpresaDAO modeloempresa = new EmpresaDAO();
    
    String menu="01";

    public ControladorPrincipal(Principal Interfaz,String menu) {
        this.Interfaz = Interfaz;
        this.menu=menu;
        this.Interfaz.JMEmpresa.addActionListener(this);
        this.Interfaz.JMUsuario.addActionListener(this);
        this.Interfaz.JMPerfil.addActionListener(this);
        this.Interfaz.JMAlmacen.addActionListener(this);
        this.Interfaz.jmDepartamento.addActionListener(this);
        this.Interfaz.jmProveedor.addActionListener(this);
        this.Interfaz.jmCompraDirecta.addActionListener(this);
        this.Interfaz.jmarticulo.addActionListener(this);
        this.Interfaz.JmVentaMostrador.addActionListener(this);
        this.Interfaz.jmAgente.addActionListener(this);
        this.Interfaz.jmTipoAgente.addActionListener(this);
        this.Interfaz.jMITipoMovimiento.addActionListener(this);
        this.Interfaz.jMIActualizar.addActionListener(this);
        this.Interfaz.jMAltaybajaCliente.addActionListener(this);
        this.Interfaz.jMenuTipoPago.addActionListener(this);
        this.Interfaz.jMenuIteUnidad.addActionListener(this);
        this.Interfaz.jMenuItemSeriesyFolios.addActionListener(this);
        this.Interfaz.jmMarca.addActionListener(this);
        this.Interfaz.jmUbicacion.addActionListener(this);

        this.Interfaz.btnPrincipalVenta.addActionListener(this);
        this.Interfaz.btnPrincipalCliente.addActionListener(this);
        this.Interfaz.btnPricipalReporteVenta.addActionListener(this);
        this.Interfaz.btnReporteCompra.addActionListener(this);
        this.Interfaz.btnPrincipalProveedor.addActionListener(this);
        this.Interfaz.btnPricipalArticulo.addActionListener(this);
        this.Interfaz.btnPrincipalCompra.addActionListener(this);
        this.Interfaz.btnReporteInventario.addActionListener(this);
         mtdLlenarTabla();
         mtdvalidarMenu();
    }
    private void mtdvalidarMenu(){
        if(menu.charAt(0)=='1'){
         this.Interfaz.EMPRESA.setEnabled(true);
        }else{
         this.Interfaz.EMPRESA.setEnabled(false);
        }
        if(menu.charAt(1)=='1'){
         this.Interfaz.EMPLEADO.setEnabled(true);
        }else{
         this.Interfaz.EMPLEADO.setEnabled(false);
        }
        if(menu.charAt(2)=='1'){
         this.Interfaz.VENTA.setEnabled(true);
         this.Interfaz.btnPrincipalVenta.setEnabled(true);
         this.Interfaz.btnPrincipalCliente.setEnabled(true);
        }else{
         this.Interfaz.VENTA.setEnabled(false);
         this.Interfaz.btnPrincipalVenta.setEnabled(false);
         this.Interfaz.btnPrincipalCliente.setEnabled(false);
        }if(menu.charAt(3)=='1'){
         this.Interfaz.COMPRA.setEnabled(true);
         this.Interfaz.btnPrincipalCompra.setEnabled(true);
         this.Interfaz.btnPrincipalProveedor.setEnabled(true);
        }else{
         this.Interfaz.COMPRA.setEnabled(false);
         this.Interfaz.btnPrincipalCompra.setEnabled(false);
         this.Interfaz.btnPrincipalProveedor.setEnabled(false);
        }if(menu.charAt(4)=='1'){
         this.Interfaz.INVENTARIO.setEnabled(true);
         this.Interfaz.btnPricipalArticulo.setEnabled(true);
         }else{
         this.Interfaz.INVENTARIO.setEnabled(false);
         this.Interfaz.btnPricipalArticulo.setEnabled(false);
        }if(menu.charAt(5)=='1'){
         this.Interfaz.btnReporteCompra.setEnabled(true);
         this.Interfaz.btnPricipalReporteVenta.setEnabled(true);
        }else{
         this.Interfaz.btnReporteCompra.setEnabled(false);
         this.Interfaz.btnPricipalReporteVenta.setEnabled(false);
        }if(menu.charAt(6)=='1'){
         this.Interfaz.SISTEMA.setEnabled(true);
        }else{
         this.Interfaz.SISTEMA.setEnabled(false);
        }
    }
    private void mtdLlenarTabla() {
        modeloempresa.listEmpresa().get(0).getClave();
        Interfaz.lblrazonSocial.setText(modeloempresa.listEmpresa().get(0).getRazonSocial());
        String ruta = modeloempresa.listEmpresa().get(0).getRutaImagen();
        try {
            ImageIcon img = new ImageIcon(ruta);
            Interfaz.lblPrincipalLogo.setIcon(new ImageIcon(img.getImage().getScaledInstance(Interfaz.Escritorio.getWidth(), Interfaz.Escritorio.getHeight(), Image.SCALE_DEFAULT)));
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "error imagen");
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == Interfaz.JMUsuario) {
            JFIUsuario interfaz = new JFIUsuario();
            this.Interfaz.Escritorio.add(interfaz);
            UsuarioDAO intanceDAO = new UsuarioDAO();
            ControladorUsuario controlador = new ControladorUsuario(interfaz, intanceDAO);
            interfaz.setVisible(true);
        }
        if (e.getSource() == Interfaz.JMPerfil) {
            JFIPerfil interfaz = new JFIPerfil();
            this.Interfaz.Escritorio.add(interfaz);
            PefilDAO perfilBD = new PefilDAO();
            ControladorPerfil control = new ControladorPerfil(interfaz, perfilBD);
            interfaz.setVisible(true);
        }
        if (e.getSource() == Interfaz.JMEmpresa) {
            JFIEmpresa interfaz = new JFIEmpresa();
            this.Interfaz.Escritorio.add(interfaz);
            try {
                interfaz.setMaximum(true);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(ControladorPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
            EmpresaDAO empresaBD = new EmpresaDAO();
            ControladorEmpresa controlador = new ControladorEmpresa(interfaz, empresaBD);
            interfaz.show();
        }
        if (e.getSource() == Interfaz.jMIActualizar) {
            mtdLlenarTabla();
        }

        if (e.getSource() == Interfaz.JMAlmacen) {
            JFIAlmacen interfaz = new JFIAlmacen();
            this.Interfaz.Escritorio.add(interfaz);
            try {
                interfaz.setMaximum(true);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(ControladorPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
            AlmacenDAO almacenDB = new AlmacenDAO();
            ControladorAlmacen controlador = new ControladorAlmacen(interfaz, almacenDB);
            interfaz.show();
        }

        if (e.getSource() == Interfaz.jmDepartamento) {
            this.mtdDepartamento();
        }
        if (e.getSource() == Interfaz.jmMarca) {
            this.mtdMarca();
        }
        if (e.getSource() == Interfaz.jmUbicacion) {
            this.mtdUbicacion();
        }
        if (e.getSource() == Interfaz.jmProveedor) {
            this.mtdProveedor();
        }
        if (e.getSource() == Interfaz.jmarticulo) {
            this.mtdArticulo();
        }
        if (e.getSource() == Interfaz.JmVentaMostrador) {
            this.mtdVentaMostrador();
        }
        if (e.getSource() == Interfaz.jmCompraDirecta) {
            this.mtdCompra();
        }
        if (e.getSource() == Interfaz.jmAgente) {
            JFIAgente interfaz = new JFIAgente();
            AgenteDAO instanceDAO = new AgenteDAO();
            this.Interfaz.Escritorio.add(interfaz);
            try {
                interfaz.setMaximum(true);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(ControladorPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
            ControladorAgente controlodor = new ControladorAgente(interfaz, instanceDAO);
            interfaz.setVisible(true);
        }
        if (e.getSource() == Interfaz.jmTipoAgente) {
            JFITipoAgente interfaz = new JFITipoAgente();
            TipoAgenteDAO instanceDAO = new TipoAgenteDAO();
            this.Interfaz.Escritorio.add(interfaz);
            try {
                interfaz.setMaximum(true);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(ControladorPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
            ControladorTipoAgente controladorTA = new ControladorTipoAgente(interfaz, instanceDAO);
            interfaz.setVisible(true);
        }
        if (e.getSource() == this.Interfaz.jMITipoMovimiento) {
            JFIEntradaySalida interfaz = new JFIEntradaySalida();
            EntradaySalidaDAO bdTipovistaMovimiento = new EntradaySalidaDAO();
            this.Interfaz.Escritorio.add(interfaz);
            try {
                interfaz.setMaximum(true);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(ControladorPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
            ControladorEntradaySalida controlador = new ControladorEntradaySalida(interfaz, bdTipovistaMovimiento);// envio de los objeto
            interfaz.setVisible(true);
        }
        if (e.getSource() == this.Interfaz.jMAltaybajaCliente) {
            this.mtdCliente();
        }
        if (e.getSource() == this.Interfaz.jMenuTipoPago) {
            JFITipoPago interfaz = new JFITipoPago();
            this.Interfaz.Escritorio.add(interfaz);
            try {
                interfaz.setMaximum(true);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(ControladorPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
            TipoPagoDAO instance = new TipoPagoDAO();
            ControladorTipoPago control = new ControladorTipoPago(interfaz, instance);
            interfaz.setVisible(true);
        }
        if (e.getSource() == this.Interfaz.jMenuIteUnidad) {
            JFIUnidad interfaz = new JFIUnidad();
            UnidadDAO instance = new UnidadDAO();
            this.Interfaz.Escritorio.add(interfaz);
            try {
                interfaz.setMaximum(true);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(ControladorPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
            ControladorUnidad control = new ControladorUnidad(interfaz, instance);
            interfaz.setVisible(true);
        }
        if (e.getSource() == this.Interfaz.jMenuItemSeriesyFolios) {
            JFISeriesyFolios interfaz = new JFISeriesyFolios();
            SeriesyFoliosDAO instanceDAO = new SeriesyFoliosDAO();
            this.Interfaz.Escritorio.add(interfaz);
            try {
                interfaz.setMaximum(true);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(ControladorPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
            ControladorSeriesyFolios control = new ControladorSeriesyFolios(interfaz, instanceDAO);
            interfaz.setVisible(true);
        }
        //btn Dde accceso rapido
        if (e.getSource() == this.Interfaz.btnPrincipalVenta) {
            this.mtdVentaMostrador();
        }
        if (e.getSource() == this.Interfaz.btnPrincipalCliente) {
            this.mtdCliente();
        }
        if (e.getSource() == this.Interfaz.btnPrincipalCompra) {
            this.mtdCompra();
        }
        if (e.getSource() == this.Interfaz.btnPrincipalProveedor) {
            this.mtdProveedor();
        }
        if (e.getSource() == this.Interfaz.btnPricipalArticulo) {
            this.mtdArticulo();
        }
        //Termina btn de acceso rapido

        //reporte 
        if (e.getSource() == this.Interfaz.btnPricipalReporteVenta) {
            JFIReportesVenta interfaz = new JFIReportesVenta();
            this.Interfaz.Escritorio.add(interfaz);
            try {
                interfaz.setMaximum(true);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(ControladorPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
            ControladorReporteVenta control = new ControladorReporteVenta(interfaz);
            interfaz.setVisible(true);

        }
          if (e.getSource() == this.Interfaz.btnReporteCompra) {
            JFIReportesCompra interfaz = new JFIReportesCompra();
            this.Interfaz.Escritorio.add(interfaz);
            try {
                interfaz.setMaximum(true);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(ControladorPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
            ControladorReporteCompra control = new ControladorReporteCompra(interfaz);
            interfaz.setVisible(true);

        }
        if (e.getSource() == this.Interfaz.btnReporteInventario) {
            JFReporteInventario interfaz = new JFReporteInventario();
            this.Interfaz.Escritorio.add(interfaz);
            try {
                interfaz.setMaximum(true);
            } catch (PropertyVetoException ex) {
                Logger.getLogger(ControladorPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
            ControladorReporteInventario control = new ControladorReporteInventario(interfaz);
            interfaz.setVisible(true);

        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    private void mtdVentaMostrador() {
        JFIVentaMostrador interfaz = new JFIVentaMostrador();
        this.Interfaz.Escritorio.add(interfaz);
        try {
            interfaz.setMaximum(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(ControladorPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
         ControladorVentaMostrador controlador = new ControladorVentaMostrador(interfaz, Integer.parseInt(this.Interfaz.CempleadoP.getText()), this.Interfaz.AlmacenesP.getText(), this.Interfaz.SeriesP.getText());
         interfaz.setVisible(true); 
    }

    private void mtdCliente() {
        JFICliente interfaz = new JFICliente();
        this.Interfaz.Escritorio.add(interfaz);
        try {
            interfaz.setMaximum(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(ControladorPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
        ClienteDAO instance = new ClienteDAO();
        ControladorCliente control = new ControladorCliente(interfaz, instance);
        interfaz.setVisible(true);
    }

    private void mtdCompra() {
        JFICompraDirecta interfaz = new JFICompraDirecta();
        this.Interfaz.Escritorio.add(interfaz);
        try {
            interfaz.setMaximum(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(ControladorPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
        ControladorCompra controlador = new ControladorCompra(interfaz, Integer.parseInt(this.Interfaz.CempleadoP.getText()), this.Interfaz.AlmacenesP.getText(), this.Interfaz.SeriesP.getText());
        interfaz.setVisible(true);
    }

    private void mtdProveedor() {
        JFIProveedor interfaz = new JFIProveedor();
        this.Interfaz.Escritorio.add(interfaz);
        try {
            interfaz.setMaximum(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(ControladorPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
        ProveedorDAO provedorBD = new ProveedorDAO();
        ControladorProveedor controlar = new ControladorProveedor(interfaz, provedorBD);
        interfaz.setVisible(true);
    }

    private void mtdArticulo() {
        JFIArticulo interfaz = new JFIArticulo();
        this.Interfaz.Escritorio.add(interfaz);
        try {
            interfaz.setMaximum(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(ControladorPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
        ArticuloDAO instanceArticuloDAO = new ArticuloDAO();
        ControladorArticulo controlador = new ControladorArticulo(interfaz, instanceArticuloDAO);
        interfaz.setVisible(true);
    }

    public void mtdDepartamento() {
        JFIDepartamento interfaz = new JFIDepartamento();
        this.Interfaz.Escritorio.add(interfaz);
        try {
            interfaz.setMaximum(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(ControladorPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
        DepartamentoDAO DbDepartamentp = new DepartamentoDAO();
        ControladorDepartamento controlador = new ControladorDepartamento(interfaz, DbDepartamentp);
        interfaz.setVisible(true);
    }

    public void mtdMarca() {
        JFIMarca interfaz = new JFIMarca();
        this.Interfaz.Escritorio.add(interfaz);
        try {
            interfaz.setMaximum(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(ControladorPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
        MarcaDAO DbMarca = new MarcaDAO();
        ControladorMarca controlador = new ControladorMarca(interfaz, DbMarca);
        interfaz.setVisible(true);
    }

    public void mtdUbicacion() {
        JFIUbicacion interfaz = new JFIUbicacion();
        this.Interfaz.Escritorio.add(interfaz);
        try {
            interfaz.setMaximum(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(ControladorPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
        UbicacionDAO instanceDAO = new UbicacionDAO();

        ControladorUbicacion controlador = new ControladorUbicacion(interfaz, instanceDAO);
        interfaz.setVisible(true);
    }

    public void mtdUnidad() {
        JFIUnidad interfaz = new JFIUnidad();
        this.Interfaz.Escritorio.add(interfaz);
        try {
            interfaz.setMaximum(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(ControladorPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
        UnidadDAO instanceDAO = new UnidadDAO();

        ControladorUnidad controlador = new ControladorUnidad(interfaz, instanceDAO);
        interfaz.setVisible(true);
    }
}
