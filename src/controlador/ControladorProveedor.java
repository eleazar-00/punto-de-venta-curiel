/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import modelo.Proveedor;
import modelo.ProveedorDAO;
import modelo.TipoAgente;
import modelo.TipoAgenteDAO;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import vista.JFIProveedor;

/**
 *
 * @author ELEAZAR
 */
public class ControladorProveedor implements ActionListener, KeyListener {

    JFIProveedor interfaz = new JFIProveedor();
    ProveedorDAO provedorBD = new ProveedorDAO();
    Proveedor instance = new Proveedor();

    TipoAgenteDAO instanceTipoAgenteDAO = new TipoAgenteDAO();

    DefaultTableModel modeloT = new DefaultTableModel();
    //para generar excel
    JFileChooser selecArchivo = new JFileChooser();
    File archivo;
    int contAccion = 0;

    public ControladorProveedor(JFIProveedor interfaz, ProveedorDAO provedorBD) {
        this.interfaz = interfaz;
        this.provedorBD = provedorBD;
        this.interfaz.btnAceptar.addActionListener(this);
        this.interfaz.btnEditar.addActionListener(this);
        this.interfaz.btnOk.addActionListener(this);
        this.interfaz.btnSalir.addActionListener(this);
        this.interfaz.btneliminar.addActionListener(this);
        this.interfaz.btnnuevo.addActionListener(this);
        this.interfaz.btncancelar.addActionListener(this);

        this.interfaz.btnExportarExcel.addActionListener(this);
        this.interfaz.btnCancelarJDexportarexcel.addActionListener(this);
        this.interfaz.btnAceptarExportarExcel.addActionListener(this);

        this.interfaz.jtDatos.addKeyListener(this);
        this.interfaz.btnAceptar.addKeyListener(this);
        this.interfaz.btnEditar.addKeyListener(this);
        this.interfaz.btnOk.addKeyListener(this);
        this.interfaz.btnSalir.addKeyListener(this);
        this.interfaz.btncancelar.addKeyListener(this);
        this.interfaz.btneliminar.addKeyListener(this);
        this.interfaz.btnnuevo.addKeyListener(this);
        this.interfaz.txtBuscarClave.addKeyListener(this);
        this.instance.setClave(0);
        mtdCabezera(this.interfaz.jtDatos);
        mtdInicar();
    }

    public ControladorProveedor() {
    }

    protected void mtdCabezera(JTable tableD) {
        tableD.setModel(modeloT);
        //nombre de la cabecera de la tabla
        modeloT.addColumn("CLAVE");
        modeloT.addColumn("RAZÓN SOCIAL");
        modeloT.addColumn("TIPO AGENTE");
    }

    public void mtdCargarTipoAgente(String tipo) {
        ArrayList<TipoAgente> tipoAgentes = this.instanceTipoAgenteDAO.listaTipoAgente(tipo);
        for (TipoAgente tipoAgente : tipoAgentes) {
            this.interfaz.jComboTipo.addItem(tipoAgente);
        }
    }

    protected void mtdLimparTable(JTable tableD) {
        int numeroFila = tableD.getRowCount();
        modeloT = (DefaultTableModel) tableD.getModel();
        for (int j = 0; j < numeroFila; j++) {
            modeloT.removeRow(0);
        }
    }

    //Rellenar tabla proveedor
    protected void mtdLlenarTabla(JTable tableD, Proveedor instance, String status, String filtro) {
        mtdLimparTable(tableD);
        Object[] columna = new Object[5];
        int numRegistro = this.provedorBD.listaProveedor(instance, status, filtro).size();
        if (filtro.equals("todo")) {
            this.mtdColumnasTable(numRegistro, columna, instance, status, filtro);
        } else {
            if (numRegistro < 6) {
                ///metodo para rellenar la tabla con los datos/////
                this.mtdColumnasTable(numRegistro, columna, instance, status, filtro);
            } else {
                JOptionPane.showMessageDialog(null, "Se encontro mas de 15 registro favor de ser mas especificos en la busqueda", "Advertencia", JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    protected void mtdColumnasTable(int numRegistro, Object[] columna, Proveedor instance, String status, String filtro) {
        if (numRegistro > 0) {
            for (int i = 0; i < numRegistro; i++) {
                columna[0] = provedorBD.listaProveedor(instance, status, filtro).get(i).getClave();
                columna[1] = provedorBD.listaProveedor(instance, status, filtro).get(i).getRfc();
                columna[2] = provedorBD.listaProveedor(instance, status, filtro).get(i).getNombre();
                columna[3] = provedorBD.listaProveedor(instance, status, filtro).get(i).getTipoAgente().getNombre();
                columna[4] = provedorBD.listaProveedor(instance, status, filtro).get(i).getDiasPlazo();
                modeloT.addRow(columna);
            }
        } else {
            JOptionPane.showMessageDialog(null, "No se encontro ningun registro", "advertencia", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    public void mtdInicar() {
        this.interfaz.jRadioButtonActivo.setSelected(true);
        this.interfaz.jRadioButtonBuscarActivo.setSelected(true);
        this.interfaz.btnAceptar.setVisible(false);
        this.interfaz.btnOk.setVisible(false);
        this.interfaz.btncancelar.setVisible(false);
        this.interfaz.btnEditar.setVisible(true);
        this.interfaz.btneliminar.setVisible(true);
        this.interfaz.btnnuevo.setVisible(true);
        this.interfaz.btnExportarExcel.setVisible(true);
        this.interfaz.jTabbedPaneProveedor.setEnabledAt(0, true);
        this.interfaz.jTabbedPaneProveedor.setSelectedIndex(0);
        this.interfaz.jTabbedPaneProveedor.setEnabledAt(1, false);
        this.interfaz.jTabbedPaneProveedor.setEnabledAt(2, false);
        this.interfaz.txtBuscarClave.grabFocus();
    }

    public void mtdLimpiarTxt() {
        this.interfaz.txtrfc.setText("");
        this.interfaz.txtEmpresa.setText("");
        this.interfaz.txtPais.setText("");
        this.interfaz.txtEstado.setText("");
        this.interfaz.txtPoblacion.setText("");
        this.interfaz.txtTelefono.setText("");
        this.interfaz.txtCelular.setText("");
        this.interfaz.txtCorreo.setText("");
        this.interfaz.txtColonia.setText("");
        this.interfaz.txtCodigoPostal.setText("");
        this.interfaz.txtCalle.setText("");
        this.interfaz.txtlimitecredito.setText("0.0");
        this.interfaz.txtdiasplazo.setText("0");
        this.interfaz.txtsaldo.setText("0.0");
    }

    public void mtdCacharDatosTXT() {
        try {
            int status = 1;
            if (this.interfaz.jRadioButtonActivo.isSelected() == true) {
                status = 1;
            }
            if (this.interfaz.jRadioButtonInactivo.isSelected() == true) {
                status = 0;
            }
            instance.setStatus(status);
            instance.setRfc(interfaz.txtrfc.getText());
            instance.setNombre(this.interfaz.txtEmpresa.getText());

            TipoAgente tA = (TipoAgente) interfaz.jComboTipo.getSelectedItem();
            System.out.println(tA);
            instance.setTipoAgente(tA);

            instance.setPais(this.interfaz.txtPais.getText());
            instance.setEstado(this.interfaz.txtEstado.getText());
            instance.setPoblacion(this.interfaz.txtPoblacion.getText());
            instance.setColonia(this.interfaz.txtColonia.getText());
            instance.setCalle(interfaz.txtCalle.getText());
            instance.setCodigoPostal(interfaz.txtCodigoPostal.getText());
            instance.setCelular(interfaz.txtCelular.getText());
            instance.setTelefono(interfaz.txtTelefono.getText());
            instance.setCorreo(this.interfaz.txtCorreo.getText());
            instance.setLimiteCredito(Double.parseDouble(this.interfaz.txtlimitecredito.getText()));
            instance.setDiasPlazo(Integer.parseInt(this.interfaz.txtdiasplazo.getText()));
            instance.setSaldo(Double.parseDouble(this.interfaz.txtsaldo.getText()));
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al cachar los datos " + e, "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void mtdNuevoEditar(String Etiqueta) {
        this.interfaz.jTabbedPaneProveedor.setEnabledAt(0, false);
        this.interfaz.jTabbedPaneProveedor.setEnabledAt(1, true);
        this.interfaz.jTabbedPaneProveedor.setEnabledAt(2, true);
        this.interfaz.jTabbedPaneProveedor.setTitleAt(1, Etiqueta);
        this.interfaz.jTabbedPaneProveedor.setSelectedIndex(1);
        this.interfaz.btnEditar.setVisible(false);
        this.interfaz.btneliminar.setVisible(false);
        this.interfaz.btnOk.setVisible(false);
        this.interfaz.btncancelar.setVisible(true);
        this.interfaz.btnnuevo.setVisible(false);
        this.interfaz.txtrfc.grabFocus();
        this.interfaz.btnSalir.setVisible(false);
        this.interfaz.btnExportarExcel.setVisible(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == interfaz.btnnuevo) {
            mtdNuevoEditar("Nuevo Proveedor");
            mtdLimpiarTxt();
            mtdCargarTipoAgente("P");
            this.interfaz.btnnuevo.setVisible(false);
            this.interfaz.btnAceptar.setVisible(true);
            int utm = provedorBD.mtdUltimoIDdeMovimiento();
            this.interfaz.txtClave.setText((utm + 1) + "");
        }
        if (e.getSource() == interfaz.btncancelar) {
            this.interfaz.btnSalir.setVisible(true);
            int itemCount = this.interfaz.jComboTipo.getItemCount();
            for (int i = 0; i < itemCount; i++) {
                this.interfaz.jComboTipo.removeItemAt(0);
            }
            this.interfaz.btnSalir.setVisible(true);
            mtdInicar();
        }
        if (e.getSource() == interfaz.btnAceptar) {
            mtdInsertar();
        }
        if (e.getSource() == interfaz.btnEditar) {
            mtdCargarTipoAgente("P");
            mtdEditarAction();
        }
        if (e.getSource() == this.interfaz.btnOk) {
            mdtActualizar();
        }
        if (e.getSource() == interfaz.btneliminar) {
            mtdELiminar();
        }
        if (e.getSource() == interfaz.btnSalir) {
            interfaz.dispose();
        }

        //Eventos Exportar Excel
        if (e.getSource() == this.interfaz.btnExportarExcel) {
            this.interfaz.jDexportarexcel.setSize(500, 400);
            this.interfaz.jDexportarexcel.setLocationRelativeTo(interfaz);
            this.interfaz.jDexportarexcel.setVisible(true);
        }
        if (e.getSource() == this.interfaz.btnCancelarJDexportarexcel) {
            this.interfaz.jDexportarexcel.dispose();
        }
        contAccion++;
        if (contAccion == 1) {
            AgregarFiltro();
        }
        if (e.getSource() == this.interfaz.btnAceptarExportarExcel) {
            if (selecArchivo.showDialog(null, "Exportar") == JFileChooser.APPROVE_OPTION) {
                archivo = selecArchivo.getSelectedFile();
                if (archivo.getName().endsWith("xls") || archivo.getName().endsWith("xlsx")) {
                    JOptionPane.showMessageDialog(null, mtdExportarExcel(archivo) + "\n Formato ." + archivo.getName().substring(archivo.getName().lastIndexOf(".") + 1));
                } else {
                    JOptionPane.showMessageDialog(null, "Elija un formato valido.");
                }
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        if (e.getSource() == this.interfaz.txtCodigoPostal) {
            char car = e.getKeyChar();
            if ((car < '0' || car > '9')) {
                e.consume();
            }
        }

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            //Metodo buscar proveedor
            if (e.getSource() == this.interfaz.txtBuscarClave) {
                this.mtdBuscarProveedor(this.interfaz.jtDatos, this.interfaz.txtBuscarClave.getText(), this.interfaz.jRadioButtonBuscarActivo, this.interfaz.jRadioButtonBuscarInactivo, this.interfaz.jRadioButtonBuscarTodos, this.interfaz.jComboBoxFiltro);
            }
        }
        if (e.getKeyCode() == KeyEvent.VK_DELETE) {
            if (e.getSource() == this.interfaz.jtDatos) {
                mtdELiminar();
            }
        }
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            this.interfaz.dispose();
        }

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    public void mtdEditarAction() {
        int numeroFila = interfaz.jtDatos.getSelectedRow();
        if (numeroFila >= 0) {
            instance.setClave(Integer.parseInt(this.interfaz.jtDatos.getValueAt(numeroFila, 0).toString()));
            mtdEditar();
            mtdNuevoEditar("Editar proveedor");
            this.interfaz.btnEditar.setVisible(false);
            this.interfaz.btnOk.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione una Fila");
        }
    }

    public void mtdInsertar() {
        String nombre = this.interfaz.txtrfc.getText();
        if (nombre.length() >= 4) {
            instance.setClave(0);
            mtdCacharDatosTXT();
            String respuesta = provedorBD.mtdInserta(instance);
            if (respuesta.equals("Registrado Correctamento")) {
                mtdLimpiarTxt();
                int utm = provedorBD.mtdUltimoIDdeMovimiento();
                this.interfaz.txtClave.setText((utm + 1) + "");
            }
            JOptionPane.showMessageDialog(null, respuesta);
        } else {
            JOptionPane.showMessageDialog(null, "Campos Razón Social Obligatorio!!!", "Error", JOptionPane.ERROR_MESSAGE);
            this.interfaz.txtrfc.grabFocus();
        }
    }

    public void mtdEditar() {
        String status = "0,1";
        String filtro = "clave";
        interfaz.txtClave.setText(String.valueOf(provedorBD.listaProveedor(instance, status, filtro).get(0).getClave()));
        int statu = (this.provedorBD.listaProveedor(instance, status, filtro).get(0).getStatus());
        if (statu == 0) {
            interfaz.jRadioButtonInactivo.setSelected(true);
            interfaz.jRadioButtonActivo.setSelected(false);
        } else {
            interfaz.jRadioButtonInactivo.setSelected(false);
            interfaz.jRadioButtonActivo.setSelected(true);
        }
        interfaz.txtEmpresa.setText(String.valueOf(this.provedorBD.listaProveedor(instance, status, filtro).get(0).getNombre()));
        interfaz.txtrfc.setText(String.valueOf(this.provedorBD.listaProveedor(instance, status, filtro).get(0).getRfc()));

        TipoAgente tipoAgente = this.provedorBD.listaProveedor(instance, status, filtro).get(0).getTipoAgente();
        this.interfaz.jComboTipo.setSelectedIndex(0);
        this.interfaz.jComboTipo.addItem(tipoAgente);
        this.interfaz.jComboTipo.setSelectedItem(tipoAgente);

        interfaz.txtPais.setText(String.valueOf(this.provedorBD.listaProveedor(instance, status, filtro).get(0).getPais()));
        interfaz.txtEstado.setText(String.valueOf(this.provedorBD.listaProveedor(instance, status, filtro).get(0).getEstado()));
        interfaz.txtPoblacion.setText(String.valueOf(this.provedorBD.listaProveedor(instance, status, filtro).get(0).getPoblacion()));
        interfaz.txtColonia.setText(String.valueOf(this.provedorBD.listaProveedor(instance, status, filtro).get(0).getColonia()));
        interfaz.txtCalle.setText(String.valueOf(this.provedorBD.listaProveedor(instance, status, filtro).get(0).getCalle()));
        interfaz.txtCodigoPostal.setText(String.valueOf(this.provedorBD.listaProveedor(instance, status, filtro).get(0).getCodigoPostal()));
        interfaz.txtCelular.setText(String.valueOf(this.provedorBD.listaProveedor(instance, status, filtro).get(0).getCelular()));
        interfaz.txtTelefono.setText(String.valueOf(this.provedorBD.listaProveedor(instance, status, filtro).get(0).getTelefono()));
        interfaz.txtCorreo.setText(String.valueOf(this.provedorBD.listaProveedor(instance, status, filtro).get(0).getCorreo()));
        interfaz.txtlimitecredito.setText(String.valueOf(this.provedorBD.listaProveedor(instance, status, filtro).get(0).getLimiteCredito()));
        interfaz.txtdiasplazo.setText(String.valueOf(this.provedorBD.listaProveedor(instance, status, filtro).get(0).getDiasPlazo()));
        interfaz.txtsaldo.setText(String.valueOf(this.provedorBD.listaProveedor(instance, status, filtro).get(0).getSaldo()));
    }

    public void mtdELiminar() {
        int numeroFila = interfaz.jtDatos.getSelectedRow();
        if (numeroFila >= 0) {
            instance.setClave(Integer.parseInt(interfaz.jtDatos.getValueAt(numeroFila, 0).toString()));
            int numero = JOptionPane.showConfirmDialog(null, "Desea eliminar la fila seleccionada");
            if (numero == 0) {
                this.provedorBD.mtdEliminar(instance);
                this.interfaz.txtBuscarClave.grabFocus();
            }
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione una fila");
        }
    }

    public void mdtActualizar() {
        String clave = this.interfaz.txtClave.getText();
        String nombre = this.interfaz.txtrfc.getText();
        if (clave.length() >= 1 && nombre.length() >= 1) {
            instance.setClave(Integer.parseInt(this.interfaz.txtClave.getText()));
            mtdCacharDatosTXT();
            int respuesta = provedorBD.mtdActualizar(instance);
            if (respuesta >= 1) {
                JOptionPane.showMessageDialog(null, "Actualizado correctamente");
                mtdLimpiarTxt();
                int itemCount = this.interfaz.jComboTipo.getItemCount();
                for (int i = 0; i < itemCount; i++) {
                    this.interfaz.jComboTipo.removeItemAt(0);
                }
                mtdInicar();
                this.interfaz.btnSalir.setVisible(true);
                this.interfaz.txtBuscarClave.grabFocus();
            }
        } else {
            JOptionPane.showMessageDialog(null, "Campos obligatios vacios", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void mtdBuscarProveedor(JTable table, String buscar, JRadioButton activoB, JRadioButton inactivoB, JRadioButton todoB, JComboBox combo) {
        String filtro = "";
        String status = "";
        if (activoB.isSelected() == true) {
            status = "1";
        }
        if (inactivoB.isSelected() == true) {
            status = "0";
        }
        if (todoB.isSelected() == true) {
            status = "0,1";
        }
        if (combo.getSelectedIndex() == 0) {
            filtro = "todo";
            mtdLlenarTabla(table, instance, status, filtro);
        }
        if (combo.getSelectedIndex() == 1) {
            filtro = "clave";
            if (buscar.length() > 0) {
                try {
                    int clave = Integer.parseInt(buscar);
                    if (clave > 0) {
                        this.instance.setClave(clave);
                        mtdLlenarTabla(table, instance, status, filtro);
                    } else {
                        JOptionPane.showMessageDialog(null, "Ingrese una clave valida", "Advertencia", JOptionPane.WARNING_MESSAGE);
                        mtdLlenarTabla(table, instance, "2", filtro);
                    }
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, "Clave Invalida    =" + e, "Error", JOptionPane.ERROR_MESSAGE);
                    mtdLlenarTabla(table, instance, "2", filtro);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Ingrese una clave ", "Advertencia", JOptionPane.WARNING_MESSAGE);
            }
        }
        if (combo.getSelectedIndex() == 2) {
            filtro = "razon";
            if (buscar.length() > 0) {
                this.instance.setNombre(buscar);
                mtdLlenarTabla(table, instance, status, filtro);
            } else {
                JOptionPane.showMessageDialog(null, "Ingrese una Razón Social valida", "Advertencia", JOptionPane.WARNING_MESSAGE);

            }
        }
    }
    //*Agregar filtro para el  excel

    public void AgregarFiltro() {
        selecArchivo.setFileFilter(new FileNameExtensionFilter("Excel (*.xls)", "xls"));
        selecArchivo.setFileFilter(new FileNameExtensionFilter("Excel (*.xlsx)", "xlsx"));
    }

    protected String mtdExportarExcel(File archivo) {
        String respuesta = "No se pudo importar correctamente";
        String status = "";
        String filtro = "";
        if (this.interfaz.jComboBoxRangoRegistroExportarExcel.getSelectedIndex() == 0) {
            filtro = "todo";
        }
        if (this.interfaz.jComboBoxRangoRegistroExportarExcel.getSelectedIndex() == 1) {
            filtro = "clave";
        }
        if (this.interfaz.jComboBoxStatusExportarExcel.getSelectedIndex() == 0) {
            status = "1";
        }
        if (this.interfaz.jComboBoxStatusExportarExcel.getSelectedIndex() == 1) {
            status = "0";
        }
        if (this.interfaz.jComboBoxStatusExportarExcel.getSelectedIndex() == 2) {
            status = "0,1";
        }
        boolean imprimirClave = Boolean.parseBoolean(this.interfaz.jTDatosExportarExcel.getValueAt(0, 2).toString());
        boolean imprimirStatus = Boolean.parseBoolean(this.interfaz.jTDatosExportarExcel.getValueAt(1, 2).toString());
        boolean imprimirRZ = Boolean.parseBoolean(this.interfaz.jTDatosExportarExcel.getValueAt(2, 2).toString());
        boolean imprimirRFC = Boolean.parseBoolean(this.interfaz.jTDatosExportarExcel.getValueAt(3, 2).toString());
        boolean imprimirPais = Boolean.parseBoolean(this.interfaz.jTDatosExportarExcel.getValueAt(4, 2).toString());
        boolean imprimirEstado = Boolean.parseBoolean(this.interfaz.jTDatosExportarExcel.getValueAt(5, 2).toString());
        boolean imprimirPoblacion = Boolean.parseBoolean(this.interfaz.jTDatosExportarExcel.getValueAt(6, 2).toString());
        //Se crea un libro excel
        XSSFWorkbook libro = new XSSFWorkbook();
        // Se crea una hoja dentro del libro
        XSSFSheet hoja = libro.createSheet();
        // Se crea una fila dentro de la hoja
        XSSFRow fila = hoja.createRow(0);
        // Se crea una celda dentro de la fila
        XSSFCell celda = fila.createCell((short) 0);
        int i = 0;
        int j = 0;
        j++;
        ArrayList vList[];
        fila = hoja.createRow(j);
        List<String> ejemploLista = new ArrayList<>();
        if (imprimirClave == true) {
            ejemploLista.add("CLAVE");
        }
        if (imprimirStatus == true) {
            ejemploLista.add("STATUS");
        }
        if (imprimirRZ == true) {
            ejemploLista.add("RAZÓN SOCIAL");
        }
        if (imprimirRFC == true) {
            ejemploLista.add("RFC");
        }
        if (imprimirPais == true) {
            ejemploLista.add("PAIS");
        }
        if (imprimirEstado == true) {
            ejemploLista.add("ESTADO");
        }
        if (imprimirPoblacion == true) {
            ejemploLista.add("POBALCION");
        }
        for (i = 0; i < ejemploLista.size(); i++) {
            celda = fila.createCell(i);
            celda.setCellValue(ejemploLista.get(i));
        }
        j++;
        fila = hoja.createRow(j);
        int numRegistro = this.provedorBD.listaProveedor(instance, status, filtro).size();
        for (int ii = 0; ii < numRegistro; ii++) {
            if (imprimirClave == true) {
                celda = fila.createCell(0);
                celda.setCellValue(provedorBD.listaProveedor(instance, status, filtro).get(ii).getClave());
            }
            if (imprimirStatus == true) {
                celda = fila.createCell(1);
                celda.setCellValue(provedorBD.listaProveedor(instance, status, filtro).get(ii).getStatus());
            }
            if (imprimirRZ == true) {
                celda = fila.createCell(2);
                celda.setCellValue(provedorBD.listaProveedor(instance, status, filtro).get(ii).getNombre());
            }
            if (imprimirRFC == true) {
                celda = fila.createCell(3);
                celda.setCellValue(provedorBD.listaProveedor(instance, status, filtro).get(ii).getRfc());
            }
            if (imprimirPais == true) {
                celda = fila.createCell(4);
                celda.setCellValue(provedorBD.listaProveedor(instance, status, filtro).get(ii).getPais());
            }
            if (imprimirEstado == true) {
                celda = fila.createCell(5);
                celda.setCellValue(provedorBD.listaProveedor(instance, status, filtro).get(ii).getEstado());
            }
            if (imprimirPoblacion == true) {
                celda = fila.createCell(6);
                celda.setCellValue(provedorBD.listaProveedor(instance, status, filtro).get(ii).getPoblacion());
            }
            j++;
            fila = hoja.createRow(j);
        }
        hoja.autoSizeColumn(0);
        hoja.autoSizeColumn(1);
        hoja.autoSizeColumn(2);
        hoja.autoSizeColumn(3);
        hoja.autoSizeColumn(4);
        hoja.autoSizeColumn(5);
        hoja.autoSizeColumn(6);
        respuesta = "Importado Correctamente";
        try {
            libro.write(new FileOutputStream(archivo));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return respuesta;
    }
}
