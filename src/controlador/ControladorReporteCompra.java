/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Agente;
import modelo.AgenteDAO;
import modelo.Conexion;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;
import vista.JFIReportesCompra;

/**
 *
 * @author CURIEL
 */
public class ControladorReporteCompra implements ActionListener {

    JFIReportesCompra interfaz = new JFIReportesCompra();
    Agente instance=new Agente();
    AgenteDAO instanceDAO=new AgenteDAO();
    Conexion conexion;
    static Connection conn = null;
    String rutaReporte = "C:\\pos\\reporte";

    public ControladorReporteCompra(JFIReportesCompra interfaz) {
        this.interfaz = interfaz;
        this.interfaz.btnGenerarPDF.addActionListener(this);
        mtdCargarAgente();
    }

    public ControladorReporteCompra() {
    }
   public void mtdCargarAgente() {
         ArrayList<Agente> agente = this.instanceDAO.mtdRellenarAgente(instance, "1", "todo");
        for (Agente tipoAgente : agente) {
            this.interfaz.jComboBoxEmpleados.addItem(tipoAgente);
        }
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.interfaz.btnGenerarPDF) {///
            if (this.interfaz.jRadioButtonCompraPorArticulo.isSelected()) {
                mtdreporteCompraPorArticulo();
            }
            if (this.interfaz.jRadioButtonCompraPorProveedor.isSelected()) {
                reporteCompraporProveedor();
            }
//            if (this.interfaz.jRadioButtonComprasPorTipoPago.isSelected()) {
//            }
        }
    }
        private void mtdreporteCompraPorArticulo(){
                try {
                String fechaInicial = new SimpleDateFormat("yyyy-MM-dd").format(interfaz.jDfechainicial.getDate());
                String fechaFinal = new SimpleDateFormat("yyyy-MM-dd").format(interfaz.jDfechafinal.getDate());
                String tipo = "'Articulo','Servicio'";
                String status="1,2";
                if (interfaz.jComboBoxTipoArticulo.getSelectedIndex() == 1) {
                    tipo = "'Articulo'";
                }
                if (interfaz.jComboBoxTipoArticulo.getSelectedIndex() == 2) {
                    tipo = "'Servicio'";
                }
                if(interfaz.jComboBoxStatus.getSelectedIndex()==1){
                 status="1";
                }
                if(interfaz.jComboBoxStatus.getSelectedIndex()==1){
                 status="2";
                }
                Map parametro = new HashMap();
                 String script=" movimiento.`serie_folio_clave`='OC'";
                if(!this.interfaz.jRadioButtonTodosAgente.isSelected()){
                    Agente agente = (Agente) interfaz.jComboBoxEmpleados.getSelectedItem();
                   script+=" and movimiento.agente_clave="+agente.getClave();
                } 
                parametro.put("agente", script);
                parametro.put("fechaInicial", fechaInicial);
                parametro.put("fechaFinal", fechaFinal);
                parametro.put("tipo", tipo);
                parametro.put("status",status);
                JasperPrint jasperPrint = null;
                conexion = new Conexion();
                jasperPrint = JasperFillManager.fillReport(this.rutaReporte+"\\reporteArticuloCompraPeriodo.jasper", parametro, conexion.getConexion());
                JasperViewer ventanaVisor = new JasperViewer(jasperPrint, false);
                ventanaVisor.setTitle("Reporte Compras por Articulo");
                ventanaVisor.setVisible(true);
            } catch (JRException ex) {
                System.out.println("errrr" + ex);
                Logger.getLogger(ControladorReporteVenta.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ControladorReporteVenta.class.getName()).log(Level.SEVERE, null, ex);
            }
            conexion.cerrarConexio();

    }
        
      private void reporteCompraporProveedor() {
        try {
            String fechaInicial = new SimpleDateFormat("yyyy-MM-dd").format(interfaz.jDfechainicial.getDate());
            String fechaFinal = new SimpleDateFormat("yyyy-MM-dd").format(interfaz.jDfechafinal.getDate());
            Map parametro = new HashMap();
            parametro.put("clave", 10);
            parametro.put("fechaInicial", fechaInicial);
            parametro.put("fechaFinal", fechaFinal);
            JasperPrint jasperPrint = null;
            conexion = new Conexion();
            jasperPrint = JasperFillManager.fillReport(this.rutaReporte+"\\reporteCompraPorProveedor.jasper", parametro, conexion.getConexion());
            JasperViewer ventanaVisor = new JasperViewer(jasperPrint, false);
            ventanaVisor.setTitle("Reporte de Compra por proveedor");
            ventanaVisor.setVisible(true);
        } catch (JRException ex) {
            System.out.println("error" + ex);
            Logger.getLogger(ControladorReporteVenta.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ControladorReporteVenta.class.getName()).log(Level.SEVERE, null, ex);
        }
        conexion.cerrarConexio();
    }

    public void mtdImprimirNota(int folio, String serie) {
        try {
            Map parametro = new HashMap();
            parametro.put("serie", serie);
            parametro.put("folio", folio);
            JasperPrint jasperPrint = null;
            conexion = new Conexion();
            jasperPrint = JasperFillManager.fillReport("C:\\pos\\reporte\\oc.jasper", parametro, conexion.getConexion());
            JasperViewer ventanaVisor = new JasperViewer(jasperPrint, false);
            ventanaVisor.setTitle("Nota de Compra");
            ventanaVisor.setVisible(true);
        } catch (JRException ex) {
            System.out.println("errrr" + ex);
            Logger.getLogger(ControladorReporteVenta.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ControladorReporteVenta.class.getName()).log(Level.SEVERE, null, ex);
        }
        conexion.cerrarConexio();
    }

}
