/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Conexion;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;
import vista.JFReporteInventario;

/**
 *
 * @author CURIEL
 */
public class ControladorReporteInventario implements ActionListener  {
     JFReporteInventario interfaz = new JFReporteInventario();
    Conexion conexion;
    static Connection conn = null;

    public ControladorReporteInventario(JFReporteInventario interfaz) {
        this.interfaz=interfaz;
        this.interfaz.btnGenrarReporteInv.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
                if (e.getSource() == this.interfaz.btnGenrarReporteInv) {///
             try {
                String tipo="'Articulo'";              
                Map parametro = new HashMap();
                parametro.put("almacen", "ALMG");
                parametro.put("tipo",tipo);
                JasperPrint jasperPrint = null;
                 conexion = new Conexion();
                jasperPrint = JasperFillManager.fillReport("C:\\pos\\reporte\\reporteInventarioTotalPorAlmacen.jasper", parametro, conexion.getConexion());
                JasperViewer ventanaVisor = new JasperViewer(jasperPrint, false);
                ventanaVisor.setTitle("Inventario");
                ventanaVisor.setVisible(true);
            } catch (JRException ex) {
                System.out.println("errrr" + ex);
                Logger.getLogger(ControladorReporteVenta.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(ControladorReporteVenta.class.getName()).log(Level.SEVERE, null, ex);
            }
            conexion.cerrarConexio();
        }
    }
    
    
}
