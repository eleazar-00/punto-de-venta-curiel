/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileReader;

import java.io.IOException;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Agente;
import modelo.AgenteDAO;
import modelo.Conexion;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

import vista.JFIReportesVenta;

/**
 *
 * @author CURIEL
 */
public class ControladorReporteVenta implements ActionListener {

    JFIReportesVenta interfaz = new JFIReportesVenta();
    Agente instance = new Agente();
    AgenteDAO instanceDAO = new AgenteDAO();
    Conexion conexion;
    static Connection conn = null;
    String rutaReporte = "C:\\pos\\reporte";

    public ControladorReporteVenta(JFIReportesVenta interfaz) {
        this.interfaz = interfaz;
        this.interfaz.btnGenerar.addActionListener(this);
        mtdCargarAgente();
    }

    ControladorReporteVenta() {
    }

    void cargarPropiedades() throws FileNotFoundException, IOException {
        Properties pro = new Properties();
        FileReader reader = new FileReader("Config.properties");
        pro.load(reader);
        this.rutaReporte = pro.getProperty("rutaReporte");
    }

    public void mtdCargarAgente() {
        ArrayList<Agente> agente = this.instanceDAO.mtdRellenarAgente(instance, "1", "todo");
        for (Agente tipoAgente : agente) {
            this.interfaz.jComboBoxEmpleados.addItem(tipoAgente);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.interfaz.btnGenerar) {///
            if (this.interfaz.jRadioButtonVentasPorArticulo.isSelected()) {
                mtdreporteVentaPorArticulo();
            }
            if (this.interfaz.jRadioButtonVentasPorCliente.isSelected()) {
                reporteventaporcliente();

            }
            if (this.interfaz.jRadioButtonVentasPorTipoPago.isSelected()) {
                reporteVentaPorTipoPago();
            }
            if (this.interfaz.jRadioButtonRetiros.isSelected()) {
                reporteRetiroEfectivo();
            }
            if (this.interfaz.jRadioButtonPorLinea.isSelected()) {
                reporteventaporlinea();
            }
        }
    }

    private void mtdreporteVentaPorArticulo() {
        try {
            String fechaInicial = new SimpleDateFormat("yyyy-MM-dd").format(interfaz.jDfechainicial.getDate());
            String fechaFinal = new SimpleDateFormat("yyyy-MM-dd").format(interfaz.jDfechafinal.getDate());
            String tipo = "'Articulo','Servicio'";
            String status = "'1','2','0'";
            if (interfaz.jComboBoxTipoArticulo.getSelectedIndex() == 1) {
                tipo = "'Articulo'";
            }
            if (interfaz.jComboBoxTipoArticulo.getSelectedIndex() == 2) {
                tipo = "'Servicio'";
            }
            if (interfaz.jComboBoxStatus.getSelectedIndex() == 1) {
                status = "'1'";
            }
            if (interfaz.jComboBoxStatus.getSelectedIndex() == 2) {
                status = "'2'";
            }
            Map parametro = new HashMap();
            String script = " movimiento.`serie_folio_clave`='TK'";
            if (!this.interfaz.jRadioButtonTodosAgente.isSelected()) {
                Agente agente = (Agente) interfaz.jComboBoxEmpleados.getSelectedItem();
                script += " and movimiento.agente_clave=" + agente.getClave();
            }
            parametro.put("agente", script);
            parametro.put("fechaInicial", fechaInicial);
            parametro.put("fechaFinal", fechaFinal);
            parametro.put("tipo", tipo);
            parametro.put("status", status);
            JasperPrint jasperPrint = null;
            conexion = new Conexion();
            jasperPrint = JasperFillManager.fillReport(this.rutaReporte + "\\reporteArticuloVentaPeriodo.jasper", parametro, conexion.getConexion());
            JasperViewer ventanaVisor = new JasperViewer(jasperPrint, false);
            ventanaVisor.setTitle("LISTADO DE SERVICIOS");
            ventanaVisor.setVisible(true);
        } catch (JRException ex) {
            System.out.println("errrr" + ex);
            Logger.getLogger(ControladorReporteVenta.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ControladorReporteVenta.class.getName()).log(Level.SEVERE, null, ex);
        }
        conexion.cerrarConexio();
    }

    public void mtdImprimirNota(int folio, String serie) {
        try {
            Map parametro = new HashMap();
            parametro.put("serie", serie);
            parametro.put("folio", folio);
            JasperPrint jasperPrint = null;
            conexion = new Conexion();
            jasperPrint = JasperFillManager.fillReport(this.rutaReporte + "\\tk.jasper", parametro, conexion.getConexion());
            JasperViewer ventanaVisor = new JasperViewer(jasperPrint, false);
            ventanaVisor.setTitle("Nota de venta");
            ventanaVisor.setVisible(true);
        } catch (JRException ex) {
            System.out.println("errrr" + ex);
            Logger.getLogger(ControladorReporteVenta.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ControladorReporteVenta.class.getName()).log(Level.SEVERE, null, ex);
        }
        conexion.cerrarConexio();
    }

    private void reporteArticuloRotacion() {
        try {
            String fechaInicial = new SimpleDateFormat("yyyy-MM-dd").format(interfaz.jDfechainicial.getDate());
            String fechaFinal = new SimpleDateFormat("yyyy-MM-dd").format(interfaz.jDfechafinal.getDate());
            Map parametro = new HashMap();
            parametro.put("serie", "TK");
            parametro.put("fechaInicial", fechaInicial);
            parametro.put("fechaFinal", fechaFinal);
            JasperPrint jasperPrint = null;
            conexion = new Conexion();
            jasperPrint = JasperFillManager.fillReport(this.rutaReporte + "\\reporteRotacionInventario.jasper", parametro, conexion.getConexion());
            JasperViewer ventanaVisor = new JasperViewer(jasperPrint, false);
            ventanaVisor.setTitle("Reporte Retacion de Inventario");
            ventanaVisor.setVisible(true);
        } catch (JRException ex) {
            System.out.println("errrr" + ex);
            Logger.getLogger(ControladorReporteVenta.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ControladorReporteVenta.class.getName()).log(Level.SEVERE, null, ex);
        }
        conexion.cerrarConexio();
    }

    private void reporteventaporcliente() {
        try {
            String fechaInicial = new SimpleDateFormat("yyyy-MM-dd").format(interfaz.jDfechainicial.getDate());
            String fechaFinal = new SimpleDateFormat("yyyy-MM-dd").format(interfaz.jDfechafinal.getDate());
            Map parametro = new HashMap();
            parametro.put("clave", 01);
            parametro.put("fechaInicial", fechaInicial);
            parametro.put("fechaFinal", fechaFinal);
            JasperPrint jasperPrint = null;
            conexion = new Conexion();
            jasperPrint = JasperFillManager.fillReport(this.rutaReporte + "\\reporteVentaPorCliente.jasper", parametro, conexion.getConexion());
            JasperViewer ventanaVisor = new JasperViewer(jasperPrint, false);
            ventanaVisor.setTitle("Reporte de venta por cliente");
            ventanaVisor.setVisible(true);
        } catch (JRException ex) {
            System.out.println("error" + ex);
            Logger.getLogger(ControladorReporteVenta.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ControladorReporteVenta.class.getName()).log(Level.SEVERE, null, ex);
        }
        conexion.cerrarConexio();
    }

    private void reporteVentaPorTipoPago() {
        try {
            String fechaInicial = new SimpleDateFormat("yyyy-MM-dd").format(interfaz.jDfechainicial.getDate());
            String fechaFinal = new SimpleDateFormat("yyyy-MM-dd").format(interfaz.jDfechafinal.getDate());
            Map parametro = new HashMap();
            parametro.put("fechaInicial", fechaInicial);
            parametro.put("fechaFinal", fechaFinal);
            String script = "movimiento.entradaysalida_clave=01";
            if (!this.interfaz.jRadioButtonTodosAgente.isSelected()) {
                Agente agente = (Agente) interfaz.jComboBoxEmpleados.getSelectedItem();
                script += " and movimiento.agente_clave=" + agente.getClave();
            }
            parametro.put("agente", script);
            JasperPrint jasperPrint = null;
            conexion = new Conexion();
            jasperPrint = JasperFillManager.fillReport(this.rutaReporte + "\\reporteVentaPorTipoPago.jasper", parametro, conexion.getConexion());
            JasperViewer ventanaVisor = new JasperViewer(jasperPrint, false);
            ventanaVisor.setTitle("Reporte de venta por tipo de Pago");
            System.out.println(ventanaVisor);
            ventanaVisor.setVisible(true);
        } catch (JRException ex) {
            System.out.println("error" + ex);
            Logger.getLogger(ControladorReporteVenta.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ControladorReporteVenta.class.getName()).log(Level.SEVERE, null, ex);
        }
        conexion.cerrarConexio();
    }

    private void reporteRetiroEfectivo() {
        try {
            String fechaInicial = new SimpleDateFormat("yyyy-MM-dd").format(interfaz.jDfechainicial.getDate());
            String fechaFinal = new SimpleDateFormat("yyyy-MM-dd").format(interfaz.jDfechafinal.getDate());
            Map parametro = new HashMap();
            parametro.put("fechaInicial", fechaInicial);
            parametro.put("fechaFinal", fechaFinal);
            String script = "retiro.`status`=1";
            if (!this.interfaz.jRadioButtonTodosAgente.isSelected()) {
                Agente agente = (Agente) interfaz.jComboBoxEmpleados.getSelectedItem();
                script += " and retiro.`agente_clave`=" + agente.getClave();
            }
            System.out.print(script);
            parametro.put("agente", script);
            JasperPrint jasperPrint = null;
            conexion = new Conexion();
            jasperPrint = JasperFillManager.fillReport(this.rutaReporte + "\\retiros.jasper", parametro, conexion.getConexion());
            JasperViewer ventanaVisor = new JasperViewer(jasperPrint, false);
            ventanaVisor.setTitle("Reporte de Retiro en Efectivo");
            System.out.println(ventanaVisor);
            ventanaVisor.setVisible(true);
        } catch (JRException ex) {
            System.out.println("error" + ex);
            Logger.getLogger(ControladorReporteVenta.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ControladorReporteVenta.class.getName()).log(Level.SEVERE, null, ex);
        }
        conexion.cerrarConexio();
    }

    private void reporteventaporlinea() {
        try {
            String fechaInicial = new SimpleDateFormat("yyyy-MM-dd").format(interfaz.jDfechainicial.getDate());
            String fechaFinal = new SimpleDateFormat("yyyy-MM-dd").format(interfaz.jDfechafinal.getDate());
            Map parametro = new HashMap();
            String scriptAgente=" movimiento.`status`=1";
             if (!this.interfaz.jRadioButtonTodosAgente.isSelected()) {
                Agente agente = (Agente) interfaz.jComboBoxEmpleados.getSelectedItem();
                scriptAgente += " AND movimiento.`agente_clave`=" + agente.getClave();
            }
            parametro.put("serie", "TK");
            parametro.put("fechaInicial", fechaInicial);
            parametro.put("fechaFinal", fechaFinal);
            parametro.put("almacen", "ALMG");
            parametro.put("agente",scriptAgente);
            JasperPrint jasperPrint = null;
            conexion = new Conexion();
            jasperPrint = JasperFillManager.fillReport(this.rutaReporte + "\\reporteVentaPorLinea.jasper", parametro, conexion.getConexion());
            JasperViewer ventanaVisor = new JasperViewer(jasperPrint, false);
            ventanaVisor.setTitle("Reporte de venta por linea");
            ventanaVisor.setVisible(true);
        } catch (JRException ex) {
            System.out.println("error" + ex);
            Logger.getLogger(ControladorReporteVenta.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ControladorReporteVenta.class.getName()).log(Level.SEVERE, null, ex);
        }
        conexion.cerrarConexio();
    }

}
