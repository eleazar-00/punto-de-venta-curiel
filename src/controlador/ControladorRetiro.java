/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.Toolkit;
import java.awt.event.*;
import java.io.File;
import java.io.FileOutputStream;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import javax.swing.JTable;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import modelo.Retiro;
import modelo.RetiroDAO;
  
 
/**
 *
 * @author eleazar
 */
public class ControladorRetiro implements ActionListener, KeyListener, MouseListener {
    RetiroDAO instanceDAO = new RetiroDAO();
    Retiro instance = new Retiro();
    DefaultTableModel modeloT = new DefaultTableModel();
    int rpt;

    public ControladorRetiro(RetiroDAO instanceDAO) {
         this.instanceDAO = instanceDAO; 
     }
    public ControladorRetiro(){
     }
    
     public void mtdCabezeraTable(JTable table) {
        table.setModel(modeloT);
        //nombre de la cabecera de la tabla
        modeloT.addColumn("Id");
        modeloT.addColumn("Serie");
        modeloT.addColumn("Folio");
        modeloT.addColumn("Concepto");
        modeloT.addColumn("Cantidad");
    }

    public void mtdLimparTable(JTable table) {
        int numeroFila = table.getRowCount();
        modeloT = (DefaultTableModel) table.getModel();
        for (int j = 0; j < numeroFila; j++) {
            modeloT.removeRow(0);
        }
    }

     public void mtdLlenarTable(JTable table,Retiro instance) {
        mtdLimparTable(table);
        Object[] columna = new Object[5];
        int numRegistro = instanceDAO.lista(instance).size();
        ///metodo para rellenar la tabla con los datos/////
        for (int i = 0; i < numRegistro; i++) {
            columna[0] = instanceDAO.lista(instance).get(i).getId();
            columna[1] = instanceDAO.lista(instance).get(i).getSerie();
            columna[2] = instanceDAO.lista(instance).get(i).getFolio();
            columna[3] = instanceDAO.lista(instance).get(i).getConcepto();
            columna[4] = instanceDAO.lista(instance).get(i).getCantidad();
            modeloT.addRow(columna);
        }
    }
 
    
     

    @Override
    public void actionPerformed(ActionEvent e) {
      
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        
    }

    @Override
    public void keyReleased(KeyEvent e) {
       
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

     
    
}
