/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import modelo.EntradaySalida;
import modelo.EntradaySalidaDAO;
import modelo.SeriesyFolios;
import modelo.SeriesyFoliosDAO;
import vista.JFISeriesyFolios;

/**
 *
 * @author eleazar
 */
public class ControladorSeriesyFolios implements ActionListener, KeyListener {

    JFISeriesyFolios interfaz = new JFISeriesyFolios();
    SeriesyFolios instance = new SeriesyFolios();
    SeriesyFoliosDAO instanceDAO = new SeriesyFoliosDAO();

    EntradaySalida instaceEys = new EntradaySalida();
    EntradaySalidaDAO instaceEysDAO = new EntradaySalidaDAO();
    ValidarCampos valiarJTextField = new ValidarCampos();
    DefaultTableModel modelT = new DefaultTableModel();
    int i = 0;
    int numeroFilas = 0;
    String filtro = "todo";

    public ControladorSeriesyFolios(JFISeriesyFolios interfaz, SeriesyFoliosDAO instanceDAO) {
        this.interfaz = interfaz;
        this.instanceDAO = instanceDAO;
        this.interfaz.btnNuevo.addActionListener(this);
        this.interfaz.btnEditar.addActionListener(this);
        this.interfaz.btnAceptar.addActionListener(this);
        this.interfaz.btnActualizar.addActionListener(this);
        this.interfaz.btnCancelar.addActionListener(this);
        this.interfaz.btnEliminar.addActionListener(this);
        this.interfaz.btnSalir.addActionListener(this);

        this.interfaz.txtBuscar.addKeyListener(this);
        this.interfaz.txtClave.addKeyListener(this);
        this.interfaz.txtfolio.addKeyListener(this);
        this.interfaz.txtNombre.addKeyListener(this);
        this.interfaz.jTableSeriesyFolios.addKeyListener(this);
        mtdCabezera(this.interfaz.jTableSeriesyFolios);
        mtdIniciar();
    }

    public ControladorSeriesyFolios() {
    }

    private void mtdIniciar() {
        this.interfaz.btnNuevo.setVisible(true);
        this.interfaz.btnEditar.setVisible(true);
        this.interfaz.btnAceptar.setVisible(false);
        this.interfaz.btnActualizar.setVisible(false);
        this.interfaz.btnCancelar.setVisible(false);
        this.interfaz.btnEliminar.setVisible(true);
        this.interfaz.btnSalir.setVisible(true);
        this.interfaz.jTabbedPaneSeriesyFolios.setSelectedIndex(0);
        this.interfaz.jTabbedPaneSeriesyFolios.setEnabledAt(0, true);
        this.interfaz.jTabbedPaneSeriesyFolios.setEnabledAt(1, false);
        mtdLimpiarTxt();
        mtdElimimarjcomboboxEyS();
    }

    private void mtdCabezera(JTable table) {
        table.setModel(modelT);
        modelT.addColumn("Clave");
        modelT.addColumn("Nombre");
        modelT.addColumn("Folio");
        modelT.addColumn("Movimiento");
    }

    public void mtdLlenarTable(JTable table, SeriesyFolios instance, String filtro) {
        mtdLimpirTable(table);
        modelT = (DefaultTableModel) table.getModel();
        Object[] columna = new Object[4];
        numeroFilas = this.instanceDAO.lista(instance, filtro,"").size();
        for (i = 0; i < numeroFilas; i++) {
            columna[0] = this.instanceDAO.lista(instance, filtro,"").get(i).getClave();
            columna[1] = this.instanceDAO.lista(instance, filtro,"").get(i).getNombre();
            columna[2] = this.instanceDAO.lista(instance, filtro,"").get(i).getFolio();
            columna[3] = this.instanceDAO.lista(instance, filtro,"").get(i).getEntradaysalida().getNombre();
            modelT.addRow(columna);
        }

    }

    private void mtdLimpirTable(JTable table) {
        numeroFilas = table.getRowCount();
        modelT = (DefaultTableModel) table.getModel();
        for (i = 0; i < numeroFilas; i++) {
            modelT.removeRow(0);
        }
    }

    private void mtdCacharTxt() {
        this.instance.setClave(this.interfaz.txtClave.getText());
        this.instance.setFolio(Integer.parseInt(this.interfaz.txtfolio.getText()));
        this.instance.setNombre(this.interfaz.txtNombre.getText());
        this.instance.setFormato(this.interfaz.txtFormato.getText());
        instaceEys = (EntradaySalida) this.interfaz.jComboBoxClaveEntradaYsalidad.getSelectedItem();
        this.instance.setEntradaysalida(instaceEys);
        int tipo = 1;
        if (this.interfaz.jRbventa.isSelected() == true) {
            tipo = 1;
        }
        if (this.interfaz.jRbcomprar.isSelected() == true) {
            tipo = 2;
        }
        if (this.interfaz.jRbminternos.isSelected() == true) {
            tipo = 3;
        }
        this.instance.setTipo(tipo);
    }

    private void mtdLimpiarTxt() {
        this.interfaz.txtClave.setText("");
        this.interfaz.txtfolio.setText("0");
        this.interfaz.txtNombre.setText("");
        this.interfaz.txtFormato.setText("");
    }

    private void mtdNuevoEditar() {
        this.interfaz.btnNuevo.setVisible(false);
        this.interfaz.btnEditar.setVisible(false);
        this.interfaz.btnCancelar.setVisible(true);
        this.interfaz.btnEliminar.setVisible(false);
        this.interfaz.btnSalir.setVisible(false);
        this.interfaz.jTabbedPaneSeriesyFolios.setSelectedIndex(1);
        this.interfaz.jTabbedPaneSeriesyFolios.setEnabledAt(0, false);
        this.interfaz.jTabbedPaneSeriesyFolios.setEnabledAt(1, true);
    }

    private void mtdBtnNuevoEditar(String btnAceptar) {
        mtdCargarComboMovimiento();
        if (btnAceptar == "Nuevo") {
            mtdNuevoEditar();
            this.interfaz.jTabbedPaneSeriesyFolios.setTitleAt(1, "Nuevo");
            this.interfaz.btnAceptar.setVisible(true);///btn Aceptar
            this.interfaz.txtClave.grabFocus();
            this.interfaz.txtClave.setEditable(true);
        }
        if (btnAceptar == "Editar" || btnAceptar == "Eliminar") {
            int numerofila = this.interfaz.jTableSeriesyFolios.getSelectedRow();
            if (numerofila >= 0) {
                this.instance.setClave(this.interfaz.jTableSeriesyFolios.getValueAt(numerofila, 0).toString());
                if (btnAceptar == "Editar") {
                    mtdNuevoEditar();
                    this.interfaz.jTabbedPaneSeriesyFolios.setTitleAt(1, "Editar");
                    filtro = "clave";
                    this.interfaz.btnActualizar.setVisible(true);///btn Editar
                    this.interfaz.txtNombre.grabFocus();
                    this.interfaz.txtClave.setEditable(false);
                    this.interfaz.txtClave.setText(this.instanceDAO.lista(instance, filtro,"").get(0).getClave());
                    this.interfaz.txtfolio.setText(String.valueOf(this.instanceDAO.lista(instance, filtro,"").get(0).getFolio()));
                    this.interfaz.txtNombre.setText(this.instanceDAO.lista(instance, filtro,"").get(0).getNombre());
                    this.interfaz.txtFormato.setText(this.instanceDAO.lista(instance, filtro,"").get(0).getFormato());

                    instaceEys = this.instanceDAO.lista(instance, filtro,"").get(0).getEntradaysalida();
                    this.interfaz.jComboBoxClaveEntradaYsalidad.setSelectedIndex(0);
                    this.interfaz.jComboBoxClaveEntradaYsalidad.addItem(instaceEys);
                    this.interfaz.jComboBoxClaveEntradaYsalidad.setSelectedItem(instaceEys);
                    int tipo = this.instanceDAO.lista(instance, filtro,"").get(0).getTipo();
                    if (tipo == 3) {
                        this.interfaz.jRbminternos.setSelected(true);
                    }
                    if (tipo == 2) {
                        this.interfaz.jRbcomprar.setSelected(true);
                    }
                    if (tipo == 1) {
                        this.interfaz.jRbventa.setSelected(true);
                    }
                }
                if (btnAceptar == "Eliminar") {
                    int rptUsuario = JOptionPane.showConfirmDialog(null, "Esta seguro de Eliminar la Serie");
                    if (rptUsuario == 0) {
                        this.instanceDAO.mtdEliminar(instance);
                        this.interfaz.txtBuscar.grabFocus();
                        instance.setClave("");
                        this.mtdLlenarTable(this.interfaz.jTableSeriesyFolios, instance, "todo");
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "Seleccione una fila");
                this.interfaz.txtBuscar.grabFocus();
            }
        }
    }

    private void mtdAceptarActualizar(String btn) {
        mtdCacharTxt();
        if (btn.equals("Actualizar")) {
            int respuesta = this.instanceDAO.mtdActualizar(instance);
            if (respuesta >= 1) {
                JOptionPane.showMessageDialog(null, "Actualizado Correctamente");
                mtdIniciar();
                instance.setClave("");
                String filtro = "todo";
                this.mtdLlenarTable(this.interfaz.jTableSeriesyFolios, instance, filtro);
            }
        }
        if (btn.equals("Aceptar")) {
            String respuesta = this.instanceDAO.mtdInserta(instance);
            if (respuesta.equals("Registrado correctamente")) {
                JOptionPane.showMessageDialog(null, respuesta);
                mtdLimpiarTxt();
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.interfaz.btnNuevo) {
            mtdBtnNuevoEditar("Nuevo");
        }
        if (e.getSource() == this.interfaz.btnEditar) {
            mtdBtnNuevoEditar("Editar");
        }
        if (e.getSource() == this.interfaz.btnAceptar) {
            mtdAceptarActualizar("Aceptar");
        }
        if (e.getSource() == this.interfaz.btnActualizar) {
            mtdAceptarActualizar("Actualizar");
        }
        if (e.getSource() == this.interfaz.btnEliminar) {
            mtdBtnNuevoEditar("Eliminar");
        }
        if (e.getSource() == this.interfaz.btnCancelar) {
            mtdIniciar();
        }
        if (e.getSource() == this.interfaz.btnSalir) {
            this.interfaz.dispose();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        if (e.getSource().equals(this.interfaz.txtClave)) {
            valiarJTextField.mtdvalidarCantidadCaracteres(e, this.interfaz.txtClave, 10);
        }
        if (e.getSource().equals(this.interfaz.txtfolio)) {
            valiarJTextField.mtdvalidarSoloNumeros(e);
        }
        if (e.getSource().equals(this.interfaz.txtNombre)) {
            valiarJTextField.mtdvalidarCantidadCaracteres(e, this.interfaz.txtNombre, 25);
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            if (e.getSource() == this.interfaz.txtBuscar) {
                if (this.interfaz.txtBuscar.getText().length() == 0) {
                    this.mtdLlenarTable(this.interfaz.jTableSeriesyFolios, instance, "todo");
                } else {
                    this.instance.setClave(this.interfaz.txtBuscar.getText());
                    this.mtdLlenarTable(this.interfaz.jTableSeriesyFolios, instance, "clave");
                }
            }
            if (e.getSource() == this.interfaz.jTableSeriesyFolios) {
                mtdBtnNuevoEditar("Editar");
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    public void mtdCargarComboMovimiento() {
        ArrayList<EntradaySalida> entradasysalidas = this.instaceEysDAO.listaTipoEYS(instaceEys, "todo");
        for (EntradaySalida entradasysalida : entradasysalidas) {
            this.interfaz.jComboBoxClaveEntradaYsalidad.addItem(entradasysalida);
        }
    }

    protected void mtdElimimarjcomboboxEyS() {
        int itemCount = this.interfaz.jComboBoxClaveEntradaYsalidad.getItemCount();
        for (int i = 0; i < itemCount; i++) {
            this.interfaz.jComboBoxClaveEntradaYsalidad.removeItemAt(0);
        }
    }
}
