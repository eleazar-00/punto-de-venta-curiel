/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import modelo.TipoAgente;
import modelo.TipoAgenteDAO;
import vista.JFITipoAgente;

/**
 *
 * @author mary
 */
public class ControladorTipoAgente implements ActionListener, KeyListener {

    JFITipoAgente vista = new JFITipoAgente();
    TipoAgenteDAO bdTipoAgente = new TipoAgenteDAO();
    TipoAgente instance = new TipoAgente();
    DefaultTableModel modeloT = new DefaultTableModel();

    ////constructor 
    public ControladorTipoAgente(JFITipoAgente vista, TipoAgenteDAO bdTipoAgente) {
        this.vista = vista;
        this.bdTipoAgente = bdTipoAgente;
        this.vista.btnNuevo.addActionListener(this);
        this.vista.btnAceptar.addActionListener(this);
        this.vista.btnEditar.addActionListener(this);
        this.vista.btnActualizar.addActionListener(this);
        this.vista.btnCancelar.addActionListener(this);
        this.vista.btnEliminar.addActionListener(this);
        this.vista.btnSalir.addActionListener(this);

        this.vista.txtBuscarClave.addKeyListener(this);
        this.vista.jRadionEmpleado.addActionListener(this);
        this.vista.jRadionEmpleado.addKeyListener(this);
        mtdRellenarCabecera(vista.jtDatos);
        mtdIniciar();
    }

    public ControladorTipoAgente() {
    }
     
    private void mtdRellenarCabecera(JTable tableD) {
        tableD.setModel(modeloT);
        //nombre de la cabecera de la tabla
        modeloT.addColumn("CLAVE TIPO");
        modeloT.addColumn("NOMBRE TIPO");
        modeloT.addColumn("TIPO");
    }

    protected void mtdRellenarTabla(JTable tableD,String tipo) {
        mtdLimparTable(tableD);
        Object[] columna = new Object[3];
         int numRegistro = bdTipoAgente.listaTipoAgente(tipo).size();
        ///metodo para rellenar la tabla con los datos/////
        for (int i = 0; i < numRegistro; i++) {
            columna[0] = bdTipoAgente.listaTipoAgente(tipo).get(i).getClave();
            columna[1] = bdTipoAgente.listaTipoAgente(tipo).get(i).getNombre();
            columna[2] = bdTipoAgente.listaTipoAgente(tipo).get(i).getTipo();
            modeloT.addRow(columna);
        }
    }

    public void mtdLimparTable(JTable tableD) {
        int numeroFila = tableD.getRowCount();
        modeloT = (DefaultTableModel) tableD.getModel();
        for (int j = 0; j < numeroFila; j++) {
            modeloT.removeRow(0);
        }
    }

    private void mtdIniciar() {
        mtdLimpiarTXT();
        this.vista.btnNuevo.setVisible(true);
        this.vista.btnEditar.setVisible(true);
        this.vista.btnAceptar.setVisible(false);
        this.vista.btnActualizar.setVisible(false);
        this.vista.btnCancelar.setVisible(false);
        this.vista.btnEliminar.setVisible(true);
        this.vista.btnSalir.setVisible(true);
        this.vista.jTabbedPaneTA.setEnabledAt(0, true);
        this.vista.jTabbedPaneTA.setEnabledAt(1, false);
        this.vista.jTabbedPaneTA.setSelectedIndex(0);
    }

    private void mtdLimpiarTXT() {
        vista.txtClaveTipAgente.setText("");
        vista.txtNombreTipAgente.setText("");
    }

    private void mtdCacharTXT() {
        String tipo = null;
        if (this.vista.jRadionEmpleado.isSelected()) {
            tipo = "E";
        }
        if (this.vista.jRadioProveedor.isSelected()) {
            tipo = "P";
        }
        if (this.vista.jRadioCliente.isSelected()) {
            tipo = "C";
        }
        instance.setClave(this.vista.txtClaveTipAgente.getText());
        instance.setNombre(this.vista.txtNombreTipAgente.getText());
        instance.setTipo(tipo);
    }

    private void mtdbtnNuevoEditar(String btn) {
        mtdLimpiarTXT();
        this.vista.btnNuevo.setVisible(false);
        this.vista.btnEditar.setVisible(false);
        this.vista.btnCancelar.setVisible(true);
        this.vista.btnEliminar.setVisible(false);
        this.vista.btnSalir.setVisible(false);
        this.vista.jTabbedPaneTA.setEnabledAt(0, false);
        this.vista.jTabbedPaneTA.setEnabledAt(1, true);
        this.vista.jTabbedPaneTA.setSelectedIndex(1);
        if (btn == "btnNuevo") {
            this.vista.btnAceptar.setVisible(true);
            this.vista.jTabbedPaneTA.setTitleAt(1, "Nuevo");
            this.vista.txtClaveTipAgente.setEditable(true);
            this.vista.txtClaveTipAgente.grabFocus();
        }
        if (btn == "btnEditar") {
            this.vista.btnActualizar.setVisible(true);
            this.vista.jTabbedPaneTA.setTitleAt(1, "Editar");
            this.vista.txtClaveTipAgente.setEditable(false);
            this.vista.txtNombreTipAgente.grabFocus();
        }
    }

    private void btnAceptarActualizar(String btn) {
        if (this.vista.txtClaveTipAgente.getText().length() <= 0) {
            JOptionPane.showMessageDialog(null, "Campo Clave vacio", "Error", JOptionPane.ERROR_MESSAGE);
            this.vista.txtClaveTipAgente.grabFocus();
        } else if (this.vista.txtNombreTipAgente.getText().length() <=0) {
            JOptionPane.showMessageDialog(null, "Campo Nombre vacio ", "Error", JOptionPane.ERROR_MESSAGE);
            this.vista.txtNombreTipAgente.grabFocus();
        } else {
            if ("btnAceptar".equals(btn)) {
                mtdCacharTXT();
                String respuesta = this.bdTipoAgente.mtdInsertar(instance);
                JOptionPane.showMessageDialog(null, respuesta);
                this.mtdLimpiarTXT();
            }
            if ("btnActualizar".equals(btn)) {
                mtdCacharTXT();
                int respEdit = bdTipoAgente.mtdActualizar(instance);
                if (respEdit > 0) {
                    JOptionPane.showMessageDialog(null, "Actualizado Correctamente");
                    this.mtdIniciar();
                } else {
                    JOptionPane.showMessageDialog(null, "No se puedo realizar la edicion");
                    this.vista.txtNombreTipAgente.grabFocus();
                }
            }
        }
    }

    private void btnEditar() {
        int filaEditar = vista.jtDatos.getSelectedRow();
        int numFS = vista.jtDatos.getSelectedRowCount();
        if (filaEditar >= 0 && numFS == 1) {
            mtdbtnNuevoEditar("btnEditar");
            String tipo = String.valueOf(vista.jtDatos.getValueAt(filaEditar, 2));
            if (tipo.equals("E")) {
                this.vista.jRadionEmpleado.setSelected(true);
            } else if (tipo.equals("C")) {
                this.vista.jRadioCliente.setSelected(true);
            } else if (tipo.equals("P")) {
                this.vista.jRadioProveedor.setSelected(true);
            }
            vista.txtClaveTipAgente.setText(String.valueOf(vista.jtDatos.getValueAt(filaEditar, 0)));
            vista.txtNombreTipAgente.setText(String.valueOf(vista.jtDatos.getValueAt(filaEditar, 1)));
        } else {
            JOptionPane.showMessageDialog(null, "Debe seleccinar almenos una fila");
        }
    }

    private void mtdEliminar() {
        int fila = vista.jtDatos.getSelectedRow();
        if (fila >= 0) {
            this.instance.setClave(this.vista.jtDatos.getValueAt(fila, 0).toString());
            int respUser = JOptionPane.showConfirmDialog(null, "Desea eliminar la fila seleccionada");
            if (respUser == 0) {
                this.bdTipoAgente.mtdEliminar(instance);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Selecciona una fila para eliminar");
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.vista.btnNuevo) {
            mtdbtnNuevoEditar("btnNuevo");
        }
        if (e.getSource() == vista.btnAceptar) {
            btnAceptarActualizar("btnAceptar");
        }
        if (e.getSource() == vista.btnEditar) {
            btnEditar();
        }
        if (e.getSource() == vista.btnActualizar) {
            btnAceptarActualizar("btnActualizar");
        }
        if (e.getSource() == vista.btnCancelar) {
            this.mtdIniciar();
        }
        if (e.getSource() == vista.btnEliminar) {
            mtdEliminar();
        }
        if (e.getSource() == vista.btnSalir) {
            vista.dispose();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getSource() == this.vista.txtBuscarClave) {
            String tipo = "";
            mtdRellenarTabla(vista.jtDatos,tipo);
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
}
