/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import modelo.TipoPago;
import modelo.TipoPagoDAO;
import vista.JFITipoPago;

/**
 *
 * @author eleazar
 */
public class ControladorTipoPago implements ActionListener, KeyListener {

    TipoPagoDAO instanceDAO = new TipoPagoDAO();
    TipoPago instance = new TipoPago();
    JFITipoPago interfaz = new JFITipoPago();
    ValidarCampos InstanceVC=new ValidarCampos();
    DefaultTableModel modeloT = new DefaultTableModel();

    public ControladorTipoPago(JFITipoPago interfaz, TipoPagoDAO instanceDAO) {
        this.interfaz = interfaz;
        this.instanceDAO = instanceDAO;

        this.interfaz.btnAceptar.addActionListener(this);
        this.interfaz.btnNuevo.addActionListener(this);
        this.interfaz.btnCancelar.addActionListener(this);
        this.interfaz.btnEditar.addActionListener(this);
        this.interfaz.btnActualizar.addActionListener(this);
        this.interfaz.btnEliminar.addActionListener(this);
        this.interfaz.btnSalir.addActionListener(this);

        this.interfaz.txtBuscar.addKeyListener(this);
        this.interfaz.jTableDatos.addKeyListener(this);

        this.interfaz.btnEditar.addKeyListener(this);
        this.interfaz.btnCancelar.addKeyListener(this);
        this.interfaz.btnEliminar.addKeyListener(this);
        this.interfaz.btnActualizar.addKeyListener(this);
        this.interfaz.btnAceptar.addKeyListener(this);
        this.interfaz.btnNuevo.addKeyListener(this);
        this.interfaz.btnSalir.addKeyListener(this);

        this.interfaz.txtClave.addKeyListener(this);
        this.interfaz.txtNombre.addKeyListener(this);

        this.interfaz.jTabbedPaneNuevo.setEnabledAt(1, false);
        mtdCabezaraTable(this.interfaz.jTableDatos);
        mtdDesactivarBtnIniciar();

    }
    public void mtdCabezaraTable(JTable table) {
        table.setModel(modeloT);
        //nombre de la cabecera de la tabla
        modeloT.addColumn("CLAVE");
        modeloT.addColumn("NOMBRE");
        modeloT.addColumn("NUM PAG2");

    }
    public void mtdLimparTable() {
        int numeroFila = this.interfaz.jTableDatos.getRowCount();
        modeloT = (DefaultTableModel) interfaz.jTableDatos.getModel();
        for (int j = 0; j < numeroFila; j++) {
            modeloT.removeRow(0);
        }
    }
    public void mtdRellenarTabla(JTable table) {
        mtdLimparTable();
        Object[] columna = new Object[3];
        int numRegistro = instanceDAO.mtdLista().size();
        ///metodo para rellenar la tabla con los datos/////
        for (int i = 0; i < numRegistro; i++) {
            columna[0] = instanceDAO.mtdLista().get(i).getClave();
            columna[1] = instanceDAO.mtdLista().get(i).getNombre();
            columna[2] = instanceDAO.mtdLista().get(i).getNum_pag();
            modeloT.addRow(columna);
        }
    }
    public void mtdDesactivarBtnIniciar() {
        this.interfaz.btnActualizar.setVisible(false);
        this.interfaz.btnAceptar.setVisible(false);
        this.interfaz.btnCancelar.setVisible(false);
        this.interfaz.btnSalir.setVisible(true);
    }

    public void mtdBtnNuevoEditar() {
        this.interfaz.jTabbedPaneNuevo.setSelectedIndex(1);
        this.interfaz.jTabbedPaneNuevo.setEnabledAt(0, false);
        this.interfaz.jTabbedPaneNuevo.setEnabledAt(1, true);
        this.interfaz.txtClave.grabFocus();
        this.interfaz.btnEliminar.setVisible(false);
        this.interfaz.btnCancelar.setVisible(true);
        this.interfaz.btnNuevo.setVisible(false);
        this.interfaz.btnEditar.setVisible(false);
        this.interfaz.btnSalir.setVisible(false);
    }

    public void mtdBtnCancelarActualizar() {
        mtdLimpiarTxt();
        mtdDesactivarBtnIniciar();
        this.interfaz.jTabbedPaneNuevo.setSelectedIndex(0);
        this.interfaz.jTabbedPaneNuevo.setEnabledAt(1, false);
        this.interfaz.jTabbedPaneNuevo.setEnabledAt(0, true);
        this.interfaz.txtBuscar.grabFocus();
        this.interfaz.btnNuevo.setVisible(true);
        this.interfaz.btnEliminar.setVisible(true);
        this.interfaz.btnEditar.setVisible(true);
    }

    public void mtdLimpiarTxt() {
        this.interfaz.txtBuscar.setText("");
        this.interfaz.txtClave.setText("");
        this.interfaz.txtnumeropago2.setText("");
        this.interfaz.txtNombre.setText("");
    }

    public void btnEditar() {
        int numeroFila = this.interfaz.jTableDatos.getSelectedRow();
        if (numeroFila >= 0) {
            this.interfaz.txtNombre.grabFocus();
            this.interfaz.txtClave.setText(String.valueOf(this.interfaz.jTableDatos.getValueAt(numeroFila, 0)));
            this.interfaz.txtNombre.setText(String.valueOf(this.interfaz.jTableDatos.getValueAt(numeroFila, 1)));
            this.interfaz.txtnumeropago2.setText(String.valueOf(this.interfaz.jTableDatos.getValueAt(numeroFila, 2)));
            this.interfaz.jTabbedPaneNuevo.setTitleAt(1, "Editar");
            this.interfaz.btnActualizar.setVisible(true);
            this.interfaz.txtClave.setEditable(false);
            mtdBtnNuevoEditar();
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione un fila");
            this.interfaz.txtBuscar.grabFocus();
        }

    }

    public void btnNuevo() {
        this.interfaz.jTabbedPaneNuevo.setTitleAt(1, "Nuevo");
        this.interfaz.btnAceptar.setVisible(true);
        this.interfaz.txtClave.setEditable(true);
        mtdBtnNuevoEditar();
    }

    public void btnAceptar() {
        if (this.interfaz.txtNombre.getText().length() <= 1) {
            this.interfaz.txtNombre.setBackground(Color.red);
            this.interfaz.txtNombre.grabFocus();
        } else {
            this.interfaz.txtNombre.setBackground(Color.white);
            this.instance.setClave(this.interfaz.txtClave.getText());
            this.instance.setNombre(this.interfaz.txtNombre.getText());
            this.instance.setNum_pag(Integer.parseInt(this.interfaz.txtnumeropago2.getText()));
            String rpt = instanceDAO.mtdInsertar(instance);
            mtdLimpiarTxt();
            this.interfaz.txtClave.grabFocus();
            JOptionPane.showMessageDialog(null, rpt);
        }
    }

    public void btnActualizar() {
        if (this.interfaz.txtClave.getText().length() == 1) {
            this.interfaz.txtClave.setBackground(Color.red);
            this.interfaz.txtClave.grabFocus();
        } else {
            this.interfaz.txtClave.setBackground(Color.white);
            if (this.interfaz.txtNombre.getText().length() <= 1) {
                this.interfaz.txtNombre.setBackground(Color.red);
                this.interfaz.txtNombre.grabFocus();
            } else {
                this.instance.setClave(this.interfaz.txtClave.getText());
                this.instance.setNombre(this.interfaz.txtNombre.getText());
                int rpt = instanceDAO.mtdActualizar(instance);
                if (rpt == 1) {
                    JOptionPane.showMessageDialog(null, "Actualizado Correctamente");
                    mtdBtnCancelarActualizar();
                } else {
                    JOptionPane.showMessageDialog(null, "Error desconocido al Actualizar", "Error", JOptionPane.ERROR_MESSAGE);

                }
            }
        }

    }

    public void btnEliminar() {
        int numeroFila = this.interfaz.jTableDatos.getSelectedRow();
        if (numeroFila >= 0) {
            this.instance.setClave(String.valueOf(this.interfaz.jTableDatos.getValueAt(numeroFila, 0)));
            int rpt = JOptionPane.showConfirmDialog(null, "Esta seguro de eliminar la forma de pago seleccionado");
            if (rpt == 0) {
                this.instanceDAO.mtdEliminar(instance);
                mtdRellenarTabla(this.interfaz.jTableDatos);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione un fila");
            this.interfaz.txtBuscar.grabFocus();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == this.interfaz.btnNuevo) {
            btnNuevo();
        }
        if (e.getSource() == this.interfaz.btnEditar) {
            btnEditar();
        }

        if (e.getSource() == this.interfaz.btnAceptar) {
             btnAceptar();
        }
        if (e.getSource() == this.interfaz.btnActualizar) {
            btnActualizar();
        }
        if (e.getSource() == this.interfaz.btnCancelar) {
            mtdBtnCancelarActualizar();
        }
        if (e.getSource() == this.interfaz.btnEliminar) {
            btnEliminar();
        }
        if (e.getSource() == this.interfaz.btnSalir) {
            this.interfaz.dispose();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            if (e.getSource() == this.interfaz.btnNuevo) {
                btnNuevo();
                this.interfaz.txtClave.grabFocus();
            }
            if (e.getSource() == this.interfaz.btnEditar) {
                btnEditar();
            }
            if (e.getSource() == this.interfaz.btnCancelar) {
                mtdBtnCancelarActualizar();
            }
            if (e.getSource() == this.interfaz.btnAceptar) {
                btnAceptar();
            }
            if (e.getSource() == this.interfaz.btnActualizar) {
                btnActualizar();
            }
            if (e.getSource() == this.interfaz.btnEliminar) {
                btnEliminar();
            }
            if (e.getSource() == this.interfaz.txtBuscar) {
                mtdRellenarTabla(this.interfaz.jTableDatos);
            }
            if (e.getSource() == this.interfaz.jTableDatos) {
                btnEditar();
            }
        }
        if (e.getKeyCode() == KeyEvent.VK_DELETE) {
            if (e.getSource() == this.interfaz.jTableDatos) {
                btnEliminar();
            }
        }
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            this.interfaz.dispose();
        }
        if (e.getKeyCode() == KeyEvent.VK_CONTROL) {
            btnNuevo();
        }

    }
    @Override
    public void keyReleased(KeyEvent e) {
        String letra;
        if (e.getSource() == this.interfaz.txtClave) {
            InstanceVC.mtdconvertirMayusculas(e);
            letra = this.interfaz.txtClave.getText().toUpperCase();
            interfaz.txtClave.setText(letra);
  
        }
        if (e.getSource() == this.interfaz.txtNombre) {
            InstanceVC.mtdconvertirMayusculas(e);
            letra= this.interfaz.txtNombre.getText().toUpperCase();
            interfaz.txtNombre.setText(letra);
        }
    }

     
}
