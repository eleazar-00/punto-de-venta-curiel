/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import modelo.Ubicacion;
import modelo.UbicacionDAO;
import vista.JFIUbicacion;
 

/**
 *
 * @author ELEAZAR
 */
public class ControladorUbicacion implements ActionListener, KeyListener{
    Ubicacion instance = new Ubicacion();
    UbicacionDAO instanceDAO = new UbicacionDAO();
    JFIUbicacion interfaz = new JFIUbicacion();
    DefaultTableModel modeloT = new DefaultTableModel();

    public ControladorUbicacion(JFIUbicacion interfaz,UbicacionDAO instanceDAO) {
        this.interfaz = interfaz;
        this.instanceDAO = instanceDAO;
        this.interfaz.btnAceptar.addActionListener(this);
        this.interfaz.btnNuevo.addActionListener(this);
        this.interfaz.btnActualizar.addActionListener(this);
        this.interfaz.btnCancelar.addActionListener(this);
        this.interfaz.btnEditar.addActionListener(this);
        this.interfaz.btnEliminar.addActionListener(this);
        this.interfaz.btnSalir.addActionListener(this);
        this.interfaz.txtBuscar.addKeyListener(this);
        mtdIniciar();
        mtdRellenarCabecera(this.interfaz.jtable);
    }

    ControladorUbicacion() {
    }
 private void mtdRellenarCabecera(JTable tableD) {
        tableD.setModel(modeloT);
        //nombre de la cabecera de la tabla
        modeloT.addColumn("CLAVE ");
        modeloT.addColumn("UBICACION");
    }

    public void mtdRellenarTabla(JTable tableD,Ubicacion instance) {
        mtdLimparTable(tableD);
        Object[] columna = new Object[3];
        int numRegistro = instanceDAO.lista(instance).size();
        ///metodo para rellenar la tabla con los datos/////
        for (int i = 0; i < numRegistro; i++) {
            columna[0] = instanceDAO.lista(instance).get(i).getClave();
            columna[1] = instanceDAO.lista(instance).get(i).getNombre();
            modeloT.addRow(columna);
        }
    }

    public void mtdLimparTable(JTable tableD) {
        int numeroFila = tableD.getRowCount();
        modeloT = (DefaultTableModel) tableD.getModel();
        for (int j = 0; j < numeroFila; j++) {
            modeloT.removeRow(0);
        }
    }

    private void mtdIniciar() {
        mtdlimpiar();
        this.interfaz.btnNuevo.setVisible(true);
        this.interfaz.btnEditar.setVisible(true);
        this.interfaz.btnAceptar.setVisible(false);
        this.interfaz.btnActualizar.setVisible(false);
        this.interfaz.btnEliminar.setVisible(true);
        this.interfaz.btnCancelar.setVisible(false);
        this.interfaz.btnSalir.setVisible(true);
        this.interfaz.jTabbedPaneMarca.setEnabledAt(0, true);
        this.interfaz.jTabbedPaneMarca.setEnabledAt(1, false);
        this.interfaz.jTabbedPaneMarca.setSelectedIndex(0);
    }

    private void mtdlimpiar() {
        this.interfaz.txtclave.setText("");
        this.interfaz.txtnombre.setText("");
    }

    private void mtdbtnNuevoEditar(String btn) {
        mtdlimpiar();
        this.interfaz.btnNuevo.setVisible(false);
        this.interfaz.btnEditar.setVisible(false);
        this.interfaz.btnCancelar.setVisible(true);
        this.interfaz.btnEliminar.setVisible(false);
        this.interfaz.btnSalir.setVisible(false);
        this.interfaz.jTabbedPaneMarca.setEnabledAt(0, false);
        this.interfaz.jTabbedPaneMarca.setEnabledAt(1, true);
        this.interfaz.jTabbedPaneMarca.setSelectedIndex(1);
        if (btn == "btnNuevo") {
            this.interfaz.btnAceptar.setVisible(true);
            this.interfaz.jTabbedPaneMarca.setTitleAt(1, "Nuevo");
            this.interfaz.txtclave.setEditable(true);
            this.interfaz.txtclave.grabFocus();
        }
        if (btn == "btnEditar") {
            this.interfaz.btnActualizar.setVisible(true);
            this.interfaz.jTabbedPaneMarca.setTitleAt(1, "Editar");
            this.interfaz.txtclave.setEditable(false);
            this.interfaz.txtnombre.grabFocus();
         }
    }

    private void mtdCacharTxt() {
        instance.setClave(this.interfaz.txtclave.getText());
        instance.setNombre(this.interfaz.txtnombre.getText());
    }

    private void btnAceptarActualizar(String btn) {
        if (this.interfaz.txtclave.getText().length() <= 0) {
            JOptionPane.showMessageDialog(null, "Campo Clave vacio", "Error", JOptionPane.ERROR_MESSAGE);
            this.interfaz.txtclave.grabFocus();
        } else if (this.interfaz.txtnombre.getText().length() <= 0) {
            JOptionPane.showMessageDialog(null, "Campo Nombre vacio ", "Error", JOptionPane.ERROR_MESSAGE);
            this.interfaz.txtnombre.grabFocus();
        } else {
            if ("btnAceptar".equals(btn)) {
                mtdCacharTxt();
                this.instanceDAO.mtdInsertar(instance);
                this.interfaz.txtclave.grabFocus();
                 this.mtdlimpiar();
            }
            if ("btnActualizar".equals(btn)) {
                mtdCacharTxt();
                this.instanceDAO.mtdActualizar(instance);
                this.mtdIniciar();
            }
        }
    }
    private void btnEditarEliminar(String btn){
        int fila=this.interfaz.jtable.getSelectedRow();
        if(fila>=0){
            if(btn.equals("btnEliminar")){
              int rptU=JOptionPane.showConfirmDialog(null,"Deseas eliminar la fila seleccionada");
              if(rptU==0){
              this.instance.setClave(this.interfaz.jtable.getValueAt(fila, 0).toString());
              this.instanceDAO.mtdEliminar(instance);
              }
            }
            if(btn.equals("btnEditar")){
                mtdbtnNuevoEditar(btn);
                this.interfaz.txtclave.setText(this.interfaz.jtable.getValueAt(fila,0).toString());
                this.interfaz.txtnombre.setText(this.interfaz.jtable.getValueAt(fila,1).toString());
            }
        }else{
        JOptionPane.showMessageDialog(null,"Seleccione una fila");
        }
    }

    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.interfaz.btnNuevo) {
            this.mtdbtnNuevoEditar("btnNuevo");
        }
        if (e.getSource() == this.interfaz.btnEditar) {
            this.btnEditarEliminar("btnEditar");
        }
        if (e.getSource() == this.interfaz.btnCancelar) {
            this.mtdIniciar();
        }
        if (e.getSource() == this.interfaz.btnAceptar) {
            btnAceptarActualizar("btnAceptar");
        }
        if (e.getSource() == this.interfaz.btnActualizar) {
            btnAceptarActualizar("btnActualizar");
        }
        if(e.getSource()==this.interfaz.btnEliminar){
            btnEditarEliminar("btnEliminar");
        }
        if(e.getSource()==this.interfaz.btnSalir){
            this.interfaz.dispose();
        }
        
    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getSource() == this.interfaz.txtBuscar) {
            mtdRellenarTabla(interfaz.jtable,instance);
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
    }
   
}
