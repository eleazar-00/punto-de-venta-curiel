/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.Toolkit;
import java.awt.event.*;
import java.io.File;
import java.io.FileOutputStream;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import javax.swing.JTable;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import modelo.Unidad;
import modelo.UnidadDAO;

import vista.JFIUnidad;

/**
 *
 * @author eleazar
 */
public class ControladorUnidad implements ActionListener, KeyListener, MouseListener {

    JFIUnidad interfaz = new JFIUnidad();
    UnidadDAO instanceDAO = new UnidadDAO();
    Unidad instance = new Unidad();
    ValidarCampos InstanceVC = new ValidarCampos();
    DefaultTableModel modeloT = new DefaultTableModel();
    int rpt;

    public ControladorUnidad(JFIUnidad interfaz, UnidadDAO instanceDAO) {
        this.interfaz = interfaz;
        this.instanceDAO = instanceDAO;
        this.interfaz.btnNuevo.addActionListener(this);
        this.interfaz.btnEditar.addActionListener(this);
        this.interfaz.btnAceptar.addActionListener(this);
        this.interfaz.btnActualizar.addActionListener(this);
        this.interfaz.btnEliminar.addActionListener(this);
        this.interfaz.btnCancelar.addActionListener(this);
        this.interfaz.btnSalir.addActionListener(this);
//        this.interfaz.btnVistaPrevia.addActionListener(this);

        this.interfaz.btnNuevo.addKeyListener(this);
        this.interfaz.btnEditar.addKeyListener(this);
        this.interfaz.btnAceptar.addKeyListener(this);
        this.interfaz.btnActualizar.addKeyListener(this);
        this.interfaz.btnEliminar.addKeyListener(this);
        this.interfaz.btnCancelar.addKeyListener(this);
        this.interfaz.btnSalir.addKeyListener(this);
//        this.interfaz.btnVistaPrevia.addKeyListener(this);
        this.interfaz.txtBusca.addKeyListener(this);
        this.interfaz.jTDatos.addKeyListener(this);

        this.interfaz.jTDatos.addMouseListener(this);
        mtdCabezeraTable(this.interfaz.jTDatos);
        mtdIniciar();
    }
    public ControladorUnidad(){
     }
    
     public void mtdCabezeraTable(JTable table) {
        table.setModel(modeloT);
        //nombre de la cabecera de la tabla
        modeloT.addColumn("CLAVE");
        modeloT.addColumn("NOMBRE");
        modeloT.addColumn("UNIDAD POR PAQUETE");
        modeloT.addColumn("1 / UNIDAD POR PAQUETE ");
    }

    public void mtdLimparTable(JTable table) {
        int numeroFila = table.getRowCount();
        modeloT = (DefaultTableModel) table.getModel();
        for (int j = 0; j < numeroFila; j++) {
            modeloT.removeRow(0);
        }
    }

     public void mtdLlenarTable(JTable table,Unidad instance) {
        mtdLimparTable(table);
        Object[] columna = new Object[4];
        int numRegistro = instanceDAO.mtdLista(instance).size();
        ///metodo para rellenar la tabla con los datos/////
        for (int i = 0; i < numRegistro; i++) {
            columna[0] = instanceDAO.mtdLista(instance).get(i).getClave();
            columna[1] = instanceDAO.mtdLista(instance).get(i).getNombre();
            columna[2] = instanceDAO.mtdLista(instance).get(i).getCantidadPaquete();
            columna[3] = instanceDAO.mtdLista(instance).get(i).getUnidadEntreCantidadPaquete();
            modeloT.addRow(columna);
        }
    }

    private void mtdLimpiarTxt() {
        this.interfaz.txtClave.setText("");
        this.interfaz.txtNombre.setText("");
        this.interfaz.txtCantidadPaquete.setText("");
        this.interfaz.txtUnoEntreCantidad.setText("");
    }

    private void mtdIniciar() {
        mtdLimpiarTxt();
        this.interfaz.btnNuevo.setVisible(true);
        this.interfaz.btnEditar.setVisible(true);
        this.interfaz.btnAceptar.setVisible(false);
        this.interfaz.btnActualizar.setVisible(false);
        this.interfaz.btnEliminar.setVisible(true);
        this.interfaz.btnCancelar.setVisible(false);
        this.interfaz.btnSalir.setVisible(true);
//        this.interfaz.btnVistaPrevia.setVisible(true);
        this.interfaz.jTabbedPaneUnidad.setEnabledAt(0, true);
        this.interfaz.jTabbedPaneUnidad.setEnabledAt(1, false);
        this.interfaz.jTabbedPaneUnidad.setSelectedIndex(0);
    }

    private void mtdCacharTxt() {
        this.instance.setClave(this.interfaz.txtClave.getText());
        this.instance.setNombre(this.interfaz.txtNombre.getText());
        this.instance.setCantidadPaquete(Float.parseFloat(this.interfaz.txtCantidadPaquete.getText()));
        this.instance.setUnidadEntreCantidadPaquete(Float.parseFloat(this.interfaz.txtUnoEntreCantidad.getText()));
    }

    private void mtdBtnNuevoEditar() {
        this.interfaz.btnNuevo.setVisible(false);
        this.interfaz.btnEditar.setVisible(false);
        this.interfaz.btnEliminar.setVisible(false);
        this.interfaz.btnCancelar.setVisible(true);
        this.interfaz.btnSalir.setVisible(false);
//        this.interfaz.btnVistaPrevia.setVisible(false);
        this.interfaz.jTabbedPaneUnidad.setEnabledAt(1, true);
        this.interfaz.jTabbedPaneUnidad.setEnabledAt(0, false);
        this.interfaz.jTabbedPaneUnidad.setSelectedIndex(1);
    }

    private void mtdBtnNuevo() {
        mtdBtnNuevoEditar();
        this.interfaz.btnAceptar.setVisible(true); //btn nuevo
        this.interfaz.jTabbedPaneUnidad.setTitleAt(1, "Nuevo");//btn Nuevo
        this.interfaz.txtClave.setEditable(true);
        this.interfaz.txtClave.grabFocus();
    }

    private void mtdBtnEditarEliminar(String btn) {
        int numeroFila = this.interfaz.jTDatos.getSelectedRow();
        if (numeroFila >= 0) {
            instance.setClave(this.interfaz.jTDatos.getValueAt(numeroFila, 0).toString());
            if (btn == "Editar") {
                mtdBtnNuevoEditar();
                this.interfaz.btnActualizar.setVisible(true);//btn editar
                this.interfaz.jTabbedPaneUnidad.setTitleAt(1, "Editar");//btn Editar
                this.interfaz.txtClave.setEditable(false);
                this.interfaz.txtNombre.grabFocus();
                this.interfaz.txtClave.setText(this.instanceDAO.mtdLista(instance).get(0).getClave());
                this.interfaz.txtNombre.setText(this.instanceDAO.mtdLista(instance).get(0).getNombre());
                this.interfaz.txtCantidadPaquete.setText(String.valueOf(this.instanceDAO.mtdLista(instance).get(0).getCantidadPaquete()));
                this.interfaz.txtUnoEntreCantidad.setText(String.valueOf(this.instanceDAO.mtdLista(instance).get(0).getUnidadEntreCantidadPaquete()));
            }
            if (btn == "Eliminar") {
                rpt = JOptionPane.showConfirmDialog(null, "Desea eliminar la fila seleccionada");
                if (rpt == 0) {
                    this.instanceDAO.mtdEliminar(instance);
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, "Selecione una fila");
        }
    }

    private void mtdBtnAceptarActualizar(String btn) {
        if (this.interfaz.txtClave.getText().length() <= 0) {
            mtdCampoVasio("Clave");
            this.interfaz.txtClave.grabFocus();
        }
        if (this.interfaz.txtNombre.getText().length() <= 0) {
            mtdCampoVasio("Nombre");
            this.interfaz.txtNombre.grabFocus();
        }
        if (this.interfaz.txtCantidadPaquete.getText().length() <= 0) {
            mtdCampoVasio("Cantidad Paquete");
            this.interfaz.txtCantidadPaquete.grabFocus();
        }
        if (this.interfaz.txtUnoEntreCantidad.getText().length() <= 0) {
            mtdCampoVasio("Uno entre Cantidad Paquete");
            this.interfaz.txtUnoEntreCantidad.grabFocus();
        } else {
            if (btn == "Aceptar") {
                mtdCacharTxt();
                String respuesta = this.instanceDAO.mtdInsertar(instance);
                JOptionPane.showMessageDialog(null, respuesta);
                this.mtdLimpiarTxt();
                this.interfaz.txtClave.grabFocus();
            }
            if (btn == "Actualizar") {
                mtdCacharTxt();
                rpt = this.instanceDAO.mtdActualizar(instance);
                if (rpt == 1) {
                    JOptionPane.showMessageDialog(null, "Actualizado Correctamente");
                    mtdIniciar();
                }
            }
        }
    }

    private void mtdCampoVasio(String dato) {
        JOptionPane.showMessageDialog(null, "Campo " + dato + " es necesario", "Error", JOptionPane.ERROR_MESSAGE);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == this.interfaz.btnNuevo) {
            mtdBtnNuevo();
        }
        if (e.getSource() == this.interfaz.btnEditar) {
            mtdBtnEditarEliminar("Editar");
        }
        if (e.getSource() == this.interfaz.btnAceptar) {
            mtdBtnAceptarActualizar("Aceptar");
        }
        if (e.getSource() == this.interfaz.btnActualizar) {
            mtdBtnAceptarActualizar("Actualizar");
        }
        if (e.getSource() == this.interfaz.btnEliminar) {
            mtdBtnEditarEliminar("Eliminar");
        }
        if (e.getSource() == this.interfaz.btnCancelar) {
            mtdIniciar();
        }
        if (e.getSource() == this.interfaz.btnSalir) {
            this.interfaz.dispose();
        }
//        if (e.getSource() == this.interfaz.btnVistaPrevia) {
//            mtdbtnImprimirCatalogo();
//        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            if (e.getSource() == this.interfaz.txtBusca) {
                instance.setClave(this.interfaz.txtBusca.getText());
                mtdLlenarTable(this.interfaz.jTDatos,instance);
            }
            if (e.getSource() == this.interfaz.jTDatos) {
                mtdBtnEditarEliminar("Editar");
            }
            if (e.getSource() == this.interfaz.btnNuevo) {
                mtdBtnNuevo();
            }
            if (e.getSource() == this.interfaz.btnEditar) {
                mtdBtnEditarEliminar("Editar");
            }
            if (e.getSource() == this.interfaz.btnAceptar) {
                mtdBtnAceptarActualizar("Aceptar");
            }
            if (e.getSource() == this.interfaz.btnActualizar) {
                mtdBtnAceptarActualizar("Actualizar");
            }
            if (e.getSource() == this.interfaz.btnEliminar) {
                mtdBtnEditarEliminar("Eliminar");
            }
            if (e.getSource() == this.interfaz.btnCancelar) {
                mtdIniciar();
            }
            if (e.getSource() == this.interfaz.btnSalir) {
                this.interfaz.dispose();
            }
//            if (e.getSource() == this.interfaz.btnVistaPrevia) {
//                mtdbtnImprimirCatalogo();
//            }
        }
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            this.interfaz.dispose();
        }
        if (e.getKeyCode() == KeyEvent.VK_DELETE) {
            this.mtdBtnEditarEliminar("Eliminar");
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == this.interfaz.txtBusca) {
            instance.setClave(this.interfaz.txtBusca.getText());
            mtdLlenarTable(this.interfaz.jTDatos,instance);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    private void mtdbtnImprimirCatalogo() {
        File archivo;
        JFileChooser seleccionArchivo = new JFileChooser();
        seleccionArchivo.setFileFilter(new FileNameExtensionFilter("Excel (*.xlsx)", "xlsx"));
        if (seleccionArchivo.showDialog(null, "Exportar") == JFileChooser.APPROVE_OPTION) {
            archivo = seleccionArchivo.getSelectedFile();
            mtgGenerarRepoteCatalogo(archivo);
        }
    }
    ReporteStyle style = new ReporteStyle();

    public void mtgGenerarRepoteCatalogo(File archivo) {
        /////Creamos nuestro libro de trabajo excel
        XSSFWorkbook excel = new XSSFWorkbook();
        //crear cuantas hojas necesitemos
        XSSFSheet hoja = excel.createSheet("Catalogo de Unidades de conversión");
        Cell celda;
        Row fila = hoja.createRow(0);
        int i;
        int j = 0;
        try {
            fila = hoja.createRow(j);
            celda = fila.createCell(j);
            hoja.addMergedRegion(new CellRangeAddress(j, j, 0, 4));
            celda.setCellStyle(style.colorGray(excel));
            celda.setCellValue("Catalogo de Unidades de conversión");
            j++;
            fila = hoja.createRow(j);
            String[] titulos = {"clave", "Nombre", "Cantidad P", "Cantidad U"};
            // Creamos el encabezado
            for (i = 0; i < titulos.length; i++) {
                celda = fila.createCell(i);
                celda.setCellValue(titulos[i]);
                celda.setCellStyle(style.colorLightBlue(excel));
            }
            j++;
            fila = hoja.createRow(j);
            ///////Extralledo datos de la base de datos//////////////////
            int numRegistro = instanceDAO.mtdLista(instance).size();
            ///metodo para rellenar la tabla con los datos/////
            for (i = 0; i < numRegistro; i++) {
                celda = fila.createCell(0);
                celda.setCellValue(instanceDAO.mtdLista(instance).get(i).getClave());
                celda.setCellStyle(style.colorVerde(excel));
                celda = fila.createCell(1);
                celda.setCellValue(instanceDAO.mtdLista(instance).get(i).getNombre());
                celda = fila.createCell(2);
                celda.setCellValue(instanceDAO.mtdLista(instance).get(i).getCantidadPaquete());
                celda = fila.createCell(3);
                celda.setCellValue(instanceDAO.mtdLista(instance).get(i).getUnidadEntreCantidadPaquete());
                j++;
                fila = hoja.createRow(j);
            }
            FileOutputStream out = new FileOutputStream(archivo + ".xlsx");
            excel.write(out);
            out.close();


            JOptionPane.showMessageDialog(null, "Documento creado correctamente");
//                outByteStream = new ByteArrayOutputStream();
//                excel.write(outByteStream);
//                outByteStream.close();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex, "Error al exportar", JOptionPane.ERROR_MESSAGE);
        }

    }
}
