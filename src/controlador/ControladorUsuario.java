/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.sql.ResultSet;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import modelo.UsuarioDAO;
import javax.swing.table.DefaultTableModel;
import modelo.Usuario;
import vista.JFIUsuario;

/**
 *
 * @author ELEAZAR
 */
public class ControladorUsuario implements ActionListener, KeyListener {

    JFIUsuario jfUsuario = new JFIUsuario();
    Usuario instance = new Usuario();
    UsuarioDAO intanceDAO = new UsuarioDAO();
    ResultSet rs;

    public ControladorUsuario(JFIUsuario jfUsuario, UsuarioDAO intanceDAO) {
        this.jfUsuario = jfUsuario;
        this.intanceDAO = intanceDAO;
        this.jfUsuario.btnAceptar.addActionListener(this);
        this.jfUsuario.btncancelar.addActionListener(this);
        this.jfUsuario.btnnuevo.addActionListener(this);
        this.jfUsuario.btnEditar.addActionListener(this);
        this.jfUsuario.btneliminar.addActionListener(this);
        this.jfUsuario.btnOk.addActionListener(this);
        this.jfUsuario.btnSalir.addActionListener(this);
        this.jfUsuario.txtUsuario.addActionListener(this);
        this.jfUsuario.txtContraseña.addActionListener(this);
        this.jfUsuario.comboPerfil.addActionListener(this);

        this.jfUsuario.jMSalir.addActionListener(this);
        this.jfUsuario.jtDatos.addAncestorListener(null);
        mtdDesactivarText();
        mtdRellenarTable(jfUsuario.jtDatos);
        mtdCargarCombo();
    }

    public void mtdCargarCombo() {
        try {
            rs = intanceDAO.cargarCombo();
            while (rs.next()) {
                this.jfUsuario.comboPerfil.addItem(rs.getString(1) + "");
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "holllll" + e);

        }
    }

    public void mtdLimpiarTxt() {
        this.jfUsuario.txtUsuario.setText("");
        this.jfUsuario.txtContraseña.setText("");
    }

    public void mtdDesactivarText() {
        this.jfUsuario.txtUsuario.setEnabled(false);
        this.jfUsuario.txtContraseña.setEnabled(false);
        this.jfUsuario.comboPerfil.setEnabled(false);
        this.jfUsuario.btnAceptar.setEnabled(false);
        this.jfUsuario.btnOk.setEnabled(false);
        this.jfUsuario.btncancelar.setEnabled(false);
    }

    public void mtdActivarText() {
        this.jfUsuario.txtUsuario.setEnabled(true);
        this.jfUsuario.txtContraseña.setEnabled(true);
        this.jfUsuario.comboPerfil.setEnabled(true);
        this.jfUsuario.btnAceptar.setEnabled(true);
        this.jfUsuario.btncancelar.setEnabled(true);
    }

    public void mtdActivarbtn() {
        this.jfUsuario.btnnuevo.setEnabled(true);
        this.jfUsuario.btnEditar.setEnabled(true);
        this.jfUsuario.btneliminar.setEnabled(true);
        this.jfUsuario.btnOk.setEnabled(true);
        this.jfUsuario.btnSalir.setEnabled(true);
    }

    public void mtdDesactivarbtn() {
        this.jfUsuario.btnnuevo.setEnabled(false);
        this.jfUsuario.btnEditar.setEnabled(false);
        this.jfUsuario.btneliminar.setEnabled(false);
        this.jfUsuario.btnOk.setEnabled(false);
        this.jfUsuario.btnSalir.setEnabled(false);
    }

    public void mtdRellenarTable(JTable tableD) {
        DefaultTableModel model = new DefaultTableModel();
        tableD.setModel(model);
        model.addColumn("Usuario");
        model.addColumn("Password");
        model.addColumn("Perfil");
        Object[] columna = new Object[4];

        int numRegistro = intanceDAO.listaUsuario().size();
        System.out.print(intanceDAO.listaUsuario());
        for (int i = 0; i < numRegistro; i++) {
            columna[0] = intanceDAO.listaUsuario().get(i).getUsuario();
            columna[1] = intanceDAO.listaUsuario().get(i).getPassword();
            columna[2] = intanceDAO.listaUsuario().get(i).getPerfil().getClavePerfil();
            model.addRow(columna);
        }
    }

    private void mtdCacharTxt() {
        instance.setUsuario(jfUsuario.txtUsuario.getText());
        instance.setPassword(jfUsuario.txtContraseña.getText());
        instance.getPerfil().setClavePerfil(this.jfUsuario.comboPerfil.getSelectedItem().toString());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == jfUsuario.jMSalir) {
            this.jfUsuario.dispose();
        }
        if (e.getSource() == jfUsuario.btncancelar) {
            mtdDesactivarText();
            mtdActivarbtn();
            this.jfUsuario.btnOk.setEnabled(false);
            mtdLimpiarTxt();
        }
        if (e.getSource() == jfUsuario.btnAceptar) {
            mtdCacharTxt();
            String respuesta = intanceDAO.mtdRegistrar(instance);
            JOptionPane.showMessageDialog(null, respuesta);
            mtdDesactivarText();
            mtdActivarbtn();
            mtdLimpiarTxt();
            mtdRellenarTable(jfUsuario.jtDatos);
            this.jfUsuario.btnOk.setEnabled(false);

        }
        if (e.getSource() == jfUsuario.btnnuevo) {
            mtdActivarText();
            mtdDesactivarbtn();
        }

        if (e.getSource() == jfUsuario.btnEditar) {
            int numero = jfUsuario.jtDatos.getSelectedRow();
            if (numero >= 0) {
                this.jfUsuario.txtUsuario.setText(String.valueOf(jfUsuario.jtDatos.getValueAt(numero, 0)));
                this.jfUsuario.txtContraseña.setText(String.valueOf(jfUsuario.jtDatos.getValueAt(numero, 1)));
                mtdActivarText();
                this.jfUsuario.txtUsuario.setEnabled(false);
                this.jfUsuario.txtContraseña.grabFocus();
                this.jfUsuario.btnAceptar.setEnabled(false);
                this.jfUsuario.btncancelar.setEnabled(true);
                mtdDesactivarbtn();
                this.jfUsuario.btnOk.setEnabled(true);
            } else {
                JOptionPane.showMessageDialog(null, "Selecione una fila para Editar");

            }

        }
        if (e.getSource() == jfUsuario.btnOk) {
            mtdCacharTxt();
            int respuesta = intanceDAO.mtdEditar(instance);
            if (respuesta == 1) {
                JOptionPane.showMessageDialog(null, "Usuario Actualizado Correctamente");
                mtdDesactivarText();
                mtdActivarbtn();
                mtdLimpiarTxt();
                mtdRellenarTable(jfUsuario.jtDatos);
                this.jfUsuario.btnOk.setEnabled(false);

            } else {
                JOptionPane.showMessageDialog(null, "Error al Actualizar");
            }
        }

        if (e.getSource() == jfUsuario.btneliminar) {
            int numeroFila = jfUsuario.jtDatos.getSelectedRow();
            if (numeroFila >= 0) {
                int confir = JOptionPane.showConfirmDialog(null, "Esta seguro que deseas eliminar el usuario?");
                if (confir == 0) {
                    this.instance.setUsuario(String.valueOf(jfUsuario.jtDatos.getValueAt(numeroFila, 0)));
                    intanceDAO.mtdEliminar(instance);
                    mtdRellenarTable(jfUsuario.jtDatos);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Seleccione una fila");
            }

        }

    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
}
