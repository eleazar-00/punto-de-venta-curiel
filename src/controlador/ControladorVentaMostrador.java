/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.Image;
import java.awt.event.*;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.StringTokenizer;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import modelo.*;
import vista.JFIVentaMostrador; 

/**
 *
 * @author eleazar
 */
public class ControladorVentaMostrador implements ActionListener, KeyListener {

    JFIVentaMostrador interfaz = new JFIVentaMostrador();

    DetalleMovimiento detmovimientoInstance = new DetalleMovimiento();
    DetalleMovimientoDAO DetalleDB = new DetalleMovimientoDAO();

    Movimiento movimientoInstance = new Movimiento();
    ControladorMovimiento instanceControladorMov = new ControladorMovimiento();
    MovimientoDAO movimientoDAO = new MovimientoDAO();

    MetodoMovimiento mtdMovimiento = new MetodoMovimiento();

    Almacen instanceAlmacen = new Almacen();
    AlmacenDAO instanceAlmacenDAO = new AlmacenDAO();

    TipoPago instanceTipopago = new TipoPago();
    TipoPagoDAO instanceTIPODAO = new TipoPagoDAO();

    SeriesyFolios instanceSerie = new SeriesyFolios();
    SeriesyFoliosDAO instanceSFDAO = new SeriesyFoliosDAO();

    EntradaySalida instaceEys = new EntradaySalida();
    EntradaySalidaDAO instaceEysDAO = new EntradaySalidaDAO();

    Articulo instanceArticulo = new Articulo();
    ArticuloDAO instanceArticuloDAO = new ArticuloDAO();
    ControladorArticulo instanceControladorArticulo = new ControladorArticulo();

    Cliente instanceCliente = new Cliente();
    ClienteDAO instanceDAOClinete = new ClienteDAO();

    ControladorCliente instanceControllerCliente = new ControladorCliente();

    ControladorReporteVenta intanceControladorVenta = new ControladorReporteVenta();

    Articulo_UnidadDAO instanceAU_DAO = new Articulo_UnidadDAO();

    Agente instanceAgente = new Agente();
    AgenteDAO instanceAgenteDAO = new AgenteDAO();

    Departamento instanceDepartamento = new Departamento();
    DepartamentoDAO instanceDepartamentoDAO = new DepartamentoDAO();

    ExistenciaAlmacen instacenExistencia = new ExistenciaAlmacen();
    ExistenciaAlmacenDAO instanceExistenciaDAO = new ExistenciaAlmacenDAO();

    Retiro retiro = new Retiro();
    RetiroDAO retiroDAO = new RetiroDAO();

    ResultSet rs;
    DefaultTableModel modeloT = new DefaultTableModel();
    int claveEmpleado;
    String Almacenes;
    String Series;

    ValidarCampos validaCampos = new ValidarCampos();

    public ControladorVentaMostrador(JFIVentaMostrador interfaz, int claveEmpleado, String Almacenes, String Series) {
        this.interfaz = interfaz;
        this.claveEmpleado = claveEmpleado;
        this.Almacenes = Almacenes;
        this.Series = Series;
        cargarControlador();
    }

    private void cargarControlador() {
        this.interfaz.txtBuscarCodigoBarrasNombre.addActionListener(this);
        /////////////Articulo/////////////////////////////////////
        this.interfaz.txtBuscarArticulo.addKeyListener(this);
        this.interfaz.jtBuscarArticulo.addKeyListener(this);
        this.interfaz.btnAgregar.addActionListener(this);
        this.interfaz.btnCancelarBuscarArticulo.addActionListener(this);
        //Agregar servicio
        this.interfaz.jTableServicios.addKeyListener(this);
        //Buscar Nota
        this.interfaz.btnImprimirNota.addActionListener(this);
        this.interfaz.btnNota.addActionListener(this);
        this.interfaz.btnBuscarMovimientos.addActionListener(this);
        this.interfaz.jTableNotaBuscar.addKeyListener(this);
        this.interfaz.btnCancelarBuscarNota.addActionListener(this);

        //Menu botones Acceso Rapido
        this.interfaz.btnsiguiente.addActionListener(this);
        this.interfaz.btnatras.addActionListener(this);
        this.interfaz.btnSalir.addActionListener(this);
        this.interfaz.btnNuevaVenta.addActionListener(this);
        this.interfaz.btnEliminarProducto.addActionListener(this);
        this.interfaz.btnBuscar.addActionListener(this);
        this.interfaz.btnBuscarServicios.addActionListener(this);
        this.interfaz.btnGuardarVenta.addActionListener(this);
        this.interfaz.btnCobrar.addActionListener(this);
        //Menu Cobro
        this.interfaz.txtRecibi.addKeyListener(this);
        this.interfaz.btnGrabarVenta.addActionListener(this);
        this.interfaz.btnCancelarCobro.addActionListener(this);
        //Mennu Buscar CLiente
        this.interfaz.btnBuscarCliente.addActionListener(this);
        this.interfaz.txtBuscarCliente.addKeyListener(this);
        this.interfaz.jTableClienteBuscar.addKeyListener(this);
        this.interfaz.btnAgregarCliente.addActionListener(this);
        this.interfaz.btnSalirCliente.addActionListener(this);

        //JtemMenus 
        this.interfaz.jMenuItemSiguiente.addActionListener(this);
        this.interfaz.jMenuItemAtras.addActionListener(this);
        this.interfaz.jMenuItemNuevo.addActionListener(this);
        this.interfaz.jMenuItemEliminar.addActionListener(this);
        this.interfaz.jMenuItemBuscarArticulo.addActionListener(this);
        this.interfaz.jMenuItemBuscarServicios.addActionListener(this);
        this.interfaz.jMenuItemBuscarCliente.addActionListener(this);
        this.interfaz.jMenuItemAplicar.addActionListener(this);
        this.interfaz.jMenuItemGuardar.addActionListener(this);
        this.interfaz.jMenuItemAceptarCobro.addActionListener(this);
        this.interfaz.jMenuItemCancelarCobro.addActionListener(this);
        this.interfaz.jMenuItemSalir.addActionListener(this);

        ///btn y jmenu Buscar articulo
        this.interfaz.jComboBoxBuscarArticulo.addKeyListener(this);
        this.interfaz.jMenuItemTipoBusqueda.addActionListener(this);
        this.interfaz.jComboBoxDepartamento.addActionListener(this);
        this.interfaz.jComboBoxDepartamento.addKeyListener(this);
        this.interfaz.jMenuItemSalirBuscarArticulo.addActionListener(this);

        //JDialogo retiro en efectivo
        this.interfaz.btnRetiro.addActionListener(this);
        this.interfaz.btnAceptarRetiro.addActionListener(this);
        this.interfaz.btnCancelarRetiro.addActionListener(this);
        this.interfaz.btnBuscarRetiro.addActionListener(this);
        this.interfaz.btnCancelarUnRetiro.addActionListener(this);

        //fin Retiro en efectivo
        mtdCargarComboAlmacen();
        mtdCargarComboTipoPago();
        mtdCargarComboSerie();
        mtdCargarComboDepartamento();
        mdtHora();
        mtdCargarInicio();
    }

    public void mtdCargarInicio() {
        this.interfaz.jTabbedPaneDocumento.setEnabledAt(1, false);
        this.interfaz.jTabbedPaneDocumento.setEnabledAt(0, true);
        this.interfaz.jTabbedPaneDocumento.setSelectedIndex(0);
        //desactivar JTemMenus
        this.interfaz.jMenuItemSiguiente.setEnabled(true);
        this.interfaz.jMenuItemAtras.setEnabled(false);
        this.interfaz.jMenuItemNuevo.setEnabled(false);
        this.interfaz.jMenuItemEliminar.setEnabled(false);
        this.interfaz.jMenuItemBuscarArticulo.setEnabled(false);
        this.interfaz.jMenuItemAplicar.setEnabled(false);
        this.interfaz.jMenuItemGuardar.setEnabled(false);
    }

    public void mdtHora() {
        Reloj reloj = new Reloj(this.interfaz.lbhora);
        reloj.start();
    }

    ////////////////METODOS CLIENTE//////
    public void mtdBtnBuscarCliente() {
        this.interfaz.jDBuscarCliente.setSize(870, 500);
        this.interfaz.jDBuscarCliente.setLocationRelativeTo(null);
        this.interfaz.jDBuscarCliente.setVisible(true);
        this.interfaz.txtBuscarCliente.grabFocus();
    }

    private void mtdAgregarCliente() {
        int numerofila = this.interfaz.jTableClienteBuscar.getSelectedRow();
        if (numerofila >= 0) {
            this.interfaz.lblClaveCliente.setText(this.interfaz.jTableClienteBuscar.getValueAt(numerofila, 0).toString());
            this.interfaz.lblNombreCliente.setText(interfaz.jTableClienteBuscar.getValueAt(numerofila, 1).toString());
            this.interfaz.jDBuscarCliente.dispose();
            this.interfaz.txtBuscarCliente.setText("");
            this.interfaz.txtBuscarCodigoBarrasNombre.grabFocus();
        } else {
            JOptionPane.showMessageDialog(null, "Seleccione  un Cliente");
        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //Evento Cliente
        if (e.getSource() == this.interfaz.btnBuscarCliente || e.getSource() == this.interfaz.jMenuItemBuscarCliente) {
            mtdBtnBuscarCliente();
        }
        if (e.getSource() == this.interfaz.btnAgregarCliente) {
            mtdAgregarCliente();
        }
        if (e.getSource() == this.interfaz.btnSalirCliente) {
            this.interfaz.jDBuscarCliente.dispose();
            this.interfaz.txtBuscarCliente.setText("");
        }
        // Termina Eventos Clientes
        //////////////Evento articulo//////////////////
        if (e.getSource() == this.interfaz.txtBuscarCodigoBarrasNombre) {
            if (this.interfaz.txtBuscarCodigoBarrasNombre.getText().length() == 0) {
                mtdBuscarArticulo();
            } else {
                this.mtdValidarBusquedaCantidad(this.interfaz.txtBuscarCodigoBarrasNombre.getText());
            }
        }
        if (e.getSource() == interfaz.btnAgregar) {
            mtdAgregarArticulo();
        }
        if (e.getSource() == this.interfaz.btnCancelarBuscarArticulo || e.getSource() == this.interfaz.jMenuItemSalirBuscarArticulo) {
            this.interfaz.jdBuscarArticulo.dispose();
            this.interfaz.txtBuscarCodigoBarrasNombre.setText("");
        }
        if (e.getSource() == this.interfaz.btnBuscar || e.getSource() == this.interfaz.jMenuItemBuscarArticulo) {
            mtdBuscarArticulo();
        }
        if (e.getSource() == this.interfaz.btnBuscarServicios || e.getSource() == this.interfaz.jMenuItemBuscarServicios) {
            mtdAbrirDBuscarServicios();
        }
        if (e.getSource() == this.interfaz.jMenuItemTipoBusqueda) {
            this.interfaz.txtBuscarArticulo.setText("");
            this.interfaz.jComboBoxBuscarArticulo.grabFocus();
        }
        //terminar Buscar Articulo
        if (e.getSource() == this.interfaz.btnsiguiente || e.getSource() == this.interfaz.jMenuItemSiguiente) {
            if (this.interfaz.JComboClaveAlmacen.getSelectedItem() == null) {
                JOptionPane.showMessageDialog(null, "El usuario no tiene asignado ningun almacen de venta");
            } else {
                if (this.interfaz.jComboxSerie.getSelectedItem() == null) {
                    JOptionPane.showMessageDialog(null, "El usuario no tiene asignado ninguna Serie de venta");
                } else {
                    mtdsiguienteatras("siguiente");
                }
            }
        }
        if (e.getSource() == this.interfaz.btnatras || e.getSource() == this.interfaz.jMenuItemAtras) {
            mtdsiguienteatras("atras");
        }

        if (e.getSource() == interfaz.btnSalir || e.getSource() == this.interfaz.jMenuItemSalir) {
            salir();
        }
        if (e.getSource() == interfaz.btnEliminarProducto || e.getSource() == this.interfaz.jMenuItemEliminar) {
            eliminarArticulo();
        }
        if (e.getSource() == this.interfaz.btnNuevaVenta || e.getSource() == this.interfaz.jMenuItemNuevo) {
            nuevaVenta();
        }
        if (e.getSource() == this.interfaz.btnGuardarVenta || e.getSource() == this.interfaz.jMenuItemGuardar) {
            mtdGrabar(0);
        }
        //cobro
        if (e.getSource() == this.interfaz.btnCobrar || e.getSource() == this.interfaz.jMenuItemAplicar) {
            this.mtdCobrar();
        }
        if (e.getSource() == this.interfaz.btnGrabarVenta || e.getSource() == this.interfaz.jMenuItemAceptarCobro) {
            mtdGrabar(1);
        }
        if (e.getSource() == this.interfaz.btnCancelarCobro || e.getSource() == this.interfaz.jMenuItemCancelarCobro) {
            this.interfaz.JDCobrar.dispose();
            this.interfaz.txtRecibi.setText("");
            this.interfaz.txtCambio.setText("");
        }
        //Nota de venta
        if (e.getSource() == this.interfaz.btnNota) {
            this.mtdAbrirBuscarNota();
        }

        if (e.getSource() == this.interfaz.btnBuscarMovimientos) {
             mtdBuscarNota();
         }
        if (e.getSource() == this.interfaz.btnImprimirNota) {
            int fila = this.interfaz.jTableNotaBuscar.getSelectedRow();
            if (fila >= 0) {
              this.interfaz.jDNota.dispose();
                int folio = Integer.parseInt(this.interfaz.jTableNotaBuscar.getValueAt(fila, 1).toString());
                String serie = this.interfaz.jTableNotaBuscar.getValueAt(fila, 0).toString();
                intanceControladorVenta.mtdImprimirNota(folio, serie);
            } else {
                JOptionPane.showMessageDialog(null, "Selecccione una venta");
            }
        }
        if (e.getSource() == this.interfaz.btnCancelarBuscarNota) {
            int fila = this.interfaz.jTableNotaBuscar.getSelectedRow();
            if (fila >= 0) {
                int folio = Integer.parseInt(this.interfaz.jTableNotaBuscar.getValueAt(fila, 1).toString());
                String serie = this.interfaz.jTableNotaBuscar.getValueAt(fila, 0).toString();
                String  signo="+";
                this.instanceAlmacen = (Almacen) this.interfaz.JComboClaveAlmacen.getSelectedItem();
                instanceControladorMov.mtdCancelarMovimiento(serie,folio,signo,instanceAlmacen.getClave());
                 mtdBuscarNota();
            } else {
                JOptionPane.showMessageDialog(null, "Seleccciones una venta");
            }
        }
        //Retiro de eefectivo

        if (e.getSource() == this.interfaz.btnRetiro) {
            mtdDialogRetiro();
        }
        if (e.getSource() == this.interfaz.btnAceptarRetiro) {
            Double cantidad = 0.0;
            try {
                cantidad = Double.parseDouble(this.interfaz.txtCantidadRetiro.getText());

            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Error en el campo cantidad verificar");
                this.interfaz.txtCantidadRetiro.grabFocus();
            }
            if (cantidad > 0) {
                if (this.interfaz.txtConceptoRetiro.getText().length() > 0) {
                    //Aplicar aceptar cobro
                    retiro.setId(0);
                    retiro.setStatus(1);
                    this.instanceSerie = (SeriesyFolios) this.interfaz.jComboxSerie.getSelectedItem();
                    retiro.setSerie("RT");
                    instanceSerie.setClave("RT");
                    retiro.setFolio(instanceSFDAO.mtdUpdateNumeroFolio(instanceSerie));
                    retiro.setConcepto(this.interfaz.txtConceptoRetiro.getText());
                    String fechaMovimiento = new SimpleDateFormat("yyyy-MM-dd").format(interfaz.JDFechaVentaMostrador.getDate());
                    retiro.setFecha(fechaMovimiento);
                    retiro.setCantidad(Double.parseDouble(this.interfaz.txtCantidadRetiro.getText()));
                    instanceAgente.setClave(claveEmpleado);
                    retiro.setAgente(instanceAgente);
                    retiro.setCaja("CAJA1");
                    this.instanceAlmacen = (Almacen) this.interfaz.JComboClaveAlmacen.getSelectedItem();
                    retiro.setAlmacen(instanceAlmacen);
                    String respuesta = retiroDAO.mtdInsertar(retiro);
                    JOptionPane.showMessageDialog(null, respuesta);
                    this.interfaz.txtCantidadRetiro.setText("");
                    this.interfaz.txtConceptoRetiro.setText("");
                    this.interfaz.txtCantidadRetiro.grabFocus();
                    //
                } else {
                    JOptionPane.showMessageDialog(null, "Favor de ingresar un concepto descriptivo");
                    this.interfaz.txtConceptoRetiro.grabFocus();
                }
            } else {
                JOptionPane.showMessageDialog(null, "La cantidad debe se mayor a cero");
                this.interfaz.txtCantidadRetiro.grabFocus();
            }
        }
        if (e.getSource() == this.interfaz.btnCancelarRetiro) {
            this.interfaz.jDRetiro.dispose();
        }
        if (e.getSource() == this.interfaz.btnBuscarRetiro) {
            this.mtdBuscarRetiros();
        }
        if (e.getSource() == this.interfaz.btnCancelarUnRetiro) {
            if (this.interfaz.tablaRetiros.getSelectedRow() >= 0) {
                int id = Integer.parseInt(this.interfaz.tablaRetiros.getValueAt(this.interfaz.tablaRetiros.getSelectedRow(), 0).toString());

                int respuesta = JOptionPane.showConfirmDialog(null, "Desea cancelar el retiro seleccionado");
                if (respuesta == 0) {
                    if (retiroDAO.mtdActualizar(id) == 1) {
                        JOptionPane.showMessageDialog(null, "Cancelado Correctamente");
                        this.mtdBuscarRetiros();
                    }
                }

            } else {
                JOptionPane.showMessageDialog(null, "Seleccione un retiro para cancelar");
            }
        }
        //fin retiro de efectiv
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            //////////////Evento Cliente////////////////// 
            if (e.getSource() == this.interfaz.txtBuscarCliente) {
                mtdBtnBuscarCliente();
                instanceControllerCliente.mtdBuscarCliente(this.interfaz.jTableClienteBuscar, this.interfaz.txtBuscarCliente.getText(), this.interfaz.jrbActivoB, this.interfaz.jrbInactivoB, this.interfaz.jrbTodosB, this.interfaz.jComboBoxBuscar);
            }
            if (e.getSource() == this.interfaz.jTableClienteBuscar) {
                mtdAgregarCliente();
            }
            //////////////Evento articulo//////////////////
            if (e.getSource() == interfaz.txtBuscarArticulo) {
                instanceControladorArticulo.mtdBuscarArticulo(this.interfaz.jtBuscarArticulo, this.interfaz.txtBuscarArticulo, this.interfaz.jRadioButtonActivoBuscar, this.interfaz.jRadioButtonInactivoBuscar, this.interfaz.jRadioButtonTodoBuscar, this.interfaz.jComboBoxBuscarArticulo, this.interfaz.jComboBoxDepartamento, "venta");
            }
            if (e.getSource() == this.interfaz.jComboBoxBuscarArticulo) {
                if (this.interfaz.jComboBoxBuscarArticulo.getSelectedIndex() == 0) {
                    this.interfaz.jComboBoxDepartamento.setVisible(false);
                    this.interfaz.txtBuscarArticulo.setVisible(true);
                    this.interfaz.txtBuscarArticulo.setBounds(180, 15, 500, 25);
                    this.interfaz.txtBuscarArticulo.grabFocus();
                }
                if (this.interfaz.jComboBoxBuscarArticulo.getSelectedIndex() == 1) {
                    this.interfaz.jComboBoxDepartamento.setVisible(true);
                    this.interfaz.txtBuscarArticulo.setVisible(false);
                    this.interfaz.jComboBoxDepartamento.grabFocus();
                }
            }
            if (e.getSource() == this.interfaz.jComboBoxDepartamento) {
                this.interfaz.txtBuscarArticulo.setText("");
                instanceControladorArticulo.mtdBuscarArticulo(this.interfaz.jtBuscarArticulo, this.interfaz.txtBuscarArticulo, this.interfaz.jRadioButtonActivoBuscar, this.interfaz.jRadioButtonInactivoBuscar, this.interfaz.jRadioButtonTodoBuscar, this.interfaz.jComboBoxBuscarArticulo, this.interfaz.jComboBoxDepartamento, "venta");
            }
            //Termina evento buscar articulo
            if (e.getSource() == this.interfaz.jtBuscarArticulo) {
                mtdAgregarArticulo();
            }
            if (e.getSource() == this.interfaz.jTableServicios) {
                this.mtdAgregarServicio();
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == this.interfaz.txtRecibi) {
            mtdMovimiento.mtdCalcularPago(this.interfaz.txtTotal, this.interfaz.txtRecibi, this.interfaz.txtCambio);
        }

    }

    public void mtdCargarComboAlmacen() {
        ArrayList<Almacen> almacenes = this.instanceAlmacenDAO.listaAlmacen(instanceAlmacen, "venta", this.Almacenes);
        for (Almacen almacen : almacenes) {
            this.interfaz.JComboClaveAlmacen.addItem(almacen);
        }
    }

    public void mtdCargarComboTipoPago() {
        ArrayList<TipoPago> tipopagos = this.instanceTIPODAO.mtdLista();
        for (TipoPago tipoPago : tipopagos) {
            this.interfaz.jComboTipoPago.addItem(tipoPago);
        }
    }

    public void mtdCargarComboDepartamento() {
        ArrayList<Departamento> departamentos = this.instanceDepartamentoDAO.mtdLista(instanceDepartamento);
        for (Departamento departamento : departamentos) {
            this.interfaz.jComboBoxDepartamento.addItem(departamento);
        }
    }

    public void mtdCargarComboSerie() {
        ArrayList<SeriesyFolios> series = this.instanceSFDAO.lista(instanceSerie, "venta", this.Series);
        for (SeriesyFolios serie : series) {
            this.interfaz.jComboxSerie.addItem(serie);
        }
    }

    public void mtdCargarComboMovimiento(EntradaySalida instaceEys, String filtro) {
        ArrayList<EntradaySalida> entradasysalidas = this.instaceEysDAO.listaTipoEYS(instaceEys, filtro);
        for (EntradaySalida entradasysalida : entradasysalidas) {
            this.interfaz.jComboBoxMovimiento.addItem(entradasysalida);
        }
    }

    protected void mtdAgregarArticulo() {
        int fila = this.interfaz.jtBuscarArticulo.getSelectedRow();
        try {
            if (fila >= 0) {
                this.interfaz.jdBuscarArticulo.dispose();
                double cantidad = 1;
                try {
                    cantidad = Double.parseDouble(this.interfaz.txtBuscarCodigoBarrasNombre.getText());
                } catch (Exception e) {
                    cantidad = 1;
                }
                mtdBuscarArticulo(cantidad, String.valueOf(this.interfaz.jtBuscarArticulo.getValueAt(fila, 0)));

            } else {
                JOptionPane.showMessageDialog(null, "Seleccione un Articulo");
            }
        } catch (Exception Oe) {
            JOptionPane.showMessageDialog(null, "Error " + Oe, "Error", JOptionPane.ERROR_MESSAGE);

        }
    }

    protected void mtdAgregarServicio() {
        int fila = this.interfaz.jTableServicios.getSelectedRow();
        try {
            if (fila >= 0) {
                this.interfaz.jDBuscarServicio.dispose();
                double cantidad = 1;
                String clave = String.valueOf(this.interfaz.jTableServicios.getValueAt(fila, 0));
                Double precioIngresada = 0.0;
                Double comision = 15.0;
                double precioUnitario = 0.0;
                Double total = 0.0;
                String nombre = String.valueOf(this.interfaz.jTableServicios.getValueAt(fila, 1));
                modeloT = (DefaultTableModel) interfaz.jtDatosVenta.getModel();
                precioUnitario = Double.parseDouble(this.interfaz.jTableServicios.getValueAt(fila, 2).toString());
                total = Double.parseDouble(this.interfaz.jTableServicios.getValueAt(fila, 2).toString());
                String unidad = "PZ";
                if (clave.equals("CFE")) {
                    precioIngresada = Double.parseDouble(JOptionPane.showInputDialog("Ingresar la cantidad a pagar"));
                    precioUnitario = precioIngresada + comision;
                    total = precioUnitario;
                };
                if (clave.equals("PC")) {
                    precioIngresada = Double.parseDouble(JOptionPane.showInputDialog("Ingresar la cantidad a pagar"));
                    precioUnitario = precioIngresada;
                    total = precioUnitario;
                };
              //  Object elemento[] = {cantidad, clave, nombre, precioUnitario, 0, total, unidad};
                Object elemento[] = {this.validaCampos.redondeoDecimalesCantidad(cantidad), clave, nombre, this.validaCampos.redondeoDecimalesPreciosCostos(precioUnitario), 0, total, unidad, this.validaCampos.redondeoDecimalesCantidad(cantidad), 0};
                modeloT.addRow(elemento);
                mtdMovimiento.buscarArticulo(modeloT, interfaz.jtDatosVenta, total, 0.0, total, this.interfaz.txtSubTotal, this.interfaz.txtDescuento, this.interfaz.txtTotal, cantidad, this.interfaz.lblCantidadArticulo);
                mtdUpdateTableCOmpra(interfaz.jtDatosVenta, this.interfaz.txtSubTotal, this.interfaz.txtTotal, this.interfaz.txtDescuento, this.interfaz.lblCantidadArticulo);

            } else {
                JOptionPane.showMessageDialog(null, "Seleccione un Articulo");
            }
        } catch (Exception Oe) {
            JOptionPane.showMessageDialog(null, "Error " + Oe, "Error", JOptionPane.ERROR_MESSAGE);

        }
    }

    protected void mtdValidarBusquedaCantidad(String codigoBarras) {
        StringTokenizer st = new StringTokenizer(codigoBarras, "*;");
        try {
            while (st.hasMoreTokens()) {
                this.mtdBuscarArticulo(Double.parseDouble(st.nextToken()), st.nextToken());
            }
        } catch (Exception e) {
            this.mtdBuscarArticulo(1.0, codigoBarras);
        }
    }

    protected void mtdBuscarArticulo(Double cantidad, String codigoBarras) {
        String status = "1,2";
        this.instanceArticulo.setCodigo(codigoBarras.trim());
        int numeroFila = this.instanceArticuloDAO.listaArticulosPorCodigo(instanceArticulo, status).size();
        if (numeroFila == 1) {
            String clave = this.instanceArticuloDAO.listaArticulosPorCodigo(instanceArticulo, status).get(0).getCodigo();
            String nombre = this.instanceArticuloDAO.listaArticulosPorCodigo(instanceArticulo, status).get(0).getNombre();
            double precioUnitario = 0;
            /// Validar precio
            Double cantidad2 = this.instanceArticuloDAO.listaArticulosPorCodigo(instanceArticulo, status).get(0).getCantidad_dos();
            Double cantidad3 = this.instanceArticuloDAO.listaArticulosPorCodigo(instanceArticulo, status).get(0).getCantidad_tres();

            if (cantidad2 == 0 && cantidad3 == 0) {
                precioUnitario = (this.instanceArticuloDAO.listaArticulosPorCodigo(instanceArticulo, status).get(0).getPrecio_lista());
            } else {
                if (cantidad2 != 0 && cantidad3 == 0) {
                    if (cantidad >= cantidad2) {
                        precioUnitario = this.instanceArticuloDAO.listaArticulosPorCodigo(instanceArticulo, status).get(0).getPrecio_dos();
                    } else {
                        precioUnitario = (this.instanceArticuloDAO.listaArticulosPorCodigo(instanceArticulo, status).get(0).getPrecio_lista());
                    }
                } else if (cantidad3 != 0) {
                    if (cantidad >= cantidad3) {
                        precioUnitario = this.instanceArticuloDAO.listaArticulosPorCodigo(instanceArticulo, status).get(0).getPrecio_tres();
                    } else {
                        if (cantidad >= cantidad2 && cantidad <= cantidad3) {
                            precioUnitario = this.instanceArticuloDAO.listaArticulosPorCodigo(instanceArticulo, status).get(0).getPrecio_dos();
                        } else {
                            precioUnitario = (this.instanceArticuloDAO.listaArticulosPorCodigo(instanceArticulo, status).get(0).getPrecio_lista());
                        }
                    }
                }
            }
            ///Fin validar precio.
            String unidad = String.valueOf(this.instanceArticuloDAO.listaArticulosPorCodigo(instanceArticulo, status).get(0).getUnidad().getClave());
            Double total = this.validaCampos.redondeoDecimalesPreciosCostos(this.validaCampos.redondeoDecimalesCantidad(cantidad) * this.validaCampos.redondeoDecimalesPreciosCostos(precioUnitario));

            modeloT = (DefaultTableModel) interfaz.jtDatosVenta.getModel();
            Object elemento[] = {this.validaCampos.redondeoDecimalesCantidad(cantidad), clave, nombre, this.validaCampos.redondeoDecimalesPreciosCostos(precioUnitario), 0, total, unidad, this.validaCampos.redondeoDecimalesCantidad(cantidad), 0};
            modeloT.addRow(elemento);
            interfaz.txtBuscarCodigoBarrasNombre.setText("");
            mtdMovimiento.buscarArticulo(modeloT, interfaz.jtDatosVenta, total, 0.0, total, this.interfaz.txtSubTotal, this.interfaz.txtDescuento, this.interfaz.txtTotal, cantidad, this.interfaz.lblCantidadArticulo);
            mtdUpdateTableCOmpra(interfaz.jtDatosVenta, this.interfaz.txtSubTotal, this.interfaz.txtTotal, this.interfaz.txtDescuento, this.interfaz.lblCantidadArticulo);
            String ruta = this.instanceArticuloDAO.listaArticulosPorCodigo(instanceArticulo, status).get(0).getUrl_imagen();
            ImageIcon img = new ImageIcon(ruta);
            interfaz.lblImagen.setIcon(new ImageIcon(img.getImage().getScaledInstance(interfaz.lblImagen.getWidth(), interfaz.lblImagen.getHeight(), Image.SCALE_DEFAULT)));
            this.interfaz.txtBuscarCodigoBarrasNombre.grabFocus();
        }
        if (numeroFila > 1) {
            mtdBuscarArticulo();
        }
        if (numeroFila == 0) {
            JOptionPane.showMessageDialog(null, "No se encontro ningun Articulo con ese codigo", "No hay resultados en la busquesa", JOptionPane.WARNING_MESSAGE);
            mtdBuscarArticulo();
        }

    }

    public static double roundToHalf(double d) {
        return Math.round(d * 2) / 2.0;
    }

    protected void mtdBuscarArticulo() {
        this.interfaz.txtBuscarArticulo.setVisible(false);
        this.interfaz.jComboBoxDepartamento.setVisible(false);
        this.interfaz.jRadioButtonActivoBuscar.setVisible(false);
        this.interfaz.jRadioButtonInactivoBuscar.setVisible(false);
        this.interfaz.jRadioButtonTodoBuscar.setVisible(false);
        this.interfaz.jdBuscarArticulo.setSize(1100, 700);
        this.interfaz.jdBuscarArticulo.setLocationRelativeTo(null);
        this.interfaz.jdBuscarArticulo.setVisible(true);
        this.interfaz.txtBuscarArticulo.grabFocus();
    }

    protected void mtdAbrirDBuscarServicios() {
        instanceControladorArticulo.mtdBuscarServicios(this.interfaz.jTableServicios, "ventaServicio");
        this.interfaz.jDBuscarServicio.setSize(700, 700);
        this.interfaz.jDBuscarServicio.setLocationRelativeTo(null);
        this.interfaz.jDBuscarServicio.setVisible(true);

    }

    protected void mtdAbrirBuscarNota() {
        this.interfaz.jDNota.setSize(700, 500);
        this.interfaz.jDNota.setLocationRelativeTo(null);
        this.interfaz.jDNota.setVisible(true);
    }
    
    private void mtdBuscarNota(){
            String fechaInicial = new SimpleDateFormat("yyyy-MM-dd").format(interfaz.JDFechaInicialNota.getDate());
            String fechaFinal = new SimpleDateFormat("yyyy-MM-dd").format(interfaz.JDFechaFinalNota.getDate());
            String numerosMovimiento = this.interfaz.jComboBoxImprimirNota.getSelectedItem().toString();
            this.instanceSerie = (SeriesyFolios) this.interfaz.jComboxSerie.getSelectedItem();
            instanceControladorMov.mtdRellenarTabla(this.interfaz.jTableNotaBuscar, instanceSerie.getClave(), "1", numerosMovimiento, fechaInicial, fechaFinal);
       
    }

    protected void mtdUpdateTableCOmpra(JTable table, JTextField txtsubTotal, JTextField txtTotal, JTextField txtDescuento, JLabel txtCantidad) {
        modeloT.addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent e) {
                if (e.getType() == TableModelEvent.UPDATE) {
                    int columna = e.getColumn();
                    int row = e.getFirstRow();
                    mtdMovimiento.actualizarTabla(columna, row, table, txtsubTotal, txtTotal, txtDescuento, txtCantidad);
                }
            }
        });
    }

    private void mtdCobrar() {
        if (Float.parseFloat(this.interfaz.txtTotal.getText()) == 0) {
            JOptionPane.showMessageDialog(null, "Total en ceros");
            this.interfaz.txtBuscarCodigoBarrasNombre.grabFocus();      
        }
        else {
            int countRowTablaVenta=this.interfaz.jtDatosVenta.getRowCount();
            double precio=1;
            double precioCero=0;
            String nombreProducto="";
            for(int i=0;i<countRowTablaVenta;i++){
               precio=Double.parseDouble(this.interfaz.jtDatosVenta.getValueAt(i, 3).toString());
               if(precio<1){
                  precioCero+=1;
                  nombreProducto+=this.interfaz.jtDatosVenta.getValueAt(i, 2).toString()+",";
               }
            }
            if(precioCero>=1){
                String nota="Articulo ";
              if(precioCero>1){
                       nota="Articulos";
              }
             JOptionPane.showMessageDialog(null,nombreProducto+" "+nota+" con precio cero favor de rectifivar");
            }else{
            this.interfaz.txtRecibi.grabFocus();
            this.interfaz.JDCobrar.setSize(330, 450);
            this.interfaz.JDCobrar.setLocationRelativeTo(null);
            this.interfaz.JDCobrar.setVisible(true);
            }
        }
    }

    private void mtdDialogRetiro() {
        this.interfaz.jDRetiro.setSize(490, 400);
        this.interfaz.jDRetiro.setLocationRelativeTo(null);
        this.interfaz.jDRetiro.setVisible(true);
        this.interfaz.txtCantidadRetiro.grabFocus();
    }

    private void mtdBuscarRetiros() {

        ControladorRetiro controladorRetiro = new ControladorRetiro();
        String fechaRetiro = new SimpleDateFormat("yyyy-MM-dd").format(interfaz.JDFechaRetiro.getDate());
        retiro.setFecha(fechaRetiro);
        instanceAgente.setClave(claveEmpleado);
        retiro.setAgente(instanceAgente);
        this.instanceAlmacen = (Almacen) this.interfaz.JComboClaveAlmacen.getSelectedItem();
        retiro.setAlmacen(instanceAlmacen);
        controladorRetiro.mtdLlenarTable(this.interfaz.tablaRetiros, retiro);
    }

    protected void mtdsiguienteatras(String tipo) {
        if (tipo.equals("siguiente")) {
            this.interfaz.jTabbedPaneDocumento.setEnabledAt(0, false);
            this.interfaz.jTabbedPaneDocumento.setEnabledAt(1, true);
            this.interfaz.jTabbedPaneDocumento.setSelectedIndex(1);
            this.interfaz.jMenuItemSiguiente.setEnabled(false);
            this.interfaz.jMenuItemAtras.setEnabled(true);
            this.interfaz.jMenuItemNuevo.setEnabled(true);
            this.interfaz.jMenuItemEliminar.setEnabled(true);
            this.interfaz.jMenuItemBuscarArticulo.setEnabled(true);
            this.interfaz.jMenuItemAplicar.setEnabled(true);
            this.interfaz.jMenuItemGuardar.setEnabled(true);
        }
        if (tipo.equals("atras")) {
            this.mtdCargarInicio();

        }
        this.instanceSerie = (SeriesyFolios) this.interfaz.jComboxSerie.getSelectedItem();
        this.instanceSerie.setClave(instanceSerie.getClave());
        // this.interfaz.lblfolio.setText(String.valueOf(this.instanceSFDAO.lista(instanceSF, "clave").get(0).getFolio() + 1));
        int itemCount = this.interfaz.jComboBoxMovimiento.getItemCount();
        for (int i = 0; i < itemCount; i++) {
            this.interfaz.jComboBoxMovimiento.removeItemAt(0);
        }
        instaceEys.setClave(instanceSerie.getEntradaysalida().getClave());
        this.mtdCargarComboMovimiento(instaceEys, "clave");

    }

    private void salir() {
        mtdMovimiento.mdtSalir(this.interfaz.jtDatosVenta, interfaz, null);
    }

    private void nuevaVenta() {
        mtdMovimiento.mtdNuevaVenta(interfaz.jtDatosVenta, modeloT, this.interfaz.txtSubTotal, this.interfaz.txtDescuento, this.interfaz.txtTotal, this.interfaz.lblCantidadArticulo, this.interfaz.txtBuscarCodigoBarrasNombre);
    }

    private void eliminarArticulo() {
        mtdMovimiento.mtdEliminarArticulo(interfaz.jtDatosVenta, modeloT, this.interfaz.txtSubTotal, this.interfaz.txtDescuento, this.interfaz.txtTotal, this.interfaz.lblCantidadArticulo, this.interfaz.txtBuscarCodigoBarrasNombre);
    }

    public void mtdGrabar(int status) {
        //            Grabar Movimiento
        movimientoInstance.setClave(0);
        movimientoInstance.setStatus(status);
        movimientoInstance.setNombre(this.interfaz.lblNombreCliente.getText());
        movimientoInstance.setImporteTotal(Double.parseDouble(this.interfaz.txtTotal.getText()));
        movimientoInstance.setIvaTotal(0);
        movimientoInstance.setDescuentoTotal(Float.parseFloat(this.interfaz.txtDescuento.getText()));
        movimientoInstance.setNumerofolio(0);
        String fechaMovimiento = new SimpleDateFormat("yyyy-MM-dd").format(interfaz.JDFechaVentaMostrador.getDate());
        if (status == 1) {
            movimientoInstance.setFechaAplicado(fechaMovimiento);
            movimientoInstance.setHoraA(this.interfaz.lbhora.getText());
            movimientoInstance.setFechaDocumento(fechaMovimiento);
            movimientoInstance.setHoraD(this.interfaz.lbhora.getText());
        }
        if (status == 0) {
            movimientoInstance.setFechaDocumento(fechaMovimiento);
            movimientoInstance.setHoraD(this.interfaz.lbhora.getText());
        }
        movimientoInstance.setClienteopreveedor(this.interfaz.lblClaveCliente.getText());
        this.instanceAlmacen = (Almacen) this.interfaz.JComboClaveAlmacen.getSelectedItem();
        this.movimientoInstance.setAlmacen(instanceAlmacen);

        movimientoInstance.getAgente().setClave(this.claveEmpleado);

        instanceTipopago = (TipoPago) this.interfaz.jComboTipoPago.getSelectedItem();
        movimientoInstance.setTipopago(instanceTipopago);
        this.instaceEys = (EntradaySalida) this.interfaz.jComboBoxMovimiento.getSelectedItem();
        movimientoInstance.setEntradaysalida(instaceEys);
        this.instanceSerie = (SeriesyFolios) this.interfaz.jComboxSerie.getSelectedItem();
        movimientoInstance.setSerieyfolio(instanceSerie);
        movimientoInstance.setNumerofolio(instanceSFDAO.mtdUpdateNumeroFolio(instanceSerie));
        int respuestaMovimientoId = this.movimientoDAO.mtdInsertar(movimientoInstance);
        if (respuestaMovimientoId >= 0) {
            JOptionPane.showMessageDialog(null, "Cabecera registrado correctamente id=" + respuestaMovimientoId);
//                Detalle movimiento
            try {
                int numeroFila = this.interfaz.jtDatosVenta.getRowCount();
                for (int i = 0; i < numeroFila; i++) {
                    detmovimientoInstance.setClave(0);
                    detmovimientoInstance.getMovimiento().setClave(respuestaMovimientoId);
                    detmovimientoInstance.getArticulo().setCodigo(this.interfaz.jtDatosVenta.getValueAt(i, 1).toString());
                    detmovimientoInstance.setCodigoBarras(this.interfaz.jtDatosVenta.getValueAt(i, 1).toString());
                    detmovimientoInstance.setCantidadArticulo(Float.parseFloat(this.interfaz.jtDatosVenta.getValueAt(i, 0).toString()));
                   
                    
                    //Actualizar Existencias
                    String signo = movimientoInstance.getEntradaysalida().getSigno();
                    Double existencias = Double.parseDouble(signo + this.interfaz.jtDatosVenta.getValueAt(i, 0).toString());
                    instanceArticulo.setCodigo(this.interfaz.jtDatosVenta.getValueAt(i, 1).toString());
                    instacenExistencia.setArticulo(instanceArticulo);
                    instacenExistencia.setAlmacen(instanceAlmacen);
                    instacenExistencia.setExistencias(existencias);
                    instanceExistenciaDAO.mtdActualizar(instacenExistencia);
                    //Terminar Actualizar Existencias

                    detmovimientoInstance.setCantidadArticulo(Float.parseFloat(this.interfaz.jtDatosVenta.getValueAt(i, 0).toString()));
                    detmovimientoInstance.setNombreArticulo(this.interfaz.jtDatosVenta.getValueAt(i, 2).toString());
                    detmovimientoInstance.setClaveUnidad(this.interfaz.jtDatosVenta.getValueAt(i, 6).toString());
                    detmovimientoInstance.setPrecioUnitarioArticulo(Float.parseFloat(this.interfaz.jtDatosVenta.getValueAt(i, 3).toString()));
                    detmovimientoInstance.setPrecioTotalArticulo(Double.parseDouble(this.interfaz.jtDatosVenta.getValueAt(i, 5).toString()));
                    detmovimientoInstance.setIvaUnitarioArticulo(0);
                    detmovimientoInstance.setIvaTotalArticulo(0);
                    detmovimientoInstance.setDescuentoUnitarioArticulo(Float.parseFloat(this.interfaz.jtDatosVenta.getValueAt(i, 4).toString()));
                    detmovimientoInstance.setDescuentoTotalArticulo(0);

                    String respuesta = DetalleDB.mtdInsertarDetalller(detmovimientoInstance);
//                    String respuesta = "";
                    if (i + 1 == numeroFila) {
                        nuevaVenta();
                        this.interfaz.JDCobrar.dispose();
                        this.interfaz.txtRecibi.setText("0");
                        this.interfaz.txtCambio.setText("0");
                        JOptionPane.showMessageDialog(null, respuesta);
                    }
                }
            } catch (Exception ee) {
                JOptionPane.showMessageDialog(null, "erro" + ee);
            }
//           Fin detalle Movimiento
        } else {
            JOptionPane.showMessageDialog(null, respuestaMovimientoId);
        }
    }

}
