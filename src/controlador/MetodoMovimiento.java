/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

 import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField; 
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author CURIEL
 */
public class MetodoMovimiento {
    ValidarCampos validar = new ValidarCampos();
    Double subTotal = 0.0;
    float descuento = 0;
    Double total = 0.0;
    Double cantidadArtiulo = 0.0;

    protected void mdtSalir(JTable table, JInternalFrame frame, JInternalFrame Jiframe) {
        int numero = table.getRowCount();
        if (numero >= 1) {
            int rpu = JOptionPane.showConfirmDialog(null, "Tienes " + numero + " Registros en la bandeja de Articulo,Si sale se perdera la información");
            if (rpu == 0) {
                if (frame == null) {
                    Jiframe.dispose();
                } else {
                    frame.dispose();
                }
            }
        } else {
            if (frame == null) {
                Jiframe.dispose();
            } else {
                frame.dispose();
            }
        }
    }

    protected void mtdNuevaVenta(JTable table, DefaultTableModel modeloT, JTextField txtSubTotal, JTextField txtDescuento, JTextField txtTotal, JLabel lblCantidadArticulo, JTextField txtBuscarCodigoBarrasNombre) {
        int numeroFila = table.getRowCount();
        modeloT = (DefaultTableModel) table.getModel();
        for (int j = 0; j < numeroFila; j++) {
            modeloT.removeRow(0);
        }
        this.subTotal = 0.0;
        this.descuento = 0;
        this.total = 0.0;
        this.cantidadArtiulo = 0.0;
        txtSubTotal.setText("0.0");
        txtDescuento.setText("0");
        txtTotal.setText("0.0");
        lblCantidadArticulo.setText("0");
        txtBuscarCodigoBarrasNombre.grabFocus();
    }

    public void mtdEliminarArticulo(JTable table, DefaultTableModel modeloT, JTextField txtSubTotal, JTextField txtDescuento, JTextField txtTotal, JLabel lblCantidadArticulo, JTextField txtBuscarCodigoBarrasNombre) {
        Double importe = 0.0, cantidad = 0.0,descuento=0.0;
        try {
            int fila = table.getSelectedRow();
            if (fila >= 0) {
                int resp = JOptionPane.showConfirmDialog(null, "Esta seguro de eliminar este Articulo", "Eliminar", JOptionPane.YES_NO_OPTION);
                if (resp == JOptionPane.YES_OPTION) {
                    cantidad = Double.parseDouble(String.valueOf(table.getValueAt(fila, 0)));
                    importe = Double.parseDouble(String.valueOf(table.getValueAt(fila, 5)));
                    descuento = Double.parseDouble(String.valueOf(table.getValueAt(fila, 4)));

                    this.subTotal -= importe;
                    this.descuento -= descuento;
                    this.total -= (importe-descuento);

                    txtSubTotal.setText(validar.redondeoDecimalesPreciosCostos(subTotal)+"");
                    txtDescuento.setText("" + validar.redondeoDecimalesPreciosCostos(this.descuento));
                    txtTotal.setText((validar.redondeoDecimalesPreciosCostos(total))+"");

                    this.cantidadArtiulo -= cantidad;
                    lblCantidadArticulo.setText(String.valueOf(validar.redondeoDecimalesCantidad(this.cantidadArtiulo)));
                    modeloT = (DefaultTableModel) table.getModel();
                    modeloT.removeRow(fila);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Seleccione el Articulo que desea eliminar", "Error", JOptionPane.WARNING_MESSAGE);
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
        txtBuscarCodigoBarrasNombre.grabFocus();
    }

    protected void buscarArticulo(DefaultTableModel modeloT,JTable table,Double subTotal,Double descuento,Double total,JTextField txtSubTotal,JTextField txtDescuento,JTextField txtTotal,Double cantidadArticulo, JLabel lblCantidadArticulo) {
        this.subTotal += subTotal;
        this.descuento += descuento;
        this.total += total;
        this.cantidadArtiulo += cantidadArticulo;
        txtSubTotal.setText(String.valueOf(validar.redondeoDecimalesPreciosCostos(this.subTotal)));
        txtDescuento.setText("" +validar.redondeoDecimalesPreciosCostos(this.descuento));
        txtTotal.setText(String.valueOf(validar.redondeoDecimalesPreciosCostos(this.total)));
        lblCantidadArticulo.setText(String.valueOf(validar.redondeoDecimalesCantidad(this.cantidadArtiulo)));
         
    }
   protected void actualizarTabla(int columna,int row,JTable table,JTextField txtSubTotal,JTextField txtTotal,JTextField txtDescuento,JLabel Cantidad){
                    Double costoUnitarioUpdate = 0.0;
                    Double costoTotalQuitar = 0.0;
                    float cantidadupdate = 0;
                    float cantidadQuitar=0;
                    Double nuevoCostoTotal = 0.0;
                    float descuentoUpdate=0;
                    float descuentoQuitar=0;
                     if (columna == 4 ||columna == 3 || columna==0) {
                        cantidadupdate = Float.parseFloat(table.getValueAt(row, 0).toString());
                        cantidadQuitar=Float.parseFloat(table.getValueAt(row,7).toString());
                        costoUnitarioUpdate = Double.parseDouble(table.getValueAt(row, 3).toString());
                        costoTotalQuitar = Double.parseDouble(table.getValueAt(row, 5).toString());  
                        descuentoUpdate=Float.parseFloat(table.getValueAt(row, 4).toString());
                        descuentoQuitar=Float.parseFloat(table.getValueAt(row, 8).toString());
                        nuevoCostoTotal = costoUnitarioUpdate * cantidadupdate;
                        this.subTotal-= costoTotalQuitar;
                        this.subTotal += nuevoCostoTotal;
                        
                        if(descuentoQuitar!=0){
                            this.descuento-=descuentoQuitar;
                        }
                        this.descuento+=descuentoUpdate;
                        
                        this.total = this.subTotal-this.descuento;
                        this.cantidadArtiulo-=cantidadQuitar;
                        this.cantidadArtiulo+=cantidadupdate;

                        txtTotal.setText(String.valueOf(validar.redondeoDecimalesPreciosCostos(total)));
                        txtDescuento.setText(validar.redondeoDecimalesPreciosCostos(this.descuento)+"");
                        txtSubTotal.setText(validar.redondeoDecimalesPreciosCostos(this.subTotal)+"");
                        Cantidad.setText(validar.redondeoDecimalesCantidad(this.cantidadArtiulo)+"");
                        table.setValueAt(validar.redondeoDecimalesPreciosCostos(nuevoCostoTotal), row, 5);
                        table.setValueAt(validar.redondeoDecimalesCantidad(cantidadupdate), row, 7);
                        table.setValueAt(validar.redondeoDecimalesPreciosCostos(descuentoUpdate), row, 8);
                    }  
   }
    //Metodos para Cobro
    protected void mtdCalcularPago(JTextField txtTotal, JTextField txtRecibi, JTextField txtCambio) {
        try {
            Double total = Double.parseDouble(txtTotal.getText());
            Double Recibi = Double.parseDouble(txtRecibi.getText());
            if (Recibi > 0) {
                Double Cambio = Recibi - total;
                txtCambio.setText(String.valueOf(validar.redondeoDecimalesPreciosCostos(Cambio)));          
            }
        } catch (Exception ex) {
        }
    }
}
