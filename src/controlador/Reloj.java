/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JLabel;

/**
 *
 * @author eleazar
 */
public class Reloj extends Thread {

    private JLabel lb;

    public Reloj(JLabel lb) {
        this.lb = lb;
    }

    public void run() {
        while(true){
            Date fecha=new Date();
            SimpleDateFormat df=new SimpleDateFormat("hh:mm:ss aa");
            lb.setText(df.format(fecha));
            try {
                sleep(1000);
            } catch (Exception e) {
            }
        }
    }
}
