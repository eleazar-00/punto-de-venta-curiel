/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JTextField;

/**
 *
 * @author eleazar
 */
public class ValidarCampos {
   
    protected void mtdconvertirMayusculas(java.awt.event.KeyEvent evt) {
        if (evt.getKeyCode() == KeyEvent.VK_BACK_SPACE || evt.getKeyCode() == KeyEvent.VK_A || evt.getKeyCode() == KeyEvent.VK_B || evt.getKeyCode() == KeyEvent.VK_C || evt.getKeyCode() == KeyEvent.VK_D
                || evt.getKeyCode() == KeyEvent.VK_E || evt.getKeyCode() == KeyEvent.VK_F || evt.getKeyCode() == KeyEvent.VK_G || evt.getKeyCode() == KeyEvent.VK_H || evt.getKeyCode() == KeyEvent.VK_I
                || evt.getKeyCode() == KeyEvent.VK_J || evt.getKeyCode() == KeyEvent.VK_K || evt.getKeyCode() == KeyEvent.VK_L || evt.getKeyCode() == KeyEvent.VK_M || evt.getKeyCode() == KeyEvent.VK_N
                || evt.getKeyCode() == KeyEvent.VK_O || evt.getKeyCode() == KeyEvent.VK_P || evt.getKeyCode() == KeyEvent.VK_Q || evt.getKeyCode() == KeyEvent.VK_R || evt.getKeyCode() == KeyEvent.VK_S
                || evt.getKeyCode() == KeyEvent.VK_T || evt.getKeyCode() == KeyEvent.VK_U || evt.getKeyCode() == KeyEvent.VK_V || evt.getKeyCode() == KeyEvent.VK_W || evt.getKeyCode() == KeyEvent.VK_X
                || evt.getKeyCode() == KeyEvent.VK_Y || evt.getKeyCode() == KeyEvent.VK_Z) {
                        evt.consume();

        }
    }

    protected void mtdvalidarSoloNumeros(java.awt.event.KeyEvent evt) {
        char car = evt.getKeyChar();
        if ((car < '0' || car > '9')) {
            evt.consume();
            
        }
    }
    protected void mtdvalidarNumerosPunto(java.awt.event.KeyEvent evt) {
        char car = evt.getKeyChar();
        if ((car < '0' || car > '9')&& car != '.') {
            evt.consume();
            
        }
    }

    protected void mtdvalidarCantidadCaracteres(java.awt.event.KeyEvent evt,JTextField txtJText,int limite) {
        if ( txtJText.getText().length() == limite) {
            evt.consume();
        }
    }
  
   protected boolean mtdEmail(String email) {
        Pattern pattern = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher mather = pattern.matcher(email);
        if (mather.find()) {
            return true;
        } else {
            return false;
        }
    }

    public double redondeoDecimalesPreciosCostos(double numero){
        String numeroDecimalesPrecios="6";
        try{
	   Properties pro = new Properties();
           FileReader reader = new FileReader("Config.properties");
           pro.load(reader);
            numeroDecimalesPrecios = pro.getProperty("numeroDecimalesPrecios"); 
	}catch(FileNotFoundException e){
			System.out.println("Error E/S: el fichero no existe");
	}
	catch(IOException e){
			System.out.println("Si el fichero no existe, este mensaje no se vera");
	}
        
        BigDecimal redondeado = new BigDecimal(numero)
                .setScale(Integer.parseInt(numeroDecimalesPrecios), RoundingMode.HALF_EVEN);
        return redondeado.doubleValue();
    }
    public double redondeoDecimalesCantidad(double numero){
        String numeroDecimalesPrecios="6";
        try{
	   Properties pro = new Properties();
           FileReader reader = new FileReader("Config.properties");
           pro.load(reader);
            numeroDecimalesPrecios = pro.getProperty("numeroDecimalesCantidad"); 
	}catch(FileNotFoundException e){
			System.out.println("Error E/S: el fichero no existe");
	}
	catch(IOException e){
			System.out.println("Si el fichero no existe, este mensaje no se vera");
	}
        
        BigDecimal redondeado = new BigDecimal(numero)
                .setScale(Integer.parseInt(numeroDecimalesPrecios), RoundingMode.HALF_EVEN);
        return redondeado.doubleValue();
    }

}
