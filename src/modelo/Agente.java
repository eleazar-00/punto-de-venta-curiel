/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;
/**
 *
 * @author ELEAZAR
 */
public class Agente {
   int clave;
   private int status;
   protected String nombre;
   private String rfc;
   private String pais;
   private String estado;
   private String poblacion;
   private String colonia;
   private String calle;
   private String codigoPostal;
   private String celular;
   private String telefono;
   private String correo;

   private String almacenes;
   private String series;
   private String precios;

   public TipoAgente tipoAgente;
   public Usuario usuario;

    public Agente() {
        clave = 0;
        status = 1;
        nombre = "";
        rfc="";
        pais = "";
        estado = "";
        poblacion = "";
        colonia = "";
        calle = "";
        codigoPostal = "";
        celular = "";
        telefono = "";
        correo = "";
        this.almacenes = "";
        this.series = "";
        this.precios = "";
    }

    public int getClave() {
        return clave;
    }

    public void setClave(int clave) {
        this.clave = clave;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getNombre() {
        return nombre;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }
    

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getPoblacion() {
        return poblacion;
    }

    public void setPoblacion(String poblacion) {
        this.poblacion = poblacion;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getAlmacenes() {
        return almacenes;
    }

    public void setAlmacenes(String almacenes) {
        this.almacenes = almacenes;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getPrecios() {
        return precios;
    }

    public void setPrecios(String precios) {
        this.precios = precios;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
    public TipoAgente getTipoAgente() {
        return tipoAgente;
    }

    public void setTipoAgente(TipoAgente tipoAgente) {
        this.tipoAgente = tipoAgente;
    }

    @Override
    public String toString() {
        return nombre;
    }
    
   
}
