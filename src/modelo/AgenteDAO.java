/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author eleazar
 */
public class AgenteDAO {

    Conexion conexion;
    PreparedStatement ps;

    public AgenteDAO() {
        conexion = new Conexion();
    }

    public String mtdInsertar(Agente agente) {
        String respuesta = "";
        try {
            Connection accesoDB = conexion.getConexion();
            String sql = "insert into agente value(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            ps = accesoDB.prepareStatement(sql);
            ps.setInt(1, agente.getClave());
            ps.setInt(2, agente.getStatus());
            ps.setString(3, agente.getNombre());
            ps.setString(4, agente.getRfc());
            ps.setString(5, agente.getPais());
            ps.setString(6, agente.getEstado());
            ps.setString(7, agente.getPoblacion());
            ps.setString(8, agente.getColonia());
            ps.setString(9, agente.getCalle());
            ps.setString(10, agente.getCodigoPostal());
            ps.setString(11, agente.getCelular());
            ps.setString(12, agente.getTelefono());
            ps.setString(13, agente.getCorreo());
            ps.setString(14, agente.getAlmacenes());
            ps.setString(15, agente.getSeries());
            ps.setString(16, agente.getPrecios());
            ps.setString(17, agente.getTipoAgente().getClave());
            ps.setString(18, agente.getUsuario().getUsuario());
            int valor = ps.executeUpdate();
            if (valor == 1) {
                respuesta = "Creado";
            } else {
                respuesta = "Error desconocido";
            }
        } catch (Exception e) {
            respuesta = "Error " + e;
        }
        return respuesta;
    }

    public ArrayList<Agente> mtdRellenarAgente(Agente agente, String status, String filtro) {
        ArrayList lista = new ArrayList();
        Usuario usuario;
        TipoAgente tipoagente;
        try {
            Connection accesoDB = conexion.getConexion();
            if (filtro.equals("todo")) {
                ps = accesoDB.prepareStatement("select *from agente a inner join usuario u on u.usuario=a.usuario_clave inner join tipo_agente ta on a.tipo_agente_clave=ta.clave where a.status in (" + status + ")");
            }
            if (filtro.equals("clave")) {
                ps = accesoDB.prepareStatement("select *from agente a inner join usuario u on u.usuario=a.usuario_clave inner join tipo_agente ta on a.tipo_agente_clave=ta.clave where a.clave=? and a.status in (" + status + ")");
                ps.setInt(1, agente.getClave());
            }
            if (filtro.equals("razon")) {
                ps = accesoDB.prepareStatement("select *from agente a inner join usuario on u.usuario=a.usuario_clave inner join tipo_agente ta on a.tipo_agente_clave=ta.clave where a.nombre LIKE ? and a.status in (" + status + ") LIMIT 30");
                ps.setString(1, "%" + agente.getNombre() + "%");
            }
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                agente = new Agente();
                agente.setClave(rs.getInt(1));
                agente.setStatus(rs.getInt(2));
                agente.setNombre(rs.getString(3));
                agente.setRfc(rs.getString(4));
                agente.setPais(rs.getString(5));
                agente.setEstado(rs.getString(6));
                agente.setPoblacion(rs.getString(7));
                agente.setColonia(rs.getString(8));
                agente.setCalle(rs.getString(9));
                agente.setCodigoPostal(rs.getString(10));
                agente.setCelular(rs.getString(11));
                agente.setTelefono(rs.getString(12));
                agente.setCorreo(rs.getString(13));
                agente.setAlmacenes(rs.getString(14));
                agente.setSeries(rs.getString(15));
                agente.setPrecios(rs.getString(16));

                usuario = new Usuario();
                usuario.setUsuario(rs.getString(19));
                usuario.setUsuario(rs.getString(20));
                usuario.setPassword(rs.getString(21));
                agente.setUsuario(usuario);

                tipoagente = new TipoAgente();
                tipoagente.setClave(rs.getString(22));
                tipoagente.setNombre(rs.getString(23));
                agente.setTipoAgente(tipoagente);
//                 agente.getTipoAgente();
                lista.add(agente);
            }
        } catch (Exception e) {
        }
        conexion.cerrarConexio();
        return lista;
    }

    public void mtdElimnar(Agente instance) {
        try {
            Connection accesoDB = conexion.getConexion();
            PreparedStatement ps = accesoDB.prepareStatement("delete from agente where clave=?");
            ps.setInt(1, instance.getClave());
            int rp = ps.executeUpdate();
            if (rp == 1) {
                JOptionPane.showMessageDialog(null, "Eliminado correctamente");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error = " + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public int mtdActualizar(Agente agente) {
        int rpt = 0;
        try {
            Connection accesoDB = conexion.getConexion();
            String sql = "update agente set status=?, nombre=?, rfc=?, pais=?, estado=?, poblacion=?, colonia=?, calle=?, codigo_postal=?, celular=?, telefono=?, correo=?, almacenes=?, series=?, precios=?, tipo_agente_clave=?, usuario_clave=? where clave=?";
            PreparedStatement ps = accesoDB.prepareStatement(sql);
            ps.setInt(18, agente.getClave());
            ps.setInt(1, agente.getStatus());
            ps.setString(2, agente.getNombre());
            ps.setString(3, agente.getRfc());
            ps.setString(4, agente.getPais());
            ps.setString(5, agente.getEstado());
            ps.setString(6, agente.getPoblacion());
            ps.setString(7, agente.getColonia());
            ps.setString(8, agente.getCalle());
            ps.setString(9, agente.getCodigoPostal());
            ps.setString(10, agente.getCelular());
            ps.setString(11, agente.getTelefono());
            ps.setString(12, agente.getCorreo());
            ps.setString(13, agente.getAlmacenes());
            ps.setString(14, agente.getSeries());
            ps.setString(15, agente.getPrecios());
            ps.setString(16, agente.getTipoAgente().getClave());
            ps.setString(17, agente.getUsuario().getUsuario());

            rpt = ps.executeUpdate();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
        return rpt;
    }

    public int mtdUltimoIDdeMovimiento() {
        int numero = 0;
        try {
            Connection accesoDB = conexion.getConexion();
            String sql = "Select MAX(clave) as id FROM agente";
            PreparedStatement ps = accesoDB.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                numero = rs.getInt(1);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al crear clave consecutivo" + e);
        }
        return numero;
    }

    public ArrayList<Agente> mtdDatosUsuarioLogin(Agente agente) {
        ArrayList lista = new ArrayList();
         try {
            Connection accesoDB = conexion.getConexion();
            ps = accesoDB.prepareStatement("select clave,status,nombre,almacenes,series,precios from agente where usuario_clave=?");
            ps.setString(1, agente.getUsuario().getUsuario());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                agente = new Agente();
                agente.setClave(rs.getInt(1));
                agente.setStatus(rs.getInt(2));
                agente.setNombre(rs.getString(3));
                agente.setAlmacenes(rs.getString(4));
                agente.setSeries(rs.getString(5));
                agente.setPrecios(rs.getString(6));
                lista.add(agente);
            }
        } catch (Exception e) {
            System.out.println("error validad exceptioon==="+e);
        }
        conexion.cerrarConexio();
        return lista;
    }
}
