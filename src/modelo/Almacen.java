package modelo;
/**
 * @author ELEAZAR
 */
public class Almacen {
    protected String clave;
    protected String nombre;
    protected String pais;
    protected String estado;
    protected String poblacion;
    protected String direccion;
    protected String telefono;
    protected String email;
    protected String flgalmacen;

    public Almacen(String clave, String nombre, String pais, String estado, String poblacion, String direccion, String telefono, String email,String flgalmacen) {
        this.clave = clave;
        this.nombre = nombre;
        this.pais = pais;
        this.estado = estado;
        this.poblacion = poblacion;
        this.direccion = direccion;
        this.telefono = telefono;
        this.email = email;
        this.flgalmacen=flgalmacen;
    }

    public Almacen() {
        clave = "";
        nombre = "";
        pais = "";
        poblacion = "";
        direccion = "";
        estado = "";
        email = "";
        telefono = "";
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getPoblacion() {
        return poblacion;
    }

    public void setPoblacion(String poblacion) {
        this.poblacion = poblacion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFlgalmacen() {
        return flgalmacen;
    }

    public void setFlgalmacen(String flgalmacen) {
        this.flgalmacen = flgalmacen;
    }

    @Override
    public String toString() {
        return  nombre;
    }

}
