
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author ELEAZAR
 */
public class AlmacenDAO {

    Conexion conexion;
    Connection acceDB;
    String sql;
    PreparedStatement st;

    public AlmacenDAO() {
        conexion = new Conexion();
    }

    public String mtdInserta(Almacen instance) {
        String rptRegistro = null;
        try {
            acceDB = conexion.getConexion();
            sql = ("insert into almacen (clave,nombre,pais,estado,poblacion,direccion,telefono,email,flgalmacen) value(?,?,?,?,?,?,?,?,?)");
            st = acceDB.prepareStatement(sql);
            st.setString(1, instance.getClave());
            st.setString(2, instance.getNombre());
            st.setString(3, instance.getPais());
            st.setString(4, instance.getEstado());
            st.setString(5, instance.getPoblacion());
            st.setString(6, instance.getDireccion());
            st.setString(7, instance.getTelefono());
            st.setString(8, instance.getEmail());
            st.setString(9, instance.getFlgalmacen());
            int numDatos = st.executeUpdate();
            if (numDatos > 0) {
                rptRegistro = "Registrado";
            } else {
                rptRegistro = "Error desconocido";
            }
        } catch (Exception e) {
            rptRegistro = "Error =" + e;
        }
        return rptRegistro;
    }

    public ArrayList<Almacen> listaAlmacen(Almacen instance, String Buscar, String ClaveAlmacen) {
        ArrayList listaAlmacen = new ArrayList();
        Almacen almacen;
        try {
            acceDB = conexion.getConexion();
            if (Buscar.equals("todo")) {
                sql = ("select *from almacen");
                st = acceDB.prepareStatement(sql);
            }
            if (Buscar.equals("clave")) {
                sql = ("select *from almacen where clave=?");
                st = acceDB.prepareStatement(sql);
                st.setString(1, instance.getClave());
            }
            if (Buscar.equals("compra")) {
                 sql = ("select *from almacen where SUBSTR(flgalmacen,1,1)='S' and clave in ("+ClaveAlmacen+")");
                st = acceDB.prepareStatement(sql);
            }
            if (Buscar.equals("venta")) {
                sql = ("select *from almacen where SUBSTR(flgalmacen,2,1)='S' and clave in ("+ClaveAlmacen+")");
                st = acceDB.prepareStatement(sql);
            }
            if (Buscar.equals("interno")) {
                sql = ("select *from almacen where SUBSTR(flgalmacen,3,1)='S' in ("+ClaveAlmacen+")");
                st = acceDB.prepareStatement(sql);
            }
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                almacen = new Almacen();
                almacen.setClave(rs.getString(1));
                almacen.setNombre(rs.getString(2));
                almacen.setPais(rs.getString(3));
                almacen.setEstado(rs.getString(4));
                almacen.setPoblacion(rs.getString(5));
                almacen.setDireccion(rs.getString(6));
                almacen.setTelefono(rs.getString(7));
                almacen.setEmail(rs.getString(8));
                almacen.setFlgalmacen(rs.getString(9));
                listaAlmacen.add(almacen);
            }
        } catch (Exception e) {
            System.out.println(e + "Error Array Almacen");
        }
        conexion.cerrarConexio();
        return listaAlmacen;
    }

    public int mtdActualizar(Almacen instance) {
        int numFA = 0;
        try {
            acceDB = conexion.getConexion();
            sql = "update almacen set nombre=?,pais=?, estado=?, poblacion=?, direccion=?, telefono=?, email=?, flgalmacen=? where clave=?";
            st = acceDB.prepareStatement(sql);
            st.setString(1, instance.getNombre());
            st.setString(2, instance.getPais());
            st.setString(3, instance.getEstado());
            st.setString(4, instance.getPoblacion());
            st.setString(5, instance.getDireccion());
            st.setString(6, instance.getTelefono());
            st.setString(7, instance.getEmail());
            st.setString(8, instance.getFlgalmacen());
            st.setString(9, instance.getClave());
            numFA = st.executeUpdate();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Erro=" + e);
        }
        return numFA;
    }

    public void mtdEliminar(Almacen instance) {
        try {
            acceDB = conexion.getConexion();
            sql = ("delete from almacen where clave=?");
            st = acceDB.prepareStatement(sql);
            st.setString(1, instance.getClave());
            st.execute();
            JOptionPane.showMessageDialog(null, "Almacen eliminado");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "El Almacen no puede ser eliminado", "Error", JOptionPane.WARNING_MESSAGE);
        }
    }

    public ResultSet cargarComboAlmacen() throws Exception {
        acceDB = conexion.getConexion();
        st = acceDB.prepareStatement("select *from almacen");
        ResultSet rs = st.executeQuery();
        return rs;
    }
}
