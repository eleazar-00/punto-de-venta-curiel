/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.FileInputStream;

/**
 *
 * @author ELEAZAR
 */
public class Articulo {
    String codigo;
    String codigoBarras;
    int status;
    String nombre;
    String tipo;
    int diasCaducidad;
    
    String url_imagen;
    FileInputStream imagen;
    
    double descuentoMaximo;
    String fechaActualizacionCosto;
    
    double costoUltimo;
    double costoReposicion;
    double costoAnterior;
    double costoPromedio;
    
    String costoUtilizar;

//    Precios
    double precio_lista;
    double utilidad_lista;

    double precio_dos;
    double utilidad_dos;
    double cantidad_dos;

    double precio_tres;
    double utilidad_tres;
    double cantidad_tres;

    //Claves relaciones 
    Departamento departamento;
    Marca marca;
    Ubicacion ubicacion;
    
    Unidad unidad;
    
    Impuesto impuesto;
    
    String clave_modeda;
    String clave_sat;
    
    public Articulo() {
        codigo="";
        codigoBarras="";
        status=1;
        nombre="";
        diasCaducidad = 0;
        costoUltimo = 0;
        costoReposicion=0;
        costoAnterior=0;
        costoPromedio=0;
        
        url_imagen = "";
        imagen = null;
        descuentoMaximo=0;
        fechaActualizacionCosto="";
        precio_lista = 0;
        utilidad_lista = 0;

        precio_dos = 0;
        utilidad_dos = 0;
        cantidad_dos = 0;

        precio_tres = 0;
        utilidad_tres = 0;
        cantidad_tres = 0;
        
        clave_modeda = "";
        clave_sat="";
    }

    public String getCodigo() {
        return codigo;
    }
    
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    public String getCodigoBarras() {
        return codigoBarras;
    }

    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }
    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getDiasCaducidad() {
        return diasCaducidad;
    }

    public void setDiasCaducidad(int diasCaducidad) {
        this.diasCaducidad = diasCaducidad;
    }
    

    public String getUrl_imagen() {
        return url_imagen;
    }

    public void setUrl_imagen(String url_imagen) {
        this.url_imagen = url_imagen;
    }

    public FileInputStream getImagen() {
        return imagen;
    }

    public void setImagen(FileInputStream imagen) {
        this.imagen = imagen;
    }

    public double getDescuentoMaximo() {
        return descuentoMaximo;
    }

    public void setDescuentoMaximo(double descuentoMaximo) {
        this.descuentoMaximo = descuentoMaximo;
    }

    public String getFechaActualizacionCosto() {
        return fechaActualizacionCosto;
    }

    public void setFechaActualizacionCosto(String fechaActualizacionCosto) {
        this.fechaActualizacionCosto = fechaActualizacionCosto;
    }

    public double getCostoUltimo() {
        return costoUltimo;
    }

    public void setCostoUltimo(double costoUltimo) {
        this.costoUltimo = costoUltimo;
    }

    public double getCostoReposicion() {
        return costoReposicion;
    }

    public void setCostoReposicion(double costoReposicion) {
        this.costoReposicion = costoReposicion;
    }

    public double getCostoAnterior() {
        return costoAnterior;
    }

    public void setCostoAnterior(double costoAnterior) {
        this.costoAnterior = costoAnterior;
    }

    public double getCostoPromedio() {
        return costoPromedio;
    }

    public void setCostoPromedio(double costoPromedio) {
        this.costoPromedio = costoPromedio;
    }

    public String getCostoUtilizar() {
        return costoUtilizar;
    }

    public void setCostoUtilizar(String costoUtilizar) {
        this.costoUtilizar = costoUtilizar;
    }

   

    public Unidad getUnidad() {
        return unidad;
    }

    public void setUnidad(Unidad unidad) {
        this.unidad = unidad;
    }

    public Impuesto getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(Impuesto impuesto) {
        this.impuesto = impuesto;
    }

    public String getClave_modeda() {
        return clave_modeda;
    }

    public void setClave_modeda(String clave_modeda) {
        this.clave_modeda = clave_modeda;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public Marca getMarca() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(Ubicacion ubicacion) {
        this.ubicacion = ubicacion;
    }

    public double getPrecio_lista() {
        return precio_lista;
    }

    public void setPrecio_lista(double precio_lista) {
        this.precio_lista = precio_lista;
    }

    public double getUtilidad_lista() {
        return utilidad_lista;
    }

    public void setUtilidad_lista(double utilidad_lista) {
        this.utilidad_lista = utilidad_lista;
    }

    public double getPrecio_dos() {
        return precio_dos;
    }

    public void setPrecio_dos(double precio_dos) {
        this.precio_dos = precio_dos;
    }

    public double getUtilidad_dos() {
        return utilidad_dos;
    }

    public void setUtilidad_dos(double utilidad_dos) {
        this.utilidad_dos = utilidad_dos;
    }

    public double getCantidad_dos() {
        return cantidad_dos;
    }

    public void setCantidad_dos(double cantidad_dos) {
        this.cantidad_dos = cantidad_dos;
    }

    public double getPrecio_tres() {
        return precio_tres;
    }

    public void setPrecio_tres(double precio_tres) {
        this.precio_tres = precio_tres;
    }

    public double getUtilidad_tres() {
        return utilidad_tres;
    }

    public void setUtilidad_tres(double utilidad_tres) {
        this.utilidad_tres = utilidad_tres;
    }

    public double getCantidad_tres() {
        return cantidad_tres;
    }

    public void setCantidad_tres(double cantidad_tres) {
        this.cantidad_tres = cantidad_tres;
    }

    public String getClave_sat() {
        return clave_sat;
    }

    public void setClave_sat(String clave_sat) {
        this.clave_sat = clave_sat;
    }

     

}
