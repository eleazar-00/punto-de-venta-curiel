/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author ELEAZAR
 */
public class ArticuloDAO {

    Conexion conexion;
    PreparedStatement st;
    Connection accesoDB;

    public ArticuloDAO() {
        conexion = new Conexion();
    }

    public String mtdInserta(Articulo instance) {
        String rptRegistro = "";
        try {
            accesoDB = conexion.getConexion();
            st = accesoDB.prepareStatement("insert into articulo (codigo,codigo_barras,status,descripcion,tipo,dias_caducidad,url_imagen,imagen,descuentoMaximo,fechaActualizacionCosto,costo_ultimo,costo_reposicion,costo_anterior,costo_promedio,costo_utilizar,precio_lista,utilidad_lista,precio_dos,utilidad_dos,cantidad_dos,precio_tres,utilidad_tres,cantidad_tres,catartlinea_clave,catartmarca_clave,catartubicacion_clave,unidad_clave_unidad,clave_sat) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            st.setString(1, instance.getCodigo());
            st.setString(2, instance.getCodigoBarras());
            st.setInt(3, instance.getStatus());
            st.setString(4, instance.getNombre());
            st.setString(5, instance.getTipo());
            st.setInt(6, instance.getDiasCaducidad());
            st.setString(7, instance.getUrl_imagen());
            st.setBinaryStream(8, instance.getImagen());
            st.setDouble(9, instance.getDescuentoMaximo());
            st.setString(10, instance.getFechaActualizacionCosto());
            st.setDouble(11, instance.getCostoUltimo());
            st.setDouble(12, instance.getCostoReposicion());
            st.setDouble(13, instance.getCostoAnterior());
            st.setDouble(14, instance.getCostoPromedio());
            st.setString(15, instance.getCostoUtilizar());
            st.setDouble(16, instance.getPrecio_lista());
            st.setDouble(17, instance.getUtilidad_lista());
            st.setDouble(18, instance.getPrecio_dos());
            st.setDouble(19, instance.getUtilidad_dos());
            st.setDouble(20, instance.getCantidad_dos());
            st.setDouble(21, instance.getPrecio_tres());
            st.setDouble(22, instance.getUtilidad_tres());
            st.setDouble(23, instance.getCantidad_tres());
            st.setString(24, instance.getDepartamento().getClave());
            st.setString(25, instance.getMarca().getClave());
            st.setString(26, instance.getUbicacion().getClave());
            st.setString(27, instance.getUnidad().getClave());
            st.setString(28, instance.getClave_sat());
            int numFAfectadas = st.executeUpdate();
            if (numFAfectadas > 0) {
                rptRegistro = "Registro Exitoso";
            }
        } catch (Exception e) {
            mtdMessageError("BD Insert " + e);
        }

        return rptRegistro;
    }

    public ArrayList<Articulo> listaArticulo(Articulo instance, String status, String filtro) {
        ArrayList lista = new ArrayList();
        Unidad unidad;
        try {
            accesoDB = conexion.getConexion();
            if (filtro.equals("todo")) {
                st = accesoDB.prepareStatement("SELECT codigo,codigo_barras,descripcion,url_imagen,costo_ultimo,precio_lista,unidad_clave_unidad FROM articulo where status in (" + status + ")");
            }
            if (filtro.equals("departamento")) {
                st = accesoDB.prepareStatement("SELECT codigo,codigo_barras,descripcion,url_imagen,costo_ultimo,precio_lista,unidad_clave_unidad FROM articulo where  catartlinea_clave=? and status in (" + status + ")");
                st.setString(1, instance.getDepartamento().getClave());
            }
            if (filtro.equals("codigo")) {
                st = accesoDB.prepareStatement("SELECT codigo,codigo_barras,descripcion,url_imagen,costo_ultimo,precio_lista,unidad_clave_unidad FROM articulo where codigo=? and status in (" + status + ")");
                st.setString(1, instance.getCodigo());
            }
            if (filtro.equals("codigo_barras")) {
                st = accesoDB.prepareStatement("SELECT codigo,codigo_barras,descripcion,url_imagen,costo_ultimo,precio_lista,unidad_clave_unidad FROM articulo where codigo_barras=? and status in (" + status + ")");
                st.setString(1, instance.getCodigoBarras());
            }
            if (filtro.equals("descripcion")) {
                st = accesoDB.prepareStatement("SELECT codigo,codigo_barras,descripcion,url_imagen,costo_ultimo,precio_lista,unidad_clave_unidad FROM articulo where descripcion like ? and status in (" + status + ")");
                st.setString(1, "%"+instance.getNombre()+"%");
            }
            if (filtro.equals("ventaServicio")) {
                st = accesoDB.prepareStatement("SELECT codigo,codigo_barras,descripcion,url_imagen,costo_ultimo,precio_lista,unidad_clave_unidad FROM articulo where tipo='Servicio' and status in (" + status + ")");
            }
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                instance = new Articulo();
                unidad = new Unidad();
                instance.setCodigo(rs.getString(1));
                instance.setCodigoBarras(rs.getString(2));
                instance.setNombre(rs.getString(3));
                instance.setUrl_imagen(rs.getString(4));
                instance.setCostoUltimo(rs.getDouble(5));
                instance.setPrecio_lista(rs.getDouble(6));
                unidad.setClave(rs.getString(7));
                instance.setUnidad(unidad);
                lista.add(instance);
            }
        } catch (Exception e) {
            mtdMessageError("BD lista " + e);
        }
        conexion.cerrarConexio();
        return lista;
    }

    public ArrayList<Articulo> listaArticulosPorCodigo(Articulo instance, String status) {
        ArrayList lista = new ArrayList();
        Departamento departamento;
        Marca marca;
        Ubicacion ubicacion;
        Unidad unidad;
        try {
            accesoDB = conexion.getConexion();
            st = accesoDB.prepareStatement("SELECT * FROM articulo where codigo=? or codigo_barras=? and status in (" + status + ")");
            st.setString(1, instance.getCodigo());
            st.setString(2, instance.getCodigo());
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                instance = new Articulo();
                departamento = new Departamento();
                marca = new Marca();
                ubicacion = new Ubicacion();
                unidad = new Unidad();
                instance.setCodigo(rs.getString(1));
                instance.setCodigoBarras(rs.getString(2));
                instance.setStatus(rs.getInt(3));
                instance.setNombre(rs.getString(4));
                instance.setTipo(rs.getString(5));
                instance.setDiasCaducidad(rs.getInt(6));
                instance.setUrl_imagen(rs.getString(7));
                instance.setDescuentoMaximo(rs.getDouble(9));
                instance.setFechaActualizacionCosto(rs.getString(10));
                instance.setCostoUltimo(rs.getDouble(11));
                instance.setCostoReposicion(rs.getDouble(12));
                instance.setCostoAnterior(rs.getDouble(13));
                instance.setCostoPromedio(rs.getDouble(14));
                instance.setCostoUtilizar(rs.getString(15));
                instance.setPrecio_lista(rs.getDouble(16));
                instance.setUtilidad_lista(rs.getDouble(17));
                instance.setPrecio_dos(rs.getDouble(18));
                instance.setUtilidad_dos(rs.getDouble(19));
                instance.setCantidad_dos(rs.getDouble(20));
                instance.setPrecio_tres(rs.getDouble(21));
                instance.setUtilidad_tres(rs.getDouble(22));
                instance.setCantidad_tres(rs.getDouble(23));
                departamento.setClave(rs.getString(24));
                instance.setDepartamento(departamento);
                marca.setClave(rs.getString(25));
                instance.setMarca(marca);
                ubicacion.setClave(rs.getString(26));
                instance.setUbicacion(ubicacion);
                unidad.setClave(rs.getString(27));
                instance.setUnidad(unidad);
                instance.setClave_sat(rs.getString(28));
                lista.add(instance);
            }
        } catch (Exception e) {
            mtdMessageError("BD lista " + e);
        }
        conexion.cerrarConexio();
        return lista;
    }

    public int mtdActulizar(Articulo instance) {
        int rptRegistro = 0;
        try {
            accesoDB = conexion.getConexion();
            st = accesoDB.prepareStatement("update articulo set codigo_barras=?, status=?, descripcion=?, tipo=?, dias_caducidad=?, url_imagen=?, imagen=?, descuentoMaximo=?, fechaActualizacionCosto=?,costo_ultimo=?,costo_reposicion=?,costo_anterior=?,costo_promedio=?,precio_lista=?,utilidad_lista=?,precio_dos=?,utilidad_dos=?,cantidad_dos=?,precio_tres=?,utilidad_tres=?,cantidad_tres=?,catartlinea_clave=?,catartmarca_clave=?,catartubicacion_clave=?,unidad_clave_unidad=?,costo_utilizar=?,clave_sat=? where codigo=?");
            st.setString(1, instance.getCodigoBarras());
            st.setInt(2, instance.getStatus());
            st.setString(3, instance.getNombre());
            st.setString(4, instance.getTipo());
            st.setInt(5, instance.getDiasCaducidad());
            st.setString(6, instance.getUrl_imagen());
            st.setBinaryStream(7, instance.getImagen());
            st.setDouble(8, instance.getDescuentoMaximo());
            st.setString(9, instance.getFechaActualizacionCosto());
            st.setDouble(10, instance.getCostoUltimo());
            st.setDouble(11, instance.getCostoReposicion());
            st.setDouble(12, instance.getCostoAnterior());
            st.setDouble(13, instance.getCostoPromedio());
            st.setDouble(14, instance.getPrecio_lista());
            st.setDouble(15, instance.getUtilidad_lista());
            st.setDouble(16, instance.getPrecio_dos());
            st.setDouble(17, instance.getUtilidad_dos());
            st.setDouble(18, instance.getCantidad_dos());
            st.setDouble(19, instance.getPrecio_tres());
            st.setDouble(20, instance.getUtilidad_tres());
            st.setDouble(21, instance.getCantidad_tres());
            st.setString(22, instance.getDepartamento().getClave());
            st.setString(23, instance.getMarca().getClave());
            st.setString(24, instance.getUbicacion().getClave());
            st.setString(25, instance.getUnidad().getClave());
            st.setString(26, instance.getCostoUtilizar());
            st.setString(27, instance.getClave_sat());
            st.setString(28, instance.getCodigo());
            rptRegistro = st.executeUpdate();
        } catch (Exception e) {
            mtdMessageError("BD Actualizar Articulo" + e);

        }
        return rptRegistro;
    }

    public int mtdActulizarCostos(Articulo instance) {
        int rptRegistro = 0;
        try {
            accesoDB = conexion.getConexion();
            st = accesoDB.prepareStatement("update articulo set costo_anterior=costo_ultimo,costo_reposicion=costo_ultimo,costo_ultimo=?,costo_promedio=(costo_anterior+(costo_ultimo))/2,utilidad_lista=(?/precio_lista)*100, fechaActualizacionCosto=?  where codigo=?");
            st.setDouble(1, instance.getCostoUltimo());
            st.setDouble(2, instance.getCostoUltimo());
            st.setString(3, instance.getFechaActualizacionCosto());
            st.setString(4, instance.getCodigoBarras());
            rptRegistro = st.executeUpdate();
        } catch (Exception e) {
            mtdMessageError("BD Actualizar Articulo" + e);

        }
        return rptRegistro;
    }

    public void mtdEliminar(Articulo instance) {
        try {
            accesoDB = conexion.getConexion();
            st = accesoDB.prepareStatement("delete from articulo where codigo=?");
            st.setString(1, instance.getCodigo());
            st.execute();
            JOptionPane.showMessageDialog(null, "Articulo eliminado correctamento");

        } catch (Exception e) {
            mtdMessageError("BD Eliminar " + e);
        }
    }

    private void mtdMessageError(String e) {
        JOptionPane.showMessageDialog(null, "Error " + e, "Error", JOptionPane.ERROR_MESSAGE);
    }

}
