/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author eleazar
 */
public class ArticuloProveedor {
    Articulo articulo;
    Proveedor Proveedor;
    String nombreProveedor;
    int plazoEntrega;
    int prioridad;
    double costoUltimoArticulo;
    String fechaUltimacompra;

    public ArticuloProveedor() {
        nombreProveedor="";
        plazoEntrega = 0;
        prioridad = 0;
        costoUltimoArticulo = 0;
        fechaUltimacompra="";
    }

    public ArticuloProveedor(Articulo articulo, Proveedor Proveedor, String nombreProveedor, int plazoEntrega, int prioridad, double costoUltimoArticulo, String fechaUltimacompra) {
        this.articulo = articulo;
        this.Proveedor = Proveedor;
        this.nombreProveedor = nombreProveedor;
        this.plazoEntrega = plazoEntrega;
        this.prioridad = prioridad;
        this.costoUltimoArticulo = costoUltimoArticulo;
        this.fechaUltimacompra = fechaUltimacompra;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public Proveedor getProveedor() {
        return Proveedor;
    }

    public void setProveedor(Proveedor Proveedor) {
        this.Proveedor = Proveedor;
    }

    public String getNombreProveedor() {
        return nombreProveedor;
    }

    public void setNombreProveedor(String nombreProveedor) {
        this.nombreProveedor = nombreProveedor;
    }

    public int getPlazoEntrega() {
        return plazoEntrega;
    }

    public void setPlazoEntrega(int plazoEntrega) {
        this.plazoEntrega = plazoEntrega;
    }

    public int getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(int prioridad) {
        this.prioridad = prioridad;
    }

    public double getCostoUltimoArticulo() {
        return costoUltimoArticulo;
    }

    public void setCostoUltimoArticulo(double costoUltimoArticulo) {
        this.costoUltimoArticulo = costoUltimoArticulo;
    }

    public String getFechaUltimacompra() {
        return fechaUltimacompra;
    }

    public void setFechaUltimacompra(String fechaUltimacompra) {
        this.fechaUltimacompra = fechaUltimacompra;
    }
    
     
    
}
