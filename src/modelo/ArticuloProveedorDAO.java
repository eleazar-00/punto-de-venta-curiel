/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author eleazar
 */
public class ArticuloProveedorDAO {

    Conexion conexion;
    String sql = "";
    Connection accesoDB;
    PreparedStatement ps;

    public ArticuloProveedorDAO() {
        conexion = new Conexion();
    }

    public String mtdRegistrar(ArticuloProveedor instance) {
        String respuesta = "";
        try {
            accesoDB = conexion.getConexion();
            sql = "insert into articulo_proveedor (articulo_codigo,proveedor_clave,nombre_proveedor,plazo_entrega,prioridad,costo_ultimo,fecha_ultima_compra) values (?,?,?,?,?,?,?)";
            ps = accesoDB.prepareStatement(sql);
            ps.setString(1, instance.getArticulo().getCodigo());
            ps.setInt(2, instance.getProveedor().getClave());
            ps.setString(3, instance.getNombreProveedor());
            ps.setInt(4, instance.getPlazoEntrega());
            ps.setInt(5, instance.getPrioridad());
            ps.setDouble(6, instance.getCostoUltimoArticulo());
            ps.setString(7, instance.getFechaUltimacompra());
            int respueta = ps.executeUpdate();
            if (respueta > 0) {
                respuesta = "Registrado Correctamente";
            } else {
                respuesta = "error deconocido";
            }
        } catch (Exception e) {
           this.mtdMessageError(" BD insert Articulo Proveedor" + e);
        }
        return respuesta;
    }

    public ArrayList<ArticuloProveedor> mtdLista(ArticuloProveedor instance) {
        ArrayList lista = new ArrayList();
        Articulo instanceArt;
        Proveedor instancePv;
        try {
            accesoDB = conexion.getConexion();
            sql = "select *from articulo_proveedor where articulo_codigo=? order by prioridad asc";
            ps = accesoDB.prepareStatement(sql);
            ps.setString(1, instance.getArticulo().getCodigo());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                instance=new ArticuloProveedor();
                instanceArt= new Articulo();
                instancePv=new Proveedor();
                instanceArt.setCodigo(rs.getString(1));
                instance.setArticulo(instanceArt);
                instancePv.setClave(rs.getInt(2));
                instance.setProveedor(instancePv);
                instance.setNombreProveedor(rs.getString(3));
                instance.setPlazoEntrega(rs.getInt(4));
                instance.setPrioridad(rs.getInt(5));
                instance.setCostoUltimoArticulo(rs.getFloat(6));
                instance.setFechaUltimacompra(rs.getString(7));
                lista.add(instance);
            }
        } catch (IOException | SQLException e) {
            this.mtdMessageError(" BD lista articulo proveedor" + e);
        }
        conexion.cerrarConexio();
        return lista;
    }
   
    public int mtdActualizar(ArticuloProveedor instance){
     int rpt=0;
      try{
         accesoDB= conexion.getConexion();
         sql="update art_proveedor set  clave_proveedor=?, nombre_empresa=?, plazo_entrega=?, prioridad=?, ultimo_cosart=? where codigo_articulo=?";
      }
      catch(Exception e){
           this.mtdMessageError("BD Actualizar" + e);
      }
     return rpt;
   }
   
   public void mtdEliminar(Articulo instance){
     try{
         accesoDB=conexion.getConexion(); 
         sql="delete from articulo_proveedor where articulo_codigo=?";
         ps=accesoDB.prepareStatement(sql);
         ps.setString(1, instance.getCodigoBarras());
         int rpt=  ps.executeUpdate();
         if(rpt>=1){
            JOptionPane.showMessageDialog(null,"Eliminado correctamente");
         }
     }
     catch(Exception e){
          this.mtdMessageError("BD Eliminar articulo preoveerodor" + e);
     }
   }
   
    private void mtdMessageError(String e) {
        JOptionPane.showMessageDialog(null, "Error " + e, "Error", JOptionPane.ERROR_MESSAGE);
    }
}
