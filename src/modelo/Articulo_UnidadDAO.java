/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author eleazar
 */
public class Articulo_UnidadDAO {

    Conexion conexion;
    Connection AccesoDB;
    String sql;
    PreparedStatement ps;

    public Articulo_UnidadDAO() {
        conexion = new Conexion();
    }

    public String mtdRegistrar(Articulo_Unidad instance) {
        String respuesta = "";
        try {
            AccesoDB = conexion.getConexion();
            sql = "insert into art_unidad value(?,?,?,?)";
            ps = AccesoDB.prepareStatement(sql);
            ps.setString(1, instance.getCodigoArticulo());
            ps.setString(2, instance.getClaveUnidad());
            ps.setString(3, instance.getNombreUnidad());
            ps.setInt(4, instance.getPosicion());
            ps.executeUpdate();
            respuesta = "registrado correctamente";
        } catch (Exception e) {
            mtdMensajeError(e + "");
        }
        return respuesta;
    }

    public ArrayList<Articulo_Unidad> mtdLista(Articulo_Unidad instance) {
        ArrayList lista = new ArrayList();
        try {
            AccesoDB = conexion.getConexion();
            sql = "select *from art_unidad where codigo_articulo=? order by posicion asc";
            ps = AccesoDB.prepareStatement(sql);
            ps.setString(1, instance.getCodigoArticulo());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                instance = new Articulo_Unidad();
                instance.setCodigoArticulo(rs.getString(1));
                instance.setClaveUnidad(rs.getString(2));
                instance.setNombreUnidad(rs.getString(3));
                instance.setPosicion(rs.getInt(4));
                lista.add(instance);
            }
        } catch (Exception e) {
            mtdMensajeError("" + e);
        }
        conexion.cerrarConexio();
        return lista;
    }
    
    public void mtdEliminar(Articulo_Unidad instance){
       try{
           AccesoDB=conexion.getConexion();
           sql="delete from art_unidad where codigo_articulo=?";
           ps=AccesoDB.prepareStatement(sql);
           ps.setString(1, instance.getCodigoArticulo());
           int rpt=ps.executeUpdate();
           if(rpt>=1){
                JOptionPane.showMessageDialog(null,"Eliminado");
           }
       }
       catch(Exception e){
       this.mtdMensajeError("Error Eliminar "+e);
       }
    }

    public ResultSet cargarComboUnida(String codigo_articulo) throws Exception {
        AccesoDB = conexion.getConexion();
        ps = AccesoDB.prepareStatement("select U.clave_unidad as claveUnida,U.nombre_unidad as nombreUnidad,U.cantidad_paquete as cantida, A.codigo_articulo from unidad U inner join art_unidad  AU on  U.clave_unidad=AU.clave_unidad inner join articulo A ON A.codigo_articulo=AU.codigo_articulo where A.codigo_articulo=? order by AU.posicion asc");
        ps.setString(1,codigo_articulo);
        ResultSet rs = ps.executeQuery();
        return rs;
    }
    public ArrayList<Articulo_Unidad> listUnidad(String codigo_articulo){
        ArrayList list=new ArrayList();
        Articulo_Unidad instance;
        try {
        AccesoDB = conexion.getConexion();
        ps = AccesoDB.prepareStatement("select U.clave_unidad as claveUnida,U.nombre_unidad as nombreUnidad,U.cantidad_paquete as cantidad, A.codigo_articulo from unidad U inner join art_unidad  AU on  U.clave_unidad=AU.clave_unidad inner join articulo A ON A.codigo_articulo=AU.codigo_articulo where A.codigo_articulo=? order by AU.posicion asc");
        ps.setString(1,codigo_articulo);
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            instance=new Articulo_Unidad();
                instance.setClaveUnidad(rs.getString(1));
                instance.setNombreUnidad(rs.getString(2));
                instance.setPosicion(rs.getInt(3));
                list.add(instance);
        }
        } catch (Exception e) {
                 mtdMensajeError("Lista unidades por articulo" + e);

        }
        return list;
    }
        
    private void mtdMensajeError(String e) {
        JOptionPane.showMessageDialog(null, "Error " + e, "Error", JOptionPane.ERROR_MESSAGE);
    }
    
}
