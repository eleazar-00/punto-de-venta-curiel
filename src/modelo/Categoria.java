/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author ELEAZAR
 */
public class Categoria {
     String claveCategoria;
     String nombreCategoria;
     String descripcionCategoria;
     String claveDepartamento;

    public Categoria() {
           claveCategoria="";
           nombreCategoria="";
           descripcionCategoria="";  
           claveDepartamento="";
    }

    public String getClaveDepartamento() {
        return claveDepartamento;
    }

    public void setClaveDepartamento(String claveDepartamento) {
        this.claveDepartamento = claveDepartamento;
    }

    public String getClaveCategoria() {
        return claveCategoria;
    }

    public void setClaveCategoria(String claveCategoria) {
        this.claveCategoria = claveCategoria;
    }

    public String getDescripcionCategoria() {
        return descripcionCategoria;
    }

    public void setDescripcionCategoria(String descripcionCategoria) {
        this.descripcionCategoria = descripcionCategoria;
    }

    public String getNombreCategoria() {
        return nombreCategoria;
    }

    public void setNombreCategoria(String nombreCategoria) {
        this.nombreCategoria = nombreCategoria;
    }
     
}
