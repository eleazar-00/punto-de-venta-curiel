/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author ELEAZAR
 */
public class CategoriaDAO {
    Conexion conexion;
    
    public CategoriaDAO() {
        conexion = new Conexion();
    }

    public String mtdRegristar(Categoria instance) {
        String rptRegistro = "";
        try {
            Connection acceDB = conexion.getConexion();
            String sql = ("insert into categoria value(?,?,?,?)");
            PreparedStatement st = acceDB.prepareStatement(sql);
            st.setString(1, instance.getClaveCategoria());
            st.setString(2, instance.getNombreCategoria());
            st.setString(3, instance.getDescripcionCategoria());
            int valor = st.executeUpdate();
            if (valor > 0) {
                rptRegistro = "regitrado Correctamente";
            } else {
                rptRegistro = "Error desconocido";
            }
        } catch (Exception e) {
            rptRegistro = "Error=" + e;
        }
        return rptRegistro;
    }

    public ArrayList<Categoria> listaCategoria(Categoria instance) {
        ArrayList lista = new ArrayList();
        Categoria categoria;
        try {
            Connection accesoDB = conexion.getConexion();
            String sql = "select *from categoria order";
            PreparedStatement ps = accesoDB.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                categoria = new Categoria();
                categoria.setClaveCategoria(rs.getString(1));
                categoria.setNombreCategoria(rs.getString(2));
                categoria.setDescripcionCategoria(rs.getString(3));
                categoria.setClaveDepartamento(rs.getString(4));
                lista.add(categoria);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
        conexion.cerrarConexio();
        return lista;

    }

    public int mtdELiminar(Categoria instance) {
        int respuesta = 0;
        try {
            Connection accesoDB = conexion.getConexion();
            String sql = "delete from categoria where clave_categoria=?";
            PreparedStatement st = accesoDB.prepareStatement(sql);
            st.setString(1, instance.getClaveCategoria());
            respuesta = st.executeUpdate();
            if (respuesta > 0) {
                JOptionPane.showMessageDialog(null, "Categoria Eliminada Correctamente");
            } else {
                JOptionPane.showMessageDialog(null, "Error desconocido", "Error", JOptionPane.ERROR_MESSAGE);

            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error" + e, "Error", JOptionPane.ERROR_MESSAGE);

        }
        return respuesta;
    }
    public int mtdActualizar(Categoria instance) {
        int respuesta = 0;
        try {
            Connection accesoDB = conexion.getConexion();
            String sql = "update categoria set nombre_categoria=?, descripcion_categoria=? where clave_categoria=?";
            PreparedStatement st = accesoDB.prepareStatement(sql);
            st.setString(1, instance.getNombreCategoria());
            st.setString(2, instance.getDescripcionCategoria());
            st.setString(3, instance.getClaveCategoria());
            respuesta = st.executeUpdate();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error=" + e);
        }
        return respuesta;
    }
    
     public ResultSet cargarComboD(String claveDepartamento) throws Exception {
        Connection accesoDB = conexion.getConexion();
        PreparedStatement st = accesoDB.prepareStatement("select *from categoria where clave_departamento='" + claveDepartamento + "'");
        ResultSet rs = st.executeQuery();
        return rs;
    }
}
