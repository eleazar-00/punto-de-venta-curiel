/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author eleazar
 */
public class Cliente extends Agente{
     Double limiteCredito;
     int diasPlazo;
     Double saldo;
     String fechaNacimiento;
     String fechaultimaCompra;
     public Cliente() {
     super();  
       limiteCredito=0.0;
       diasPlazo=0;
       saldo=0.0;
       fechaNacimiento="";
       fechaultimaCompra="";
     } 

    public Double getLimiteCredito() {
        return limiteCredito;
    }

    public void setLimiteCredito(Double limiteCredito) {
        this.limiteCredito = limiteCredito;
    }

    public int getDiasPlazo() {
        return diasPlazo;
    }

    public void setDiasPlazo(int diasPlazo) {
        this.diasPlazo = diasPlazo;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }
    
    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public String getFechaultimaCompra() {
        return fechaultimaCompra;
    }

    public void setFechaultimaCompra(String fechaultimaCompra) {
        this.fechaultimaCompra = fechaultimaCompra;
    }
     
}
