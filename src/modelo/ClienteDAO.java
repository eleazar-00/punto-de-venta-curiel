/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author eleazar
 */
public class ClienteDAO {
    Conexion conexion;
    Connection accesoDB;
    PreparedStatement ps;
    
    public ClienteDAO() {
        conexion = new Conexion();
    }

    public String mtdInsertar(Cliente cliente) {
        String respuesta = "";
        try {       
            accesoDB = conexion.getConexion();
            ps = accesoDB.prepareStatement("insert into cliente value(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            ps.setInt(1, cliente.getClave());
            ps.setInt(2, cliente.getStatus());
            ps.setString(3, cliente.getNombre());
            ps.setString(4, cliente.getRfc());
            ps.setString(5, cliente.getPais());
            ps.setString(6, cliente.getEstado());
            ps.setString(7, cliente.getPoblacion());
            ps.setString(8, cliente.getColonia());
            ps.setString(9, cliente.getCalle());
            ps.setString(10, cliente.getCodigoPostal());
            ps.setString(11, cliente.getCelular());
            ps.setString(12, cliente.getTelefono());
            ps.setString(13, cliente.getCorreo());
            ps.setString(14, cliente.getTipoAgente().getClave());
            ps.setDouble(15, cliente.getLimiteCredito());
            ps.setInt(16, cliente.getDiasPlazo());
            ps.setDouble(17, cliente.getSaldo());
            ps.setString(18, cliente.getFechaNacimiento());
            ps.setString(19, cliente.getFechaultimaCompra());
            ps.executeUpdate(); 
             respuesta = "Registrado correctamente";
        } catch (Exception e) {
            respuesta = e + "";
        }
        return respuesta;
    }

     public ArrayList<Cliente> mtdList(Cliente instance,String status,String filtro) { 
        ArrayList lista = new ArrayList();
        TipoAgente tipoA;
        try {
            accesoDB = conexion.getConexion();
            if(filtro.equals("todo")){
                 ps = accesoDB.prepareStatement("select * from cliente c inner join tipo_agente ta ON c.tipo_agente_clave=ta.clave where c.status in ("+status+")");
            }
            if(filtro.equals("clave")){
                 ps = accesoDB.prepareStatement("select *from cliente c inner join tipo_agente ta ON c.tipo_agente_clave=ta.clave where c.clave=? and c.status in ("+status+")");
                 ps.setInt(1, instance.getClave());
            }
            if(filtro.equals("razon")){
                 ps = accesoDB.prepareStatement("select *from cliente c inner join tipo_agente ta ON c.tipo_agente_clave=ta.clave where c.razon_social LIKE ? and c.status in ("+status+") LIMIT 30");
                 ps.setString(1,"%"+instance.getNombre()+"%");
            }
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                instance=new Cliente();
                tipoA=new TipoAgente();
                instance.setClave(rs.getInt(1));
                instance.setStatus(rs.getInt(2));
                instance.setNombre(rs.getString(3));
                instance.setRfc(rs.getString(4));
                instance.setPais(rs.getString(5));
                instance.setEstado(rs.getString(6));
                instance.setPoblacion(rs.getString(7));
                instance.setColonia(rs.getString(8));
                instance.setCalle(rs.getString(9));
                instance.setCodigoPostal(rs.getString(10));
                instance.setCelular(rs.getString(11));
                instance.setTelefono(rs.getString(12));
                instance.setCorreo(rs.getString(13));
                tipoA.setClave(rs.getString(14));
                tipoA.setNombre(rs.getString(16));
                instance.setTipoAgente(tipoA);
                lista.add(instance);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error lista inicio" + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
        conexion.cerrarConexio();
        return lista;
    }
     
    public int mtdActualizar(Cliente cliente) {
        int rpt = 0;
        try {
            accesoDB = conexion.getConexion();
            String sql = "update cliente set status=?, razon_social=?, rfc=?, pais=?, estado=?, poblacion=?, colonia=?, calle=?, codigo_postal=?, celular=?, telefono=?, correo=?, tipo_agente_clave=? where clave=?";
            ps = accesoDB.prepareStatement(sql);
            ps.setInt(14, cliente.getClave());
            ps.setInt(1, cliente.getStatus());
            ps.setString(2, cliente.getNombre());
            ps.setString(3, cliente.getRfc());
            ps.setString(4, cliente.getPais());
            ps.setString(5, cliente.getEstado());
            ps.setString(6, cliente.getPoblacion());
            ps.setString(7, cliente.getColonia());
            ps.setString(8, cliente.getCalle());
            ps.setString(9, cliente.getCodigoPostal());
            ps.setString(10, cliente.getCelular());
            ps.setString(11, cliente.getTelefono());
            ps.setString(12, cliente.getCorreo());
            ps.setString(13, cliente.getTipoAgente().getClave());
             rpt=ps.executeUpdate();     
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error de DAO update= " + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
        return rpt;
    }

    public void mtdEliminar(Cliente instance) {
        try {
            accesoDB = conexion.getConexion();
            ps = accesoDB.prepareStatement("delete from cliente where clave=?");
            ps.setInt(1, instance.getClave());
            ps.executeUpdate();
            JOptionPane.showMessageDialog(null, "Eliminado Correctamente");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error de DAO Eliminar=" + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
     public int mtdUltimoIDdeMovimiento() {
        int numero = 0;
        try {
            Connection accesoDB=conexion.getConexion();
            String sql="Select MAX(clave) as id FROM cliente";
            PreparedStatement ps= accesoDB.prepareStatement(sql);
            ResultSet rs=ps.executeQuery();
            while(rs.next()){
               numero=rs.getInt(1);
            }
        } catch (Exception e) {
           JOptionPane.showMessageDialog(null, "Error al crear clave consecutivo" + e);
        }
        return numero;
    }
}
