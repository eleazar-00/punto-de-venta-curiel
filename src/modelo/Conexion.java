/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.*;
import javax.swing.JOptionPane;
import contants.Propiedades;

/**
 *
 * @author ELEAZAR
 */
public class Conexion {

    public Conexion() {
     }
    Connection conexion = null;

    public Connection getConexion() throws FileNotFoundException, IOException {
        Properties pro = new Properties();
        FileReader reader = new FileReader(Propiedades.CONFIG_FILE_NAME);
        pro.load(reader);
        String url = pro.getProperty(Propiedades.URL_BASEDATOS);
        String usuario = pro.getProperty(Propiedades.USUARIO_BD);
        String password = pro.getProperty(Propiedades.PASSWORD_BD);
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conexion = DriverManager.getConnection(url, usuario, password);

        } catch (ClassNotFoundException | SQLException e) {
            JOptionPane.showMessageDialog(null, "Error al conectar a la base de Datos=" + e);
        }
        return conexion;

    }

    public void cerrarConexio() {
        if (conexion != null) {
            try {
                conexion.close();
            } catch (Exception e) {
            }
        }
    }
}
