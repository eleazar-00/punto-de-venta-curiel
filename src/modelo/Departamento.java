/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author ELEAZAR
 */
public class Departamento {
    String clave;
    String nombre;
    
    public Departamento(){
         clave=""; 
         nombre="";
     } 
    public Departamento(String clave,String nombre){
        this.clave=clave;
        this.nombre=nombre; 
    }
    
    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return  nombre;
    }
    
}
