/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;
/**
 *
 * @author ELEAZAR
 */
public class DepartamentoDAO {

    Conexion conexion;
    Connection accesoDB;
    String sql;
    PreparedStatement st;

    public DepartamentoDAO() {
        conexion = new Conexion();
    }

    public String mtdRegistrar(Departamento instance) {
        String respuesta = "Error desconocido";
        try {
            accesoDB = conexion.getConexion();
            sql = "insert into CatArtLinea value(?,?)";
            st = accesoDB.prepareStatement(sql);
            st.setString(1, instance.getClave());
            st.setString(2, instance.getNombre());
             int valor = st.executeUpdate();
            if (valor > 0) {
                respuesta = "Registrado Correctamente";
            }
        } catch (Exception e) {
            mtdMessajeError("Insertar " + e);
        }
        return respuesta;
    }

    public boolean mtdELiminar(Departamento instance) {
        boolean respuesta = false;
        try {
            accesoDB = conexion.getConexion();
            sql = "delete from CatArtLinea where clave=?";
            st = accesoDB.prepareStatement(sql);
            st.setString(1, instance.getClave());
            int valor= st.executeUpdate();
            if(valor==0){
              JOptionPane.showMessageDialog(null, "Error al Eliminar");
            }else{
            respuesta=true;
            JOptionPane.showMessageDialog(null,"Eliminado Correctamente");
            }
        } catch (Exception e) {
            mtdMessajeError("Eliminar " + e);
        }
        return respuesta;
    }

    public int mtdActualizar(Departamento instance) {
        int respuesta = 0;
        try {
            accesoDB = conexion.getConexion();
            sql = "Update CatArtLinea set descripcion=? where clave=?";
            st = accesoDB.prepareStatement(sql);
            st.setString(1, instance.getNombre());
             st.setString(2, instance.getClave());
            respuesta = st.executeUpdate();
         } catch (Exception e) {
            mtdMessajeError("Actualizar " + e);
        }
        return respuesta;
    }

    public ArrayList<Departamento> mtdLista(Departamento instance) {
        ArrayList lista = new ArrayList();
        Departamento departamento;
        try {
            accesoDB = conexion.getConexion(); 
            sql = "select * from CatArtLinea";
            st = accesoDB.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                departamento = new Departamento();
                departamento.setClave(rs.getString("clave"));
                departamento.setNombre(rs.getString("descripcion"));
                lista.add(departamento);
            }
        } catch (Exception e) {
            mtdMessajeError("lista " + e);
        }
        conexion.cerrarConexio();
        return lista;
    }

    protected void mtdMessajeError(String e) {
        JOptionPane.showMessageDialog(null, "Error DB " + e, "Error", JOptionPane.ERROR_MESSAGE);
    }
    
    public ResultSet cargarComboDepartamento() throws Exception {
        Connection accesoDB = conexion.getConexion();
        PreparedStatement st = accesoDB.prepareStatement("select *from CatArtLinea");
        ResultSet rs = st.executeQuery();
        return rs;
    }
}
