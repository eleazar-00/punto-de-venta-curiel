package modelo;
/**
 *
 * @author eleazar
 */
public class DetalleMovimiento {
    Movimiento movimiento;
    int clave;
    Articulo articulo;
    String codigoBarras;    
    float cantidadArticulo;
    String nombreArticulo;
    String claveUnidad;
    float precioUnitarioArticulo;
    double precioTotalArticulo;
    float ivaUnitarioArticulo;
    float ivaTotalArticulo;
    float descuentoUnitarioArticulo;
    float descuentoTotalArticulo;
    
    public DetalleMovimiento() {
        movimiento=new Movimiento();
        articulo=new Articulo();
        clave=0;
        codigoBarras="";
        cantidadArticulo=0;
        nombreArticulo="";
        claveUnidad="";
        precioUnitarioArticulo=0;
        precioTotalArticulo=0;
        ivaUnitarioArticulo=0;
        ivaTotalArticulo=0;
        descuentoUnitarioArticulo=0;
        descuentoTotalArticulo=0;
     }
    public int getClave() {
        return clave;
    }
    public void setClave(int clave) {
        this.clave = clave;
    }
    public float getDescuentoUnitarioArticulo() {
        return descuentoUnitarioArticulo;
    }
    public void setDescuentoUnitarioArticulo(float descuentoUnitarioArticulo) {
        this.descuentoUnitarioArticulo = descuentoUnitarioArticulo;
    }
    public float getDescuentoTotalArticulo() {
        return descuentoTotalArticulo;
    }
    public void setDescuentoTotalArticulo(float descuentoTotalArticulo) {
        this.descuentoTotalArticulo = descuentoTotalArticulo;
    }
    public float getIvaTotalArticulo() {
        return ivaTotalArticulo;
    }
    public void setIvaTotalArticulo(float ivaTotalArticulo) {
        this.ivaTotalArticulo = ivaTotalArticulo;
    }
    public float getIvaUnitarioArticulo() {
        return ivaUnitarioArticulo;
    }
    public void setIvaUnitarioArticulo(float ivaUnitarioArticulo) {
        this.ivaUnitarioArticulo = ivaUnitarioArticulo;
    }
    public double getPrecioTotalArticulo() {
        return precioTotalArticulo;
    }
    public void setPrecioTotalArticulo(double precioTotalArticulo) {
        this.precioTotalArticulo = precioTotalArticulo;
    }
    public float getPrecioUnitarioArticulo() {
        return precioUnitarioArticulo;
    }
    public void setPrecioUnitarioArticulo(float precioUnitarioArticulo) {
        this.precioUnitarioArticulo = precioUnitarioArticulo;
    }
    public float getCantidadArticulo() {
        return cantidadArticulo;
    }
    public void setCantidadArticulo(float cantidadArticulo) {
        this.cantidadArticulo = cantidadArticulo;
    } 

    public Movimiento getMovimiento() {
        return movimiento;
    }

    public void setMovimiento(Movimiento movimiento) {
        this.movimiento = movimiento;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public String getCodigoBarras() {
        return codigoBarras;
    }

    public void setCodigoBarras(String codigoBarras) {
        this.codigoBarras = codigoBarras;
    }
   
    public String getNombreArticulo() {
        return nombreArticulo;
    }
    public void setNombreArticulo(String nombreArticulo) {
        this.nombreArticulo = nombreArticulo;
    }  

    public String getClaveUnidad() {
        return claveUnidad;
    }

    public void setClaveUnidad(String claveUnidad) {
        this.claveUnidad = claveUnidad;
    }
}
