/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author eleazar
 */
public class DetalleMovimientoDAO {

    Conexion conexion;
    PreparedStatement ps;
    public DetalleMovimientoDAO() {
        conexion = new Conexion();
    }

    public String mtdInsertarDetalller(DetalleMovimiento instance) {
        String respuesta = "";
        try {
            Connection accesoDB = conexion.getConexion();
            String sql = ("insert into detalle_movimiento value(?,?,?,?,?,?,?,?,?,?,?,?,?)");
            ps = accesoDB.prepareStatement(sql);
            ps.setInt(1, instance.getClave());
            ps.setInt(2, instance.getMovimiento().getClave());
            ps.setString(3, instance.getArticulo().getCodigo());
            ps.setString(4, instance.getCodigoBarras());
            ps.setFloat(5, instance.getCantidadArticulo());
            ps.setString(6, instance.getNombreArticulo());
            ps.setString(7, instance.getClaveUnidad());
            ps.setFloat(8, instance.getPrecioUnitarioArticulo());
            ps.setDouble(9,instance.getPrecioTotalArticulo());
            ps.setFloat(10, instance.getIvaUnitarioArticulo());
            ps.setFloat(11, instance.getIvaTotalArticulo());
            ps.setFloat(12, instance.getDescuentoUnitarioArticulo());
            ps.setFloat(13, instance.getDescuentoUnitarioArticulo());
            int valor = ps.executeUpdate();
            if (valor > 0) {
                respuesta = "Detalle registrado correctamente";
            } else {
                respuesta = "Error desconocido";
            }
        } catch (Exception e) {
            respuesta = "Error" + e;
        }
        return respuesta;
    }
    
    public ArrayList<DetalleMovimiento> listaDellate(int id) {
        ArrayList lista = new ArrayList();  
        DetalleMovimiento instance;
        Articulo articulo;
        try {
            Connection accesoDB = conexion.getConexion();
            String sql = "select articulo_codigo,cantidad_articulo from detalle_movimiento where movimiento_clave=?";
            ps = accesoDB.prepareStatement(sql);
            ps.setInt(1, id); 
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                instance = new DetalleMovimiento();
                articulo=new Articulo();
                articulo.setCodigo(rs.getString(1));
                instance.setArticulo(articulo);
                instance.setCantidadArticulo(rs.getFloat(2));
                lista.add(instance);
            }
        } catch (Exception e) { 
            System.out.println("error extrar articulo"+e);
        }
        conexion.cerrarConexio();
        return lista;
    }
}
