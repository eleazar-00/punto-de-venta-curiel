 
package modelo;
import java.sql.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;
/**
 *
 * @author ELEAZAR
 */
public class EmpresaDAO {
    Conexion conexion;
    Connection accesoDB;
    PreparedStatement st;
    public EmpresaDAO(){
      conexion = new Conexion();    
    }
    
    public String mtdInserta(Empresa empresa){
         String rptRegistro=null;
         try {
             accesoDB=conexion.getConexion();
             st=accesoDB.prepareCall("insert into empresa value(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
             st.setString(1, empresa.getClave());
             st.setString(2, empresa.getRazonSocial());
             st.setString(3, empresa.getRfc());
             st.setString(4, empresa.getPais());
             st.setString(5, empresa.getEstado());
             st.setString(6, empresa.getPoblacion());
             st.setString(7, empresa.getColonia());
             st.setString(8, empresa.getCalle());
             st.setString(9, empresa.getNumeroInterior());
             st.setString(10, empresa.getNumeroExterior());
             st.setString(11, empresa.getCodigoPostal());
             st.setString(12, empresa.getTelefono());
             st.setString(13, empresa.getEmail());
             st.setString(14, empresa.getRutaImagen());
             st.setAsciiStream(15, empresa.getImagen());
              int numFAfectadas=st.executeUpdate();
             if(numFAfectadas > 0){
                rptRegistro="Registro Exitoso";
             }  
        } catch (Exception e) {
        }
         return rptRegistro;
    }
    
    public ArrayList<Empresa> listEmpresa(){
        ArrayList listaEmpresa=new ArrayList();
        Empresa empresa;
        try {
            accesoDB=conexion.getConexion();
            PreparedStatement st=accesoDB.prepareStatement("SELECT *FROM EMPRESA");
            ResultSet rs= st.executeQuery();
            while(rs.next()){
                empresa = new Empresa();
                empresa.setClave(rs.getString(1));
                empresa.setRazonSocial(rs.getString(2));
                empresa.setRfc(rs.getString(3));
                empresa.setPais(rs.getString(4));
                empresa.setEstado(rs.getString(5));
                empresa.setPoblacion(rs.getString(6));
                empresa.setColonia(rs.getString(7));
                empresa.setCalle(rs.getString(8));
                empresa.setNumeroInterior(rs.getString(9));
                empresa.setNumeroExterior(rs.getString(10));
                empresa.setCodigoPostal(rs.getString(11));
                empresa.setTelefono(rs.getString(12));
                empresa.setEmail(rs.getString(13));
                empresa.setRutaImagen(rs.getString(14));
                listaEmpresa.add(empresa);
            }
        } catch (Exception e) {
        }
        conexion.cerrarConexio();
        return listaEmpresa;
    }
    
    public int mtdEditar(Empresa empresa){
         int numFA=0;
        try {
             accesoDB=conexion.getConexion();
             st = accesoDB.prepareStatement("update empresa SET razon_social=?, rfc=?, pais=?, estado=?, poblacion=?, colonia=?, calle=?, numero_interior=?, numero_exterior=?, codigo_postal=?, telefono=?, email=?, logo=?, imagen=? where clave=?");
             st.setString(15, empresa.getClave());
             st.setString(1, empresa.getRazonSocial());
             st.setString(2, empresa.getRfc());
             st.setString(3, empresa.getPais());
             st.setString(4, empresa.getEstado());
             st.setString(5, empresa.getPoblacion());
             st.setString(6, empresa.getColonia());
             st.setString(7, empresa.getCalle());
             st.setString(8, empresa.getNumeroInterior());
             st.setString(9, empresa.getNumeroExterior());
             st.setString(10, empresa.getCodigoPostal());
             st.setString(11, empresa.getTelefono());
             st.setString(12, empresa.getEmail());
             st.setString(13, empresa.getRutaImagen());
             st.setBinaryStream(14, empresa.getImagen());

            numFA=st.executeUpdate();
        } catch (Exception e) {
             JOptionPane.showMessageDialog(null, e);
        }
        return numFA;
    }
     
    public ResultSet cargarComboAlmacen() throws Exception {
        accesoDB = conexion.getConexion();
        st = accesoDB.prepareStatement("select *from empresa");
        ResultSet rs = st.executeQuery();
        return rs;
    }
}
