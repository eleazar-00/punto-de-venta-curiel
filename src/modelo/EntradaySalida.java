/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;
public class EntradaySalida {
  String clave;
  String nombre;
  String signo;

    public EntradaySalida() {
     clave="";
     nombre="";
     signo="";
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSigno() {
        return signo;
    }

    public void setSigno(String signo) {
        this.signo = signo;
    }

    @Override
    public String toString() {
        return nombre;
    }
    
}
