/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

//import com.mysql.jdbc.PreparedStatement;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author mary
 */
public class EntradaySalidaDAO {

    Conexion conexion;
    PreparedStatement st;

    public EntradaySalidaDAO() {
        conexion = new Conexion();
    }

    public String mtdInsertarEYS(EntradaySalida instance) {
        String respuesta = "";
        try {
            Connection accesoBD = conexion.getConexion();
            String sql = ("insert into entradaysalida value(?,?,?)");
            CallableStatement cst = accesoBD.prepareCall(sql);
            cst.setString(1, instance.getClave());
            cst.setString(2, instance.getNombre());
            cst.setString(3, instance.getSigno());
            int valor = cst.executeUpdate();
            if (valor > 0) {
                respuesta = "Registrado correctamente";
            }
        } catch (Exception e) {
            mtdMenssajeError(e + "");
        }
        return respuesta;
    }

    public ArrayList<EntradaySalida> listaTipoEYS(EntradaySalida instance, String filtro) {
        ArrayList listaEYS = new ArrayList();
        try {
            Connection accesoDB;
            accesoDB = conexion.getConexion();
            if (filtro.equals("todo")) {
                String sql = "select *from entradaysalida";
                st = accesoDB.prepareStatement(sql);
            }
            if (filtro.equals("clave")) {
                String sql = "select *from entradaysalida where clave=?";
                st = accesoDB.prepareStatement(sql);
                st.setString(1, instance.getClave());
            }
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                instance = new EntradaySalida();
                instance.setClave(rs.getString(1));
                instance.setNombre(rs.getString(2));
                instance.setSigno(rs.getString(3));
                listaEYS.add(instance);
            }
        } catch (Exception e) {
            mtdMenssajeError(e + "");
        }
        conexion.cerrarConexio();
        return listaEYS;
    }

    public int mtdEliminar(EntradaySalida instance) {
        int valor = 0;
        try {
            String sql = "delete from entradaysalida where clave=?";
            Connection accesoDB = conexion.getConexion();
            PreparedStatement st = accesoDB.prepareStatement(sql);
            st.setString(1, instance.getClave());
            st.execute();
            JOptionPane.showMessageDialog(null, "Eliminado Correctamente");
        } catch (Exception e) {
            mtdMenssajeError(e + "");
        }
        return valor;
    }

    public int mtdActualizar(EntradaySalida instance) {
        int respuesta = 0;
        try {
            Connection accesoDB = conexion.getConexion();
            String sql = "update entradaysalida set signo=?, nombre=? where clave=?";
            PreparedStatement st = accesoDB.prepareStatement(sql);
            st.setString(1, instance.getSigno());
            st.setString(2, instance.getNombre());
            st.setString(3, instance.getClave());
            int valor = st.executeUpdate();
            respuesta = valor;
        } catch (Exception e) {
            mtdMenssajeError(e + "");
        }
        return respuesta;
    }

    public ResultSet cargarComboEntradaySalida() throws Exception {
        Connection accesoDB = conexion.getConexion();
        PreparedStatement st = accesoDB.prepareStatement("select *from entradaysalida");
        ResultSet rs = st.executeQuery();
        return rs;
    }

    private void mtdMenssajeError(String rept) {
        JOptionPane.showMessageDialog(null, rept, "Error", JOptionPane.ERROR_MESSAGE);
    }
}
