/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author ELEAZAR
 */
public class ExistenciaAlmacen {
    Articulo articulo;
    Almacen almacen;
    Double existencias;
    Double apartado;
    Double stockMinimo;
    Double stackMaximo;

    public ExistenciaAlmacen(Articulo articulo, Almacen almacen, Double existencias, Double apartado, Double stockMinimo, Double stackMaximo) {
        this.articulo = articulo;
        this.almacen = almacen;
        this.existencias = existencias;
        this.apartado = apartado;
        this.stockMinimo = stockMinimo;
        this.stackMaximo = stackMaximo;
    }

    public ExistenciaAlmacen() {
        
        
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public Almacen getAlmacen() {
        return almacen;
    }

    public void setAlmacen(Almacen almacen) {
        this.almacen = almacen;
    }

    public Double getExistencias() {
        return existencias;
    }

    public void setExistencias(Double existencias) {
        this.existencias = existencias;
    }

    public Double getApartado() {
        return apartado;
    }

    public void setApartado(Double apartado) {
        this.apartado = apartado;
    }

    public Double getStockMinimo() {
        return stockMinimo;
    }

    public void setStockMinimo(Double stockMinimo) {
        this.stockMinimo = stockMinimo;
    }

    public Double getStackMaximo() {
        return stackMaximo;
    }

    public void setStackMaximo(Double stackMaximo) {
        this.stackMaximo = stackMaximo;
    }
    
 
}
