/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author ELEAZAR
 */
public class ExistenciaAlmacenDAO {

    Conexion conexion;
    Connection accesoDB;
    String sql;
    PreparedStatement ps;

    public ExistenciaAlmacenDAO() {
        conexion = new Conexion();
    }

    public String mtdInsertar(ExistenciaAlmacen instance) {
        String respuesta = "";
        try {
            accesoDB = conexion.getConexion();
            sql = "insert into existencias_almacen (articulo_codigo,almacen_clave,existencias,apartados,stock_minimo,stock_maximo) values (?,?,?,?,?,?)";
            ps = accesoDB.prepareStatement(sql);
            ps.setString(1, instance.getArticulo().getCodigo());
            ps.setString(2, instance.getAlmacen().getClave());
            ps.setDouble(3, instance.getExistencias());
            ps.setDouble(4, instance.getApartado());
            ps.setDouble(5, instance.getStockMinimo());
            ps.setDouble(6, instance.getStackMaximo());
            int rpt = ps.executeUpdate();
            if (rpt > 0) {
                respuesta = "Registrado Correctamente";
            } else {
                respuesta = "Error desconocido al registrar";
            }
        } catch (Exception e) {
            mtdMensageError("Error Registrar almacen =" + e);
        }
         return respuesta;
    }

    public ArrayList<ExistenciaAlmacen> listaExistencias(ExistenciaAlmacen instance) {
        ArrayList listaAlmacen = new ArrayList();
        ExistenciaAlmacen almacenExistencias;
        Almacen almacen;
        try {
            accesoDB = conexion.getConexion();
            sql = ("select ea.*,a.nombre from existencias_almacen ea inner join almacen a on ea.almacen_clave=a.clave  where ea.articulo_codigo=?");
            ps = accesoDB.prepareStatement(sql);
            ps.setString(1, instance.getArticulo().getCodigo());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                almacenExistencias = new ExistenciaAlmacen();
                almacen = new Almacen();
                almacen.setClave(rs.getString(2));
                almacen.setNombre(rs.getString(7));
                almacenExistencias.setAlmacen(almacen);
                almacenExistencias.setExistencias(rs.getDouble(3));
                almacenExistencias.setApartado(rs.getDouble(4));
                almacenExistencias.setStockMinimo(rs.getDouble(5));
                almacenExistencias.setStackMaximo(rs.getDouble(6));
                listaAlmacen.add(almacenExistencias);
                System.out.println(listaAlmacen);
            }
        } catch (Exception e) {
        }
        conexion.cerrarConexio();
        return listaAlmacen;
    }

    public int mtdActualizar(ExistenciaAlmacen instance) {
        int respuesta = 0;
        try {
            accesoDB = conexion.getConexion();
            sql = "update existencias_almacen set existencias=existencias+(?) where articulo_codigo=? and almacen_clave=?";
            ps = accesoDB.prepareStatement(sql);
            ps.setDouble(1, instance.getExistencias());
            ps.setString(2, instance.getArticulo().getCodigo());
            ps.setString(3, instance.getAlmacen().getClave());
            respuesta = ps.executeUpdate();
            if (respuesta > 0) {
                System.out.println("actualizado existencias " + respuesta);
            } else {
                instance.setApartado(0.0);
                instance.setStackMaximo(0.0);
                instance.setStockMinimo(0.0);
                this.mtdInsertar(instance);
                System.out.println("No actualizado inserto exitencias");
            }
        } catch (Exception e) {
            mtdMensageError(" Actualizar =" + e);
        }
        return respuesta;
    }

    public void mtdEliminar(String ClaveProducto) {
         try {
            accesoDB = conexion.getConexion();
            sql = "delete from existencias_almacen where articulo_codigo=?";
            ps = accesoDB.prepareStatement(sql);
            ps.setString(1, ClaveProducto);
            int rpt = ps.executeUpdate();
            if (rpt >= 1) {
                System.out.println("Eliminados correctamente existencias Articulo");
            }
        } catch (Exception e) {
            this.mtdMensageError("BD Eliminar articulo almance" + e);
        }
    }

    public void mtdMensageError(String e) {
        JOptionPane.showMessageDialog(null, "Error" + e);
    }
}
