/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;
/**
 *
 *@author VISORUS SOFT
 */
public class Monedero {
    boolean habilitar;
    int moneda;
    int punto;
    char usarpor;
    
    public Monedero() {
        habilitar=false;
        usarpor='V';
        moneda=1;
        punto=1;
    }

    public boolean isHabilitar() {
        return habilitar;
    }

    public void setHabilitar(boolean habilitar) {
        this.habilitar = habilitar;
    }

    public char getUsarpor() {
        return usarpor;
    }

    public void setUsarpor(char usarpor) {
        this.usarpor = usarpor;
    }
       
}
