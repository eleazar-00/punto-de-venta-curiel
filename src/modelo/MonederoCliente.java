/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;
/**
 *
 * @author VISORUS SOFT
 */
public class MonederoCliente {
   Cliente cliente;
   boolean status;
   String numeroTarjeta;
   int puntos;

    public MonederoCliente(Cliente cliente, boolean status, String numeroTarjeta, int puntos) {
        this.cliente = cliente;
        this.status = status;
        this.numeroTarjeta = numeroTarjeta;
        this.puntos = puntos;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
   
     
    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getNumeroTarjeta() {
        return numeroTarjeta;
    }

    public void setNumeroTarjeta(String numeroTarjeta) {
        this.numeroTarjeta = numeroTarjeta;
    }

    public int getPuntos() {
        return puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }
   
}
