/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;
/**
 *
 * @author eleazar
 */
public class Movimiento {
    int clave;
    int status;
    String nombre;
    Double importeTotal;
    float ivaTotal;
    float descuentoTotal;
    String fechaDocumento;
    String fechaAplicado;
    String horaD;
    String horaA;
    
    Almacen almacen;
    Agente agente;
    SeriesyFolios serieyfolio;
    int numerofolio;
    EntradaySalida entradaysalida;
    String clienteopreveedor;
    TipoPago tipopago;
    
    public Movimiento() {
       nombre="";
       fechaDocumento="";
       fechaAplicado="";
       horaD="";
       horaA="";
       
       almacen=new Almacen();
       agente=new Agente();
       serieyfolio= new SeriesyFolios();
       entradaysalida=new EntradaySalida();
       clienteopreveedor="";
       tipopago=new TipoPago();
    }
    
    public TipoPago getTipopago() {
        return tipopago;
    }

    public void setTipopago(TipoPago tipopago) {
        this.tipopago = tipopago;
    }

    public int getClave() {
        return clave;
    }

    public void setClave(int clave) {
        this.clave = clave;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getImporteTotal() {
        return importeTotal;
    }

    public void setImporteTotal(Double importeTotal) {
        this.importeTotal = importeTotal;
    }

    public float getIvaTotal() {
        return ivaTotal;
    }

    public void setIvaTotal(float ivaTotal) {
        this.ivaTotal = ivaTotal;
    }

    public float getDescuentoTotal() {
        return descuentoTotal;
    }

    public void setDescuentoTotal(float descuentoTotal) {
        this.descuentoTotal = descuentoTotal;
    }

    public String getFechaDocumento() {
        return fechaDocumento;
    }

    public void setFechaDocumento(String fechaDocumento) {
        this.fechaDocumento = fechaDocumento;
    }

    public String getFechaAplicado() {
        return fechaAplicado;
    }

    public void setFechaAplicado(String fechaAplicado) {
        this.fechaAplicado = fechaAplicado;
    }

    public String getHoraD() {
        return horaD;
    }

    public void setHoraD(String horaD) {
        this.horaD = horaD;
    }

    public String getHoraA() {
        return horaA;
    }

    public void setHoraA(String horaA) {
        this.horaA = horaA;
    }

    public Almacen getAlmacen() {
        return almacen;
    }

    public void setAlmacen(Almacen almacen) {
        this.almacen = almacen;
    }

    public Agente getAgente() {
        return agente;
    }

    public void setAgente(Agente agente) {
        this.agente = agente;
    }

    public SeriesyFolios getSerieyfolio() {
        return serieyfolio;
    }

    public void setSerieyfolio(SeriesyFolios serieyfolio) {
        this.serieyfolio = serieyfolio;
    }

    public int getNumerofolio() {
        return numerofolio;
    }

    public void setNumerofolio(int numerofolio) {
        this.numerofolio = numerofolio;
    }

    public EntradaySalida getEntradaysalida() {
        return entradaysalida;
    }

    public void setEntradaysalida(EntradaySalida entradaysalida) {
        this.entradaysalida = entradaysalida;
    }

    public String getClienteopreveedor() {
        return clienteopreveedor;
    }

    public void setClienteopreveedor(String clienteopreveedor) {
        this.clienteopreveedor = clienteopreveedor;
    }

    
    
    
}
