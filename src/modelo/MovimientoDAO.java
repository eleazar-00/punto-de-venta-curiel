/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author eleazar
 */
public class MovimientoDAO {

    Conexion conexion;
    Almacen instanceAlm;
    TipoPago instanceTp;
    Connection accesoDB;
    PreparedStatement ps;
    String sql;

    public MovimientoDAO() {
        conexion = new Conexion();
    }

    public int mtdInsertar(Movimiento instance) {
        int id = 0;
        ResultSet rs = null;
        try {
            accesoDB = conexion.getConexion();
            String sql = "insert into movimiento value(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement ps = accesoDB.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, instance.getClave());
            ps.setInt(2, instance.getStatus());
            ps.setString(3, instance.getSerieyfolio().getClave());
            ps.setInt(4, instance.getNumerofolio());
            ps.setInt(5, instance.getAgente().getClave());
            ps.setString(6, instance.getAlmacen().getClave());
            ps.setString(7, instance.getNombre());
            ps.setDouble(8, instance.getImporteTotal());
            ps.setFloat(9, instance.getIvaTotal());
            ps.setFloat(10, instance.getDescuentoTotal());
            ps.setString(11, instance.getFechaDocumento());
            ps.setString(12, instance.getFechaAplicado());
            ps.setString(13, instance.getHoraD());
            ps.setString(14, instance.getHoraA());
            ps.setString(15, instance.getClienteopreveedor());
            ps.setString(16, instance.getTipopago().getClave());
            ps.setString(17, instance.getEntradaysalida().getClave());
            ps.executeUpdate();
            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getInt(1);
            }
            System.out.println("Return ID: " + id);
            rs.close();
            ps.close();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Error al insert Movimiento" + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            id = 0;
        }
        conexion.cerrarConexio();
        return id;
    }

    public ArrayList<Movimiento> listafiltros(Movimiento instance, String serie, String filtro, String status, String fechaIni, String fechaFin) {
        ArrayList lista = new ArrayList();
        try {
            accesoDB = conexion.getConexion();
            String filtolimite = "";
            if (filtro != "Todos") {
                filtolimite = "LIMIT " + filtro;
            } else {
                filtolimite = "";
            }
            sql = "select * from movimiento where status=? and serie_folio_clave=? and fecha_aplicado between ? and ? ORDER BY clave DESC " + filtolimite;
            ps = accesoDB.prepareStatement(sql);
            ps.setString(1, status);
            ps.setString(2, serie);
            ps.setString(3, fechaIni);
            ps.setString(4, fechaFin);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                instance = new Movimiento();
                instance.setClave(rs.getInt(1));
                instance.getSerieyfolio().setClave(rs.getString(3));
                instance.setNombre(rs.getString(7));
                instance.setNumerofolio(rs.getInt(4));
                instance.setImporteTotal(rs.getDouble(8));
                instance.setFechaAplicado(rs.getString(12));
                instance.setHoraA(rs.getString(14));
                lista.add(instance);
            }
        } catch (Exception e) {
            this.mtdMensejeError("Error lista " + e);
        }
        conexion.cerrarConexio();
        return lista;
    }

    private void mtdMensejeError(String e) {
        JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
    }
    public int mtdIdMovimiento(String serie, int folio) {
        int id = 0;
        try {
            Connection accesoDB = conexion.getConexion();
            String sql = "select clave  from movimiento where serie_folio_clave=? and folio=?";
            PreparedStatement ps = accesoDB.prepareStatement(sql);
            ps.setString(1, serie);
            ps.setInt(2, folio);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                id = rs.getInt(1);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al traer el id del movimiento" + e);
        }
        conexion.cerrarConexio();
        return id;
    }

    public int mtdCancelarMmovimiento(String serie, int folio) {
        int rpt = 0;
        try {
            Connection accesoDB = conexion.getConexion();
            String sql = "update movimiento set status=3,importe__total=0,iva_total=0,descuento_total=0 where serie_folio_clave=? and folio=?";
            PreparedStatement ps = accesoDB.prepareStatement(sql);
            ps.setString(1, serie);
            ps.setInt(2, folio);
            rpt=ps.executeUpdate();
        } catch (Exception e) {
           System.out.println(e);
        }
        conexion.cerrarConexio();
        return rpt;
    }
}
