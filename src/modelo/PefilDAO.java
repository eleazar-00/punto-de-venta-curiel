/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author ELEAZAR
 */
public class PefilDAO {
    Conexion conexion;
    public PefilDAO(){
        conexion = new Conexion();
    }
    
    public String mtdRegistrar(Perfil instance){
    String respuesta=null;
         try{
             Connection accesoBD=conexion.getConexion();
             CallableStatement st= accesoBD.prepareCall("insert into perfil value(?,?,?)");
             st.setString(1, instance.getClavePerfil());
             st.setString(2, instance.getNombrePerfil());
             st.setString(3, instance.getMenu());
             int valor=st.executeUpdate();
             if(valor>0){
               respuesta="Registrado correctamente";
             }


         }
         catch(Exception e){
         JOptionPane.showMessageDialog(null,"Error al regigstrar "+e);
         }

    return respuesta;
    
    }
    
    public int mtdEliminar(String clavePerfil){
     int valor=0;
         try{
             String sql="delete from perfil where clave_perfil=?";
             Connection accesoDB=conexion.getConexion();
             PreparedStatement st= accesoDB.prepareStatement(sql);
             st.setString(1, clavePerfil);
             st.execute();
             JOptionPane.showMessageDialog(null,"Eliminado Correctamente");
         }
         catch(Exception e){
         JOptionPane.showMessageDialog(null,"Error al eliminar"+ e);
         }
     return valor;
    
    }
    
    
    public ArrayList<Perfil>  mtdLista(){
     ArrayList lista= new ArrayList();
     Perfil perfil;
        try {
              Connection accesoDB=conexion.getConexion();
              PreparedStatement st= accesoDB.prepareStatement("Select *from perfil");
              ResultSet rs= st.executeQuery();
              while(rs.next()){
              perfil = new Perfil();
              perfil.setClavePerfil(rs.getString(1));
              perfil.setNombrePerfil(rs.getString( 2));
              perfil.setMenu(rs.getString( 3));
              lista.add(perfil);
              }
        }
        
        catch(Exception  e){
          JOptionPane.showMessageDialog(null,"Error al cargar la lista");
        }
     return lista;
    } 
    
    public int mtdActualizar(Perfil perfil){
    int respuesta=0;
      try{
          Connection accesoDB=conexion.getConexion();
          String sql="update perfil set  menu=?, nombre_perfil=? where clave_perfil=?";
          PreparedStatement st=accesoDB.prepareStatement(sql);
          st.setString(1, perfil.getMenu());
          st.setString(2, perfil.getNombrePerfil());
          st.setString(3, perfil.getClavePerfil());
          int valor=st.executeUpdate();
          if(valor==1){
           respuesta=1;
          }
      }
      catch(Exception e){
          respuesta=2;
      }
    return respuesta;
    }
    
     public ArrayList<Perfil>  findByClave(String clave){
     ArrayList lista= new ArrayList();
     Perfil perfil;
        try {
              Connection accesoDB=conexion.getConexion();
              PreparedStatement st= accesoDB.prepareStatement("Select *from perfil where clave=");
              st.setString(1,clave);
              ResultSet rs= st.executeQuery();
              while(rs.next()){
              perfil = new Perfil();
              perfil.setClavePerfil(rs.getString(1));
              perfil.setNombrePerfil(rs.getString( 2));
              perfil.setMenu(rs.getString(3));
              lista.add(perfil);
              }
        }
        catch(Exception  e){
          JOptionPane.showMessageDialog(null,"Error al cargar la lista");
        }
     conexion.cerrarConexio();
     return lista;
    } 
    
}
