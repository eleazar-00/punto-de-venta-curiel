/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author ELEAZAR
 */
public class Perfil {
    String clavePerfil;
    String nombrePerfil;
    String menu;

    public Perfil() {
        clavePerfil="";
        nombrePerfil="";
        menu="";
    }

    public String getClavePerfil() {
        return clavePerfil;
    }

    public void setClavePerfil(String clavePerfil) {
        this.clavePerfil = clavePerfil;
    }

    public String getNombrePerfil() {
        return nombrePerfil;
    }

    public void setNombrePerfil(String nombrePerfil) {
        this.nombrePerfil = nombrePerfil;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    @Override
    public String toString() {
        return nombrePerfil;
    }
    
}
