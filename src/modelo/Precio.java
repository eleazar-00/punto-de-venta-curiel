/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author eleazar
 */
public class Precio {
    int numeroPrecio;
    float precio;
    float utilidadPorcentaje;
    float utilidadPesos;
    String formula;
    String condicion;
    float descuento;
    String codigoArticulo;

    public Precio() {
    numeroPrecio=0;
    precio=0;
    utilidadPorcentaje=0;
    utilidadPesos=0;
    formula="";
    condicion="";
    descuento=0;
    codigoArticulo="";
    }

    public String getCodigoArticulo() {
        return codigoArticulo;
    }

    public void setCodigoArticulo(String codigoArticulo) {
        this.codigoArticulo = codigoArticulo;
    }

    public String getCondicion() {
        return condicion;
    }

    public void setCondicion(String condicion) {
        this.condicion = condicion;
    }

    public float getDescuento() {
        return descuento;
    }

    public void setDescuento(float descuento) {
        this.descuento = descuento;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public int getNumeroPrecio() {
        return numeroPrecio;
    }

    public void setNumeroPrecio(int numeroPrecio) {
        this.numeroPrecio = numeroPrecio;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getUtilidadPesos() {
        return utilidadPesos;
    }

    public void setUtilidadPesos(float utilidadPesos) {
        this.utilidadPesos = utilidadPesos;
    }

    public float getUtilidadPorcentaje() {
        return utilidadPorcentaje;
    }

    public void setUtilidadPorcentaje(float utilidadPorcentaje) {
        this.utilidadPorcentaje = utilidadPorcentaje;
    }
    
}
