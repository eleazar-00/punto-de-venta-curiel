/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author eleazar
 */
public class PrecioDAO {

    Conexion conexion;
    Connection accesoDB;
    String sql;
    PreparedStatement ps;

    public PrecioDAO() {
        conexion = new Conexion();
    }

    public String mtdRegistra(Precio instance) {
        String repuesta = "";
        try {
            accesoDB = conexion.getConexion();
            sql = "insert into precio value(?,?,?,?,?,?,?,?)";
            ps = accesoDB.prepareStatement(sql);
            ps.setInt(1, instance.getNumeroPrecio());
            ps.setFloat(2, instance.getPrecio());
            ps.setFloat(3, instance.getUtilidadPorcentaje());
            ps.setFloat(4, instance.getUtilidadPesos());
            ps.setString(5, instance.getCondicion());
            ps.setString(6, instance.getFormula());
            ps.setFloat(7, instance.getDescuento());
            ps.setString(8, instance.getCodigoArticulo());
            ps.executeUpdate();
        } catch (Exception e) {
            mtdMensajeError(e + "");
        }
        return repuesta;
    }

    public ArrayList<Precio> mtdList(Precio instance) {
        ArrayList lista = new ArrayList();
        try {
            accesoDB = conexion.getConexion();
            sql = "select * from precio where codigo_articulo=? order by numero_precio asc";
            ps = accesoDB.prepareStatement(sql);
            ps.setString(1, instance.getCodigoArticulo());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                instance = new Precio();
                instance.setNumeroPrecio(rs.getInt(1));
                instance.setPrecio(rs.getFloat(2));
                instance.setUtilidadPorcentaje(rs.getFloat(3));
                instance.setUtilidadPesos(rs.getFloat(4));
                instance.setCondicion(rs.getString(5));
                instance.setFormula(rs.getString(6));
                instance.setDescuento(rs.getFloat(7));
                instance.setCodigoArticulo(rs.getString(8));
                lista.add(instance);
            }
        } catch (Exception e) {
        }
        conexion.cerrarConexio();
        return lista;
    }

    public int mdtAtualizar(Precio instance) {
        int rpt = 0;
        try {
            accesoDB=conexion.getConexion();
            sql="update precio set  precio=?, utilidad_porcentaje=?, utilidad_peso=?, condicion=?, formula=?, descuento=? where	codigo_articulo=? and numero_precio=?";
            ps=accesoDB.prepareStatement(sql);
            ps.setFloat(1, instance.getPrecio());
            ps.setFloat(2, instance.getUtilidadPorcentaje());
            ps.setFloat(3, instance.getUtilidadPesos());
            ps.setString(4, instance.getCondicion());
            ps.setString(5, instance.getFormula());
            ps.setFloat(6, instance.getDescuento());
            ps.setString(7, instance.getCodigoArticulo());
            ps.setInt(8, instance.getNumeroPrecio());
            ps.executeUpdate();
        } catch (Exception e) {
        }
        return rpt;
    }

    private void mtdMensajeError(String e) {
        JOptionPane.showMessageDialog(null, "Error " + e, "Error", JOptionPane.ERROR_MESSAGE);
    }
}
