/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author ELEAZAR
 */
public class Proveedor extends Agente{
     Double limiteCredito;
     int diasPlazo;
      Double saldo;
     
     public Proveedor() {
       super();
       limiteCredito=0.0;
       diasPlazo=0;
       saldo=0.0;
      }

    public Double getLimiteCredito() {
        return limiteCredito;
    }

    public void setLimiteCredito(Double limiteCredito) {
        this.limiteCredito = limiteCredito;
    }

    public int getDiasPlazo() {
        return diasPlazo;
    }

    public void setDiasPlazo(int diasPlazo) {
        this.diasPlazo = diasPlazo;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    } 
}
