/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;


/**
 *
 * @author ELEAZAR
 */
public class ProveedorDAO {

    Conexion conexion;
    Connection acceDB;
    PreparedStatement ps;

    public ProveedorDAO() {
        conexion = new Conexion();
    }

    public String mtdInserta(Proveedor proveedor) {
        String rptRegistro = "";
        try {
            acceDB = conexion.getConexion();
            String sql = ("insert into proveedor value(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            ps= acceDB.prepareStatement(sql);
            ps.setInt(1, proveedor.getClave());
            ps.setInt(2, proveedor.getStatus());
            ps.setString(3, proveedor.getNombre());
            ps.setString(4, proveedor.getRfc());
            ps.setString(5, proveedor.getPais());
            ps.setString(6, proveedor.getEstado());
            ps.setString(7, proveedor.getPoblacion());
            ps.setString(8, proveedor.getColonia());
            ps.setString(9, proveedor.getCalle());
            ps.setString(10, proveedor.getCodigoPostal());
            ps.setString(11, proveedor.getCelular());
            ps.setString(12, proveedor.getTelefono());
            ps.setString(13, proveedor.getCorreo());
            ps.setString(14, proveedor.getTipoAgente().getClave());
            ps.setDouble(15, proveedor.getLimiteCredito());
            ps.setInt(16, proveedor.getDiasPlazo());
            ps.setDouble(17, proveedor.getSaldo());
            int numDatos = ps.executeUpdate();
            if (numDatos > 0) {
                rptRegistro = "Registrado Correctamento";
            } else {
                rptRegistro = "Error desconocido";
            }
        } catch (Exception e) {
            rptRegistro = "Error =" + e;
        }

        return rptRegistro;
    }

    public ArrayList<Proveedor> listaProveedor(Proveedor proveedor,String status,String filtro) {
        ArrayList lista = new ArrayList();
         TipoAgente tipoA;
        PreparedStatement st = null;
         try {
            acceDB = conexion.getConexion();
            if(filtro.equals("todo")){
                 st = acceDB.prepareStatement("select *from proveedor p inner join tipo_agente ta ON p.tipo_agente_clave=ta.clave where p.status In ("+status+") Limit 50");
            }
            if(filtro.equals("clave")){
                st = acceDB.prepareStatement("select *from proveedor p inner join tipo_agente ta ON p.tipo_agente_clave=ta.clave where p.status In ("+status+") and p.clave like ? Limit 6 ");
                st.setInt(1, proveedor.getClave());
            }
            if(filtro.equals("razon")){
                  st = acceDB.prepareStatement("select *from proveedor p inner join tipo_agente ta ON p.tipo_agente_clave=ta.clave where p.status In ("+status+") and p.razon_social like ? Limit 6 ");
                  st.setString(1,"%"+proveedor.getNombre()+"%");      
            }
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                proveedor = new Proveedor();
                tipoA=new TipoAgente();
                proveedor.setClave(rs.getInt(1));
                proveedor.setStatus(rs.getInt(2));
                proveedor.setNombre(rs.getString(3));
                proveedor.setRfc(rs.getString(4));
                proveedor.setPais(rs.getString(5));
                proveedor.setEstado(rs.getString(6));
                proveedor.setPoblacion(rs.getString(7));
                proveedor.setColonia(rs.getString(8));
                proveedor.setCalle(rs.getString(9));
                proveedor.setCodigoPostal(rs.getString(10));
                proveedor.setCelular(rs.getString(11));
                proveedor.setTelefono(rs.getString(12));
                proveedor.setCorreo(rs.getString(13));
                proveedor.setLimiteCredito(rs.getDouble(15));
                proveedor.setDiasPlazo(rs.getInt(16));
                proveedor.setSaldo(rs.getDouble(17));
                tipoA.setClave(rs.getString(18));
                tipoA.setNombre(rs.getString(19));
                tipoA.setTipo(rs.getString(20));
                proveedor.setTipoAgente(tipoA);
                
                lista.add(proveedor);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error" + e, "Error", JOptionPane.ERROR_MESSAGE);
        }
        conexion.cerrarConexio();
        return lista;
    }

    public int mtdActualizar(Proveedor proveedor) {
        int numFA = 0;
        try {
            acceDB = conexion.getConexion();
            String sql = "update proveedor set status=?, razon_social=?, rfc=?, pais=?, estado=?, poblacion=?, colonia=?, calle=?, codigo_postal=?, celular=?, telefono=?, correo=?, tipo_agente_clave=?,limite_credito=?,dias_plazo=?,saldo=? where clave=?";
            ps = acceDB.prepareStatement(sql);
            ps.setInt(17, proveedor.getClave());
            ps.setInt(1, proveedor.getStatus());
            ps.setString(2, proveedor.getNombre());
            ps.setString(3, proveedor.getRfc());
            ps.setString(4, proveedor.getPais());
            ps.setString(5, proveedor.getEstado());
            ps.setString(6, proveedor.getPoblacion());
            ps.setString(7, proveedor.getColonia());
            ps.setString(8, proveedor.getCalle());
            ps.setString(9, proveedor.getCodigoPostal());
            ps.setString(10, proveedor.getCelular());
            ps.setString(11, proveedor.getTelefono());
            ps.setString(12, proveedor.getCorreo());
            ps.setString(13, proveedor.getTipoAgente().getClave());
            ps.setDouble(14, proveedor.getLimiteCredito());
            ps.setInt(15, proveedor.getDiasPlazo());
            ps.setDouble(16, proveedor.getSaldo());
            numFA = ps.executeUpdate();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error Actualizar DAO" + e,"ERROR",JOptionPane.ERROR_MESSAGE);
        }
        return numFA;
    }

    public void mtdEliminar(Proveedor instance) {
        try {
            acceDB = conexion.getConexion();
            String sql = ("delete from  proveedor where clave=?");
            PreparedStatement st = acceDB.prepareStatement(sql);
            st.setInt(1, instance.getClave());
            st.execute();
            JOptionPane.showMessageDialog(null, "Proveedor eliminado correctamente");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al eliminar");
        }
    }
    public int mtdUltimoIDdeMovimiento() {
        int numero = 0;
        try {
            Connection accesoDB=conexion.getConexion();
            String sql="Select MAX(clave) as id FROM proveedor";
            PreparedStatement ps= accesoDB.prepareStatement(sql);
            ResultSet rs=ps.executeQuery();
            while(rs.next()){
               numero=rs.getInt(1);
            }
        } catch (Exception e) {
           JOptionPane.showMessageDialog(null, "Error al crear clave consecutivo" + e);
        }
        return numero;
    }
    
}
