package modelo;
/**
 *
 * @author CURIEL
 */
public class Retiro {
    long id;
    int status;
    String Serie;
    int folio;
    String concepto;
    String fecha;
    double cantidad;
    String caja;
    Agente agente;
    Almacen almacen;

    public Retiro(long id,int status,String Serie, int folio, String concepto, String fecha, double cantidad, Agente agente, String caja, Almacen almacen) {
       
        this.id=id;
        this.status=status;
        this.Serie = Serie;
        this.folio = folio;
        this.concepto = concepto;
        this.fecha = fecha;
        this.cantidad = cantidad;
        this.agente = agente;
        this.caja = caja;
        this.almacen = almacen;
    }

    public Retiro() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getSerie() {
        return Serie;
    }

    public void setSerie(String Serie) {
        this.Serie = Serie;
    }

    public int getFolio() {
        return folio;
    }

    public void setFolio(int folio) {
        this.folio = folio;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public Agente getAgente() {
        return agente;
    }

    public void setAgente(Agente agente) {
        this.agente = agente;
    }

    public String getCaja() {
        return caja;
    }

    public void setCaja(String caja) {
        this.caja = caja;
    }

    public Almacen getAlmacen() {
        return almacen;
    }

    public void setAlmacen(Almacen almacen) {
        this.almacen = almacen;
    }
}
