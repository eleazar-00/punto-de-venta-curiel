/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author CURIEL
 */
public class RetiroDAO {
    Conexion conexion;
    Connection accesoDB;
    PreparedStatement ps;
    ResultSet rs;
    String sql;
    public RetiroDAO(){
            conexion = new Conexion();
    }
    public String mtdInsertar(Retiro instance) {
        String respuesta = "";
        try {
            accesoDB = conexion.getConexion();
            sql = "insert into retiro (id,status,serie,folio,concepto,fecha,cantidad,caja,agente_clave,almacen_clave) values (?,?,?,?,?,?,?,?,?,?)";
            ps = accesoDB.prepareStatement(sql);
            ps.setInt(1, 0);
            ps.setInt(2, 1);
            ps.setString(3, instance.getSerie());
            ps.setInt(4, instance.getFolio());
            ps.setString(5, instance.getConcepto());
            ps.setString(6, instance.getFecha());
            ps.setDouble(7, instance.getCantidad());
            ps.setString(8, instance.getCaja());
            ps.setInt(9,instance.getAgente().getClave());
            ps.setString(10, instance.getAlmacen().getClave());
            int rpt = ps.executeUpdate();
            if (rpt == 1) {
                respuesta = "Registrado correctamente";
            } else {
                respuesta = "Error desconocido al registrar";
            }
        } catch (Exception e) {
            mtdMensageError("Registrar"+e);
        }
        return respuesta;
    }
    public int mtdActualizar(int id) {
        int respuesta = 0;
        try {
            accesoDB = conexion.getConexion();
            sql = "update retiro set cantidad=0, status=0 where id=?";
            ps = accesoDB.prepareStatement(sql);
            ps.setInt(1, id);
            respuesta = ps.executeUpdate();
        } catch (Exception e) {
            mtdMensageError("Error actualizar " + e);
        }
        return respuesta;
    }
    
    public ArrayList<Retiro> lista(Retiro instance) {
        ArrayList lista = new ArrayList();
        try {  
              accesoDB = conexion.getConexion();
                sql = "select id,serie,folio,cantidad,concepto from retiro where status=1 and almacen_clave=? and agente_clave=? and fecha=?";
                ps = accesoDB.prepareStatement(sql);
                ps.setString(1, instance.getAlmacen().getClave());
                ps.setInt(2,instance.getAgente().getClave());
                ps.setString(3, instance.getFecha());
             ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                instance = new Retiro();
                instance.setId(rs.getInt(1));
                instance.setSerie(rs.getString(2));
                instance.setFolio(rs.getInt(3));
                instance.setCantidad(rs.getDouble(4));
                instance.setConcepto(rs.getString(5));
                 lista.add(instance);
            }
        } catch (Exception e) {
           mtdMensageError("Error lista " + e);
        }
        conexion.cerrarConexio();
        return lista;
    }
    private void mtdMensageError(String e) {
        JOptionPane.showMessageDialog(null, "Error " + e, "Error", JOptionPane.ERROR_MESSAGE);
    }
}
