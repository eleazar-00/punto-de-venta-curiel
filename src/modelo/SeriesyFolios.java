/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author eleazar        return entradaysalida;

 */
public class SeriesyFolios {
    String clave;
    int folio;
    String nombre;
    int tipo;
    String formato;
    EntradaySalida entradaysalida;
    
    public SeriesyFolios(){
        String clave="";
        int folio=0;
        String nombre="";
        int tipo=0;
        String formato="";
        entradaysalida=new EntradaySalida();
    }

    public EntradaySalida getEntradaysalida() {
        return entradaysalida;
    }

    public void setEntradaysalida(EntradaySalida entradaysalida) {
        this.entradaysalida = entradaysalida;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getFormato() {
        return formato;
    }

    public void setFormato(String formato) {
        this.formato = formato;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getFolio() {
        return folio;
    }

    public void setFolio(int folio) {
        this.folio = folio;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return nombre;
    }
    
    
}
