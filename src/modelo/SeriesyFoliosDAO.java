/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author eleazar
 */
public class SeriesyFoliosDAO {

    Conexion conexion;
    Connection accesoDB;
    PreparedStatement ps;
    String sql;

    public SeriesyFoliosDAO() {
        conexion = new Conexion();
    }

    public String mtdInserta(SeriesyFolios instance) {
        String respuesta = "";
        try {
            accesoDB = conexion.getConexion();
            sql = "insert into serie_folio (clave,folio,nombre,tipo,formato,entradaysalida_clave) value (?,?,?,?,?,?)";
            ps = accesoDB.prepareStatement(sql);
            ps.setString(1, instance.getClave());
            ps.setInt(2, instance.getFolio());
            ps.setString(3, instance.getNombre());
            ps.setInt(4, instance.getTipo());
            ps.setString(5, instance.getFormato());
            ps.setString(6, instance.getEntradaysalida().getClave());
            ps.execute();
            respuesta = "Registrado correctamente";
        } catch (Exception e) {
            mtdMensejeError("Error insertar " + e);
        }
        return respuesta;
    }

    public int mtdActualizar(SeriesyFolios instance) {
        int respuesta = 0;
        try {
            accesoDB = conexion.getConexion();
            sql = "update serie_folio set nombre=?,folio=?, formato=?,tipo=?,entradaysalida_clave=? where clave=?";
            ps = accesoDB.prepareStatement(sql);
            ps.setString(1, instance.getNombre());
            ps.setInt(2, instance.getFolio());
            ps.setString(3, instance.getFormato());
            ps.setInt(4, instance.getTipo());
            ps.setString(5, instance.getEntradaysalida().getClave());
            ps.setString(6, instance.getClave());
            respuesta = ps.executeUpdate();
        } catch (Exception e) {
            mtdMensejeError("Error actualizar " + e);
        }
        return respuesta;
    }

    public int mtdUpdateNumeroFolio(SeriesyFolios instance) {
        int respuesta = 0;
        try {
            accesoDB = conexion.getConexion();
            sql = "update serie_folio set folio=folio+1  where clave=?";
            ps = accesoDB.prepareStatement(sql);
            ps.setString(1, instance.getClave());
            respuesta = ps.executeUpdate();
             
           sql = "SELECT MAX(folio) FROM serie_folio where clave='"+instance.getClave()+"'";
            ResultSet rs = ps.executeQuery(sql);
            if (rs.next()) {
                respuesta = rs.getInt(1);
            }
            rs.close();
            ps.close();
            conexion.cerrarConexio();
        } catch (Exception e) {
            mtdMensejeError("Error actualizar " + e);
        }
        return respuesta;
    }

    public ArrayList<SeriesyFolios> lista(SeriesyFolios instance, String filtro,String Series) {
        ArrayList lista = new ArrayList();
        try {
            accesoDB = conexion.getConexion();
            if (filtro.equals("todo")) {
                sql = "select * from serie_folio sf inner join entradaysalida es ON  sf.entradaysalida_clave=es.clave";
                ps = accesoDB.prepareStatement(sql);
            }
            if (filtro.equals("clave")) {
                sql = "select * from serie_folio sf inner join entradaysalida es ON  sf.entradaysalida_clave=es.clave where sf.clave=? or sf.nombre=?";
                ps = accesoDB.prepareStatement(sql);
                ps.setString(1, instance.getClave());
                ps.setString(2, instance.getClave());
            }
            if (filtro.equals("compra")) {
                sql = "select * from serie_folio sf inner join entradaysalida es ON  sf.entradaysalida_clave=es.clave where sf.tipo=2 and sf.clave in ("+Series+")";
                ps = accesoDB.prepareStatement(sql);
            }
            if (filtro.equals("venta")) {
                sql = "select * from serie_folio sf inner join entradaysalida es ON  sf.entradaysalida_clave=es.clave where sf.tipo=1";
                ps = accesoDB.prepareStatement(sql);
            }
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                instance = new SeriesyFolios();
                instance.setClave(rs.getString(1));
                instance.setFolio(rs.getInt(2));
                instance.setNombre(rs.getString(3));
                instance.setTipo(rs.getInt(4));
                instance.setFormato(rs.getString(5));
                instance.getEntradaysalida().setClave(rs.getString(6));
                instance.getEntradaysalida().setNombre(rs.getString(8));
                instance.getEntradaysalida().setSigno(rs.getString(9));
                lista.add(instance);
            }
        } catch (Exception e) {
            this.mtdMensejeError("Error lista " + e);
        }
        conexion.cerrarConexio();
        return lista;
    }

    public void mtdEliminar(SeriesyFolios instance) {
        try {
            accesoDB = conexion.getConexion();
            sql = "delete from serie_folio where clave=?";
            ps = accesoDB.prepareStatement(sql);
            ps.setString(1, instance.getClave());
            ps.executeUpdate();
            JOptionPane.showMessageDialog(null, "Eliminado Correctamente");

        } catch (Exception e) {
            mtdMensejeError("Error Eliminar " + e);
        }
    }

    private void mtdMensejeError(String e) {
        JOptionPane.showMessageDialog(null, e, "Error", JOptionPane.ERROR_MESSAGE);
    }
}
