/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Reader;
/**
 *
 * @author ELEAZAR
 */
public class TipoAgente {
    public String clave;
    String nombre;
    String tipo;
    
    public TipoAgente() {
        clave="";
        nombre="";
        tipo="";
     }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
  
    
    @Override
    public String toString() {
        return  nombre;
    }
     
    
}
