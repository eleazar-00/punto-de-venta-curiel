/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;
/**
 *
 * @author mary
 */
public class TipoAgenteDAO {

    Conexion conexion;
     public TipoAgenteDAO() {
        conexion = new Conexion();
    }

    public String mtdInsertar(TipoAgente instance) {
        String respuesta = "";
        try {
            Connection accesoBD = conexion.getConexion();
            CallableStatement st = accesoBD.prepareCall("insert into tipo_agente value(?,?,?)");
            st.setString(1, instance.clave);
            st.setString(2, instance.nombre);
            st.setString(3, instance.getTipo());
            int valor = st.executeUpdate();
            if (valor > 0) {
                respuesta = "Registrado correctamente";
            }
        } catch (Exception e) {
            respuesta = "Error=" + e;
        }

        return respuesta;

    }

    public ArrayList<TipoAgente> listaTipoAgente(String tipo) {
        ArrayList lista = new ArrayList();
        TipoAgente tipoAgente;
        try {
            Connection accesoDB;
            accesoDB = conexion.getConexion();
            String sql="";
            if(tipo.isEmpty()){
                 sql = "select *from tipo_agente";
            }else{
                sql = "select *from tipo_agente where tipo='"+tipo+"'";
            }
            PreparedStatement st;
            st = accesoDB.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                tipoAgente = new TipoAgente();
                tipoAgente.setClave(rs.getString(1));
                tipoAgente.setNombre(rs.getString(2));
                tipoAgente.setTipo(rs.getString(3));
                lista.add(tipoAgente);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error" + e,"Error", JOptionPane.ERROR_MESSAGE);
        }
        conexion.cerrarConexio();
        return lista;
    }

    public int mtdEliminar(TipoAgente instance) {
        int valor = 0;
        try {
            String sql = "delete from tipo_agente where clave=?";
            Connection accesoDB = conexion.getConexion();
            PreparedStatement st = accesoDB.prepareStatement(sql);
            st.setString(1, instance.clave);
            st.execute();
            JOptionPane.showMessageDialog(null, "Eliminado Correctamente");
        } catch (Exception e) {
            String erroRorenea="com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException: Cannot delete or update a parent row: a foreign key constraint fails (`ventas`.`agente`, CONSTRAINT `fk_agente_tipo_agente1` FOREIGN KEY (`tipo_agente_clave`) REFERENCES `tipo_agente` (`clave`) ON DELETE NO ACTION ON UPDATE NO ACTION)";
            String error;
            if(e.toString().equals(erroRorenea)){
                 error="El Tipo no se puede eliminar ya que tiene relacion con un Empleado,cliente o proveedor";
            }else{
                error="Error desconocido"+e;
            }
            
            JOptionPane.showMessageDialog(null, error,"Error",JOptionPane.ERROR_MESSAGE);
         }
        return valor;
    }

    public int mtdActualizar(TipoAgente instance) {
        int respuesta = 0;
        try {
            Connection accesoDB = conexion.getConexion();
            String sql = "update tipo_agente set nombre=?, tipo=?  where clave=?";
            PreparedStatement st = accesoDB.prepareStatement(sql);
            st.setString(1, instance.getNombre());
            st.setString(2, instance.getTipo());
            st.setString(3, instance.getClave());
              respuesta = st.executeUpdate();
         } catch (Exception e) {
              JOptionPane.showMessageDialog(null,"Error" + e,"Error",JOptionPane.ERROR_MESSAGE);
        }
        return respuesta;
    }
    
    public ArrayList<TipoAgente> listaTipo(String clave) {
        ArrayList lista = new ArrayList();
        TipoAgente tipoAgente;
        try {
            Connection accesoDB;
            accesoDB = conexion.getConexion();
            String sql="";
            if(clave.isEmpty()){
                        sql = "select *from tipo_agente";
            }else{
                sql = "select * from tipo_agente where clave='"+clave+"'";
            }
            PreparedStatement st;
            st = accesoDB.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                tipoAgente = new TipoAgente();
                tipoAgente.setClave(rs.getString(1));
                tipoAgente.setNombre(rs.getString(2));
                tipoAgente.setTipo(rs.getString(3));
                lista.add(tipoAgente);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error" + e,"Error", JOptionPane.ERROR_MESSAGE);
        }
        conexion.cerrarConexio();
        return lista;
    }
    public ResultSet cargarCombo() throws Exception {
        Connection accesoDB = conexion.getConexion();
        PreparedStatement st = accesoDB.prepareStatement("select * from tipo_agente");
        ResultSet rs = st.executeQuery();
        return rs;
    }
}
