/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;
/**
 *
 * @author eleazar
 */
public class TipoPagoDAO {
    Conexion conexion;
    Connection accesoDB;
    PreparedStatement ps;
    ResultSet rs;
    String sql;

    public TipoPagoDAO() {
        conexion = new Conexion();
    }
    
    public String mtdInsertar(TipoPago instance) {
        String respuesta = "";
        try {
            accesoDB = conexion.getConexion();
            sql = "insert into tipo_pago (clave,nombre,num_pag) values (?,?,?)";
            ps = accesoDB.prepareStatement(sql);
            ps.setString(1, instance.getClave());
            ps.setString(2, instance.getNombre());
            ps.setInt(3, instance.getNum_pag());
             int rpt = ps.executeUpdate();
            if (rpt >= 1) {
                respuesta = "Registrado correctamente";
            } else {
                respuesta = "Error desconocido al registrar";
            }
        } catch (Exception e) {
            mtdMensageError("Registrar"+e);
        }
        return respuesta;
    }

    public int mtdActualizar(TipoPago instance) {
        int respuesta = 0;
        try {
            accesoDB = conexion.getConexion();
            sql = "update tipo_pago set nombre=?, num_pago=? where clave=?";
            ps = accesoDB.prepareStatement(sql);
            ps.setString(1, instance.getNombre());
            ps.setInt(2, instance.getNum_pag());
            ps.setString(2, instance.getClave());
            respuesta = ps.executeUpdate();

        } catch (Exception e) {
            mtdMensageError("Actualizar " + e);
        }
        return respuesta;
    }
   
    
    public void mtdEliminar(Unidad instance) {
        try {
            accesoDB = conexion.getConexion();
            sql = "delete from tipo_pago where clave=?";
            ps = accesoDB.prepareStatement(sql);
            ps.setString(1, instance.getClave());
            ps.execute();
            JOptionPane.showMessageDialog(null, "Eliminado Correctamente");
        } catch (Exception e) {
            mtdMensageError(" Eliminar =" + e);
        }
    }

    public ArrayList<TipoPago> mtdLista() {
        ArrayList list = new ArrayList();
        TipoPago tipopago;
        try {
            accesoDB = conexion.getConexion();
            sql = "select *from tipo_pago";
            ps = accesoDB.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                tipopago = new TipoPago();
                tipopago.setClave(rs.getString(1));
                tipopago.setNombre(rs.getString(2));
                tipopago.setNum_pag(rs.getInt(3));
                list.add(tipopago);
            }

        } catch (Exception e) {
            mtdMensageError(" Lista =" + e);
        }
        conexion.cerrarConexio();
        return list;
    }

    public ResultSet cargarCombo() throws Exception {
        accesoDB = conexion.getConexion();
        ps = accesoDB.prepareStatement("select *from tipo_pago");
        rs = ps.executeQuery();
        return rs;
    }

    private void mtdMensageError(String e) {
        JOptionPane.showMessageDialog(null, "Error " + e, "Error", JOptionPane.ERROR_MESSAGE);
    }
}
