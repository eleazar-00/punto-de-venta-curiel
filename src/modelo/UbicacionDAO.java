/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author ELEAZAR
 */
public class UbicacionDAO {
      Conexion conexcion;
    Connection accesoDB;
    PreparedStatement ps;
    String sql = "";

    public UbicacionDAO() {
        conexcion = new Conexion();
    }

    public String mtdInsertar(Ubicacion instance) {
        String rpt = "";
        int rptSQL = 0;
        try {
            accesoDB = conexcion.getConexion();
            sql = "insert into catartubicacion (clave,descripcion) value (?,?)";
            ps = accesoDB.prepareStatement(sql);
            ps.setString(1, instance.getClave());
            ps.setString(2, instance.getNombre());
            rptSQL = ps.executeUpdate();
            if (rptSQL == 1) {
                mtdMensaje("registrado Correctamente", 1);
            } else {
                mtdMensaje("Error desconocido", 0);
            }
        } catch (Exception e) {
            mtdMensaje("Error desconocido ="+e, 0);
        }
        return rpt;
    }

    public ArrayList<Ubicacion> lista(Ubicacion instance) {
        ArrayList<Ubicacion> list = new ArrayList();
         try {
            accesoDB = conexcion.getConexion();
            sql = "Select * from catartubicacion";
             ps = accesoDB.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                instance = new Ubicacion();
                instance.setClave(rs.getString(1));
                instance.setNombre(rs.getString(2));
                list.add(instance);
            }
        } catch (Exception e) {
        }
        conexcion.cerrarConexio();
        return list;
    }
    public int mtdActualizar(Ubicacion instance){
    int rpt=0;
    try{
        accesoDB=conexcion.getConexion();
        sql="update catartubicacion set descripcion=? where clave=?";
        ps=accesoDB.prepareStatement(sql);
        ps.setString(2, instance.getClave());
        ps.setString(1, instance.getNombre());
        rpt = ps.executeUpdate();
            if (rpt == 1) {
                mtdMensaje("Actualizado correctamente ", 1);
            } else {
                mtdMensaje("Error desconocido", 0);
            }
    }catch(Exception e){
           mtdMensaje("Error desconocido ="+e, 0);
    }
    return rpt;
    }
    
    public void mtdEliminar(Ubicacion instance){
        try{
            accesoDB=conexcion.getConexion();
            sql="delete from catartubicacion where clave=?";
            ps=accesoDB.prepareStatement(sql);
            ps.setString(1, instance.getClave());
            int rp=ps.executeUpdate();
            if(rp==1){
            mtdMensaje("Eliminado corectamente", 1);
            }else{
             mtdMensaje("Error desconocido", 0);
            }
        }
        catch(Exception e){
              mtdMensaje("Error desconocido ="+e, 0);
        }
    }
    
    private void mtdMensaje(String msj, int tmsj) {
        if (tmsj == 1) {
            JOptionPane.showMessageDialog(null, msj);
        } else {
            JOptionPane.showMessageDialog(null, msj, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
}
