/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author ELEAZAR
 */
public class Unidad {
    String clave;
    String nombre;
    float cantidadPaquete;
    float unidadEntreCantidadPaquete;

    public Unidad() {
     clave="";
     nombre="";
     cantidadPaquete=0;
     unidadEntreCantidadPaquete=0;
    }

    public float getCantidadPaquete() {
        return cantidadPaquete;
    }

    public void setCantidadPaquete(float cantidadPaquete) {
        this.cantidadPaquete = cantidadPaquete;
    }

    public float getUnidadEntreCantidadPaquete() {
        return unidadEntreCantidadPaquete;
    }

    public void setUnidadEntreCantidadPaquete(float unidadEntreCantidadPaquete) {
        this.unidadEntreCantidadPaquete = unidadEntreCantidadPaquete;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

 
}
