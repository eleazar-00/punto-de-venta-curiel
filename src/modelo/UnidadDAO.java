/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author eleazar
 */
public class UnidadDAO {
    Conexion conexion;
    Connection accesoDB;
    String sql;
    PreparedStatement ps;

    public UnidadDAO() {
        conexion = new Conexion();
    }

    public String mtdInsertar(Unidad instance) {
        String respuesta = "";
        try {
            accesoDB = conexion.getConexion();
            sql = "insert into unidad value(?,?,?,?)";
            ps = accesoDB.prepareStatement(sql);
            ps.setString(1, instance.getClave());
            ps.setString(2, instance.getNombre());
            ps.setFloat(3, instance.getCantidadPaquete());
            ps.setFloat(4, instance.getUnidadEntreCantidadPaquete());
            int rpt = ps.executeUpdate();
            if (rpt > 0) {
                respuesta = "Registrado Correctamente";
            } else {
                respuesta = "Error desconocido al registrar";
            }
        } catch (Exception e) {
            mtdMensageError(" Registrar =" + e);
        }
        return respuesta;
    }

    public void mtdEliminar(Unidad instance) {
        try {
            accesoDB = conexion.getConexion();
            sql = "delete from unidad where clave_unidad=?";
            ps = accesoDB.prepareStatement(sql);
            ps.setString(1, instance.getClave());
            ps.execute();
             JOptionPane.showMessageDialog(null, "Eliminado Correctamente");
        } catch (Exception e) {
            mtdMensageError(" Eliminar =" + e);
        }
    }

    public int mtdActualizar(Unidad instance) {
        int respuesta = 0;
        try {
            accesoDB = conexion.getConexion(); 
            sql = "update unidad set nombre_unidad=?, cantidad_paquete=?, unidad_entre_cantidad_paquete=?  where clave_unidad=?";
            ps = accesoDB.prepareStatement(sql);
            ps.setString(1, instance.getNombre());
            ps.setFloat(2, instance.getCantidadPaquete());
            ps.setFloat(3, instance.getUnidadEntreCantidadPaquete());
            ps.setString(4, instance.getClave());
            respuesta = ps.executeUpdate();
        } catch (Exception e) {
            mtdMensageError(" Actualizar =" + e);
        }
        return respuesta;
    }

    public ArrayList<Unidad> mtdLista(Unidad instance) {
        ArrayList list = new ArrayList();
        Unidad unidad;
        try {
            accesoDB = conexion.getConexion();
            if (instance.getClave().length() > 0) {
                sql = "select * from unidad where clave_unidad like ?";
                ps = accesoDB.prepareStatement(sql);
                ps.setString(1,"%"+instance.getClave()+"%");
            } else {
                sql = "select * from unidad";
                ps = accesoDB.prepareStatement(sql);
            }
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                unidad = new Unidad();
                unidad.setClave(rs.getString(1));
                unidad.setNombre(rs.getString(2));
                unidad.setCantidadPaquete(rs.getFloat(3));
                unidad.setUnidadEntreCantidadPaquete(rs.getFloat(4));
                list.add(unidad);
            }
        } catch (Exception e) {
            mtdMensageError(" Lista =" + e);
        }
        conexion.cerrarConexio();
        return list;
    }

    public void mtdMensageError(String e) {
        JOptionPane.showMessageDialog(null, "Error" + e);
    }
}
