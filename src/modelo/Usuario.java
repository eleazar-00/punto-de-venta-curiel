/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author ELEAZAR
 */
public class Usuario {
    private String usuario;
   private String password;
   //Composicción
   private Perfil perfil;
   
    public Usuario(){
       usuario="";
       password="";
       perfil= new Perfil();
       
    } 
    public Usuario(String usuario,String password){
       this.usuario=usuario;
       
       this.password=password;
       perfil= new Perfil();
       
    } 

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
  
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Perfil getPerfil() {
        return perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }

    @Override
    public String toString() {
        return  usuario;
    }
    
     
}
