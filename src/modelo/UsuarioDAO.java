/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author ELEAZAR
 */
public class UsuarioDAO {

    Conexion conexion;

    public UsuarioDAO() {
        conexion = new Conexion();
    }

    public String mtdValidarUsuario(Usuario instance) {
        String menu="";
         try {
            Connection accesoDB = conexion.getConexion();
            PreparedStatement st = accesoDB.prepareStatement("select p.menu from usuario u inner join perfil p on u.perfil_clave_perfil=p.clave_perfil where u.usuario=? and u.usuario=?");
            st.setString(1, instance.getUsuario());
            st.setString(2, instance.getPassword());
            ResultSet rs = st.executeQuery();
             while (rs.next()) {
               menu=rs.getString(1);
              }
        } catch (Exception e) {
            System.out.println(e);
         }
         return menu;
     }

    public String mtdRegistrar(Usuario instance) {

        String registrado = null;
        try {
            Connection accesoDB = conexion.getConexion();
            PreparedStatement cs = accesoDB.prepareCall("insert into usuario (usuario,password,perfil_clave_perfil) values (?,?,?)");
            cs.setString(1, instance.getUsuario()); 
            cs.setString(2, instance.getPassword());
            cs.setString(3, instance.getPerfil().getClavePerfil());
            int valor = cs.executeUpdate();
            if (valor > 0) {
                registrado = "Usuario registrado Correctamento";
            }

        } catch (Exception e) {
            registrado = "Error al registrar usuario" + e;
        }
        return registrado;
    }

    public int mtdEditar(Usuario instance) {
        int respuesta = 0;
        try {
            Connection accesoDB = conexion.getConexion();
            CallableStatement cs = accesoDB.prepareCall("update usuario set password=?, perfil_clave_perfil=? where usuario=?");
            cs.setString(1, instance.getPassword());
            cs.setString(2, instance.getPerfil().getClavePerfil());
            cs.setString(3, instance.getUsuario());
            respuesta = cs.executeUpdate();

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al actualizar" + e);
        }
        return respuesta;

    }

    public void mtdEliminar(Usuario instance) {
        try {
            Connection accesoDB = conexion.getConexion();
            PreparedStatement cs = accesoDB.prepareStatement("delete from usuario where usuario=?");
            cs.setString(1, instance.getUsuario());
            cs.execute();
            JOptionPane.showMessageDialog(null, "Se elimino correctamente");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error a eliminar" + e);
        }
    }

    public ArrayList<Usuario> listaUsuario() {
        ArrayList lista = new ArrayList();
        Usuario usuario;
        try {
            Connection accesoDB = conexion.getConexion();
            PreparedStatement cs = accesoDB.prepareStatement("select usuario,password,perfil_clave_perfil from usuario");
            ResultSet rs = cs.executeQuery();
            while (rs.next()) {
                usuario = new Usuario();
                usuario.setUsuario(rs.getString(1)); 
                usuario.setPassword(rs.getString(2));
                usuario.getPerfil().setClavePerfil(rs.getString(3));
                lista.add(usuario);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al consulta" + e);
        }
        return lista;
    }

    public ResultSet cargarCombo() throws Exception {
        Connection accesoDB = conexion.getConexion();
        PreparedStatement st = accesoDB.prepareStatement("select *from perfil");
        ResultSet rs = st.executeQuery();
        return rs;
    }

    public ResultSet cargarComboUsuario() throws Exception {
        Connection accesoDB = conexion.getConexion();
        PreparedStatement st = accesoDB.prepareStatement("select *from usuario");
        ResultSet rs = st.executeQuery();
        return rs;
    }
}
